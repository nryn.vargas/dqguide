﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;
using System.Windows.Forms;
using System.Data;
using MySql.Data.MySqlClient;
using System.Collections;

namespace dqs
{
	class DBConnect
	{
		private MySqlConnection connection;
		public string Server { get; set; }
		public string Database { get; set; }
		private string uid;
		private string password;

		public bool IsOpen { get; private set; }

		//Constructor
		public DBConnect()
		{
			this.Server = "localhost";
			this.Database = "sample";
			Initialize();
		}

		public DBConnect(string dbServer, string dbName)
		{
			this.Server = dbServer;
			this.Database = dbName; uid = "dqguide";
			Initialize();
		}

		public void DBDispose()
		{
			this.connection.Dispose();
			//this.
		}

		//Initialize values
		private void Initialize()
		{
			uid = "dqguide";
			password = "D320C901D5A609C12";
			string connectionString;
			connectionString = (
				"SERVER=" + Server + ";"
				+ "DATABASE=" + Database + ";"
				+ "UID=" + uid + ";"
				+ "PASSWORD=" + password + ";"
				);

			connection = new MySqlConnection(connectionString);
		}

		// ==========================================================================================
		// CONNECTION 
		// ==========================================================================================

		//open connection to database
		public bool OpenConnection(bool Verbose = true)
		{
			try
			{
				connection.Open();
				IsOpen = true;
				return true;
			}
			catch (MySqlException ex)
			{
				//When handling errors, you can your application's response based on the error number.
				//The two most common error numbers when connecting are as follows:
				//0: Cannot connect to server.
				//1045: Invalid user name and/or password.
				if (Verbose)
				{
					switch (ex.Number)
					{
						case 0:
							MessageBox.Show(
								"Cannot connect to server.  Please contact your administrator.",
								"Error",
								MessageBoxButtons.OK,
								MessageBoxIcon.Exclamation
								);
							break;

						case 1045:
							MessageBox.Show(
								"Invalid database access/credentials.",
								"Error",
								MessageBoxButtons.OK,
								MessageBoxIcon.Exclamation
								);
							break;
					}
				}
				IsOpen = false;
				return false;
			}
		}

		//Close connection
		public bool CloseConnection(bool Verbose = true)
		{
			try
			{
				connection.Close();
				IsOpen = false;
				return true;
			}
			catch (MySqlException ex)
			{
				if (Verbose)
				{
					MessageBox.Show(ex.Message);
				}
				return false;
			}
		}

		// ==========================================================================================
		// SELECT FUNCTIONS 
		// ==========================================================================================

		// Pass SQL Select Statement to Select Multiple Rows
		public DataTable SelectTable(string command)
		{
			DataTable dTable = new DataTable();
			if (this.IsOpen)
			{
				MySqlDataAdapter cmd = new MySqlDataAdapter(command, connection);
				cmd.Fill(dTable);
				return dTable;
			}
			else
			{
				this.OpenConnection();
				if (this.IsOpen)
				{
					return SelectTable(command);
				}
				else
				{
					return dTable;
				}
			}
		}

		// Pass SQL Select Statement to return list of values (for ComboBox Items)
		public List<string> SelectAllVal(string command)
		{
			DataTable dTable = new DataTable();
			if (this.IsOpen)
			{
				MySqlDataAdapter cmd = new MySqlDataAdapter(command, connection);
				cmd.Fill(dTable);
			}
			else
			{
				this.OpenConnection();
				if (this.IsOpen)
				{
					dTable = SelectTable(command);
				}
			}
			List<string> list = new List<string>();
			foreach (DataRow row in dTable.Rows)
			{
				list.Add(row[0].ToString());
			}
			return list;
		}

		// Pass SQL Select Statement to Select Single Row 
		public List<string> SelectCmd(string command)
		{
			if (this.IsOpen)
			{
				// Create Command from "command" parameter
				MySqlCommand cmd = new MySqlCommand(command, connection);
				// Create data reader and execute the command
				MySqlDataReader dataReader = cmd.ExecuteReader();

				// Create a list to store the result
				var list = new List<string>(dataReader.FieldCount);
				//int i = 0;
				//while (i < dataReader.FieldCount)
				//{
					//list[i] = "";
					//i++;
				//}

				// Read the data and store result in the list
				int i = 0;
				while (dataReader.Read())
				{
					i = 0;
					while (i < dataReader.FieldCount)
					{
						list.Add(dataReader[i] + "");
						i++;
					}
				}

				//close Data Reader
				dataReader.Close();

				//close Connection
				this.CloseConnection();

				//return list to be displayed
				return list;
			}
			else
			{
				this.OpenConnection();
				if (this.IsOpen)
				{
					return SelectCmd(command);
				}
				else
				{
					var list = new List<string>(0);
					return list;
				}
			}
		}

		// Pass SQL Select Statement to Select Single Field 
		public string SelectStrField(string command, string fieldName)
		{
			if (this.IsOpen)
			{
				// Create Command from "command" parameter
				MySqlCommand cmd = new MySqlCommand(command, connection);
				// Create data reader and execute the command
				MySqlDataReader dataReader = cmd.ExecuteReader();

				string resStr = "";
				dataReader.Read();
				if (dataReader.HasRows)
				{
					resStr = dataReader[fieldName] + "";
				}

				//close Data Reader
				dataReader.Close();

				//close Connection
				this.CloseConnection();

				//return list to be displayed
				return resStr;
			}
			else
			{
				this.OpenConnection();
				if (this.IsOpen)
				{
					return SelectStrField(command, fieldName);
				}
				else
				{
					return string.Empty;
				}
			}
		}

		// ==========================================================================================
		// ORIGINAL
		// ==========================================================================================

		//Insert statement
		public void Insert()
		{
			string query = "INSERT INTO tableinfo (name, age) VALUES('John Smith', '33')";

			//open connection
			if (this.OpenConnection() == true)
			{
				//create command and assign the query and connection from the constructor
				MySqlCommand cmd = new MySqlCommand(query, connection);

				//Execute command
				cmd.ExecuteNonQuery();

				//close connection
				this.CloseConnection();
			}
		}

		//Update statement
		public void Update()
		{
			string query = "UPDATE tableinfo SET name='Joe', age='22' WHERE name='John Smith'";

			//Open connection
			if (this.OpenConnection() == true)
			{
				//create mysql command
				MySqlCommand cmd = new MySqlCommand();
				//Assign the query using CommandText
				cmd.CommandText = query;
				//Assign the connection using Connection
				cmd.Connection = connection;

				//Execute query
				cmd.ExecuteNonQuery();

				//close connection
				this.CloseConnection();
			}
		}

		//Delete statement
		public void Delete()
		{
			string query = "DELETE FROM tableinfo WHERE name='John Smith'";

			if (this.OpenConnection() == true)
			{
				MySqlCommand cmd = new MySqlCommand(query, connection);
				cmd.ExecuteNonQuery();
				this.CloseConnection();
			}
		}


		//Select statement
		public List<string>[] Select()
		{
			string query = "SELECT * FROM tableinfo";

			//Create a list to store the result
			List<string>[] list = new List<string>[3];
			list[0] = new List<string>();
			list[1] = new List<string>();
			list[2] = new List<string>();

			//Open connection
			if (this.OpenConnection() == true)
			{
				//Create Command
				MySqlCommand cmd = new MySqlCommand(query, connection);
				//Create a data reader and Execute the command
				MySqlDataReader dataReader = cmd.ExecuteReader();

				//Read the data and store them in the list
				while (dataReader.Read())
				{
					list[0].Add(dataReader["id"] + "");
					list[1].Add(dataReader["name"] + "");
					list[2].Add(dataReader["age"] + "");
				}

				//close Data Reader
				dataReader.Close();

				//close Connection
				this.CloseConnection();

				//return list to be displayed
				return list;
			}
			else
			{
				return list;
			}
		}

		//Count statement
		public int Count()
		{
			string query = "SELECT Count(*) FROM tableinfo";
			int Count = -1;

			//Open Connection
			if (this.OpenConnection() == true)
			{
				//Create Mysql Command
				MySqlCommand cmd = new MySqlCommand(query, connection);

				//ExecuteScalar will return one value
				Count = int.Parse(cmd.ExecuteScalar() + "");

				//close Connection
				this.CloseConnection();

				return Count;
			}
			else
			{
				return Count;
			}
		}

		//Backup
		public void Backup()
		{
			try
			{
				DateTime Time = DateTime.Now;
				int year = Time.Year;
				int month = Time.Month;
				int day = Time.Day;
				int hour = Time.Hour;
				int minute = Time.Minute;
				int second = Time.Second;
				int millisecond = Time.Millisecond;

				//Save file to C:\ with the current date as a filename
				string path;
				path = "C:\\" + year + "-" + month + "-" + day + "-" + hour + "-" + minute + "-" + second + "-" + millisecond + ".sql";
				StreamWriter file = new StreamWriter(path);


				ProcessStartInfo psi = new ProcessStartInfo();
				psi.FileName = "mysqldump";
				psi.RedirectStandardInput = false;
				psi.RedirectStandardOutput = true;
				psi.Arguments = string.Format(@"-u{0} -p{1} -h{2} {3}", uid, password, Server, Database);
				psi.UseShellExecute = false;

				Process process = Process.Start(psi);

				string output;
				output = process.StandardOutput.ReadToEnd();
				file.WriteLine(output);
				process.WaitForExit();
				file.Close();
				process.Close();
			}
#pragma warning disable CS0168 // Variable is declared but never used
			catch (IOException ex)
#pragma warning restore CS0168 // Variable is declared but never used
			{
				MessageBox.Show("Error , unable to backup!");
			}
		}

		//Restore
		public void Restore()
		{
			try
			{
				//Read file from C:\
				string path;
				path = "C:\\MySqlBackup.sql";
				StreamReader file = new StreamReader(path);
				string input = file.ReadToEnd();
				file.Close();


				ProcessStartInfo psi = new ProcessStartInfo();
				psi.FileName = "mysql";
				psi.RedirectStandardInput = true;
				psi.RedirectStandardOutput = false;
				psi.Arguments = string.Format(@"-u{0} -p{1} -h{2} {3}", uid, password, Server, Database);
				psi.UseShellExecute = false;


				Process process = Process.Start(psi);
				process.StandardInput.WriteLine(input);
				process.StandardInput.Close();
				process.WaitForExit();
				process.Close();
			}
#pragma warning disable CS0168 // Variable is declared but never used
			catch (IOException ex)
#pragma warning restore CS0168 // Variable is declared but never used
			{
				MessageBox.Show("Error , unable to Restore!");
			}
		}
	}

		public static class HashExtensions
	{
		public static string EncryptPasswd(this string input)
		{
			if (!string.IsNullOrEmpty(input))
			{
				using (var sha = SHA256.Create())
				{
					var bytes = Encoding.UTF8.GetBytes(input);
					var hash = sha.ComputeHash(bytes);

					return Convert.ToBase64String(hash);
				}
			}
			else
			{
				return string.Empty;
			}
		}
	}
}
