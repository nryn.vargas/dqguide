﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace dqs
{
	public class UserInfo
	{
		public string database;
		public string username;
		public string password;
		public bool isAdmin;

		public UserInfo()
		{
			database = "";
			username = "";
			password = "";
			isAdmin = false;
		}
    }
	static class Program
	{
		public static DBConnect DBConnection { get; set; }

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			//DBConnection = new DBConnect();
			Application.Run(new frmMain());
		}
		public static Bitmap ChangeBitmapColor(Bitmap bitMap, Color newColor)
		{
			Bitmap newBitmap = new Bitmap(bitMap);
			for (int iWd = 0; iWd < newBitmap.Width; iWd++)
			{
				for (int iHt = 0; iHt < newBitmap.Height; iHt++)
				{
					Color origColor = bitMap.GetPixel(iWd, iHt);
					Color newColorA = Color.FromArgb(origColor.A, newColor);
					newBitmap.SetPixel(iWd, iHt, newColorA);
				}
			}
			return newBitmap;
		}

		public static void ChangeButtonColor(Button button, Color newColor)
		{
			Bitmap bitMap = new Bitmap(button.Image);
			bitMap = ChangeBitmapColor(bitMap, newColor);
			button.Image = (Image)bitMap;
			button.ForeColor = newColor;
		}

		public static void ChangeButtonColor(ToolStripDropDownButton button, Color newColor)
		{
			Bitmap bitMap = new Bitmap(button.Image);
			bitMap = ChangeBitmapColor(bitMap, newColor);
			button.Image = (Image)bitMap;
			button.ForeColor = newColor;
		}

		public static void ChangePicBoxColor(PictureBox pictureBox, Color newColor)
		{
			Bitmap bitMap = new Bitmap(pictureBox.Image);
			bitMap = ChangeBitmapColor(bitMap, newColor);
			pictureBox.Image = (Image)bitMap;
			pictureBox.ForeColor = newColor;
		}
	}
}
