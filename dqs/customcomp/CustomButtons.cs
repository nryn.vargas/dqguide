﻿using System;
using System.Windows.Forms;

public class FocuslessButton : Button
{
	protected override bool ShowFocusCues
	{
		get
		{
			return false;
		}
	}
}

public class CustomColorButton : System.Windows.Forms.Button
{
	protected override bool ShowFocusCues
	{
		get
		{
			return false;
		}
	}
	protected override void OnGotFocus(EventArgs e)
	{
		base.OnGotFocus(e);
		if (FlatStyle == FlatStyle.Flat)
		{
			FlatAppearance.BorderSize = 1;
		}
	}

	protected override void OnLostFocus(EventArgs e)
	{
		base.OnLostFocus(e);
		if (FlatStyle == FlatStyle.Flat)
		{
			FlatAppearance.BorderSize = 0;
		}
	}
}