﻿using System;
using System.Windows.Forms;
using System.ComponentModel;
using System.Drawing;

public class CustomComboBox : ComboBox
{
    private bool fReadOnly;
    private const int WM_PAINT = 0xF;
    private int buttonWidth = SystemInformation.HorizontalScrollBarArrowWidth;
    public CustomComboBox()
    {
        BorderColor = Color.DimGray;
        ReadOnly = false;
    }

    [Browsable(true)]
    [Category("Appearance")]
    [DefaultValue(typeof(Color), "DimGray")]
    public Color BorderColor { get; set; }

    [Browsable(true)]
    [Category("Behavior")]
    [DefaultValue(false)]
    public bool ReadOnly 
    {
        get { return fReadOnly; }
        set
        {
            if (value)
            {
                DropDownStyle = ComboBoxStyle.Simple;
                //Region = new Region(new Rectangle(3, 3, Width - 7, Height - 7));
            }
            else
            {
                DropDownStyle = ComboBoxStyle.DropDownList;
            }
            fReadOnly = value;
        }
        
    }
    protected override void OnKeyDown(KeyEventArgs e)
    {
        if (ReadOnly)
        {
            e.SuppressKeyPress = true;
            return;
        }
        base.OnKeyDown(e);
    }
    protected override void WndProc(ref Message m)
    {
        base.WndProc(ref m);
        if (m.Msg == WM_PAINT)
        {
            using (var g = Graphics.FromHwnd(Handle))
            {
                var borderColor = BorderColor;
                if (ReadOnly)
                {
                    borderColor = Parent.BackColor;
                }
                using (var p = new Pen(borderColor))
                {
                    g.DrawRectangle(p, 0, 0, Width - 1, Height - 1);

                    var d = FlatStyle == FlatStyle.Popup ? 1 : 0;
                    g.DrawLine(p, Width - d,
                        0, Width - d, Height);
                }
            }
        }
    }

}