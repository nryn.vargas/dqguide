﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

public class ImageViewer : Panel
{
    public Image Image
    {
        get { return this.image; }
        set
        {
            this.image = value;
            this.ZoomExtents();
            this.LimitBasePoint(basePoint.X, basePoint.Y);
            this.Invalidate();
        }
    }

    public string ScaleFactor
    {
        get
        {
            return (this._ScaleFactor * 100).ToString();
        }
    }

    private bool drag;
    private float _ScaleFactor = 1;
    private Point basePoint;
    private Image image;
    private int x, y;

    /// <summary>
    /// Class constructor
    /// </summary>
    public ImageViewer()
    {
        this.DoubleBuffered = true;
    }

    /// <summary>
    /// Mouse button down event
    /// </summary>
    protected override void OnMouseDown(MouseEventArgs e)
    {
        this.drag = true;
        x = e.X;
        y = e.Y;

        base.OnMouseDown(e);
    }

    /// <summary>
    /// Mouse wheel event
    /// </summary>
    protected override void OnMouseWheel(MouseEventArgs e)
    {
        int delta = Math.Abs(e.Delta / 120);
        if (e.Delta > 0)
        {
            this.ZoomIn(delta);
        }
        else if (e.Delta < 0)
        {
            this.ZoomOut(delta);
        }

        base.OnMouseWheel(e);
    }

    /// <summary>
    /// Mouse button up event
    /// </summary>
    protected override void OnMouseUp(MouseEventArgs e)
    {
        drag = false;
        base.OnMouseUp(e);
    }

    /// <summary>
    /// Mouse move event
    /// </summary>
    protected override void OnMouseMove(MouseEventArgs e)
    {
        if (drag)
        {
            LimitBasePoint(basePoint.X + e.X - x, basePoint.Y + e.Y - y);
            x = e.X;
            y = e.Y;
            this.Invalidate();
        }

        base.OnMouseMove(e);
    }

    /// <summary>
    /// Resize event
    /// </summary>
    protected override void OnResize(EventArgs e)
    {
        LimitBasePoint(basePoint.X, basePoint.Y);
        this.Invalidate();

        base.OnResize(e);
    }

    /// <summary>
    /// Paint event
    /// </summary>
    protected override void OnPaint(PaintEventArgs pe)
    {
        if (this.Image != null)
        {
            Rectangle src = new Rectangle(0, 0, Image.Width, Image.Height);
            Rectangle dst = new Rectangle(basePoint.X, basePoint.Y, (int)(Image.Width * _ScaleFactor), (int)(Image.Height * _ScaleFactor));
            pe.Graphics.DrawImage(Image, dst, src, GraphicsUnit.Pixel);
        }

        base.OnPaint(pe);
    }

    private void ZoomExtents()
    {
        if (this.Image != null)
            this._ScaleFactor = (float)Math.Min((double)this.Width / this.Image.Width, (double)this.Height / this.Image.Height);
    }

    public void ZoomIn(int Val = 1)
    {
        if (_ScaleFactor < 10)
        {
            int x = (int)((this.Width / 2 - basePoint.X) / _ScaleFactor);
            int y = (int)((this.Height / 2 - basePoint.Y) / _ScaleFactor);
            _ScaleFactor += (float)(Val * 0.1);
            LimitBasePoint((int)(this.Width / 2 - x * _ScaleFactor), (int)(this.Height / 2 - y * _ScaleFactor));
            this.Invalidate();
        }
    }

    public void ZoomOut(int Val = 1)
    {
        if (_ScaleFactor > .1)
        {
            int x = (int)((this.Width / 2 - basePoint.X) / _ScaleFactor);
            int y = (int)((this.Height / 2 - basePoint.Y) / _ScaleFactor);
            _ScaleFactor -= (float)(Val * 0.1);
            LimitBasePoint((int)(this.Width / 2 - x * _ScaleFactor), (int)(this.Height / 2 - y * _ScaleFactor));
            this.Invalidate();
        }
    }

    public void Zoom(float New_ScaleFactor = 1)
    {
        if (New_ScaleFactor > .1)
        {
            int x = (int)((this.Width / 2 - basePoint.X) / _ScaleFactor);
            int y = (int)((this.Height / 2 - basePoint.Y) / _ScaleFactor);
            _ScaleFactor = New_ScaleFactor;
            LimitBasePoint((int)(this.Width / 2 - x * _ScaleFactor), (int)(this.Height / 2 - y * _ScaleFactor));
            this.Invalidate();
        }
    }

    private void LimitBasePoint(int x, int y)
    {
        if (this.Image == null)
            return;

        int width = this.Width - (int)(Image.Width * _ScaleFactor);
        int height = this.Height - (int)(Image.Height * _ScaleFactor);
        if (width < 0)
        {
            x = Math.Max(Math.Min(x, 0), width);
        }
        else
        {
            x = width / 2;
        }
        if (height < 0)
        {
            y = Math.Max(Math.Min(y, 0), height);
        }
        else
        {
            y = height / 2;
        }
        basePoint = new Point(x, y);
    }
}