﻿namespace dqs
{
	partial class frmBeasts
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            this.sconBts = new System.Windows.Forms.SplitContainer();
            this.sconFilter = new System.Windows.Forms.SplitContainer();
            this.dgBeasts = new System.Windows.Forms.DataGridView();
            this.colID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colFamily = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colExp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colGold = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colHP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colMP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAtk = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDef = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAgi = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colFire = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colIce = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colWind = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colEarth = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDark = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colLight = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colBlast = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDazle = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colSleep = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDeath = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDrainMP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colConfusion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colFizle = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colStop = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPoison = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colParalyze = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDecAtk = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDecDef = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDecSpd = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDecMR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colMesmerize = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colHaunts = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDesc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colItem1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colRate1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colItem2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colRate2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colItem3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colRate3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colItem4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colRate4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colItem5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colRate5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colItem6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colRate6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colItem7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colRate7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colFilename = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnFilter = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnApply = new System.Windows.Forms.Button();
            this.lblFtCaption = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.edtFtOrder2 = new System.Windows.Forms.ComboBox();
            this.lblOrder = new System.Windows.Forms.Label();
            this.edtFtOrder = new System.Windows.Forms.ComboBox();
            this.lblFtDesc = new System.Windows.Forms.Label();
            this.lblFtLoc = new System.Windows.Forms.Label();
            this.lblFtItems = new System.Windows.Forms.Label();
            this.edtFtDesc = new System.Windows.Forms.TextBox();
            this.edtFtHaunts = new System.Windows.Forms.TextBox();
            this.edtFtItems = new System.Windows.Forms.TextBox();
            this.edtFtFamily = new System.Windows.Forms.ComboBox();
            this.lblFtFamily = new System.Windows.Forms.Label();
            this.lblFtName = new System.Windows.Forms.Label();
            this.edtFtName = new System.Windows.Forms.TextBox();
            this.lblFamily = new System.Windows.Forms.Label();
            this.lblDesc = new System.Windows.Forms.Label();
            this.edtItems = new System.Windows.Forms.RichTextBox();
            this.edtStop = new System.Windows.Forms.TextBox();
            this.lblStop = new System.Windows.Forms.TextBox();
            this.edtDecMR = new System.Windows.Forms.TextBox();
            this.lblDecMR = new System.Windows.Forms.TextBox();
            this.edtDecSpeed = new System.Windows.Forms.TextBox();
            this.lblDecSpeed = new System.Windows.Forms.TextBox();
            this.edtDecDef = new System.Windows.Forms.TextBox();
            this.lblDecDef = new System.Windows.Forms.TextBox();
            this.edtDecAtk = new System.Windows.Forms.TextBox();
            this.lblDecAtk = new System.Windows.Forms.TextBox();
            this.edtDrainMP = new System.Windows.Forms.TextBox();
            this.lblDrainMP = new System.Windows.Forms.TextBox();
            this.edtMesmerize = new System.Windows.Forms.TextBox();
            this.lblMesmerize = new System.Windows.Forms.TextBox();
            this.edtParalyze = new System.Windows.Forms.TextBox();
            this.lblParalyze = new System.Windows.Forms.TextBox();
            this.edtConfusion = new System.Windows.Forms.TextBox();
            this.lblConfusion = new System.Windows.Forms.TextBox();
            this.edtPoison = new System.Windows.Forms.TextBox();
            this.lblPoison = new System.Windows.Forms.TextBox();
            this.edtFizle = new System.Windows.Forms.TextBox();
            this.lblFizle = new System.Windows.Forms.TextBox();
            this.edtDeath = new System.Windows.Forms.TextBox();
            this.lblDeath = new System.Windows.Forms.TextBox();
            this.edtLight = new System.Windows.Forms.TextBox();
            this.lblLight = new System.Windows.Forms.TextBox();
            this.edtDark = new System.Windows.Forms.TextBox();
            this.lblDark = new System.Windows.Forms.TextBox();
            this.edtEarth = new System.Windows.Forms.TextBox();
            this.lblEarth = new System.Windows.Forms.TextBox();
            this.edtSleep = new System.Windows.Forms.TextBox();
            this.lblSleep = new System.Windows.Forms.TextBox();
            this.edtDazle = new System.Windows.Forms.TextBox();
            this.lblDazle = new System.Windows.Forms.TextBox();
            this.edtBlast = new System.Windows.Forms.TextBox();
            this.lblBlast = new System.Windows.Forms.TextBox();
            this.edtWind = new System.Windows.Forms.TextBox();
            this.lblWind = new System.Windows.Forms.TextBox();
            this.edtIce = new System.Windows.Forms.TextBox();
            this.lblIce = new System.Windows.Forms.TextBox();
            this.edtFire = new System.Windows.Forms.TextBox();
            this.lblFire = new System.Windows.Forms.TextBox();
            this.edtAgi = new System.Windows.Forms.TextBox();
            this.lblAgi = new System.Windows.Forms.TextBox();
            this.edtDef = new System.Windows.Forms.TextBox();
            this.lblDef = new System.Windows.Forms.TextBox();
            this.edtAtk = new System.Windows.Forms.TextBox();
            this.lblAtk = new System.Windows.Forms.TextBox();
            this.lin3 = new System.Windows.Forms.Panel();
            this.lblItems = new System.Windows.Forms.Label();
            this.lblMP = new System.Windows.Forms.Label();
            this.lblHP = new System.Windows.Forms.Label();
            this.lblGold = new System.Windows.Forms.Label();
            this.edtHP = new System.Windows.Forms.TextBox();
            this.lblExp = new System.Windows.Forms.Label();
            this.edtMP = new System.Windows.Forms.TextBox();
            this.edtGold = new System.Windows.Forms.TextBox();
            this.edtExp = new System.Windows.Forms.TextBox();
            this.lin2 = new System.Windows.Forms.Panel();
            this.edtHaunts = new System.Windows.Forms.TextBox();
            this.lblHaunts = new System.Windows.Forms.Label();
            this.edtDesc = new System.Windows.Forms.TextBox();
            this.lin1 = new System.Windows.Forms.Panel();
            this.edtFamily = new System.Windows.Forms.TextBox();
            this.edtID = new System.Windows.Forms.TextBox();
            this.picImage = new System.Windows.Forms.PictureBox();
            this.edtName = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.sconBts)).BeginInit();
            this.sconBts.Panel1.SuspendLayout();
            this.sconBts.Panel2.SuspendLayout();
            this.sconBts.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sconFilter)).BeginInit();
            this.sconFilter.Panel1.SuspendLayout();
            this.sconFilter.Panel2.SuspendLayout();
            this.sconFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgBeasts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picImage)).BeginInit();
            this.SuspendLayout();
            // 
            // sconBts
            // 
            this.sconBts.BackColor = System.Drawing.Color.White;
            this.sconBts.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.sconBts.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sconBts.Location = new System.Drawing.Point(0, 0);
            this.sconBts.Name = "sconBts";
            // 
            // sconBts.Panel1
            // 
            this.sconBts.Panel1.Controls.Add(this.sconFilter);
            this.sconBts.Panel1.Cursor = System.Windows.Forms.Cursors.Default;
            this.sconBts.Panel1MinSize = 310;
            // 
            // sconBts.Panel2
            // 
            this.sconBts.Panel2.BackColor = System.Drawing.Color.White;
            this.sconBts.Panel2.Controls.Add(this.lblFamily);
            this.sconBts.Panel2.Controls.Add(this.lblDesc);
            this.sconBts.Panel2.Controls.Add(this.edtItems);
            this.sconBts.Panel2.Controls.Add(this.edtStop);
            this.sconBts.Panel2.Controls.Add(this.lblStop);
            this.sconBts.Panel2.Controls.Add(this.edtDecMR);
            this.sconBts.Panel2.Controls.Add(this.lblDecMR);
            this.sconBts.Panel2.Controls.Add(this.edtDecSpeed);
            this.sconBts.Panel2.Controls.Add(this.lblDecSpeed);
            this.sconBts.Panel2.Controls.Add(this.edtDecDef);
            this.sconBts.Panel2.Controls.Add(this.lblDecDef);
            this.sconBts.Panel2.Controls.Add(this.edtDecAtk);
            this.sconBts.Panel2.Controls.Add(this.lblDecAtk);
            this.sconBts.Panel2.Controls.Add(this.edtDrainMP);
            this.sconBts.Panel2.Controls.Add(this.lblDrainMP);
            this.sconBts.Panel2.Controls.Add(this.edtMesmerize);
            this.sconBts.Panel2.Controls.Add(this.lblMesmerize);
            this.sconBts.Panel2.Controls.Add(this.edtParalyze);
            this.sconBts.Panel2.Controls.Add(this.lblParalyze);
            this.sconBts.Panel2.Controls.Add(this.edtConfusion);
            this.sconBts.Panel2.Controls.Add(this.lblConfusion);
            this.sconBts.Panel2.Controls.Add(this.edtPoison);
            this.sconBts.Panel2.Controls.Add(this.lblPoison);
            this.sconBts.Panel2.Controls.Add(this.edtFizle);
            this.sconBts.Panel2.Controls.Add(this.lblFizle);
            this.sconBts.Panel2.Controls.Add(this.edtDeath);
            this.sconBts.Panel2.Controls.Add(this.lblDeath);
            this.sconBts.Panel2.Controls.Add(this.edtLight);
            this.sconBts.Panel2.Controls.Add(this.lblLight);
            this.sconBts.Panel2.Controls.Add(this.edtDark);
            this.sconBts.Panel2.Controls.Add(this.lblDark);
            this.sconBts.Panel2.Controls.Add(this.edtEarth);
            this.sconBts.Panel2.Controls.Add(this.lblEarth);
            this.sconBts.Panel2.Controls.Add(this.edtSleep);
            this.sconBts.Panel2.Controls.Add(this.lblSleep);
            this.sconBts.Panel2.Controls.Add(this.edtDazle);
            this.sconBts.Panel2.Controls.Add(this.lblDazle);
            this.sconBts.Panel2.Controls.Add(this.edtBlast);
            this.sconBts.Panel2.Controls.Add(this.lblBlast);
            this.sconBts.Panel2.Controls.Add(this.edtWind);
            this.sconBts.Panel2.Controls.Add(this.lblWind);
            this.sconBts.Panel2.Controls.Add(this.edtIce);
            this.sconBts.Panel2.Controls.Add(this.lblIce);
            this.sconBts.Panel2.Controls.Add(this.edtFire);
            this.sconBts.Panel2.Controls.Add(this.lblFire);
            this.sconBts.Panel2.Controls.Add(this.edtAgi);
            this.sconBts.Panel2.Controls.Add(this.lblAgi);
            this.sconBts.Panel2.Controls.Add(this.edtDef);
            this.sconBts.Panel2.Controls.Add(this.lblDef);
            this.sconBts.Panel2.Controls.Add(this.edtAtk);
            this.sconBts.Panel2.Controls.Add(this.lblAtk);
            this.sconBts.Panel2.Controls.Add(this.lin3);
            this.sconBts.Panel2.Controls.Add(this.lblItems);
            this.sconBts.Panel2.Controls.Add(this.lblMP);
            this.sconBts.Panel2.Controls.Add(this.lblHP);
            this.sconBts.Panel2.Controls.Add(this.lblGold);
            this.sconBts.Panel2.Controls.Add(this.edtHP);
            this.sconBts.Panel2.Controls.Add(this.lblExp);
            this.sconBts.Panel2.Controls.Add(this.edtMP);
            this.sconBts.Panel2.Controls.Add(this.edtGold);
            this.sconBts.Panel2.Controls.Add(this.edtExp);
            this.sconBts.Panel2.Controls.Add(this.lin2);
            this.sconBts.Panel2.Controls.Add(this.edtHaunts);
            this.sconBts.Panel2.Controls.Add(this.lblHaunts);
            this.sconBts.Panel2.Controls.Add(this.edtDesc);
            this.sconBts.Panel2.Controls.Add(this.lin1);
            this.sconBts.Panel2.Controls.Add(this.edtFamily);
            this.sconBts.Panel2.Controls.Add(this.edtID);
            this.sconBts.Panel2.Controls.Add(this.picImage);
            this.sconBts.Panel2.Controls.Add(this.edtName);
            this.sconBts.Size = new System.Drawing.Size(1079, 549);
            this.sconBts.SplitterDistance = 310;
            this.sconBts.SplitterWidth = 5;
            this.sconBts.TabIndex = 0;
            this.sconBts.TabStop = false;
            // 
            // sconFilter
            // 
            this.sconFilter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.sconFilter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sconFilter.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.sconFilter.IsSplitterFixed = true;
            this.sconFilter.Location = new System.Drawing.Point(0, 0);
            this.sconFilter.Name = "sconFilter";
            // 
            // sconFilter.Panel1
            // 
            this.sconFilter.Panel1.BackColor = System.Drawing.Color.White;
            this.sconFilter.Panel1.Controls.Add(this.dgBeasts);
            this.sconFilter.Panel1.Controls.Add(this.btnFilter);
            // 
            // sconFilter.Panel2
            // 
            this.sconFilter.Panel2.Controls.Add(this.btnClear);
            this.sconFilter.Panel2.Controls.Add(this.btnApply);
            this.sconFilter.Panel2.Controls.Add(this.lblFtCaption);
            this.sconFilter.Panel2.Controls.Add(this.btnCancel);
            this.sconFilter.Panel2.Controls.Add(this.edtFtOrder2);
            this.sconFilter.Panel2.Controls.Add(this.lblOrder);
            this.sconFilter.Panel2.Controls.Add(this.edtFtOrder);
            this.sconFilter.Panel2.Controls.Add(this.lblFtDesc);
            this.sconFilter.Panel2.Controls.Add(this.lblFtLoc);
            this.sconFilter.Panel2.Controls.Add(this.lblFtItems);
            this.sconFilter.Panel2.Controls.Add(this.edtFtDesc);
            this.sconFilter.Panel2.Controls.Add(this.edtFtHaunts);
            this.sconFilter.Panel2.Controls.Add(this.edtFtItems);
            this.sconFilter.Panel2.Controls.Add(this.edtFtFamily);
            this.sconFilter.Panel2.Controls.Add(this.lblFtFamily);
            this.sconFilter.Panel2.Controls.Add(this.lblFtName);
            this.sconFilter.Panel2.Controls.Add(this.edtFtName);
            this.sconFilter.Size = new System.Drawing.Size(310, 549);
            this.sconFilter.SplitterDistance = 74;
            this.sconFilter.SplitterWidth = 6;
            this.sconFilter.TabIndex = 1;
            // 
            // dgBeasts
            // 
            this.dgBeasts.AllowUserToAddRows = false;
            this.dgBeasts.AllowUserToDeleteRows = false;
            this.dgBeasts.AllowUserToResizeColumns = false;
            this.dgBeasts.AllowUserToResizeRows = false;
            this.dgBeasts.BackgroundColor = System.Drawing.Color.White;
            this.dgBeasts.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgBeasts.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgBeasts.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.dgBeasts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgBeasts.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colID,
            this.colName,
            this.colFamily,
            this.colExp,
            this.colGold,
            this.colHP,
            this.colMP,
            this.colAtk,
            this.colDef,
            this.colAgi,
            this.colFire,
            this.colIce,
            this.colWind,
            this.colEarth,
            this.colDark,
            this.colLight,
            this.colBlast,
            this.colDazle,
            this.colSleep,
            this.colDeath,
            this.colDrainMP,
            this.colConfusion,
            this.colFizle,
            this.colStop,
            this.colPoison,
            this.colParalyze,
            this.colDecAtk,
            this.colDecDef,
            this.colDecSpd,
            this.colDecMR,
            this.colMesmerize,
            this.colHaunts,
            this.colDesc,
            this.colItem1,
            this.colRate1,
            this.colItem2,
            this.colRate2,
            this.colItem3,
            this.colRate3,
            this.colItem4,
            this.colRate4,
            this.colItem5,
            this.colRate5,
            this.colItem6,
            this.colRate6,
            this.colItem7,
            this.colRate7,
            this.colFilename});
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(151)))), ((int)(((byte)(167)))));
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgBeasts.DefaultCellStyle = dataGridViewCellStyle12;
            this.dgBeasts.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgBeasts.GridColor = System.Drawing.SystemColors.ControlLight;
            this.dgBeasts.Location = new System.Drawing.Point(0, 0);
            this.dgBeasts.MultiSelect = false;
            this.dgBeasts.Name = "dgBeasts";
            this.dgBeasts.ReadOnly = true;
            this.dgBeasts.RowHeadersVisible = false;
            this.dgBeasts.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgBeasts.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgBeasts.ShowEditingIcon = false;
            this.dgBeasts.Size = new System.Drawing.Size(72, 516);
            this.dgBeasts.StandardTab = true;
            this.dgBeasts.TabIndex = 2;
            this.dgBeasts.SelectionChanged += new System.EventHandler(this.dgBeasts_SelectionChanged);
            // 
            // colID
            // 
            this.colID.DataPropertyName = "id";
            this.colID.HeaderText = "ID";
            this.colID.Name = "colID";
            this.colID.ReadOnly = true;
            this.colID.Visible = false;
            // 
            // colName
            // 
            this.colName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colName.DataPropertyName = "name";
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colName.DefaultCellStyle = dataGridViewCellStyle11;
            this.colName.HeaderText = "Name";
            this.colName.Name = "colName";
            this.colName.ReadOnly = true;
            this.colName.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // colFamily
            // 
            this.colFamily.DataPropertyName = "family";
            this.colFamily.HeaderText = "Family";
            this.colFamily.Name = "colFamily";
            this.colFamily.ReadOnly = true;
            this.colFamily.Visible = false;
            // 
            // colExp
            // 
            this.colExp.DataPropertyName = "exp";
            this.colExp.HeaderText = "EXP";
            this.colExp.Name = "colExp";
            this.colExp.ReadOnly = true;
            this.colExp.Visible = false;
            // 
            // colGold
            // 
            this.colGold.DataPropertyName = "gold";
            this.colGold.HeaderText = "Gold";
            this.colGold.Name = "colGold";
            this.colGold.ReadOnly = true;
            this.colGold.Visible = false;
            // 
            // colHP
            // 
            this.colHP.DataPropertyName = "hp";
            this.colHP.HeaderText = "HP";
            this.colHP.Name = "colHP";
            this.colHP.ReadOnly = true;
            this.colHP.Visible = false;
            // 
            // colMP
            // 
            this.colMP.DataPropertyName = "mp";
            this.colMP.HeaderText = "MP";
            this.colMP.Name = "colMP";
            this.colMP.ReadOnly = true;
            this.colMP.Visible = false;
            // 
            // colAtk
            // 
            this.colAtk.DataPropertyName = "atk";
            this.colAtk.HeaderText = "ATK";
            this.colAtk.Name = "colAtk";
            this.colAtk.ReadOnly = true;
            this.colAtk.Visible = false;
            // 
            // colDef
            // 
            this.colDef.DataPropertyName = "def";
            this.colDef.HeaderText = "DEF";
            this.colDef.Name = "colDef";
            this.colDef.ReadOnly = true;
            this.colDef.Visible = false;
            // 
            // colAgi
            // 
            this.colAgi.DataPropertyName = "agi";
            this.colAgi.HeaderText = "AGI";
            this.colAgi.Name = "colAgi";
            this.colAgi.ReadOnly = true;
            this.colAgi.Visible = false;
            // 
            // colFire
            // 
            this.colFire.DataPropertyName = "fire";
            this.colFire.HeaderText = "Fire";
            this.colFire.Name = "colFire";
            this.colFire.ReadOnly = true;
            this.colFire.Visible = false;
            // 
            // colIce
            // 
            this.colIce.DataPropertyName = "ice";
            this.colIce.HeaderText = "Ice";
            this.colIce.Name = "colIce";
            this.colIce.ReadOnly = true;
            this.colIce.Visible = false;
            // 
            // colWind
            // 
            this.colWind.DataPropertyName = "wind";
            this.colWind.HeaderText = "Wind";
            this.colWind.Name = "colWind";
            this.colWind.ReadOnly = true;
            this.colWind.Visible = false;
            // 
            // colEarth
            // 
            this.colEarth.DataPropertyName = "earth";
            this.colEarth.HeaderText = "Earth";
            this.colEarth.Name = "colEarth";
            this.colEarth.ReadOnly = true;
            this.colEarth.Visible = false;
            // 
            // colDark
            // 
            this.colDark.DataPropertyName = "dark";
            this.colDark.HeaderText = "Dark";
            this.colDark.Name = "colDark";
            this.colDark.ReadOnly = true;
            this.colDark.Visible = false;
            // 
            // colLight
            // 
            this.colLight.DataPropertyName = "light";
            this.colLight.HeaderText = "Light";
            this.colLight.Name = "colLight";
            this.colLight.ReadOnly = true;
            this.colLight.Visible = false;
            // 
            // colBlast
            // 
            this.colBlast.DataPropertyName = "blast";
            this.colBlast.HeaderText = "Blast";
            this.colBlast.Name = "colBlast";
            this.colBlast.ReadOnly = true;
            this.colBlast.Visible = false;
            // 
            // colDazle
            // 
            this.colDazle.DataPropertyName = "dazle";
            this.colDazle.HeaderText = "Dazle";
            this.colDazle.Name = "colDazle";
            this.colDazle.ReadOnly = true;
            this.colDazle.Visible = false;
            // 
            // colSleep
            // 
            this.colSleep.DataPropertyName = "sleep";
            this.colSleep.HeaderText = "Sleep";
            this.colSleep.Name = "colSleep";
            this.colSleep.ReadOnly = true;
            this.colSleep.Visible = false;
            // 
            // colDeath
            // 
            this.colDeath.DataPropertyName = "death";
            this.colDeath.HeaderText = "Death";
            this.colDeath.Name = "colDeath";
            this.colDeath.ReadOnly = true;
            this.colDeath.Visible = false;
            // 
            // colDrainMP
            // 
            this.colDrainMP.DataPropertyName = "drainMP";
            this.colDrainMP.HeaderText = "DrainMP";
            this.colDrainMP.Name = "colDrainMP";
            this.colDrainMP.ReadOnly = true;
            this.colDrainMP.Visible = false;
            // 
            // colConfusion
            // 
            this.colConfusion.DataPropertyName = "confusion";
            this.colConfusion.HeaderText = "Confusion";
            this.colConfusion.Name = "colConfusion";
            this.colConfusion.ReadOnly = true;
            this.colConfusion.Visible = false;
            // 
            // colFizle
            // 
            this.colFizle.DataPropertyName = "fizle";
            this.colFizle.HeaderText = "Fizle";
            this.colFizle.Name = "colFizle";
            this.colFizle.ReadOnly = true;
            this.colFizle.Visible = false;
            // 
            // colStop
            // 
            this.colStop.DataPropertyName = "stop";
            this.colStop.HeaderText = "Stop";
            this.colStop.Name = "colStop";
            this.colStop.ReadOnly = true;
            this.colStop.Visible = false;
            // 
            // colPoison
            // 
            this.colPoison.DataPropertyName = "poison";
            this.colPoison.HeaderText = "Poison";
            this.colPoison.Name = "colPoison";
            this.colPoison.ReadOnly = true;
            this.colPoison.Visible = false;
            // 
            // colParalyze
            // 
            this.colParalyze.DataPropertyName = "paralyze";
            this.colParalyze.HeaderText = "Paralyze";
            this.colParalyze.Name = "colParalyze";
            this.colParalyze.ReadOnly = true;
            this.colParalyze.Visible = false;
            // 
            // colDecAtk
            // 
            this.colDecAtk.DataPropertyName = "decAtk";
            this.colDecAtk.HeaderText = "DecAtk";
            this.colDecAtk.Name = "colDecAtk";
            this.colDecAtk.ReadOnly = true;
            this.colDecAtk.Visible = false;
            // 
            // colDecDef
            // 
            this.colDecDef.DataPropertyName = "decDef";
            this.colDecDef.HeaderText = "DecDef";
            this.colDecDef.Name = "colDecDef";
            this.colDecDef.ReadOnly = true;
            this.colDecDef.Visible = false;
            // 
            // colDecSpd
            // 
            this.colDecSpd.DataPropertyName = "decSpeed";
            this.colDecSpd.HeaderText = "DecSpd";
            this.colDecSpd.Name = "colDecSpd";
            this.colDecSpd.ReadOnly = true;
            this.colDecSpd.Visible = false;
            // 
            // colDecMR
            // 
            this.colDecMR.DataPropertyName = "decMR";
            this.colDecMR.HeaderText = "DecMR";
            this.colDecMR.Name = "colDecMR";
            this.colDecMR.ReadOnly = true;
            this.colDecMR.Visible = false;
            // 
            // colMesmerize
            // 
            this.colMesmerize.DataPropertyName = "mesmerize";
            this.colMesmerize.HeaderText = "Mesmerize";
            this.colMesmerize.Name = "colMesmerize";
            this.colMesmerize.ReadOnly = true;
            this.colMesmerize.Visible = false;
            // 
            // colHaunts
            // 
            this.colHaunts.DataPropertyName = "haunts";
            this.colHaunts.HeaderText = "Haunts";
            this.colHaunts.Name = "colHaunts";
            this.colHaunts.ReadOnly = true;
            this.colHaunts.Visible = false;
            // 
            // colDesc
            // 
            this.colDesc.DataPropertyName = "description";
            this.colDesc.HeaderText = "Description";
            this.colDesc.Name = "colDesc";
            this.colDesc.ReadOnly = true;
            this.colDesc.Visible = false;
            // 
            // colItem1
            // 
            this.colItem1.DataPropertyName = "item1";
            this.colItem1.HeaderText = "Item1";
            this.colItem1.Name = "colItem1";
            this.colItem1.ReadOnly = true;
            this.colItem1.Visible = false;
            // 
            // colRate1
            // 
            this.colRate1.DataPropertyName = "rate1";
            this.colRate1.HeaderText = "Rate1";
            this.colRate1.Name = "colRate1";
            this.colRate1.ReadOnly = true;
            this.colRate1.Visible = false;
            // 
            // colItem2
            // 
            this.colItem2.DataPropertyName = "item2";
            this.colItem2.HeaderText = "Item2";
            this.colItem2.Name = "colItem2";
            this.colItem2.ReadOnly = true;
            this.colItem2.Visible = false;
            // 
            // colRate2
            // 
            this.colRate2.DataPropertyName = "rate2";
            this.colRate2.HeaderText = "Rate2";
            this.colRate2.Name = "colRate2";
            this.colRate2.ReadOnly = true;
            this.colRate2.Visible = false;
            // 
            // colItem3
            // 
            this.colItem3.DataPropertyName = "item3";
            this.colItem3.HeaderText = "Item3";
            this.colItem3.Name = "colItem3";
            this.colItem3.ReadOnly = true;
            this.colItem3.Visible = false;
            // 
            // colRate3
            // 
            this.colRate3.DataPropertyName = "rate3";
            this.colRate3.HeaderText = "Rate3";
            this.colRate3.Name = "colRate3";
            this.colRate3.ReadOnly = true;
            this.colRate3.Visible = false;
            // 
            // colItem4
            // 
            this.colItem4.DataPropertyName = "item4";
            this.colItem4.HeaderText = "Item4";
            this.colItem4.Name = "colItem4";
            this.colItem4.ReadOnly = true;
            this.colItem4.Visible = false;
            // 
            // colRate4
            // 
            this.colRate4.DataPropertyName = "rate4";
            this.colRate4.HeaderText = "Rate4";
            this.colRate4.Name = "colRate4";
            this.colRate4.ReadOnly = true;
            this.colRate4.Visible = false;
            // 
            // colItem5
            // 
            this.colItem5.DataPropertyName = "item5";
            this.colItem5.HeaderText = "Item5";
            this.colItem5.Name = "colItem5";
            this.colItem5.ReadOnly = true;
            this.colItem5.Visible = false;
            // 
            // colRate5
            // 
            this.colRate5.DataPropertyName = "rate5";
            this.colRate5.HeaderText = "Rate5";
            this.colRate5.Name = "colRate5";
            this.colRate5.ReadOnly = true;
            this.colRate5.Visible = false;
            // 
            // colItem6
            // 
            this.colItem6.DataPropertyName = "item6";
            this.colItem6.HeaderText = "Item6";
            this.colItem6.Name = "colItem6";
            this.colItem6.ReadOnly = true;
            this.colItem6.Visible = false;
            // 
            // colRate6
            // 
            this.colRate6.DataPropertyName = "rate6";
            this.colRate6.HeaderText = "Rate6";
            this.colRate6.Name = "colRate6";
            this.colRate6.ReadOnly = true;
            this.colRate6.Visible = false;
            // 
            // colItem7
            // 
            this.colItem7.DataPropertyName = "item7";
            this.colItem7.HeaderText = "Item7";
            this.colItem7.Name = "colItem7";
            this.colItem7.ReadOnly = true;
            this.colItem7.Visible = false;
            // 
            // colRate7
            // 
            this.colRate7.DataPropertyName = "rate7";
            this.colRate7.HeaderText = "Rate7";
            this.colRate7.Name = "colRate7";
            this.colRate7.ReadOnly = true;
            this.colRate7.Visible = false;
            // 
            // colFilename
            // 
            this.colFilename.DataPropertyName = "filename";
            this.colFilename.HeaderText = "Filename";
            this.colFilename.Name = "colFilename";
            this.colFilename.ReadOnly = true;
            this.colFilename.Visible = false;
            // 
            // btnFilter
            // 
            this.btnFilter.AutoSize = true;
            this.btnFilter.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnFilter.Image = global::dqs.Properties.Resources.filter;
            this.btnFilter.Location = new System.Drawing.Point(0, 516);
            this.btnFilter.Name = "btnFilter";
            this.btnFilter.Size = new System.Drawing.Size(72, 31);
            this.btnFilter.TabIndex = 1;
            this.btnFilter.Text = "Filter";
            this.btnFilter.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFilter.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnFilter.UseVisualStyleBackColor = true;
            this.btnFilter.Click += new System.EventHandler(this.btnFilter_Click);
            // 
            // btnClear
            // 
            this.btnClear.AutoSize = true;
            this.btnClear.BackColor = System.Drawing.Color.Transparent;
            this.btnClear.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnClear.Image = global::dqs.Properties.Resources.eraser;
            this.btnClear.Location = new System.Drawing.Point(0, 454);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(228, 31);
            this.btnClear.TabIndex = 13;
            this.btnClear.Text = "C&lear";
            this.btnClear.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnClear.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnApply
            // 
            this.btnApply.AutoSize = true;
            this.btnApply.BackColor = System.Drawing.Color.Transparent;
            this.btnApply.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnApply.Image = global::dqs.Properties.Resources.check_bold;
            this.btnApply.Location = new System.Drawing.Point(0, 485);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(228, 31);
            this.btnApply.TabIndex = 14;
            this.btnApply.Text = "&Apply";
            this.btnApply.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnApply.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnApply.UseVisualStyleBackColor = false;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // lblFtCaption
            // 
            this.lblFtCaption.AutoSize = true;
            this.lblFtCaption.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFtCaption.Location = new System.Drawing.Point(6, 6);
            this.lblFtCaption.Name = "lblFtCaption";
            this.lblFtCaption.Size = new System.Drawing.Size(58, 15);
            this.lblFtCaption.TabIndex = 14;
            this.lblFtCaption.Text = "Filtrer By";
            // 
            // btnCancel
            // 
            this.btnCancel.AutoSize = true;
            this.btnCancel.BackColor = System.Drawing.Color.Transparent;
            this.btnCancel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnCancel.Image = global::dqs.Properties.Resources.close_thick;
            this.btnCancel.Location = new System.Drawing.Point(0, 516);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(228, 31);
            this.btnCancel.TabIndex = 15;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // edtFtOrder2
            // 
            this.edtFtOrder2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.edtFtOrder2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.edtFtOrder2.FormattingEnabled = true;
            this.edtFtOrder2.Items.AddRange(new object[] {
            "ASC",
            "DESC"});
            this.edtFtOrder2.Location = new System.Drawing.Point(146, 308);
            this.edtFtOrder2.Name = "edtFtOrder2";
            this.edtFtOrder2.Size = new System.Drawing.Size(56, 23);
            this.edtFtOrder2.TabIndex = 12;
            // 
            // lblOrder
            // 
            this.lblOrder.AutoSize = true;
            this.lblOrder.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.lblOrder.Location = new System.Drawing.Point(6, 290);
            this.lblOrder.Name = "lblOrder";
            this.lblOrder.Size = new System.Drawing.Size(57, 15);
            this.lblOrder.TabIndex = 10;
            this.lblOrder.Text = "Order By";
            // 
            // edtFtOrder
            // 
            this.edtFtOrder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtFtOrder.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.edtFtOrder.FormattingEnabled = true;
            this.edtFtOrder.Items.AddRange(new object[] {
            "ID",
            "Name",
            "Exp",
            "Gold",
            "Atk"});
            this.edtFtOrder.Location = new System.Drawing.Point(21, 308);
            this.edtFtOrder.Name = "edtFtOrder";
            this.edtFtOrder.Size = new System.Drawing.Size(119, 23);
            this.edtFtOrder.TabIndex = 11;
            // 
            // lblFtDesc
            // 
            this.lblFtDesc.AutoSize = true;
            this.lblFtDesc.Location = new System.Drawing.Point(18, 232);
            this.lblFtDesc.Name = "lblFtDesc";
            this.lblFtDesc.Size = new System.Drawing.Size(67, 15);
            this.lblFtDesc.TabIndex = 8;
            this.lblFtDesc.Text = "Description";
            // 
            // lblFtLoc
            // 
            this.lblFtLoc.AutoSize = true;
            this.lblFtLoc.Location = new System.Drawing.Point(18, 182);
            this.lblFtLoc.Name = "lblFtLoc";
            this.lblFtLoc.Size = new System.Drawing.Size(45, 15);
            this.lblFtLoc.TabIndex = 6;
            this.lblFtLoc.Text = "Haunts";
            // 
            // lblFtItems
            // 
            this.lblFtItems.AutoSize = true;
            this.lblFtItems.Location = new System.Drawing.Point(18, 132);
            this.lblFtItems.Name = "lblFtItems";
            this.lblFtItems.Size = new System.Drawing.Size(36, 15);
            this.lblFtItems.TabIndex = 4;
            this.lblFtItems.Text = "Items";
            // 
            // edtFtDesc
            // 
            this.edtFtDesc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtFtDesc.Location = new System.Drawing.Point(21, 250);
            this.edtFtDesc.Name = "edtFtDesc";
            this.edtFtDesc.Size = new System.Drawing.Size(181, 23);
            this.edtFtDesc.TabIndex = 9;
            // 
            // edtFtHaunts
            // 
            this.edtFtHaunts.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtFtHaunts.Location = new System.Drawing.Point(21, 200);
            this.edtFtHaunts.Name = "edtFtHaunts";
            this.edtFtHaunts.Size = new System.Drawing.Size(181, 23);
            this.edtFtHaunts.TabIndex = 7;
            // 
            // edtFtItems
            // 
            this.edtFtItems.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtFtItems.Location = new System.Drawing.Point(21, 150);
            this.edtFtItems.Name = "edtFtItems";
            this.edtFtItems.Size = new System.Drawing.Size(181, 23);
            this.edtFtItems.TabIndex = 5;
            // 
            // edtFtFamily
            // 
            this.edtFtFamily.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtFtFamily.BackColor = System.Drawing.Color.White;
            this.edtFtFamily.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.edtFtFamily.FormattingEnabled = true;
            this.edtFtFamily.Location = new System.Drawing.Point(21, 100);
            this.edtFtFamily.Name = "edtFtFamily";
            this.edtFtFamily.Size = new System.Drawing.Size(181, 23);
            this.edtFtFamily.TabIndex = 3;
            // 
            // lblFtFamily
            // 
            this.lblFtFamily.AutoSize = true;
            this.lblFtFamily.Location = new System.Drawing.Point(18, 82);
            this.lblFtFamily.Name = "lblFtFamily";
            this.lblFtFamily.Size = new System.Drawing.Size(42, 15);
            this.lblFtFamily.TabIndex = 2;
            this.lblFtFamily.Text = "Family";
            // 
            // lblFtName
            // 
            this.lblFtName.AutoSize = true;
            this.lblFtName.Location = new System.Drawing.Point(18, 30);
            this.lblFtName.Name = "lblFtName";
            this.lblFtName.Size = new System.Drawing.Size(39, 15);
            this.lblFtName.TabIndex = 0;
            this.lblFtName.Text = "Name";
            // 
            // edtFtName
            // 
            this.edtFtName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtFtName.Location = new System.Drawing.Point(21, 50);
            this.edtFtName.Name = "edtFtName";
            this.edtFtName.Size = new System.Drawing.Size(181, 23);
            this.edtFtName.TabIndex = 1;
            // 
            // lblFamily
            // 
            this.lblFamily.BackColor = System.Drawing.Color.White;
            this.lblFamily.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFamily.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblFamily.Location = new System.Drawing.Point(215, 63);
            this.lblFamily.Name = "lblFamily";
            this.lblFamily.Size = new System.Drawing.Size(104, 24);
            this.lblFamily.TabIndex = 67;
            this.lblFamily.Text = "Family";
            this.lblFamily.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDesc
            // 
            this.lblDesc.BackColor = System.Drawing.Color.White;
            this.lblDesc.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDesc.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblDesc.Location = new System.Drawing.Point(215, 93);
            this.lblDesc.Name = "lblDesc";
            this.lblDesc.Size = new System.Drawing.Size(104, 24);
            this.lblDesc.TabIndex = 66;
            this.lblDesc.Text = "Description";
            this.lblDesc.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // edtItems
            // 
            this.edtItems.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtItems.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.edtItems.Location = new System.Drawing.Point(325, 233);
            this.edtItems.Name = "edtItems";
            this.edtItems.Size = new System.Drawing.Size(426, 95);
            this.edtItems.TabIndex = 65;
            this.edtItems.Text = "[items_dropped]";
            // 
            // edtStop
            // 
            this.edtStop.BackColor = System.Drawing.Color.White;
            this.edtStop.Enabled = false;
            this.edtStop.Location = new System.Drawing.Point(648, 424);
            this.edtStop.Name = "edtStop";
            this.edtStop.ReadOnly = true;
            this.edtStop.Size = new System.Drawing.Size(35, 23);
            this.edtStop.TabIndex = 64;
            this.edtStop.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblStop
            // 
            this.lblStop.BackColor = System.Drawing.Color.White;
            this.lblStop.Enabled = false;
            this.lblStop.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblStop.Location = new System.Drawing.Point(577, 424);
            this.lblStop.Name = "lblStop";
            this.lblStop.ReadOnly = true;
            this.lblStop.Size = new System.Drawing.Size(70, 23);
            this.lblStop.TabIndex = 63;
            this.lblStop.Text = "Stop";
            this.lblStop.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // edtDecMR
            // 
            this.edtDecMR.BackColor = System.Drawing.Color.White;
            this.edtDecMR.Enabled = false;
            this.edtDecMR.Location = new System.Drawing.Point(535, 424);
            this.edtDecMR.Name = "edtDecMR";
            this.edtDecMR.ReadOnly = true;
            this.edtDecMR.Size = new System.Drawing.Size(35, 23);
            this.edtDecMR.TabIndex = 62;
            this.edtDecMR.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblDecMR
            // 
            this.lblDecMR.BackColor = System.Drawing.Color.White;
            this.lblDecMR.Enabled = false;
            this.lblDecMR.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblDecMR.Location = new System.Drawing.Point(464, 424);
            this.lblDecMR.Name = "lblDecMR";
            this.lblDecMR.ReadOnly = true;
            this.lblDecMR.Size = new System.Drawing.Size(70, 23);
            this.lblDecMR.TabIndex = 61;
            this.lblDecMR.Text = "Dec MR";
            this.lblDecMR.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // edtDecSpeed
            // 
            this.edtDecSpeed.BackColor = System.Drawing.Color.White;
            this.edtDecSpeed.Enabled = false;
            this.edtDecSpeed.Location = new System.Drawing.Point(422, 424);
            this.edtDecSpeed.Name = "edtDecSpeed";
            this.edtDecSpeed.ReadOnly = true;
            this.edtDecSpeed.Size = new System.Drawing.Size(35, 23);
            this.edtDecSpeed.TabIndex = 60;
            this.edtDecSpeed.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblDecSpeed
            // 
            this.lblDecSpeed.BackColor = System.Drawing.Color.White;
            this.lblDecSpeed.Enabled = false;
            this.lblDecSpeed.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblDecSpeed.Location = new System.Drawing.Point(351, 424);
            this.lblDecSpeed.Name = "lblDecSpeed";
            this.lblDecSpeed.ReadOnly = true;
            this.lblDecSpeed.Size = new System.Drawing.Size(70, 23);
            this.lblDecSpeed.TabIndex = 59;
            this.lblDecSpeed.Text = "Dec Speed";
            this.lblDecSpeed.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // edtDecDef
            // 
            this.edtDecDef.BackColor = System.Drawing.Color.White;
            this.edtDecDef.Enabled = false;
            this.edtDecDef.Location = new System.Drawing.Point(309, 424);
            this.edtDecDef.Name = "edtDecDef";
            this.edtDecDef.ReadOnly = true;
            this.edtDecDef.Size = new System.Drawing.Size(35, 23);
            this.edtDecDef.TabIndex = 58;
            this.edtDecDef.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblDecDef
            // 
            this.lblDecDef.BackColor = System.Drawing.Color.White;
            this.lblDecDef.Enabled = false;
            this.lblDecDef.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblDecDef.Location = new System.Drawing.Point(238, 424);
            this.lblDecDef.Name = "lblDecDef";
            this.lblDecDef.ReadOnly = true;
            this.lblDecDef.Size = new System.Drawing.Size(70, 23);
            this.lblDecDef.TabIndex = 57;
            this.lblDecDef.Text = "Dec Def";
            this.lblDecDef.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // edtDecAtk
            // 
            this.edtDecAtk.BackColor = System.Drawing.Color.White;
            this.edtDecAtk.Enabled = false;
            this.edtDecAtk.Location = new System.Drawing.Point(196, 424);
            this.edtDecAtk.Name = "edtDecAtk";
            this.edtDecAtk.ReadOnly = true;
            this.edtDecAtk.Size = new System.Drawing.Size(35, 23);
            this.edtDecAtk.TabIndex = 56;
            this.edtDecAtk.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblDecAtk
            // 
            this.lblDecAtk.BackColor = System.Drawing.Color.White;
            this.lblDecAtk.Enabled = false;
            this.lblDecAtk.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblDecAtk.Location = new System.Drawing.Point(125, 424);
            this.lblDecAtk.Name = "lblDecAtk";
            this.lblDecAtk.ReadOnly = true;
            this.lblDecAtk.Size = new System.Drawing.Size(70, 23);
            this.lblDecAtk.TabIndex = 55;
            this.lblDecAtk.Text = "Dec Atk";
            this.lblDecAtk.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // edtDrainMP
            // 
            this.edtDrainMP.BackColor = System.Drawing.Color.White;
            this.edtDrainMP.Enabled = false;
            this.edtDrainMP.Location = new System.Drawing.Point(83, 424);
            this.edtDrainMP.Name = "edtDrainMP";
            this.edtDrainMP.ReadOnly = true;
            this.edtDrainMP.Size = new System.Drawing.Size(35, 23);
            this.edtDrainMP.TabIndex = 54;
            this.edtDrainMP.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblDrainMP
            // 
            this.lblDrainMP.BackColor = System.Drawing.Color.White;
            this.lblDrainMP.Enabled = false;
            this.lblDrainMP.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblDrainMP.Location = new System.Drawing.Point(12, 424);
            this.lblDrainMP.Name = "lblDrainMP";
            this.lblDrainMP.ReadOnly = true;
            this.lblDrainMP.Size = new System.Drawing.Size(70, 23);
            this.lblDrainMP.TabIndex = 53;
            this.lblDrainMP.Text = "Drain MP";
            this.lblDrainMP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // edtMesmerize
            // 
            this.edtMesmerize.BackColor = System.Drawing.Color.White;
            this.edtMesmerize.Enabled = false;
            this.edtMesmerize.Location = new System.Drawing.Point(648, 400);
            this.edtMesmerize.Name = "edtMesmerize";
            this.edtMesmerize.ReadOnly = true;
            this.edtMesmerize.Size = new System.Drawing.Size(35, 23);
            this.edtMesmerize.TabIndex = 52;
            this.edtMesmerize.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblMesmerize
            // 
            this.lblMesmerize.BackColor = System.Drawing.Color.White;
            this.lblMesmerize.Enabled = false;
            this.lblMesmerize.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblMesmerize.Location = new System.Drawing.Point(577, 400);
            this.lblMesmerize.Name = "lblMesmerize";
            this.lblMesmerize.ReadOnly = true;
            this.lblMesmerize.Size = new System.Drawing.Size(70, 23);
            this.lblMesmerize.TabIndex = 51;
            this.lblMesmerize.Text = "Mesmerize";
            this.lblMesmerize.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // edtParalyze
            // 
            this.edtParalyze.BackColor = System.Drawing.Color.White;
            this.edtParalyze.Enabled = false;
            this.edtParalyze.Location = new System.Drawing.Point(535, 400);
            this.edtParalyze.Name = "edtParalyze";
            this.edtParalyze.ReadOnly = true;
            this.edtParalyze.Size = new System.Drawing.Size(35, 23);
            this.edtParalyze.TabIndex = 50;
            this.edtParalyze.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblParalyze
            // 
            this.lblParalyze.BackColor = System.Drawing.Color.White;
            this.lblParalyze.Enabled = false;
            this.lblParalyze.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblParalyze.Location = new System.Drawing.Point(464, 400);
            this.lblParalyze.Name = "lblParalyze";
            this.lblParalyze.ReadOnly = true;
            this.lblParalyze.Size = new System.Drawing.Size(70, 23);
            this.lblParalyze.TabIndex = 49;
            this.lblParalyze.Text = "Paralyze";
            this.lblParalyze.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // edtConfusion
            // 
            this.edtConfusion.BackColor = System.Drawing.Color.White;
            this.edtConfusion.Enabled = false;
            this.edtConfusion.Location = new System.Drawing.Point(422, 400);
            this.edtConfusion.Name = "edtConfusion";
            this.edtConfusion.ReadOnly = true;
            this.edtConfusion.Size = new System.Drawing.Size(35, 23);
            this.edtConfusion.TabIndex = 48;
            this.edtConfusion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblConfusion
            // 
            this.lblConfusion.BackColor = System.Drawing.Color.White;
            this.lblConfusion.Enabled = false;
            this.lblConfusion.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblConfusion.Location = new System.Drawing.Point(351, 400);
            this.lblConfusion.Name = "lblConfusion";
            this.lblConfusion.ReadOnly = true;
            this.lblConfusion.Size = new System.Drawing.Size(70, 23);
            this.lblConfusion.TabIndex = 47;
            this.lblConfusion.Text = "Confusion";
            this.lblConfusion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // edtPoison
            // 
            this.edtPoison.BackColor = System.Drawing.Color.White;
            this.edtPoison.Enabled = false;
            this.edtPoison.Location = new System.Drawing.Point(309, 400);
            this.edtPoison.Name = "edtPoison";
            this.edtPoison.ReadOnly = true;
            this.edtPoison.Size = new System.Drawing.Size(35, 23);
            this.edtPoison.TabIndex = 46;
            this.edtPoison.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblPoison
            // 
            this.lblPoison.BackColor = System.Drawing.Color.White;
            this.lblPoison.Enabled = false;
            this.lblPoison.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblPoison.Location = new System.Drawing.Point(238, 400);
            this.lblPoison.Name = "lblPoison";
            this.lblPoison.ReadOnly = true;
            this.lblPoison.Size = new System.Drawing.Size(70, 23);
            this.lblPoison.TabIndex = 45;
            this.lblPoison.Text = "Poison";
            this.lblPoison.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // edtFizle
            // 
            this.edtFizle.BackColor = System.Drawing.Color.White;
            this.edtFizle.Enabled = false;
            this.edtFizle.Location = new System.Drawing.Point(196, 400);
            this.edtFizle.Name = "edtFizle";
            this.edtFizle.ReadOnly = true;
            this.edtFizle.Size = new System.Drawing.Size(35, 23);
            this.edtFizle.TabIndex = 44;
            this.edtFizle.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblFizle
            // 
            this.lblFizle.BackColor = System.Drawing.Color.White;
            this.lblFizle.Enabled = false;
            this.lblFizle.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblFizle.Location = new System.Drawing.Point(125, 400);
            this.lblFizle.Name = "lblFizle";
            this.lblFizle.ReadOnly = true;
            this.lblFizle.Size = new System.Drawing.Size(70, 23);
            this.lblFizle.TabIndex = 43;
            this.lblFizle.Text = "Fizle";
            this.lblFizle.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // edtDeath
            // 
            this.edtDeath.BackColor = System.Drawing.Color.White;
            this.edtDeath.Enabled = false;
            this.edtDeath.Location = new System.Drawing.Point(83, 400);
            this.edtDeath.Name = "edtDeath";
            this.edtDeath.ReadOnly = true;
            this.edtDeath.Size = new System.Drawing.Size(35, 23);
            this.edtDeath.TabIndex = 42;
            this.edtDeath.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblDeath
            // 
            this.lblDeath.BackColor = System.Drawing.Color.White;
            this.lblDeath.Enabled = false;
            this.lblDeath.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblDeath.Location = new System.Drawing.Point(12, 400);
            this.lblDeath.Name = "lblDeath";
            this.lblDeath.ReadOnly = true;
            this.lblDeath.Size = new System.Drawing.Size(70, 23);
            this.lblDeath.TabIndex = 41;
            this.lblDeath.Text = "Death";
            this.lblDeath.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // edtLight
            // 
            this.edtLight.BackColor = System.Drawing.Color.White;
            this.edtLight.Enabled = false;
            this.edtLight.Location = new System.Drawing.Point(648, 376);
            this.edtLight.Name = "edtLight";
            this.edtLight.ReadOnly = true;
            this.edtLight.Size = new System.Drawing.Size(35, 23);
            this.edtLight.TabIndex = 40;
            this.edtLight.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblLight
            // 
            this.lblLight.BackColor = System.Drawing.Color.White;
            this.lblLight.Enabled = false;
            this.lblLight.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblLight.Location = new System.Drawing.Point(577, 376);
            this.lblLight.Name = "lblLight";
            this.lblLight.ReadOnly = true;
            this.lblLight.Size = new System.Drawing.Size(70, 23);
            this.lblLight.TabIndex = 39;
            this.lblLight.Text = "Light";
            this.lblLight.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // edtDark
            // 
            this.edtDark.BackColor = System.Drawing.Color.White;
            this.edtDark.Enabled = false;
            this.edtDark.Location = new System.Drawing.Point(535, 376);
            this.edtDark.Name = "edtDark";
            this.edtDark.ReadOnly = true;
            this.edtDark.Size = new System.Drawing.Size(35, 23);
            this.edtDark.TabIndex = 38;
            this.edtDark.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblDark
            // 
            this.lblDark.BackColor = System.Drawing.Color.White;
            this.lblDark.Enabled = false;
            this.lblDark.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblDark.Location = new System.Drawing.Point(464, 376);
            this.lblDark.Name = "lblDark";
            this.lblDark.ReadOnly = true;
            this.lblDark.Size = new System.Drawing.Size(70, 23);
            this.lblDark.TabIndex = 37;
            this.lblDark.Text = "Dark";
            this.lblDark.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // edtEarth
            // 
            this.edtEarth.BackColor = System.Drawing.Color.White;
            this.edtEarth.Enabled = false;
            this.edtEarth.Location = new System.Drawing.Point(422, 376);
            this.edtEarth.Name = "edtEarth";
            this.edtEarth.ReadOnly = true;
            this.edtEarth.Size = new System.Drawing.Size(35, 23);
            this.edtEarth.TabIndex = 36;
            this.edtEarth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblEarth
            // 
            this.lblEarth.BackColor = System.Drawing.Color.White;
            this.lblEarth.Enabled = false;
            this.lblEarth.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblEarth.Location = new System.Drawing.Point(351, 376);
            this.lblEarth.Name = "lblEarth";
            this.lblEarth.ReadOnly = true;
            this.lblEarth.Size = new System.Drawing.Size(70, 23);
            this.lblEarth.TabIndex = 35;
            this.lblEarth.Text = "Earth";
            this.lblEarth.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // edtSleep
            // 
            this.edtSleep.BackColor = System.Drawing.Color.White;
            this.edtSleep.Enabled = false;
            this.edtSleep.Location = new System.Drawing.Point(309, 376);
            this.edtSleep.Name = "edtSleep";
            this.edtSleep.ReadOnly = true;
            this.edtSleep.Size = new System.Drawing.Size(35, 23);
            this.edtSleep.TabIndex = 34;
            this.edtSleep.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblSleep
            // 
            this.lblSleep.BackColor = System.Drawing.Color.White;
            this.lblSleep.Enabled = false;
            this.lblSleep.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblSleep.Location = new System.Drawing.Point(238, 376);
            this.lblSleep.Name = "lblSleep";
            this.lblSleep.ReadOnly = true;
            this.lblSleep.Size = new System.Drawing.Size(70, 23);
            this.lblSleep.TabIndex = 33;
            this.lblSleep.Text = "Sleep";
            this.lblSleep.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // edtDazle
            // 
            this.edtDazle.BackColor = System.Drawing.Color.White;
            this.edtDazle.Enabled = false;
            this.edtDazle.Location = new System.Drawing.Point(196, 376);
            this.edtDazle.Name = "edtDazle";
            this.edtDazle.ReadOnly = true;
            this.edtDazle.Size = new System.Drawing.Size(35, 23);
            this.edtDazle.TabIndex = 32;
            this.edtDazle.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblDazle
            // 
            this.lblDazle.BackColor = System.Drawing.Color.White;
            this.lblDazle.Enabled = false;
            this.lblDazle.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblDazle.Location = new System.Drawing.Point(125, 376);
            this.lblDazle.Name = "lblDazle";
            this.lblDazle.ReadOnly = true;
            this.lblDazle.Size = new System.Drawing.Size(70, 23);
            this.lblDazle.TabIndex = 31;
            this.lblDazle.Text = "Dazle";
            this.lblDazle.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // edtBlast
            // 
            this.edtBlast.BackColor = System.Drawing.Color.White;
            this.edtBlast.Enabled = false;
            this.edtBlast.Location = new System.Drawing.Point(83, 376);
            this.edtBlast.Name = "edtBlast";
            this.edtBlast.ReadOnly = true;
            this.edtBlast.Size = new System.Drawing.Size(35, 23);
            this.edtBlast.TabIndex = 30;
            this.edtBlast.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblBlast
            // 
            this.lblBlast.BackColor = System.Drawing.Color.White;
            this.lblBlast.Enabled = false;
            this.lblBlast.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblBlast.Location = new System.Drawing.Point(12, 376);
            this.lblBlast.Name = "lblBlast";
            this.lblBlast.ReadOnly = true;
            this.lblBlast.Size = new System.Drawing.Size(70, 23);
            this.lblBlast.TabIndex = 29;
            this.lblBlast.Text = "Blast";
            this.lblBlast.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // edtWind
            // 
            this.edtWind.BackColor = System.Drawing.Color.White;
            this.edtWind.Enabled = false;
            this.edtWind.Location = new System.Drawing.Point(648, 352);
            this.edtWind.Name = "edtWind";
            this.edtWind.ReadOnly = true;
            this.edtWind.Size = new System.Drawing.Size(35, 23);
            this.edtWind.TabIndex = 28;
            this.edtWind.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblWind
            // 
            this.lblWind.BackColor = System.Drawing.Color.White;
            this.lblWind.Enabled = false;
            this.lblWind.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblWind.Location = new System.Drawing.Point(577, 352);
            this.lblWind.Name = "lblWind";
            this.lblWind.ReadOnly = true;
            this.lblWind.Size = new System.Drawing.Size(70, 23);
            this.lblWind.TabIndex = 27;
            this.lblWind.Text = "Wind";
            this.lblWind.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // edtIce
            // 
            this.edtIce.BackColor = System.Drawing.Color.White;
            this.edtIce.Enabled = false;
            this.edtIce.Location = new System.Drawing.Point(535, 352);
            this.edtIce.Name = "edtIce";
            this.edtIce.ReadOnly = true;
            this.edtIce.Size = new System.Drawing.Size(35, 23);
            this.edtIce.TabIndex = 26;
            this.edtIce.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblIce
            // 
            this.lblIce.BackColor = System.Drawing.Color.White;
            this.lblIce.Enabled = false;
            this.lblIce.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblIce.Location = new System.Drawing.Point(464, 352);
            this.lblIce.Name = "lblIce";
            this.lblIce.ReadOnly = true;
            this.lblIce.Size = new System.Drawing.Size(70, 23);
            this.lblIce.TabIndex = 25;
            this.lblIce.Text = "Ice";
            this.lblIce.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // edtFire
            // 
            this.edtFire.BackColor = System.Drawing.Color.White;
            this.edtFire.Enabled = false;
            this.edtFire.Location = new System.Drawing.Point(422, 352);
            this.edtFire.Name = "edtFire";
            this.edtFire.ReadOnly = true;
            this.edtFire.Size = new System.Drawing.Size(35, 23);
            this.edtFire.TabIndex = 24;
            this.edtFire.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblFire
            // 
            this.lblFire.BackColor = System.Drawing.Color.White;
            this.lblFire.Enabled = false;
            this.lblFire.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblFire.Location = new System.Drawing.Point(351, 352);
            this.lblFire.Name = "lblFire";
            this.lblFire.ReadOnly = true;
            this.lblFire.Size = new System.Drawing.Size(70, 23);
            this.lblFire.TabIndex = 23;
            this.lblFire.Text = "Fire";
            this.lblFire.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // edtAgi
            // 
            this.edtAgi.BackColor = System.Drawing.Color.White;
            this.edtAgi.Enabled = false;
            this.edtAgi.Location = new System.Drawing.Point(309, 352);
            this.edtAgi.Name = "edtAgi";
            this.edtAgi.ReadOnly = true;
            this.edtAgi.Size = new System.Drawing.Size(35, 23);
            this.edtAgi.TabIndex = 22;
            this.edtAgi.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblAgi
            // 
            this.lblAgi.BackColor = System.Drawing.Color.White;
            this.lblAgi.Enabled = false;
            this.lblAgi.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblAgi.Location = new System.Drawing.Point(238, 352);
            this.lblAgi.Name = "lblAgi";
            this.lblAgi.ReadOnly = true;
            this.lblAgi.Size = new System.Drawing.Size(70, 23);
            this.lblAgi.TabIndex = 21;
            this.lblAgi.Text = "Agi";
            this.lblAgi.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // edtDef
            // 
            this.edtDef.BackColor = System.Drawing.Color.White;
            this.edtDef.Enabled = false;
            this.edtDef.Location = new System.Drawing.Point(196, 352);
            this.edtDef.Name = "edtDef";
            this.edtDef.ReadOnly = true;
            this.edtDef.Size = new System.Drawing.Size(35, 23);
            this.edtDef.TabIndex = 20;
            this.edtDef.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblDef
            // 
            this.lblDef.BackColor = System.Drawing.Color.White;
            this.lblDef.Enabled = false;
            this.lblDef.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblDef.Location = new System.Drawing.Point(125, 352);
            this.lblDef.Name = "lblDef";
            this.lblDef.ReadOnly = true;
            this.lblDef.Size = new System.Drawing.Size(70, 23);
            this.lblDef.TabIndex = 19;
            this.lblDef.Text = "Def";
            this.lblDef.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // edtAtk
            // 
            this.edtAtk.BackColor = System.Drawing.Color.White;
            this.edtAtk.Enabled = false;
            this.edtAtk.Location = new System.Drawing.Point(83, 352);
            this.edtAtk.Name = "edtAtk";
            this.edtAtk.ReadOnly = true;
            this.edtAtk.Size = new System.Drawing.Size(35, 23);
            this.edtAtk.TabIndex = 18;
            this.edtAtk.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblAtk
            // 
            this.lblAtk.BackColor = System.Drawing.Color.White;
            this.lblAtk.Enabled = false;
            this.lblAtk.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblAtk.Location = new System.Drawing.Point(12, 352);
            this.lblAtk.Name = "lblAtk";
            this.lblAtk.ReadOnly = true;
            this.lblAtk.Size = new System.Drawing.Size(70, 23);
            this.lblAtk.TabIndex = 17;
            this.lblAtk.Text = "Atk";
            this.lblAtk.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lin3
            // 
            this.lin3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lin3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(66)))), ((int)(((byte)(66)))));
            this.lin3.Enabled = false;
            this.lin3.Location = new System.Drawing.Point(3, 343);
            this.lin3.Name = "lin3";
            this.lin3.Size = new System.Drawing.Size(748, 1);
            this.lin3.TabIndex = 28;
            // 
            // lblItems
            // 
            this.lblItems.AutoSize = true;
            this.lblItems.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblItems.Location = new System.Drawing.Point(215, 233);
            this.lblItems.Name = "lblItems";
            this.lblItems.Size = new System.Drawing.Size(85, 15);
            this.lblItems.TabIndex = 15;
            this.lblItems.Text = "Items Dropped";
            // 
            // lblMP
            // 
            this.lblMP.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblMP.Location = new System.Drawing.Point(10, 301);
            this.lblMP.Name = "lblMP";
            this.lblMP.Size = new System.Drawing.Size(70, 23);
            this.lblMP.TabIndex = 13;
            this.lblMP.Text = "MP";
            this.lblMP.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblHP
            // 
            this.lblHP.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblHP.Location = new System.Drawing.Point(10, 277);
            this.lblHP.Name = "lblHP";
            this.lblHP.Size = new System.Drawing.Size(70, 23);
            this.lblHP.TabIndex = 11;
            this.lblHP.Text = "HP";
            this.lblHP.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblGold
            // 
            this.lblGold.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblGold.Location = new System.Drawing.Point(10, 253);
            this.lblGold.Name = "lblGold";
            this.lblGold.Size = new System.Drawing.Size(70, 23);
            this.lblGold.TabIndex = 9;
            this.lblGold.Text = "Gold";
            this.lblGold.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // edtHP
            // 
            this.edtHP.BackColor = System.Drawing.Color.White;
            this.edtHP.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.edtHP.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.edtHP.Location = new System.Drawing.Point(83, 276);
            this.edtHP.Name = "edtHP";
            this.edtHP.ReadOnly = true;
            this.edtHP.Size = new System.Drawing.Size(75, 22);
            this.edtHP.TabIndex = 12;
            this.edtHP.Text = "[HP]";
            this.edtHP.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblExp
            // 
            this.lblExp.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblExp.Location = new System.Drawing.Point(10, 229);
            this.lblExp.Name = "lblExp";
            this.lblExp.Size = new System.Drawing.Size(70, 23);
            this.lblExp.TabIndex = 7;
            this.lblExp.Text = "EXP";
            this.lblExp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // edtMP
            // 
            this.edtMP.BackColor = System.Drawing.Color.White;
            this.edtMP.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.edtMP.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.edtMP.Location = new System.Drawing.Point(83, 300);
            this.edtMP.Name = "edtMP";
            this.edtMP.ReadOnly = true;
            this.edtMP.Size = new System.Drawing.Size(75, 22);
            this.edtMP.TabIndex = 14;
            this.edtMP.Text = "[MP]";
            this.edtMP.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // edtGold
            // 
            this.edtGold.BackColor = System.Drawing.Color.White;
            this.edtGold.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.edtGold.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.edtGold.Location = new System.Drawing.Point(83, 252);
            this.edtGold.Name = "edtGold";
            this.edtGold.ReadOnly = true;
            this.edtGold.Size = new System.Drawing.Size(75, 22);
            this.edtGold.TabIndex = 10;
            this.edtGold.Text = "[gold]";
            this.edtGold.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // edtExp
            // 
            this.edtExp.BackColor = System.Drawing.Color.White;
            this.edtExp.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.edtExp.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.edtExp.Location = new System.Drawing.Point(83, 228);
            this.edtExp.Name = "edtExp";
            this.edtExp.ReadOnly = true;
            this.edtExp.Size = new System.Drawing.Size(75, 22);
            this.edtExp.TabIndex = 8;
            this.edtExp.Text = "[exp]";
            this.edtExp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lin2
            // 
            this.lin2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lin2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(66)))), ((int)(((byte)(66)))));
            this.lin2.Enabled = false;
            this.lin2.Location = new System.Drawing.Point(3, 221);
            this.lin2.Name = "lin2";
            this.lin2.Size = new System.Drawing.Size(748, 1);
            this.lin2.TabIndex = 12;
            // 
            // edtHaunts
            // 
            this.edtHaunts.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtHaunts.BackColor = System.Drawing.Color.White;
            this.edtHaunts.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.edtHaunts.Location = new System.Drawing.Point(325, 197);
            this.edtHaunts.Name = "edtHaunts";
            this.edtHaunts.ReadOnly = true;
            this.edtHaunts.Size = new System.Drawing.Size(426, 16);
            this.edtHaunts.TabIndex = 6;
            this.edtHaunts.Text = "[haunts]";
            // 
            // lblHaunts
            // 
            this.lblHaunts.AutoSize = true;
            this.lblHaunts.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblHaunts.Location = new System.Drawing.Point(215, 197);
            this.lblHaunts.Name = "lblHaunts";
            this.lblHaunts.Size = new System.Drawing.Size(45, 15);
            this.lblHaunts.TabIndex = 5;
            this.lblHaunts.Text = "Haunts";
            // 
            // edtDesc
            // 
            this.edtDesc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtDesc.BackColor = System.Drawing.Color.White;
            this.edtDesc.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.edtDesc.Location = new System.Drawing.Point(325, 98);
            this.edtDesc.Multiline = true;
            this.edtDesc.Name = "edtDesc";
            this.edtDesc.ReadOnly = true;
            this.edtDesc.Size = new System.Drawing.Size(426, 83);
            this.edtDesc.TabIndex = 4;
            this.edtDesc.Text = "[description]";
            // 
            // lin1
            // 
            this.lin1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lin1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(66)))), ((int)(((byte)(66)))));
            this.lin1.Enabled = false;
            this.lin1.Location = new System.Drawing.Point(3, 44);
            this.lin1.Name = "lin1";
            this.lin1.Size = new System.Drawing.Size(748, 1);
            this.lin1.TabIndex = 3;
            // 
            // edtFamily
            // 
            this.edtFamily.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtFamily.BackColor = System.Drawing.Color.White;
            this.edtFamily.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.edtFamily.Font = new System.Drawing.Font("Segoe UI", 14.25F);
            this.edtFamily.Location = new System.Drawing.Point(325, 60);
            this.edtFamily.Name = "edtFamily";
            this.edtFamily.ReadOnly = true;
            this.edtFamily.Size = new System.Drawing.Size(426, 26);
            this.edtFamily.TabIndex = 2;
            this.edtFamily.Text = "[family]";
            // 
            // edtID
            // 
            this.edtID.BackColor = System.Drawing.Color.White;
            this.edtID.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.edtID.Enabled = false;
            this.edtID.Font = new System.Drawing.Font("Segoe UI", 20.25F);
            this.edtID.Location = new System.Drawing.Point(5, 3);
            this.edtID.Name = "edtID";
            this.edtID.ReadOnly = true;
            this.edtID.Size = new System.Drawing.Size(67, 36);
            this.edtID.TabIndex = 1;
            this.edtID.Text = "[000]";
            // 
            // picImage
            // 
            this.picImage.BackColor = System.Drawing.Color.White;
            this.picImage.Enabled = false;
            this.picImage.Location = new System.Drawing.Point(3, 51);
            this.picImage.Name = "picImage";
            this.picImage.Size = new System.Drawing.Size(206, 167);
            this.picImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picImage.TabIndex = 1;
            this.picImage.TabStop = false;
            // 
            // edtName
            // 
            this.edtName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtName.BackColor = System.Drawing.Color.White;
            this.edtName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.edtName.Font = new System.Drawing.Font("Segoe UI", 20.25F);
            this.edtName.Location = new System.Drawing.Point(83, 3);
            this.edtName.Name = "edtName";
            this.edtName.ReadOnly = true;
            this.edtName.Size = new System.Drawing.Size(673, 36);
            this.edtName.TabIndex = 0;
            this.edtName.Text = "[name]";
            // 
            // frmBeasts
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.Controls.Add(this.sconBts);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "frmBeasts";
            this.Size = new System.Drawing.Size(1079, 549);
            this.sconBts.Panel1.ResumeLayout(false);
            this.sconBts.Panel2.ResumeLayout(false);
            this.sconBts.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sconBts)).EndInit();
            this.sconBts.ResumeLayout(false);
            this.sconFilter.Panel1.ResumeLayout(false);
            this.sconFilter.Panel1.PerformLayout();
            this.sconFilter.Panel2.ResumeLayout(false);
            this.sconFilter.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sconFilter)).EndInit();
            this.sconFilter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgBeasts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picImage)).EndInit();
            this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.SplitContainer sconBts;
		private System.Windows.Forms.SplitContainer sconFilter;
		private System.Windows.Forms.TextBox edtName;
		private System.Windows.Forms.PictureBox picImage;
		private System.Windows.Forms.TextBox edtFamily;
		private System.Windows.Forms.TextBox edtID;
		private System.Windows.Forms.Panel lin2;
		private System.Windows.Forms.TextBox edtHaunts;
		private System.Windows.Forms.Label lblHaunts;
		private System.Windows.Forms.TextBox edtDesc;
		private System.Windows.Forms.Panel lin1;
		private System.Windows.Forms.Label lblItems;
		private System.Windows.Forms.Label lblMP;
		private System.Windows.Forms.Label lblHP;
		private System.Windows.Forms.Label lblGold;
		private System.Windows.Forms.TextBox edtHP;
		private System.Windows.Forms.Label lblExp;
		private System.Windows.Forms.TextBox edtMP;
		private System.Windows.Forms.TextBox edtGold;
		private System.Windows.Forms.TextBox edtExp;
		private System.Windows.Forms.Panel lin3;
		private System.Windows.Forms.TextBox edtStop;
		private System.Windows.Forms.TextBox lblStop;
		private System.Windows.Forms.TextBox edtDecMR;
		private System.Windows.Forms.TextBox lblDecMR;
		private System.Windows.Forms.TextBox edtDecSpeed;
		private System.Windows.Forms.TextBox lblDecSpeed;
		private System.Windows.Forms.TextBox edtDecDef;
		private System.Windows.Forms.TextBox lblDecDef;
		private System.Windows.Forms.TextBox edtDecAtk;
		private System.Windows.Forms.TextBox lblDecAtk;
		private System.Windows.Forms.TextBox edtDrainMP;
		private System.Windows.Forms.TextBox lblDrainMP;
		private System.Windows.Forms.TextBox edtMesmerize;
		private System.Windows.Forms.TextBox lblMesmerize;
		private System.Windows.Forms.TextBox edtParalyze;
		private System.Windows.Forms.TextBox lblParalyze;
		private System.Windows.Forms.TextBox edtConfusion;
		private System.Windows.Forms.TextBox lblConfusion;
		private System.Windows.Forms.TextBox edtPoison;
		private System.Windows.Forms.TextBox lblPoison;
		private System.Windows.Forms.TextBox edtFizle;
		private System.Windows.Forms.TextBox lblFizle;
		private System.Windows.Forms.TextBox edtDeath;
		private System.Windows.Forms.TextBox lblDeath;
		private System.Windows.Forms.TextBox edtLight;
		private System.Windows.Forms.TextBox lblLight;
		private System.Windows.Forms.TextBox edtDark;
		private System.Windows.Forms.TextBox lblDark;
		private System.Windows.Forms.TextBox edtEarth;
		private System.Windows.Forms.TextBox lblEarth;
		private System.Windows.Forms.TextBox edtSleep;
		private System.Windows.Forms.TextBox lblSleep;
		private System.Windows.Forms.TextBox edtDazle;
		private System.Windows.Forms.TextBox lblDazle;
		private System.Windows.Forms.TextBox edtBlast;
		private System.Windows.Forms.TextBox lblBlast;
		private System.Windows.Forms.TextBox edtWind;
		private System.Windows.Forms.TextBox lblWind;
		private System.Windows.Forms.TextBox edtIce;
		private System.Windows.Forms.TextBox lblIce;
		private System.Windows.Forms.TextBox edtFire;
		private System.Windows.Forms.TextBox lblFire;
		private System.Windows.Forms.TextBox edtAgi;
		private System.Windows.Forms.TextBox lblAgi;
		private System.Windows.Forms.TextBox edtDef;
		private System.Windows.Forms.TextBox lblDef;
		private System.Windows.Forms.TextBox edtAtk;
		private System.Windows.Forms.TextBox lblAtk;
        private System.Windows.Forms.Button btnFilter;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.ComboBox edtFtOrder2;
        private System.Windows.Forms.Label lblOrder;
        private System.Windows.Forms.ComboBox edtFtOrder;
        private System.Windows.Forms.Label lblFtDesc;
        private System.Windows.Forms.Label lblFtLoc;
        private System.Windows.Forms.Label lblFtItems;
        private System.Windows.Forms.TextBox edtFtDesc;
        private System.Windows.Forms.TextBox edtFtHaunts;
        private System.Windows.Forms.TextBox edtFtItems;
        private System.Windows.Forms.ComboBox edtFtFamily;
        private System.Windows.Forms.Label lblFtFamily;
        private System.Windows.Forms.Label lblFtName;
        private System.Windows.Forms.TextBox edtFtName;
        private System.Windows.Forms.DataGridView dgBeasts;
        private System.Windows.Forms.DataGridViewTextBoxColumn colID;
        private System.Windows.Forms.DataGridViewTextBoxColumn colName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colFamily;
        private System.Windows.Forms.DataGridViewTextBoxColumn colExp;
        private System.Windows.Forms.DataGridViewTextBoxColumn colGold;
        private System.Windows.Forms.DataGridViewTextBoxColumn colHP;
        private System.Windows.Forms.DataGridViewTextBoxColumn colMP;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAtk;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDef;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAgi;
        private System.Windows.Forms.DataGridViewTextBoxColumn colFire;
        private System.Windows.Forms.DataGridViewTextBoxColumn colIce;
        private System.Windows.Forms.DataGridViewTextBoxColumn colWind;
        private System.Windows.Forms.DataGridViewTextBoxColumn colEarth;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDark;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLight;
        private System.Windows.Forms.DataGridViewTextBoxColumn colBlast;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDazle;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSleep;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDeath;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDrainMP;
        private System.Windows.Forms.DataGridViewTextBoxColumn colConfusion;
        private System.Windows.Forms.DataGridViewTextBoxColumn colFizle;
        private System.Windows.Forms.DataGridViewTextBoxColumn colStop;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPoison;
        private System.Windows.Forms.DataGridViewTextBoxColumn colParalyze;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDecAtk;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDecDef;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDecSpd;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDecMR;
        private System.Windows.Forms.DataGridViewTextBoxColumn colMesmerize;
        private System.Windows.Forms.DataGridViewTextBoxColumn colHaunts;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDesc;
        private System.Windows.Forms.DataGridViewTextBoxColumn colItem1;
        private System.Windows.Forms.DataGridViewTextBoxColumn colRate1;
        private System.Windows.Forms.DataGridViewTextBoxColumn colItem2;
        private System.Windows.Forms.DataGridViewTextBoxColumn colRate2;
        private System.Windows.Forms.DataGridViewTextBoxColumn colItem3;
        private System.Windows.Forms.DataGridViewTextBoxColumn colRate3;
        private System.Windows.Forms.DataGridViewTextBoxColumn colItem4;
        private System.Windows.Forms.DataGridViewTextBoxColumn colRate4;
        private System.Windows.Forms.DataGridViewTextBoxColumn colItem5;
        private System.Windows.Forms.DataGridViewTextBoxColumn colRate5;
        private System.Windows.Forms.DataGridViewTextBoxColumn colItem6;
        private System.Windows.Forms.DataGridViewTextBoxColumn colRate6;
        private System.Windows.Forms.DataGridViewTextBoxColumn colItem7;
        private System.Windows.Forms.DataGridViewTextBoxColumn colRate7;
        private System.Windows.Forms.DataGridViewTextBoxColumn colFilename;
        private System.Windows.Forms.Label lblFtCaption;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnApply;
        private System.Windows.Forms.Label lblDesc;
        private System.Windows.Forms.Label lblFamily;
        private System.Windows.Forms.RichTextBox edtItems;
    }
}
