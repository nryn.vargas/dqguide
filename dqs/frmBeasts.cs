﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Data;
using System.IO;
using System.Windows.Forms;

namespace dqs
{
	public partial class frmBeasts : UserControl
	{
		private bool iUpdating;

		public frmBeasts()
		{
			InitializeComponent();
			UpdateUI();
			this.edtFtFamily.Items.Add("Any/All");
			List<string> list = Program.DBConnection.SelectAllVal(
				"SELECT name FROM families");
			foreach (string item in list)
			{
				this.edtFtFamily.Items.Add(item);
			}
			this.edtFtFamily.SelectedIndex = 0;
			this.edtFtOrder.SelectedIndex = 0;
			this.edtFtOrder2.SelectedIndex = 0;
			this.sconFilter.Panel1Collapsed = false;
			this.sconFilter.Panel2Collapsed = true;
			LoadData();
		}

		private void LoadData()
		{
			iUpdating = true;
			string StrWhere = "WHERE (m.name <> '') ";
			string StrSelect = "SELECT m.id, m.name, f.name as family, " +
				"exp, gold, hp, mp, atk, def, agi, fire, ice, wind, blast, earth, dark, light, " +
				"dazle, sleep, death, drainMP, confusion, fizle, stop, poison, paralyze, " +
				"decAtk, decDef, decSpeed, decMR, mesmerize, description, haunts, " +
				"concat_ws(' ', item1, item2, item3, item4, item5, item6, item7) as item_drops, " +
				"item1, rate1, item2, rate2, item3, rate3, item4, rate4, " +
				"item5, rate5, item6, rate6, item7, rate7, stolen, filename " +
				"from monsters m " +
				"left join families f on m.family_id = f.id ";
			if (edtFtName.Text != "")
			{
				StrWhere = StrWhere + "AND (m.name LIKE '%" + edtFtName.Text + "%') ";
			}
			if (edtFtFamily.SelectedIndex > 0)
			{
				StrWhere = StrWhere + "AND (f.name LIKE '%" + edtFtFamily.Text + "%') ";
			}
			if (edtFtDesc.Text != "")
			{
				StrWhere = StrWhere + "AND (description LIKE '%" + edtFtDesc.Text + "%') ";
			}
			if (edtFtHaunts.Text != "")
			{
				StrWhere = StrWhere + "AND (haunts LIKE '%" + edtFtHaunts.Text + "%') ";
			}
			if (edtFtItems.Text != "")
			{
				StrWhere = StrWhere + "HAVING (item_drops LIKE '%" + edtFtItems.Text + "%') ";
			}
			StrSelect = StrSelect + StrWhere + "ORDER BY " + 
				this.edtFtOrder.Text + " " + this.edtFtOrder2.Text;
			DataTable Ttable = Program.DBConnection.SelectTable(StrSelect);
			BindingSource SBind = new BindingSource();
			SBind.DataSource = Ttable;

			dgBeasts.AutoGenerateColumns = false;
			dgBeasts.DataSource = Ttable;
			dgBeasts.DataSource = SBind;
			dgBeasts.Refresh();
			iUpdating = false;
			dgBeasts_SelectionChanged(dgBeasts, null);
			dgBeasts.Focus();
		}

		private void btnFilter_Click(object sender, EventArgs e)
		{
			sconFilter.Panel1Collapsed = true;
			sconFilter.Panel2Collapsed = false;
		}

		private void btnClear_Click(object sender, EventArgs e)
		{
			edtFtName.Clear();
			edtFtDesc.Clear();
			edtFtItems.Clear();
			edtFtHaunts.Clear();
			edtFtFamily.SelectedIndex = 0;
			edtFtOrder.SelectedIndex = 0;
			edtFtOrder2.SelectedIndex = 0;
		}

		private void btnApply_Click(object sender, EventArgs e)
		{
			LoadData();
			sconFilter.Panel1Collapsed = false;
			sconFilter.Panel2Collapsed = true;
		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			sconFilter.Panel1Collapsed = false;
			sconFilter.Panel2Collapsed = true;
		}

		private void dgBeasts_SelectionChanged(object sender, EventArgs e)
		{
			void LoadText(TextBox textBox, DataGridViewTextBoxColumn ColName)
			{
				textBox.Text = dgBeasts.SelectedRows[0].Cells[ColName.Index].Value.ToString();
			}

			void AddLineText(ref string StringBuffer, DataGridViewTextBoxColumn ColItem, 
				DataGridViewTextBoxColumn ColRate)
			{
				
				string sVal = dgBeasts.SelectedRows[0].Cells[ColItem.Index]
					.Value.ToString().Trim();
				if (sVal != "")
				{
					StringBuffer = StringBuffer + (sVal + ": ");
					sVal = dgBeasts.SelectedRows[0].Cells[ColRate.Index]
						.Value.ToString().Trim();
					try
					{
						double fVal = Convert.ToDouble(sVal);
						sVal = fVal.ToString() + "%";
					}
					catch (Exception)
					{
						//
					}
					StringBuffer = StringBuffer + sVal;
				}
				StringBuffer = StringBuffer + "\r\n";
			}

			void RefreshText(string StringBuffer, RichTextBox textBox)
			{
				while (StringBuffer.EndsWith("\r\n"))
				{
					StringBuffer = StringBuffer.Remove(StringBuffer.Length - 2, 2);
				}
				textBox.Text = StringBuffer;
				textBox.SelectionStart = 0;
				textBox.SelectionLength = 0;
			}

			if ((iUpdating == false) && (dgBeasts.SelectedRows.Count > 0))
			{
				this.Cursor = Cursors.WaitCursor;
				LoadText(edtID, colID);
				LoadText(edtName, colName);
				LoadText(edtFamily, colFamily);
				LoadText(edtDesc, colDesc);
				LoadText(edtHaunts, colHaunts);
				LoadText(edtExp, colExp);
				LoadText(edtGold, colGold);
				LoadText(edtHP, colHP);
				LoadText(edtMP, colMP);

				LoadText(edtAtk, colAtk);
				LoadText(edtDef, colDef);
				LoadText(edtAgi, colAgi);
				LoadText(edtFire, colFire);
				LoadText(edtIce, colIce);
				LoadText(edtWind, colWind);
				LoadText(edtEarth, colEarth);
				LoadText(edtBlast, colBlast);
				LoadText(edtDark, colDark);
				LoadText(edtLight, colLight);
				LoadText(edtDazle, colDazle);
				LoadText(edtSleep, colSleep);
				LoadText(edtDeath, colDeath);
				LoadText(edtDrainMP, colDrainMP);
				LoadText(edtConfusion, colConfusion);
				LoadText(edtMesmerize, colMesmerize);
				LoadText(edtFizle, colFizle);
				LoadText(edtStop, colStop);
				LoadText(edtPoison, colPoison);
				LoadText(edtParalyze, colParalyze);
				LoadText(edtDecAtk, colDecAtk);
				LoadText(edtDecDef, colDecDef);
				LoadText(edtDecMR, colDecMR);
				LoadText(edtDecSpeed, colDecSpd);
				//
				string TextValue = "";
				AddLineText(ref TextValue, colItem1, colRate1);
				AddLineText(ref TextValue, colItem2, colRate2);
				AddLineText(ref TextValue, colItem3, colRate3);
				AddLineText(ref TextValue, colItem4, colRate4);
				AddLineText(ref TextValue, colItem5, colRate5);
				AddLineText(ref TextValue, colItem6, colRate6);
				AddLineText(ref TextValue, colItem7, colRate7);
				RefreshText(TextValue, edtItems);
				//
				string sfile = dgBeasts.SelectedRows[0].Cells[colFilename.Index].Value.ToString();
				Image img;
				Size newsize;
				img = (Image)Properties.Resources.ResourceManager.GetObject("placeholder");
				newsize = new Size((int)(img.Width), (int)(img.Height));
				picImage.Tag = -1;
				if (sfile != "")
				{
					if (File.Exists("monsters\\" + sfile))
					{
						sfile = "monsters\\" + sfile;
						try
						{
							picImage.Tag = 1;
							img = Image.FromFile(sfile);
						}
						catch (Exception)
						{
							//picImage.Tag = -1;
						}
					}
					if (picImage.Tag.Equals(1))
					{
						if ((img.Height < picImage.Height) || (img.Width < picImage.Width))
						{
							newsize.Width = (newsize.Width * 5);
							newsize.Height = (newsize.Height * 5);
							while ((newsize.Height > picImage.Height) || (newsize.Width > picImage.Width))
							{
								newsize.Height = (int)(0.8 * newsize.Height);
								newsize.Width = (int)(0.8 * newsize.Width);
							}
						}
						else
						{
							newsize = new Size((int)(img.Width * 0.8), (int)(img.Height * 0.8));
							while ((newsize.Height > picImage.Height) || (newsize.Width > picImage.Width))
							{
								newsize.Height = (int)(0.8 * newsize.Height);
								newsize.Width = (int)(0.8 * newsize.Width);
							}
						} 
					}
				}
				Bitmap bmp = new Bitmap(img, newsize);
				picImage.Image = bmp;
				img.Dispose();
				if (picImage.Tag.Equals(-1))
				{
					Program.ChangePicBoxColor(picImage, Properties.Settings.Default.colorPrimary);
				}
				this.Cursor = Cursors.Default;
				this.Cursor = Cursors.Default;
				this.Cursor = Cursors.Default;
			}
		}

		public void UpdateUI()
		{
			Color colorPrimary = Properties.Settings.Default.colorPrimary;
			Color colorPriText = Properties.Settings.Default.colorPriText;

			dgBeasts.DefaultCellStyle.SelectionBackColor = colorPrimary;
			dgBeasts.DefaultCellStyle.SelectionForeColor = colorPriText;
			Program.ChangeButtonColor(btnFilter, colorPrimary);
			Program.ChangeButtonColor(btnClear, colorPrimary);
			Program.ChangeButtonColor(btnApply, colorPrimary);
			Program.ChangeButtonColor(btnCancel, colorPrimary);
		}

		public int GetSplitterDistance()
		{
			return sconBts.SplitterDistance;
		}

		public void SetSplitterDistance(int split)
		{
			sconBts.SplitterDistance = split;
		}
	}
}
