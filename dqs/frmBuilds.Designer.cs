﻿namespace dqs
{
    partial class frmBuilds
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.sconBuilds = new System.Windows.Forms.SplitContainer();
            this.dgBuilds = new System.Windows.Forms.DataGridView();
            this.colID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colSpent = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colUnspent = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colBestVoc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colNote = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlAllocations = new System.Windows.Forms.Panel();
            this.dgAllocations = new System.Windows.Forms.DataGridView();
            this.colAllocation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAllocNote = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblUnspent = new System.Windows.Forms.Label();
            this.lblSpent = new System.Windows.Forms.Label();
            this.lblSP = new System.Windows.Forms.Label();
            this.lblAllocations = new System.Windows.Forms.Label();
            this.lblNote = new System.Windows.Forms.Label();
            this.edtNote = new System.Windows.Forms.TextBox();
            this.edtUnspent = new System.Windows.Forms.TextBox();
            this.edtSpent = new System.Windows.Forms.TextBox();
            this.lin2 = new System.Windows.Forms.Panel();
            this.edtBestVoc = new System.Windows.Forms.TextBox();
            this.lblBestVoc = new System.Windows.Forms.Label();
            this.lin1 = new System.Windows.Forms.Panel();
            this.edtName = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.sconBuilds)).BeginInit();
            this.sconBuilds.Panel1.SuspendLayout();
            this.sconBuilds.Panel2.SuspendLayout();
            this.sconBuilds.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgBuilds)).BeginInit();
            this.pnlAllocations.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgAllocations)).BeginInit();
            this.SuspendLayout();
            // 
            // sconBuilds
            // 
            this.sconBuilds.BackColor = System.Drawing.Color.White;
            this.sconBuilds.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.sconBuilds.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sconBuilds.Location = new System.Drawing.Point(0, 0);
            this.sconBuilds.Name = "sconBuilds";
            // 
            // sconBuilds.Panel1
            // 
            this.sconBuilds.Panel1.Controls.Add(this.dgBuilds);
            this.sconBuilds.Panel1.Cursor = System.Windows.Forms.Cursors.Default;
            this.sconBuilds.Panel1MinSize = 310;
            // 
            // sconBuilds.Panel2
            // 
            this.sconBuilds.Panel2.BackColor = System.Drawing.Color.White;
            this.sconBuilds.Panel2.Controls.Add(this.pnlAllocations);
            this.sconBuilds.Panel2.Controls.Add(this.lblUnspent);
            this.sconBuilds.Panel2.Controls.Add(this.lblSpent);
            this.sconBuilds.Panel2.Controls.Add(this.lblSP);
            this.sconBuilds.Panel2.Controls.Add(this.lblAllocations);
            this.sconBuilds.Panel2.Controls.Add(this.lblNote);
            this.sconBuilds.Panel2.Controls.Add(this.edtNote);
            this.sconBuilds.Panel2.Controls.Add(this.edtUnspent);
            this.sconBuilds.Panel2.Controls.Add(this.edtSpent);
            this.sconBuilds.Panel2.Controls.Add(this.lin2);
            this.sconBuilds.Panel2.Controls.Add(this.edtBestVoc);
            this.sconBuilds.Panel2.Controls.Add(this.lblBestVoc);
            this.sconBuilds.Panel2.Controls.Add(this.lin1);
            this.sconBuilds.Panel2.Controls.Add(this.edtName);
            this.sconBuilds.Size = new System.Drawing.Size(1137, 633);
            this.sconBuilds.SplitterDistance = 310;
            this.sconBuilds.SplitterWidth = 5;
            this.sconBuilds.TabIndex = 1;
            this.sconBuilds.TabStop = false;
            // 
            // dgBuilds
            // 
            this.dgBuilds.AllowUserToAddRows = false;
            this.dgBuilds.AllowUserToDeleteRows = false;
            this.dgBuilds.AllowUserToResizeColumns = false;
            this.dgBuilds.AllowUserToResizeRows = false;
            this.dgBuilds.BackgroundColor = System.Drawing.Color.White;
            this.dgBuilds.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgBuilds.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgBuilds.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgBuilds.ColumnHeadersVisible = false;
            this.dgBuilds.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colID,
            this.colName,
            this.colSpent,
            this.colUnspent,
            this.colBestVoc,
            this.colNote});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(151)))), ((int)(((byte)(167)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgBuilds.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgBuilds.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgBuilds.GridColor = System.Drawing.SystemColors.ControlLight;
            this.dgBuilds.Location = new System.Drawing.Point(0, 0);
            this.dgBuilds.MultiSelect = false;
            this.dgBuilds.Name = "dgBuilds";
            this.dgBuilds.ReadOnly = true;
            this.dgBuilds.RowHeadersVisible = false;
            this.dgBuilds.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgBuilds.RowTemplate.Height = 50;
            this.dgBuilds.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgBuilds.ShowEditingIcon = false;
            this.dgBuilds.Size = new System.Drawing.Size(308, 631);
            this.dgBuilds.StandardTab = true;
            this.dgBuilds.TabIndex = 4;
            this.dgBuilds.SelectionChanged += new System.EventHandler(this.dgBuilds_SelectionChanged);
            // 
            // colID
            // 
            this.colID.DataPropertyName = "id";
            this.colID.HeaderText = "ID";
            this.colID.Name = "colID";
            this.colID.ReadOnly = true;
            this.colID.Visible = false;
            // 
            // colName
            // 
            this.colName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colName.DataPropertyName = "name";
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colName.DefaultCellStyle = dataGridViewCellStyle2;
            this.colName.HeaderText = "Name";
            this.colName.Name = "colName";
            this.colName.ReadOnly = true;
            this.colName.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // colSpent
            // 
            this.colSpent.DataPropertyName = "spent";
            this.colSpent.HeaderText = "Spent";
            this.colSpent.Name = "colSpent";
            this.colSpent.ReadOnly = true;
            this.colSpent.Visible = false;
            // 
            // colUnspent
            // 
            this.colUnspent.DataPropertyName = "unspent";
            this.colUnspent.HeaderText = "Unspent";
            this.colUnspent.Name = "colUnspent";
            this.colUnspent.ReadOnly = true;
            this.colUnspent.Visible = false;
            // 
            // colBestVoc
            // 
            this.colBestVoc.DataPropertyName = "best_voc";
            this.colBestVoc.HeaderText = "Best Voc";
            this.colBestVoc.Name = "colBestVoc";
            this.colBestVoc.ReadOnly = true;
            this.colBestVoc.Visible = false;
            // 
            // colNote
            // 
            this.colNote.DataPropertyName = "note";
            this.colNote.HeaderText = "Note";
            this.colNote.Name = "colNote";
            this.colNote.ReadOnly = true;
            this.colNote.Visible = false;
            // 
            // pnlAllocations
            // 
            this.pnlAllocations.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlAllocations.Controls.Add(this.dgAllocations);
            this.pnlAllocations.Location = new System.Drawing.Point(37, 304);
            this.pnlAllocations.Name = "pnlAllocations";
            this.pnlAllocations.Size = new System.Drawing.Size(762, 309);
            this.pnlAllocations.TabIndex = 15;
            this.pnlAllocations.Visible = false;
            // 
            // dgAllocations
            // 
            this.dgAllocations.AllowUserToAddRows = false;
            this.dgAllocations.AllowUserToDeleteRows = false;
            this.dgAllocations.AllowUserToResizeRows = false;
            this.dgAllocations.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgAllocations.BackgroundColor = System.Drawing.Color.White;
            this.dgAllocations.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgAllocations.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dgAllocations.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgAllocations.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colAllocation,
            this.colAllocNote});
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(151)))), ((int)(((byte)(167)))));
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgAllocations.DefaultCellStyle = dataGridViewCellStyle6;
            this.dgAllocations.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgAllocations.GridColor = System.Drawing.SystemColors.ControlLight;
            this.dgAllocations.Location = new System.Drawing.Point(0, 0);
            this.dgAllocations.MultiSelect = false;
            this.dgAllocations.Name = "dgAllocations";
            this.dgAllocations.ReadOnly = true;
            this.dgAllocations.RowHeadersVisible = false;
            this.dgAllocations.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgAllocations.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgAllocations.ShowEditingIcon = false;
            this.dgAllocations.Size = new System.Drawing.Size(762, 309);
            this.dgAllocations.StandardTab = true;
            this.dgAllocations.TabIndex = 11;
            // 
            // colAllocation
            // 
            this.colAllocation.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.colAllocation.DataPropertyName = "allocation";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.colAllocation.DefaultCellStyle = dataGridViewCellStyle4;
            this.colAllocation.HeaderText = "Allocation";
            this.colAllocation.Name = "colAllocation";
            this.colAllocation.ReadOnly = true;
            this.colAllocation.Width = 250;
            // 
            // colAllocNote
            // 
            this.colAllocNote.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colAllocNote.DataPropertyName = "note";
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.colAllocNote.DefaultCellStyle = dataGridViewCellStyle5;
            this.colAllocNote.HeaderText = "Note";
            this.colAllocNote.Name = "colAllocNote";
            this.colAllocNote.ReadOnly = true;
            // 
            // lblUnspent
            // 
            this.lblUnspent.AutoSize = true;
            this.lblUnspent.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblUnspent.Location = new System.Drawing.Point(34, 250);
            this.lblUnspent.Name = "lblUnspent";
            this.lblUnspent.Size = new System.Drawing.Size(51, 15);
            this.lblUnspent.TabIndex = 14;
            this.lblUnspent.Text = "Unspent";
            // 
            // lblSpent
            // 
            this.lblSpent.AutoSize = true;
            this.lblSpent.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblSpent.Location = new System.Drawing.Point(34, 214);
            this.lblSpent.Name = "lblSpent";
            this.lblSpent.Size = new System.Drawing.Size(37, 15);
            this.lblSpent.TabIndex = 13;
            this.lblSpent.Text = "Spent";
            // 
            // lblSP
            // 
            this.lblSP.AutoSize = true;
            this.lblSP.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblSP.Location = new System.Drawing.Point(10, 188);
            this.lblSP.Name = "lblSP";
            this.lblSP.Size = new System.Drawing.Size(61, 15);
            this.lblSP.TabIndex = 3;
            this.lblSP.Text = "Skillpoints";
            // 
            // lblAllocations
            // 
            this.lblAllocations.AutoSize = true;
            this.lblAllocations.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblAllocations.Location = new System.Drawing.Point(34, 286);
            this.lblAllocations.Name = "lblAllocations";
            this.lblAllocations.Size = new System.Drawing.Size(74, 15);
            this.lblAllocations.TabIndex = 10;
            this.lblAllocations.Text = "▼ Allocation";
            this.lblAllocations.Click += new System.EventHandler(this.lblAllocations_Click);
            // 
            // lblNote
            // 
            this.lblNote.AutoSize = true;
            this.lblNote.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblNote.Location = new System.Drawing.Point(10, 120);
            this.lblNote.Name = "lblNote";
            this.lblNote.Size = new System.Drawing.Size(38, 15);
            this.lblNote.TabIndex = 8;
            this.lblNote.Text = "Notes";
            // 
            // edtNote
            // 
            this.edtNote.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtNote.BackColor = System.Drawing.Color.White;
            this.edtNote.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.edtNote.Location = new System.Drawing.Point(145, 120);
            this.edtNote.Multiline = true;
            this.edtNote.Name = "edtNote";
            this.edtNote.ReadOnly = true;
            this.edtNote.Size = new System.Drawing.Size(641, 39);
            this.edtNote.TabIndex = 9;
            this.edtNote.Text = "[notes]";
            // 
            // edtUnspent
            // 
            this.edtUnspent.BackColor = System.Drawing.Color.White;
            this.edtUnspent.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.edtUnspent.Font = new System.Drawing.Font("Segoe UI", 14.25F);
            this.edtUnspent.Location = new System.Drawing.Point(145, 242);
            this.edtUnspent.Margin = new System.Windows.Forms.Padding(0);
            this.edtUnspent.Name = "edtUnspent";
            this.edtUnspent.ReadOnly = true;
            this.edtUnspent.Size = new System.Drawing.Size(631, 26);
            this.edtUnspent.TabIndex = 7;
            this.edtUnspent.Text = "[unspent]";
            // 
            // edtSpent
            // 
            this.edtSpent.BackColor = System.Drawing.Color.White;
            this.edtSpent.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.edtSpent.Font = new System.Drawing.Font("Segoe UI", 14.25F);
            this.edtSpent.Location = new System.Drawing.Point(145, 206);
            this.edtSpent.Margin = new System.Windows.Forms.Padding(0);
            this.edtSpent.Name = "edtSpent";
            this.edtSpent.ReadOnly = true;
            this.edtSpent.Size = new System.Drawing.Size(631, 26);
            this.edtSpent.TabIndex = 6;
            this.edtSpent.Text = "[spent]";
            // 
            // lin2
            // 
            this.lin2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lin2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(66)))), ((int)(((byte)(66)))));
            this.lin2.Enabled = false;
            this.lin2.Location = new System.Drawing.Point(12, 175);
            this.lin2.Name = "lin2";
            this.lin2.Size = new System.Drawing.Size(787, 1);
            this.lin2.TabIndex = 12;
            // 
            // edtBestVoc
            // 
            this.edtBestVoc.BackColor = System.Drawing.Color.White;
            this.edtBestVoc.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.edtBestVoc.Font = new System.Drawing.Font("Segoe UI", 14.25F);
            this.edtBestVoc.Location = new System.Drawing.Point(145, 66);
            this.edtBestVoc.Name = "edtBestVoc";
            this.edtBestVoc.ReadOnly = true;
            this.edtBestVoc.Size = new System.Drawing.Size(642, 26);
            this.edtBestVoc.TabIndex = 2;
            this.edtBestVoc.Text = "[recommended_vocations]";
            // 
            // lblBestVoc
            // 
            this.lblBestVoc.AutoSize = true;
            this.lblBestVoc.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblBestVoc.Location = new System.Drawing.Point(10, 74);
            this.lblBestVoc.Name = "lblBestVoc";
            this.lblBestVoc.Size = new System.Drawing.Size(83, 15);
            this.lblBestVoc.TabIndex = 1;
            this.lblBestVoc.Text = "Best Vocations";
            // 
            // lin1
            // 
            this.lin1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lin1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(66)))), ((int)(((byte)(66)))));
            this.lin1.Enabled = false;
            this.lin1.Location = new System.Drawing.Point(3, 51);
            this.lin1.Name = "lin1";
            this.lin1.Size = new System.Drawing.Size(787, 1);
            this.lin1.TabIndex = 11;
            // 
            // edtName
            // 
            this.edtName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtName.BackColor = System.Drawing.Color.White;
            this.edtName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.edtName.Font = new System.Drawing.Font("Segoe UI", 20.25F);
            this.edtName.Location = new System.Drawing.Point(3, 3);
            this.edtName.Name = "edtName";
            this.edtName.ReadOnly = true;
            this.edtName.Size = new System.Drawing.Size(772, 36);
            this.edtName.TabIndex = 0;
            this.edtName.Text = "[name]";
            // 
            // frmBuilds
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.sconBuilds);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "frmBuilds";
            this.Size = new System.Drawing.Size(1137, 633);
            this.sconBuilds.Panel1.ResumeLayout(false);
            this.sconBuilds.Panel2.ResumeLayout(false);
            this.sconBuilds.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sconBuilds)).EndInit();
            this.sconBuilds.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgBuilds)).EndInit();
            this.pnlAllocations.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgAllocations)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer sconBuilds;
        private System.Windows.Forms.Label lblNote;
        private System.Windows.Forms.TextBox edtNote;
        private System.Windows.Forms.TextBox edtUnspent;
        private System.Windows.Forms.TextBox edtSpent;
        private System.Windows.Forms.Panel lin2;
        private System.Windows.Forms.TextBox edtBestVoc;
        private System.Windows.Forms.Label lblBestVoc;
        private System.Windows.Forms.Panel lin1;
        private System.Windows.Forms.TextBox edtName;
        private System.Windows.Forms.DataGridView dgBuilds;
        private System.Windows.Forms.Label lblAllocations;
        private System.Windows.Forms.DataGridView dgAllocations;
        private System.Windows.Forms.Label lblSP;
        private System.Windows.Forms.DataGridViewTextBoxColumn colID;
        private System.Windows.Forms.DataGridViewTextBoxColumn colName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSpent;
        private System.Windows.Forms.DataGridViewTextBoxColumn colUnspent;
        private System.Windows.Forms.DataGridViewTextBoxColumn colBestVoc;
        private System.Windows.Forms.DataGridViewTextBoxColumn colNote;
        private System.Windows.Forms.Label lblSpent;
        private System.Windows.Forms.Label lblUnspent;
        private System.Windows.Forms.Panel pnlAllocations;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAllocation;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAllocNote;
    }
}
