﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace dqs
{
    public partial class frmBuilds : UserControl
    {
        private bool iUpdating;
        public frmBuilds()
        {
            InitializeComponent();
            UpdateUI();
            LoadData();
		}

		private void LoadData()
		{
			iUpdating = true;
			string StrSelect = "SELECT id, name, spent, unspent, best_voc, note " +
				"FROM builds ORDER BY name";
			DataTable Ttable = Program.DBConnection.SelectTable(StrSelect);
			BindingSource SBind = new BindingSource();
			SBind.DataSource = Ttable;

			dgBuilds.AutoGenerateColumns = false;
			dgBuilds.DataSource = Ttable;
			dgBuilds.DataSource = SBind;
			dgBuilds.Refresh();
			iUpdating = false;
			dgBuilds_SelectionChanged(dgBuilds, null);
			dgBuilds.Focus();
		}

        private void dgBuilds_SelectionChanged(object sender, EventArgs e)
		{
			void LoadText(TextBox textBox, DataGridViewTextBoxColumn ColName)
			{
				textBox.Text = dgBuilds.SelectedRows[0].Cells[ColName.Index].Value.ToString();
			}

			if ((iUpdating == false) && (dgBuilds.SelectedRows.Count > 0))
			{
				this.Cursor = Cursors.WaitCursor;
				LoadText(edtName, colName);
				LoadText(edtBestVoc, colBestVoc);
				LoadText(edtSpent, colSpent);
				LoadText(edtUnspent, colUnspent);
				LoadText(edtNote, colNote);
				//
				string SID = dgBuilds.SelectedRows[0].Cells[colID.Index].Value.ToString();
				string StrSelect = "SELECT seq, build_id, allocation, note " +
					"FROM build_allocations WHERE build_id = " + SID + " " +
					"ORDER BY seq ASC";
				DataTable Ttable = Program.DBConnection.SelectTable(StrSelect);
				BindingSource SBind = new BindingSource();
				SBind.DataSource = Ttable;

				dgAllocations.AutoGenerateColumns = false;
				dgAllocations.DataSource = Ttable;
				dgAllocations.DataSource = SBind;
				dgAllocations.Refresh();
				//
				this.Cursor = Cursors.Default;
				this.Cursor = Cursors.Default;
				this.Cursor = Cursors.Default;
			}
		}

		private void lblAllocations_Click(object sender, EventArgs e)
		{
			pnlAllocations.Visible = !pnlAllocations.Visible;
			if (pnlAllocations.Visible) { lblAllocations.Text = "▲ Allocation"; }
			else { lblAllocations.Text = "▼ Allocation"; }
		}

		public void UpdateUI()
		{
			Color colorPrimary = Properties.Settings.Default.colorPrimary;
			Color colorPriText = Properties.Settings.Default.colorPriText;

			dgBuilds.DefaultCellStyle.SelectionBackColor = colorPrimary;
			dgBuilds.DefaultCellStyle.SelectionForeColor = colorPriText;
			dgAllocations.DefaultCellStyle.SelectionBackColor = colorPrimary;
			dgAllocations.DefaultCellStyle.SelectionForeColor = colorPriText;
		}

		public int GetSplitterDistance()
		{
			return sconBuilds.SplitterDistance;
		}

		public void SetSplitterDistance(int split)
		{
			sconBuilds.SplitterDistance = split;
		}
	}
}
