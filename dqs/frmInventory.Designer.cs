﻿namespace dqs
{
	partial class frmInventory
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            this.sconInvn = new System.Windows.Forms.SplitContainer();
            this.sconFilter = new System.Windows.Forms.SplitContainer();
            this.dgInvn = new System.Windows.Forms.DataGridView();
            this.colID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colRarity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colFilename = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDesc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colFound = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colBuy = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDrop = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colRecipe = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnFilter = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnApply = new System.Windows.Forms.Button();
            this.lblFtCaption = new System.Windows.Forms.Label();
            this.lblFtDesc = new System.Windows.Forms.Label();
            this.edtFtDesc = new System.Windows.Forms.TextBox();
            this.lblFtType = new System.Windows.Forms.Label();
            this.edtFtType = new System.Windows.Forms.ComboBox();
            this.edtFtOrder2 = new System.Windows.Forms.ComboBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblOrder = new System.Windows.Forms.Label();
            this.edtFtOrder = new System.Windows.Forms.ComboBox();
            this.lblFtRecipe = new System.Windows.Forms.Label();
            this.lblFtFound = new System.Windows.Forms.Label();
            this.lblFtIDrop = new System.Windows.Forms.Label();
            this.edtFtRecipe = new System.Windows.Forms.TextBox();
            this.edtFtFound = new System.Windows.Forms.TextBox();
            this.edtFtDrop = new System.Windows.Forms.TextBox();
            this.edtFtRarity = new System.Windows.Forms.ComboBox();
            this.lblFtRarity = new System.Windows.Forms.Label();
            this.lblFtName = new System.Windows.Forms.Label();
            this.edtFtName = new System.Windows.Forms.TextBox();
            this.lblDesc = new System.Windows.Forms.Label();
            this.listDrop = new System.Windows.Forms.ListBox();
            this.listBuy = new System.Windows.Forms.ListBox();
            this.lblDrop = new System.Windows.Forms.Label();
            this.lblBuy = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.edtFound = new System.Windows.Forms.TextBox();
            this.lblFound = new System.Windows.Forms.Label();
            this.lin2 = new System.Windows.Forms.Panel();
            this.edtRarity = new CustomComboBox();
            this.edtType = new CustomComboBox();
            this.lblRarity = new System.Windows.Forms.Label();
            this.lblType = new System.Windows.Forms.Label();
            this.lblRecipe = new System.Windows.Forms.Label();
            this.edtRecipe = new System.Windows.Forms.TextBox();
            this.edtDesc = new System.Windows.Forms.TextBox();
            this.edtName = new System.Windows.Forms.TextBox();
            this.lin1 = new System.Windows.Forms.Panel();
            this.picImage = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.sconInvn)).BeginInit();
            this.sconInvn.Panel1.SuspendLayout();
            this.sconInvn.Panel2.SuspendLayout();
            this.sconInvn.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sconFilter)).BeginInit();
            this.sconFilter.Panel1.SuspendLayout();
            this.sconFilter.Panel2.SuspendLayout();
            this.sconFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgInvn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picImage)).BeginInit();
            this.SuspendLayout();
            // 
            // sconInvn
            // 
            this.sconInvn.BackColor = System.Drawing.Color.White;
            this.sconInvn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.sconInvn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sconInvn.Location = new System.Drawing.Point(0, 0);
            this.sconInvn.Name = "sconInvn";
            // 
            // sconInvn.Panel1
            // 
            this.sconInvn.Panel1.Controls.Add(this.sconFilter);
            this.sconInvn.Panel1MinSize = 310;
            // 
            // sconInvn.Panel2
            // 
            this.sconInvn.Panel2.BackColor = System.Drawing.Color.White;
            this.sconInvn.Panel2.Controls.Add(this.lblDesc);
            this.sconInvn.Panel2.Controls.Add(this.listDrop);
            this.sconInvn.Panel2.Controls.Add(this.listBuy);
            this.sconInvn.Panel2.Controls.Add(this.lblDrop);
            this.sconInvn.Panel2.Controls.Add(this.lblBuy);
            this.sconInvn.Panel2.Controls.Add(this.panel2);
            this.sconInvn.Panel2.Controls.Add(this.edtFound);
            this.sconInvn.Panel2.Controls.Add(this.lblFound);
            this.sconInvn.Panel2.Controls.Add(this.lin2);
            this.sconInvn.Panel2.Controls.Add(this.edtRarity);
            this.sconInvn.Panel2.Controls.Add(this.edtType);
            this.sconInvn.Panel2.Controls.Add(this.lblRarity);
            this.sconInvn.Panel2.Controls.Add(this.lblType);
            this.sconInvn.Panel2.Controls.Add(this.lblRecipe);
            this.sconInvn.Panel2.Controls.Add(this.edtRecipe);
            this.sconInvn.Panel2.Controls.Add(this.edtDesc);
            this.sconInvn.Panel2.Controls.Add(this.edtName);
            this.sconInvn.Panel2.Controls.Add(this.lin1);
            this.sconInvn.Panel2.Controls.Add(this.picImage);
            this.sconInvn.Size = new System.Drawing.Size(1048, 630);
            this.sconInvn.SplitterDistance = 310;
            this.sconInvn.SplitterWidth = 5;
            this.sconInvn.TabIndex = 0;
            this.sconInvn.TabStop = false;
            // 
            // sconFilter
            // 
            this.sconFilter.BackColor = System.Drawing.Color.White;
            this.sconFilter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.sconFilter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sconFilter.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.sconFilter.IsSplitterFixed = true;
            this.sconFilter.Location = new System.Drawing.Point(0, 0);
            this.sconFilter.Name = "sconFilter";
            // 
            // sconFilter.Panel1
            // 
            this.sconFilter.Panel1.BackColor = System.Drawing.Color.White;
            this.sconFilter.Panel1.Controls.Add(this.dgInvn);
            this.sconFilter.Panel1.Controls.Add(this.btnFilter);
            // 
            // sconFilter.Panel2
            // 
            this.sconFilter.Panel2.Controls.Add(this.btnClear);
            this.sconFilter.Panel2.Controls.Add(this.btnApply);
            this.sconFilter.Panel2.Controls.Add(this.lblFtCaption);
            this.sconFilter.Panel2.Controls.Add(this.lblFtDesc);
            this.sconFilter.Panel2.Controls.Add(this.edtFtDesc);
            this.sconFilter.Panel2.Controls.Add(this.lblFtType);
            this.sconFilter.Panel2.Controls.Add(this.edtFtType);
            this.sconFilter.Panel2.Controls.Add(this.edtFtOrder2);
            this.sconFilter.Panel2.Controls.Add(this.btnCancel);
            this.sconFilter.Panel2.Controls.Add(this.lblOrder);
            this.sconFilter.Panel2.Controls.Add(this.edtFtOrder);
            this.sconFilter.Panel2.Controls.Add(this.lblFtRecipe);
            this.sconFilter.Panel2.Controls.Add(this.lblFtFound);
            this.sconFilter.Panel2.Controls.Add(this.lblFtIDrop);
            this.sconFilter.Panel2.Controls.Add(this.edtFtRecipe);
            this.sconFilter.Panel2.Controls.Add(this.edtFtFound);
            this.sconFilter.Panel2.Controls.Add(this.edtFtDrop);
            this.sconFilter.Panel2.Controls.Add(this.edtFtRarity);
            this.sconFilter.Panel2.Controls.Add(this.lblFtRarity);
            this.sconFilter.Panel2.Controls.Add(this.lblFtName);
            this.sconFilter.Panel2.Controls.Add(this.edtFtName);
            this.sconFilter.Panel2MinSize = 210;
            this.sconFilter.Size = new System.Drawing.Size(310, 630);
            this.sconFilter.SplitterDistance = 75;
            this.sconFilter.SplitterWidth = 6;
            this.sconFilter.TabIndex = 2;
            // 
            // dgInvn
            // 
            this.dgInvn.AllowUserToAddRows = false;
            this.dgInvn.AllowUserToDeleteRows = false;
            this.dgInvn.AllowUserToResizeColumns = false;
            this.dgInvn.AllowUserToResizeRows = false;
            this.dgInvn.BackgroundColor = System.Drawing.Color.White;
            this.dgInvn.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgInvn.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle28.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle28.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle28.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle28.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle28.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle28.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgInvn.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle28;
            this.dgInvn.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgInvn.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colID,
            this.colRarity,
            this.colFilename,
            this.colName,
            this.colType,
            this.colDesc,
            this.colFound,
            this.colBuy,
            this.colDrop,
            this.colRecipe});
            this.dgInvn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgInvn.GridColor = System.Drawing.SystemColors.ControlLight;
            this.dgInvn.Location = new System.Drawing.Point(0, 0);
            this.dgInvn.MultiSelect = false;
            this.dgInvn.Name = "dgInvn";
            this.dgInvn.ReadOnly = true;
            dataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle30.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle30.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle30.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle30.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle30.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle30.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgInvn.RowHeadersDefaultCellStyle = dataGridViewCellStyle30;
            this.dgInvn.RowHeadersVisible = false;
            this.dgInvn.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgInvn.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgInvn.ShowEditingIcon = false;
            this.dgInvn.Size = new System.Drawing.Size(73, 597);
            this.dgInvn.StandardTab = true;
            this.dgInvn.TabIndex = 0;
            this.dgInvn.SelectionChanged += new System.EventHandler(this.dgInvn_SelectionChanged);
            // 
            // colID
            // 
            this.colID.DataPropertyName = "id";
            this.colID.HeaderText = "ID";
            this.colID.Name = "colID";
            this.colID.ReadOnly = true;
            this.colID.Visible = false;
            // 
            // colRarity
            // 
            this.colRarity.DataPropertyName = "rarity";
            this.colRarity.HeaderText = "Rarity";
            this.colRarity.Name = "colRarity";
            this.colRarity.ReadOnly = true;
            this.colRarity.Visible = false;
            // 
            // colFilename
            // 
            this.colFilename.DataPropertyName = "filename";
            this.colFilename.HeaderText = "Filename";
            this.colFilename.Name = "colFilename";
            this.colFilename.ReadOnly = true;
            this.colFilename.Visible = false;
            // 
            // colName
            // 
            this.colName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colName.DataPropertyName = "name";
            dataGridViewCellStyle29.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colName.DefaultCellStyle = dataGridViewCellStyle29;
            this.colName.HeaderText = "Name";
            this.colName.Name = "colName";
            this.colName.ReadOnly = true;
            this.colName.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // colType
            // 
            this.colType.DataPropertyName = "type";
            this.colType.HeaderText = "Type";
            this.colType.Name = "colType";
            this.colType.ReadOnly = true;
            this.colType.Visible = false;
            // 
            // colDesc
            // 
            this.colDesc.DataPropertyName = "desc";
            this.colDesc.HeaderText = "Description";
            this.colDesc.Name = "colDesc";
            this.colDesc.ReadOnly = true;
            this.colDesc.Visible = false;
            // 
            // colFound
            // 
            this.colFound.DataPropertyName = "found";
            this.colFound.HeaderText = "Found";
            this.colFound.Name = "colFound";
            this.colFound.ReadOnly = true;
            this.colFound.Visible = false;
            // 
            // colBuy
            // 
            this.colBuy.DataPropertyName = "buy";
            this.colBuy.HeaderText = "Buy";
            this.colBuy.Name = "colBuy";
            this.colBuy.ReadOnly = true;
            this.colBuy.Visible = false;
            // 
            // colDrop
            // 
            this.colDrop.DataPropertyName = "drop";
            this.colDrop.HeaderText = "Drop";
            this.colDrop.Name = "colDrop";
            this.colDrop.ReadOnly = true;
            this.colDrop.Visible = false;
            // 
            // colRecipe
            // 
            this.colRecipe.DataPropertyName = "recipe";
            this.colRecipe.HeaderText = "Recipe";
            this.colRecipe.Name = "colRecipe";
            this.colRecipe.ReadOnly = true;
            this.colRecipe.Visible = false;
            // 
            // btnFilter
            // 
            this.btnFilter.AutoSize = true;
            this.btnFilter.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnFilter.Image = global::dqs.Properties.Resources.filter;
            this.btnFilter.Location = new System.Drawing.Point(0, 597);
            this.btnFilter.Name = "btnFilter";
            this.btnFilter.Size = new System.Drawing.Size(73, 31);
            this.btnFilter.TabIndex = 1;
            this.btnFilter.Text = "Filter";
            this.btnFilter.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFilter.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnFilter.UseVisualStyleBackColor = true;
            this.btnFilter.Click += new System.EventHandler(this.btnFilter_Click);
            // 
            // btnClear
            // 
            this.btnClear.AutoSize = true;
            this.btnClear.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnClear.Image = global::dqs.Properties.Resources.eraser;
            this.btnClear.Location = new System.Drawing.Point(0, 535);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(227, 31);
            this.btnClear.TabIndex = 17;
            this.btnClear.Text = "C&lear";
            this.btnClear.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnClear.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnApply
            // 
            this.btnApply.AutoSize = true;
            this.btnApply.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnApply.Image = global::dqs.Properties.Resources.check_bold;
            this.btnApply.Location = new System.Drawing.Point(0, 566);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(227, 31);
            this.btnApply.TabIndex = 18;
            this.btnApply.Text = "&Apply";
            this.btnApply.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnApply.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnApply.UseVisualStyleBackColor = true;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // lblFtCaption
            // 
            this.lblFtCaption.AutoSize = true;
            this.lblFtCaption.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFtCaption.Location = new System.Drawing.Point(6, 6);
            this.lblFtCaption.Name = "lblFtCaption";
            this.lblFtCaption.Size = new System.Drawing.Size(58, 15);
            this.lblFtCaption.TabIndex = 18;
            this.lblFtCaption.Text = "Filtrer By";
            // 
            // lblFtDesc
            // 
            this.lblFtDesc.AutoSize = true;
            this.lblFtDesc.Location = new System.Drawing.Point(16, 282);
            this.lblFtDesc.Name = "lblFtDesc";
            this.lblFtDesc.Size = new System.Drawing.Size(67, 15);
            this.lblFtDesc.TabIndex = 12;
            this.lblFtDesc.Text = "Description";
            // 
            // edtFtDesc
            // 
            this.edtFtDesc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtFtDesc.Location = new System.Drawing.Point(19, 300);
            this.edtFtDesc.Name = "edtFtDesc";
            this.edtFtDesc.Size = new System.Drawing.Size(185, 23);
            this.edtFtDesc.TabIndex = 13;
            // 
            // lblFtType
            // 
            this.lblFtType.AutoSize = true;
            this.lblFtType.Location = new System.Drawing.Point(120, 82);
            this.lblFtType.Name = "lblFtType";
            this.lblFtType.Size = new System.Drawing.Size(31, 15);
            this.lblFtType.TabIndex = 4;
            this.lblFtType.Text = "Type";
            // 
            // edtFtType
            // 
            this.edtFtType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtFtType.BackColor = System.Drawing.Color.White;
            this.edtFtType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.edtFtType.FormattingEnabled = true;
            this.edtFtType.Location = new System.Drawing.Point(123, 100);
            this.edtFtType.Name = "edtFtType";
            this.edtFtType.Size = new System.Drawing.Size(81, 23);
            this.edtFtType.TabIndex = 5;
            // 
            // edtFtOrder2
            // 
            this.edtFtOrder2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.edtFtOrder2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.edtFtOrder2.FormattingEnabled = true;
            this.edtFtOrder2.Items.AddRange(new object[] {
            "ASC",
            "DESC"});
            this.edtFtOrder2.Location = new System.Drawing.Point(123, 357);
            this.edtFtOrder2.Name = "edtFtOrder2";
            this.edtFtOrder2.Size = new System.Drawing.Size(81, 23);
            this.edtFtOrder2.TabIndex = 16;
            // 
            // btnCancel
            // 
            this.btnCancel.AutoSize = true;
            this.btnCancel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnCancel.Image = global::dqs.Properties.Resources.close_thick;
            this.btnCancel.Location = new System.Drawing.Point(0, 597);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(227, 31);
            this.btnCancel.TabIndex = 19;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lblOrder
            // 
            this.lblOrder.AutoSize = true;
            this.lblOrder.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.lblOrder.Location = new System.Drawing.Point(6, 339);
            this.lblOrder.Name = "lblOrder";
            this.lblOrder.Size = new System.Drawing.Size(57, 15);
            this.lblOrder.TabIndex = 14;
            this.lblOrder.Text = "Order By";
            // 
            // edtFtOrder
            // 
            this.edtFtOrder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtFtOrder.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.edtFtOrder.FormattingEnabled = true;
            this.edtFtOrder.Items.AddRange(new object[] {
            "Name",
            "Type",
            "Rarity"});
            this.edtFtOrder.Location = new System.Drawing.Point(19, 357);
            this.edtFtOrder.Name = "edtFtOrder";
            this.edtFtOrder.Size = new System.Drawing.Size(98, 23);
            this.edtFtOrder.TabIndex = 15;
            // 
            // lblFtRecipe
            // 
            this.lblFtRecipe.AutoSize = true;
            this.lblFtRecipe.Location = new System.Drawing.Point(18, 132);
            this.lblFtRecipe.Name = "lblFtRecipe";
            this.lblFtRecipe.Size = new System.Drawing.Size(42, 15);
            this.lblFtRecipe.TabIndex = 6;
            this.lblFtRecipe.Text = "Recipe";
            // 
            // lblFtFound
            // 
            this.lblFtFound.AutoSize = true;
            this.lblFtFound.Location = new System.Drawing.Point(16, 232);
            this.lblFtFound.Name = "lblFtFound";
            this.lblFtFound.Size = new System.Drawing.Size(54, 15);
            this.lblFtFound.TabIndex = 10;
            this.lblFtFound.Text = "Found in";
            // 
            // lblFtIDrop
            // 
            this.lblFtIDrop.AutoSize = true;
            this.lblFtIDrop.Location = new System.Drawing.Point(18, 182);
            this.lblFtIDrop.Name = "lblFtIDrop";
            this.lblFtIDrop.Size = new System.Drawing.Size(69, 15);
            this.lblFtIDrop.TabIndex = 8;
            this.lblFtIDrop.Text = "Dropped by";
            // 
            // edtFtRecipe
            // 
            this.edtFtRecipe.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtFtRecipe.Location = new System.Drawing.Point(19, 150);
            this.edtFtRecipe.Name = "edtFtRecipe";
            this.edtFtRecipe.Size = new System.Drawing.Size(185, 23);
            this.edtFtRecipe.TabIndex = 7;
            // 
            // edtFtFound
            // 
            this.edtFtFound.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtFtFound.Location = new System.Drawing.Point(19, 250);
            this.edtFtFound.Name = "edtFtFound";
            this.edtFtFound.Size = new System.Drawing.Size(185, 23);
            this.edtFtFound.TabIndex = 11;
            // 
            // edtFtDrop
            // 
            this.edtFtDrop.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtFtDrop.Location = new System.Drawing.Point(19, 200);
            this.edtFtDrop.Name = "edtFtDrop";
            this.edtFtDrop.Size = new System.Drawing.Size(185, 23);
            this.edtFtDrop.TabIndex = 9;
            // 
            // edtFtRarity
            // 
            this.edtFtRarity.BackColor = System.Drawing.Color.White;
            this.edtFtRarity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.edtFtRarity.Enabled = false;
            this.edtFtRarity.FormattingEnabled = true;
            this.edtFtRarity.Items.AddRange(new object[] {
            "All",
            "5",
            "4",
            "3",
            "2",
            "1"});
            this.edtFtRarity.Location = new System.Drawing.Point(19, 100);
            this.edtFtRarity.Name = "edtFtRarity";
            this.edtFtRarity.Size = new System.Drawing.Size(98, 23);
            this.edtFtRarity.TabIndex = 3;
            // 
            // lblFtRarity
            // 
            this.lblFtRarity.AutoSize = true;
            this.lblFtRarity.Location = new System.Drawing.Point(18, 82);
            this.lblFtRarity.Name = "lblFtRarity";
            this.lblFtRarity.Size = new System.Drawing.Size(37, 15);
            this.lblFtRarity.TabIndex = 2;
            this.lblFtRarity.Text = "Rarity";
            // 
            // lblFtName
            // 
            this.lblFtName.AutoSize = true;
            this.lblFtName.Location = new System.Drawing.Point(18, 30);
            this.lblFtName.Name = "lblFtName";
            this.lblFtName.Size = new System.Drawing.Size(39, 15);
            this.lblFtName.TabIndex = 0;
            this.lblFtName.Text = "Name";
            // 
            // edtFtName
            // 
            this.edtFtName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtFtName.Location = new System.Drawing.Point(21, 50);
            this.edtFtName.Name = "edtFtName";
            this.edtFtName.Size = new System.Drawing.Size(185, 23);
            this.edtFtName.TabIndex = 1;
            // 
            // lblDesc
            // 
            this.lblDesc.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblDesc.Location = new System.Drawing.Point(215, 59);
            this.lblDesc.Name = "lblDesc";
            this.lblDesc.Size = new System.Drawing.Size(67, 15);
            this.lblDesc.TabIndex = 30;
            this.lblDesc.Text = "Description";
            // 
            // listDrop
            // 
            this.listDrop.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listDrop.FormattingEnabled = true;
            this.listDrop.ItemHeight = 15;
            this.listDrop.Items.AddRange(new object[] {
            "[dropped_by]"});
            this.listDrop.Location = new System.Drawing.Point(125, 376);
            this.listDrop.Name = "listDrop";
            this.listDrop.Size = new System.Drawing.Size(582, 34);
            this.listDrop.TabIndex = 29;
            this.listDrop.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.list_DrawItem);
            // 
            // listBuy
            // 
            this.listBuy.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listBuy.FormattingEnabled = true;
            this.listBuy.ItemHeight = 15;
            this.listBuy.Items.AddRange(new object[] {
            "[buy_from]"});
            this.listBuy.Location = new System.Drawing.Point(125, 336);
            this.listBuy.Name = "listBuy";
            this.listBuy.Size = new System.Drawing.Size(582, 34);
            this.listBuy.TabIndex = 28;
            this.listBuy.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.list_DrawItem);
            // 
            // lblDrop
            // 
            this.lblDrop.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblDrop.Location = new System.Drawing.Point(12, 378);
            this.lblDrop.Name = "lblDrop";
            this.lblDrop.Size = new System.Drawing.Size(107, 15);
            this.lblDrop.TabIndex = 27;
            this.lblDrop.Text = "▼ Dropped by";
            this.lblDrop.Click += new System.EventHandler(this.lblDrop_Click);
            // 
            // lblBuy
            // 
            this.lblBuy.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblBuy.Location = new System.Drawing.Point(12, 339);
            this.lblBuy.Name = "lblBuy";
            this.lblBuy.Size = new System.Drawing.Size(107, 15);
            this.lblBuy.TabIndex = 25;
            this.lblBuy.Text = "▼ Buy from";
            this.lblBuy.Click += new System.EventHandler(this.lblBuy_Click);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(66)))), ((int)(((byte)(66)))));
            this.panel2.Enabled = false;
            this.panel2.Location = new System.Drawing.Point(5, 327);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(718, 1);
            this.panel2.TabIndex = 14;
            // 
            // edtFound
            // 
            this.edtFound.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtFound.BackColor = System.Drawing.Color.White;
            this.edtFound.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.edtFound.Location = new System.Drawing.Point(125, 266);
            this.edtFound.Multiline = true;
            this.edtFound.Name = "edtFound";
            this.edtFound.ReadOnly = true;
            this.edtFound.Size = new System.Drawing.Size(582, 55);
            this.edtFound.TabIndex = 24;
            this.edtFound.Text = "[found]";
            // 
            // lblFound
            // 
            this.lblFound.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblFound.Location = new System.Drawing.Point(12, 266);
            this.lblFound.Name = "lblFound";
            this.lblFound.Size = new System.Drawing.Size(107, 15);
            this.lblFound.TabIndex = 23;
            this.lblFound.Text = "Found";
            // 
            // lin2
            // 
            this.lin2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lin2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(66)))), ((int)(((byte)(66)))));
            this.lin2.Enabled = false;
            this.lin2.Location = new System.Drawing.Point(3, 224);
            this.lin2.Name = "lin2";
            this.lin2.Size = new System.Drawing.Size(718, 1);
            this.lin2.TabIndex = 13;
            // 
            // edtRarity
            // 
            this.edtRarity.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtRarity.BackColor = System.Drawing.Color.White;
            this.edtRarity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple;
            this.edtRarity.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.edtRarity.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.edtRarity.FormattingEnabled = true;
            this.edtRarity.Items.AddRange(new object[] {
            "All",
            "5",
            "4",
            "3",
            "2",
            "1"});
            this.edtRarity.Location = new System.Drawing.Point(288, 159);
            this.edtRarity.Name = "edtRarity";
            this.edtRarity.ReadOnly = true;
            this.edtRarity.Size = new System.Drawing.Size(419, 25);
            this.edtRarity.TabIndex = 22;
            this.edtRarity.Text = "[rarity]";
            this.edtRarity.Visible = false;
            // 
            // edtType
            // 
            this.edtType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtType.BackColor = System.Drawing.Color.White;
            this.edtType.BorderColor = System.Drawing.Color.Transparent;
            this.edtType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple;
            this.edtType.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.edtType.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.edtType.FormattingEnabled = true;
            this.edtType.Location = new System.Drawing.Point(288, 190);
            this.edtType.Name = "edtType";
            this.edtType.Size = new System.Drawing.Size(419, 28);
            this.edtType.TabIndex = 21;
            this.edtType.Text = "[type]";
            // 
            // lblRarity
            // 
            this.lblRarity.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblRarity.Location = new System.Drawing.Point(215, 167);
            this.lblRarity.Name = "lblRarity";
            this.lblRarity.Size = new System.Drawing.Size(37, 15);
            this.lblRarity.TabIndex = 18;
            this.lblRarity.Text = "Rarity";
            this.lblRarity.Visible = false;
            // 
            // lblType
            // 
            this.lblType.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblType.Location = new System.Drawing.Point(215, 198);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(31, 15);
            this.lblType.TabIndex = 17;
            this.lblType.Text = "Type";
            // 
            // lblRecipe
            // 
            this.lblRecipe.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblRecipe.Location = new System.Drawing.Point(12, 232);
            this.lblRecipe.Name = "lblRecipe";
            this.lblRecipe.Size = new System.Drawing.Size(107, 15);
            this.lblRecipe.TabIndex = 16;
            this.lblRecipe.Text = "Recipe";
            // 
            // edtRecipe
            // 
            this.edtRecipe.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtRecipe.BackColor = System.Drawing.Color.White;
            this.edtRecipe.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.edtRecipe.Location = new System.Drawing.Point(125, 232);
            this.edtRecipe.Multiline = true;
            this.edtRecipe.Name = "edtRecipe";
            this.edtRecipe.ReadOnly = true;
            this.edtRecipe.Size = new System.Drawing.Size(582, 28);
            this.edtRecipe.TabIndex = 15;
            this.edtRecipe.Text = "[recipe]";
            // 
            // edtDesc
            // 
            this.edtDesc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtDesc.BackColor = System.Drawing.Color.White;
            this.edtDesc.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.edtDesc.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.edtDesc.Location = new System.Drawing.Point(288, 54);
            this.edtDesc.Multiline = true;
            this.edtDesc.Name = "edtDesc";
            this.edtDesc.ReadOnly = true;
            this.edtDesc.Size = new System.Drawing.Size(419, 130);
            this.edtDesc.TabIndex = 14;
            this.edtDesc.Text = "[description]";
            // 
            // edtName
            // 
            this.edtName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtName.BackColor = System.Drawing.Color.White;
            this.edtName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.edtName.Font = new System.Drawing.Font("Segoe UI", 20.25F);
            this.edtName.Location = new System.Drawing.Point(5, 3);
            this.edtName.Name = "edtName";
            this.edtName.ReadOnly = true;
            this.edtName.Size = new System.Drawing.Size(720, 36);
            this.edtName.TabIndex = 13;
            this.edtName.Text = "[name]";
            // 
            // lin1
            // 
            this.lin1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lin1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(66)))), ((int)(((byte)(66)))));
            this.lin1.Enabled = false;
            this.lin1.Location = new System.Drawing.Point(3, 44);
            this.lin1.Name = "lin1";
            this.lin1.Size = new System.Drawing.Size(718, 1);
            this.lin1.TabIndex = 12;
            // 
            // picImage
            // 
            this.picImage.BackColor = System.Drawing.Color.White;
            this.picImage.Enabled = false;
            this.picImage.Location = new System.Drawing.Point(3, 51);
            this.picImage.Name = "picImage";
            this.picImage.Size = new System.Drawing.Size(206, 167);
            this.picImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picImage.TabIndex = 2;
            this.picImage.TabStop = false;
            // 
            // frmInventory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.sconInvn);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "frmInventory";
            this.Size = new System.Drawing.Size(1048, 630);
            this.sconInvn.Panel1.ResumeLayout(false);
            this.sconInvn.Panel2.ResumeLayout(false);
            this.sconInvn.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sconInvn)).EndInit();
            this.sconInvn.ResumeLayout(false);
            this.sconFilter.Panel1.ResumeLayout(false);
            this.sconFilter.Panel1.PerformLayout();
            this.sconFilter.Panel2.ResumeLayout(false);
            this.sconFilter.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sconFilter)).EndInit();
            this.sconFilter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgInvn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picImage)).EndInit();
            this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.SplitContainer sconInvn;
		private System.Windows.Forms.SplitContainer sconFilter;
		private System.Windows.Forms.ComboBox edtFtOrder2;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Label lblOrder;
		private System.Windows.Forms.ComboBox edtFtOrder;
		private System.Windows.Forms.Label lblFtRecipe;
		private System.Windows.Forms.Label lblFtFound;
		private System.Windows.Forms.Label lblFtIDrop;
		private System.Windows.Forms.TextBox edtFtRecipe;
		private System.Windows.Forms.TextBox edtFtFound;
		private System.Windows.Forms.TextBox edtFtDrop;
		private System.Windows.Forms.ComboBox edtFtRarity;
		private System.Windows.Forms.Label lblFtRarity;
		private System.Windows.Forms.Label lblFtName;
		private System.Windows.Forms.TextBox edtFtName;
		private System.Windows.Forms.DataGridView dgInvn;
		private System.Windows.Forms.Button btnFilter;
        private System.Windows.Forms.PictureBox picImage;
        private System.Windows.Forms.Panel lin1;
        private System.Windows.Forms.TextBox edtName;
        private System.Windows.Forms.TextBox edtDesc;
        private System.Windows.Forms.TextBox edtRecipe;
        private System.Windows.Forms.Label lblRecipe;
        private System.Windows.Forms.Label lblRarity;
        private System.Windows.Forms.Label lblType;
        private CustomComboBox edtRarity;
        private CustomComboBox edtType;
        private System.Windows.Forms.Panel lin2;
        private System.Windows.Forms.Label lblBuy;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox edtFound;
        private System.Windows.Forms.Label lblFound;
        private System.Windows.Forms.Label lblDrop;
        private System.Windows.Forms.Label lblFtType;
        private System.Windows.Forms.ComboBox edtFtType;
        private System.Windows.Forms.Label lblFtDesc;
        private System.Windows.Forms.TextBox edtFtDesc;
        private System.Windows.Forms.DataGridViewTextBoxColumn colID;
        private System.Windows.Forms.DataGridViewTextBoxColumn colRarity;
        private System.Windows.Forms.DataGridViewTextBoxColumn colFilename;
        private System.Windows.Forms.DataGridViewTextBoxColumn colName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colType;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDesc;
        private System.Windows.Forms.DataGridViewTextBoxColumn colFound;
        private System.Windows.Forms.DataGridViewTextBoxColumn colBuy;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDrop;
        private System.Windows.Forms.DataGridViewTextBoxColumn colRecipe;
        private System.Windows.Forms.ListBox listBox;
        private System.Windows.Forms.ListBox listDrop;
        private System.Windows.Forms.ListBox listBuy;
        private System.Windows.Forms.Label lblFtCaption;
        private System.Windows.Forms.Label lblDesc;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnApply;
    }
}
