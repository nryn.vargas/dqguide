﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Data;
using System.IO;
using System.Windows.Forms;

namespace dqs
{
	public partial class frmInventory : UserControl
	{
		private bool iUpdating;

		public frmInventory()
		{
			InitializeComponent();
			this.sconFilter.Panel1Collapsed = false;
			this.sconFilter.Panel2Collapsed = true;
			this.edtRarity.ReadOnly = false;
			this.edtType.ReadOnly = true;
			this.picImage.Tag = 0;
			this.edtFtOrder.SelectedIndex = 0;
			this.edtFtOrder2.SelectedIndex = 0;
			this.edtFtType.Items.Add("Any/All");
			List<string> list = Program.DBConnection.SelectAllVal(
				"SELECT name FROM item_types");
			foreach (string item in list)
			{
				this.edtType.Items.Add(item);
				this.edtFtType.Items.Add(item);
			}
			this.edtFtType.SelectedIndex = 0;
			this.listBuy.DrawMode = DrawMode.OwnerDrawFixed;
			this.listDrop.DrawMode = DrawMode.OwnerDrawFixed;
			LoadData();
			UpdateUI();
		}

		private void LoadData()
		{
			iUpdating = true;
			string StrWhere = "WHERE (i.name <> '') ";
			string StrSelect = "SELECT i.name, t.name as `type`, " +
				"`description` as `desc`, `found`, buy, `drop`, recipe, filename " +
				"from items i " +
				"inner join item_types t on t.id=i.type_id ";
			if (edtFtName.Text != "")
			{
				StrWhere = StrWhere + "AND (i.name LIKE '%" + edtFtName.Text + "%') ";
			}
			if (edtFtType.SelectedIndex > 0)
			{
				StrWhere = StrWhere + "AND (t.name LIKE '%" + edtFtType.Text + "%') ";
			}
			if (edtFtRecipe.Text != "")
			{
				StrWhere = StrWhere + "AND (recipe LIKE '%" + edtFtRecipe.Text + "%') ";
			}
			if (edtFtFound.Text != "")
			{
				StrWhere = StrWhere + "AND (`found` LIKE '%" + edtFtFound.Text + "%') ";
			}
			if (edtFtDrop.Text != "")
			{
				StrWhere = StrWhere + "AND (`drop` LIKE '%" + edtFtDrop.Text + "%') ";
			}
			if (edtFtDesc.Text != "")
			{
				StrWhere = StrWhere + "AND (`desc` LIKE '%" + edtFtDesc.Text + "%') ";
			}
			StrSelect = StrSelect + StrWhere + "ORDER BY " +
				this.edtFtOrder.Text + " " + this.edtFtOrder2.Text;
			DataTable Ttable = Program.DBConnection.SelectTable(StrSelect);
			BindingSource SBind = new BindingSource();
			SBind.DataSource = Ttable;

			dgInvn.AutoGenerateColumns = false;
			dgInvn.DataSource = Ttable;
			dgInvn.DataSource = SBind;
			dgInvn.Refresh();
			iUpdating = false;
			dgInvn_SelectionChanged(dgInvn, null);
			dgInvn.Focus();
		}

		private void btnFilter_Click(object sender, EventArgs e)
		{
			this.sconFilter.Panel1Collapsed = true;
			this.sconFilter.Panel2Collapsed = false;
		}

		private void dgInvn_SelectionChanged(object sender, EventArgs e)
		{
			void LoadText(TextBox textBox, DataGridViewTextBoxColumn ColName)
			{
				textBox.Text = dgInvn.SelectedRows[0].Cells[ColName.Index].Value.ToString();
			}

			void LoadLines(TextBox textBox, DataGridViewTextBoxColumn ColName)
			{
				string text = "";
				string str = dgInvn.SelectedRows[0].Cells[ColName.Index].Value.ToString();
				string[] strs = str.Split(';');
				foreach (string item in strs)
				{
					text = text + item.Trim() + Environment.NewLine;
				}
				textBox.Text = text.Trim();
			}

			void LoadTexts(ListBox listBox, DataGridViewTextBoxColumn ColName)
			{
				string str = dgInvn.SelectedRows[0].Cells[ColName.Index].Value.ToString();
				string[] strs = str.Split(';');
				listBox.Items.Clear();
				foreach (string item in strs)
				{
					listBox.Items.Add(item.Trim());
				}
			}

			void LoadCombo(ComboBox comboBox, DataGridViewTextBoxColumn ColName)
			{
				comboBox.Text = dgInvn.SelectedRows[0].Cells[ColName.Index].Value.ToString();
			}

			if ((iUpdating == false) && (dgInvn.SelectedRows.Count > 0))
			{
				this.Cursor = Cursors.WaitCursor;
				LoadText(edtName, colName);
				LoadText(edtDesc, colDesc);
				LoadText(edtRecipe, colRecipe);
				LoadLines(edtFound, colFound);
				LoadCombo(edtType, colType);
				LoadTexts(listBuy, colBuy);
				LoadTexts(listDrop, colDrop);
				//
				string sfile = dgInvn.SelectedRows[0].Cells[colFilename.Index].Value.ToString();
				Image img;
				Size newsize;
				img = (Image)Properties.Resources.ResourceManager.GetObject("placeholder");
				newsize = new Size((int)(img.Width), (int)(img.Height));
				if (sfile != "")
				{
					if (File.Exists("items\\" + sfile))
					{
						sfile = "items\\" + sfile;
						try
						{
							picImage.Tag = 1;
							img = Image.FromFile(sfile);
						}
						catch (Exception)
						{
							picImage.Tag = -1;
						}
					}
					if ((img.Height < picImage.Height) || (img.Width < picImage.Width))
					{
						newsize.Width = (newsize.Width * 5);
						newsize.Height = (newsize.Height * 5);
						while ((newsize.Height > picImage.Height) || (newsize.Width > picImage.Width))
						{
							newsize.Height = (int)(0.8 * newsize.Height);
							newsize.Width = (int)(0.8 * newsize.Width);
						}
					}
					else
					{
						newsize = new Size((int)(img.Width * 0.8), (int)(img.Height * 0.8));
						while ((newsize.Height > picImage.Height) || (newsize.Width > picImage.Width))
						{
							newsize.Height = (int)(0.8 * newsize.Height);
							newsize.Width = (int)(0.8 * newsize.Width);
						}
					}
				}
				else
				{
					picImage.Tag = -1;
				}
				Bitmap bmp = new Bitmap(img, newsize);
				picImage.Image = bmp;
				img.Dispose();
				if (picImage.Tag.Equals(-1))
				{
					Program.ChangePicBoxColor(picImage, Properties.Settings.Default.colorPrimary);
				}
				this.Cursor = Cursors.Default;
				this.Cursor = Cursors.Default;
				this.Cursor = Cursors.Default;
			}
		}

		private void btnClear_Click(object sender, EventArgs e)
		{
			edtFtName.Clear();
			edtFtDrop.Clear();
			edtFtDesc.Clear();
			edtFtFound.Clear();
			edtFtRecipe.Clear();
			edtFtRarity.SelectedIndex = 0;
			edtFtType.SelectedIndex = 0;
			edtFtOrder.SelectedIndex = 0;
			edtFtOrder2.SelectedIndex = 0;
		}

		private void btnApply_Click(object sender, EventArgs e)
		{
			LoadData();
			this.sconFilter.Panel1Collapsed = false;
			this.sconFilter.Panel2Collapsed = true;
		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			this.sconFilter.Panel1Collapsed = false;
			this.sconFilter.Panel2Collapsed = true;
		}

		private void ExpandCollapseLists()
		{
			bool expBuy = lblBuy.Text.StartsWith("▲");
			bool expDrp = lblDrop.Text.StartsWith("▲");
			int hmin = 74;
			int havl = sconInvn.Height - listBuy.Location.Y - 4;
			//
			listBuy.Height = 34;
			listDrop.Height = 34;
			if (havl > hmin)
            {
				int hhlf = (havl / 2);
				if (expBuy)
				{
					listBuy.Height = hhlf;
				}
				if (expDrp)
				{
					listDrop.Height = hhlf;
				}
			}
			listDrop.Location = new Point(listBuy.Location.X, listBuy.Location.Y + listBuy.Height + 6);
			lblDrop.Location = new Point(lblDrop.Location.X, listDrop.Location.Y + 2);
			listBuy.Refresh();
			listDrop.Refresh();
		}

		private void lblBuy_Click(object sender, EventArgs e)
		{
			if (lblBuy.Text.StartsWith("▼"))
            {
				lblBuy.Text = "▲ Buy from";
			}
            else
            {
				lblBuy.Text = "▼ Buy from";
			}
			ExpandCollapseLists();
		}

		private void lblDrop_Click(object sender, EventArgs e)
		{
			if (lblDrop.Text.StartsWith("▼"))
			{
				lblDrop.Text = "▲ Dropped by";
			}
			else
			{
				lblDrop.Text = "▼ Dropped by";
			}
			ExpandCollapseLists();
		}

		public void UpdateUI()
		{
			void UpdateListColor(ListBox listBox)
            {

				listBox.BeginUpdate();
				listBox.DataSource = null;
				listBox.EndUpdate();
			}

			Color colorPrimary = Properties.Settings.Default.colorPrimary;
			Color colorPriText = Properties.Settings.Default.colorPriText;

			dgInvn.DefaultCellStyle.SelectionBackColor = colorPrimary;
			dgInvn.DefaultCellStyle.SelectionForeColor = colorPriText;
            if (picImage.Tag.Equals(-1))
            {
				Program.ChangePicBoxColor(picImage, Properties.Settings.Default.colorPrimary);
			}
			UpdateListColor(listBuy);
			UpdateListColor(listDrop);
			Program.ChangeButtonColor(btnFilter, colorPrimary);
			Program.ChangeButtonColor(btnClear, colorPrimary);
			Program.ChangeButtonColor(btnCancel, colorPrimary);
			Program.ChangeButtonColor(btnApply, colorPrimary);
		}

		public int GetSplitterDistance()
		{
			return sconInvn.SplitterDistance;
		}

		public void SetSplitterDistance(int split)
		{
			sconInvn.SplitterDistance = split;
		}

		public void UpdateReadOnly()
        {
			this.edtType.ReadOnly = !this.edtType.ReadOnly;
			this.edtRarity.ReadOnly = !this.edtRarity.ReadOnly;
		}

        private void list_DrawItem(object sender, DrawItemEventArgs e)
		{
			ListBox listBox = (sender as ListBox);
			//
			Color colorFill = Color.White;
			Color colorText = Color.Black;
			string txt = listBox.GetItemText(listBox.Items[e.Index]);
			if ((e.State & DrawItemState.Selected) == DrawItemState.Selected)
			{
				colorFill = Properties.Settings.Default.colorPrimary;
				colorText = Properties.Settings.Default.colorPriText;
			}
			using (var brush = new SolidBrush(colorFill))
				e.Graphics.FillRectangle(brush, e.Bounds);
			TextRenderer.DrawText(e.Graphics, txt, listBox.Font, e.Bounds, colorText,
				TextFormatFlags.VerticalCenter | TextFormatFlags.Left);
		}
    }
}
