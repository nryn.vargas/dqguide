﻿namespace dqs
{
	partial class frmLogin
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmLogin));
            this.edtUsername = new System.Windows.Forms.TextBox();
            this.edtPassword = new System.Windows.Forms.TextBox();
            this.btnLogin = new FocuslessButton();
            this.edtDatabase = new System.Windows.Forms.TextBox();
            this.lineDatabase = new System.Windows.Forms.Panel();
            this.icoDB = new System.Windows.Forms.PictureBox();
            this.lineUsername = new System.Windows.Forms.Panel();
            this.icoUser = new System.Windows.Forms.PictureBox();
            this.linePassword = new System.Windows.Forms.Panel();
            this.btnShow = new FocuslessButton();
            this.icoPassword = new System.Windows.Forms.PictureBox();
            this.lblTitle = new System.Windows.Forms.Label();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.btnClose = new System.Windows.Forms.Button();
            this.lineDatabase.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.icoDB)).BeginInit();
            this.lineUsername.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.icoUser)).BeginInit();
            this.linePassword.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.icoPassword)).BeginInit();
            this.SuspendLayout();
            // 
            // edtUsername
            // 
            this.edtUsername.BackColor = System.Drawing.SystemColors.Control;
            this.edtUsername.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.edtUsername.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.edtUsername.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(44)))), ((int)(((byte)(44)))));
            this.edtUsername.Location = new System.Drawing.Point(38, 6);
            this.edtUsername.Name = "edtUsername";
            this.edtUsername.Size = new System.Drawing.Size(188, 22);
            this.edtUsername.TabIndex = 1;
            this.edtUsername.Text = "neil";
            this.toolTip.SetToolTip(this.edtUsername, "Username");
            this.edtUsername.Enter += new System.EventHandler(this.edt_Enter);
            this.edtUsername.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.edtUsername_KeyPress);
            this.edtUsername.Leave += new System.EventHandler(this.edt_Leave);
            // 
            // edtPassword
            // 
            this.edtPassword.BackColor = System.Drawing.SystemColors.Control;
            this.edtPassword.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.edtPassword.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.edtPassword.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(44)))), ((int)(((byte)(44)))));
            this.edtPassword.Location = new System.Drawing.Point(38, 6);
            this.edtPassword.Name = "edtPassword";
            this.edtPassword.PasswordChar = '*';
            this.edtPassword.Size = new System.Drawing.Size(152, 22);
            this.edtPassword.TabIndex = 2;
            this.edtPassword.Text = "armstrong";
            this.toolTip.SetToolTip(this.edtPassword, "Password");
            this.edtPassword.Enter += new System.EventHandler(this.edt_Enter);
            this.edtPassword.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.edtPassword_KeyPress);
            this.edtPassword.Leave += new System.EventHandler(this.edt_Leave);
            // 
            // btnLogin
            // 
            this.btnLogin.BackColor = System.Drawing.Color.Transparent;
            this.btnLogin.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogin.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogin.ForeColor = System.Drawing.Color.Black;
            this.btnLogin.Location = new System.Drawing.Point(19, 320);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(274, 38);
            this.btnLogin.TabIndex = 3;
            this.btnLogin.Text = "LOG IN";
            this.btnLogin.UseVisualStyleBackColor = false;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            this.btnLogin.Enter += new System.EventHandler(this.btnLogin_MouseEnter);
            this.btnLogin.Leave += new System.EventHandler(this.btnLogin_MouseLeave);
            this.btnLogin.MouseClick += new System.Windows.Forms.MouseEventHandler(this.btnLogin_MouseClick);
            this.btnLogin.MouseEnter += new System.EventHandler(this.btnLogin_MouseEnter);
            this.btnLogin.MouseLeave += new System.EventHandler(this.btnLogin_MouseLeave);
            // 
            // edtDatabase
            // 
            this.edtDatabase.AllowDrop = true;
            this.edtDatabase.BackColor = System.Drawing.SystemColors.Control;
            this.edtDatabase.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.edtDatabase.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.edtDatabase.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(44)))), ((int)(((byte)(44)))));
            this.edtDatabase.Location = new System.Drawing.Point(38, 6);
            this.edtDatabase.Name = "edtDatabase";
            this.edtDatabase.Size = new System.Drawing.Size(188, 22);
            this.edtDatabase.TabIndex = 0;
            this.edtDatabase.Text = "dq";
            this.toolTip.SetToolTip(this.edtDatabase, "Database");
            this.edtDatabase.Enter += new System.EventHandler(this.edt_Enter);
            this.edtDatabase.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.edtDatabase_KeyPress);
            this.edtDatabase.Leave += new System.EventHandler(this.edt_Leave);
            // 
            // lineDatabase
            // 
            this.lineDatabase.BackColor = System.Drawing.Color.Transparent;
            this.lineDatabase.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lineDatabase.Controls.Add(this.icoDB);
            this.lineDatabase.Controls.Add(this.edtDatabase);
            this.lineDatabase.ForeColor = System.Drawing.Color.Black;
            this.lineDatabase.Location = new System.Drawing.Point(41, 101);
            this.lineDatabase.Name = "lineDatabase";
            this.lineDatabase.Size = new System.Drawing.Size(231, 36);
            this.lineDatabase.TabIndex = 10;
            this.toolTip.SetToolTip(this.lineDatabase, "Database");
            // 
            // icoDB
            // 
            this.icoDB.Image = global::dqs.Properties.Resources.database;
            this.icoDB.ImageLocation = "";
            this.icoDB.Location = new System.Drawing.Point(5, 5);
            this.icoDB.Name = "icoDB";
            this.icoDB.Size = new System.Drawing.Size(24, 24);
            this.icoDB.TabIndex = 1;
            this.icoDB.TabStop = false;
            // 
            // lineUsername
            // 
            this.lineUsername.BackColor = System.Drawing.Color.Transparent;
            this.lineUsername.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lineUsername.Controls.Add(this.icoUser);
            this.lineUsername.Controls.Add(this.edtUsername);
            this.lineUsername.Location = new System.Drawing.Point(41, 156);
            this.lineUsername.Name = "lineUsername";
            this.lineUsername.Size = new System.Drawing.Size(231, 36);
            this.lineUsername.TabIndex = 11;
            this.toolTip.SetToolTip(this.lineUsername, "Username");
            // 
            // icoUser
            // 
            this.icoUser.Image = global::dqs.Properties.Resources.account;
            this.icoUser.Location = new System.Drawing.Point(5, 5);
            this.icoUser.Name = "icoUser";
            this.icoUser.Size = new System.Drawing.Size(24, 24);
            this.icoUser.TabIndex = 2;
            this.icoUser.TabStop = false;
            // 
            // linePassword
            // 
            this.linePassword.BackColor = System.Drawing.Color.Transparent;
            this.linePassword.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.linePassword.Controls.Add(this.btnShow);
            this.linePassword.Controls.Add(this.icoPassword);
            this.linePassword.Controls.Add(this.edtPassword);
            this.linePassword.Location = new System.Drawing.Point(41, 214);
            this.linePassword.Name = "linePassword";
            this.linePassword.Size = new System.Drawing.Size(231, 36);
            this.linePassword.TabIndex = 11;
            this.toolTip.SetToolTip(this.linePassword, "Password");
            // 
            // btnShow
            // 
            this.btnShow.FlatAppearance.BorderSize = 0;
            this.btnShow.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver;
            this.btnShow.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnShow.ForeColor = System.Drawing.Color.Black;
            this.btnShow.Image = global::dqs.Properties.Resources.eye;
            this.btnShow.Location = new System.Drawing.Point(194, 3);
            this.btnShow.Name = "btnShow";
            this.btnShow.Size = new System.Drawing.Size(32, 29);
            this.btnShow.TabIndex = 4;
            this.btnShow.TabStop = false;
            this.btnShow.UseVisualStyleBackColor = true;
            this.btnShow.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnShow_MouseDown);
            this.btnShow.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnShow_MouseUp);
            // 
            // icoPassword
            // 
            this.icoPassword.Image = global::dqs.Properties.Resources.key;
            this.icoPassword.Location = new System.Drawing.Point(5, 5);
            this.icoPassword.Name = "icoPassword";
            this.icoPassword.Size = new System.Drawing.Size(24, 24);
            this.icoPassword.TabIndex = 3;
            this.icoPassword.TabStop = false;
            // 
            // lblTitle
            // 
            this.lblTitle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(151)))), ((int)(((byte)(167)))));
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblTitle.Font = new System.Drawing.Font("Segoe UI", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.ForeColor = System.Drawing.Color.White;
            this.lblTitle.Location = new System.Drawing.Point(0, 0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(313, 59);
            this.lblTitle.TabIndex = 14;
            this.lblTitle.Text = "DQ Guide";
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblTitle.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lblTitle_MouseDown);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(151)))), ((int)(((byte)(167)))));
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Red;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.ForeColor = System.Drawing.Color.White;
            this.btnClose.Location = new System.Drawing.Point(285, 0);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(28, 27);
            this.btnClose.TabIndex = 16;
            this.btnClose.TabStop = false;
            this.btnClose.Text = "❌";
            this.btnClose.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            this.btnClose.MouseEnter += new System.EventHandler(this.btnClose_MouseEnter);
            this.btnClose.MouseLeave += new System.EventHandler(this.btnClose_MouseLeave);
            // 
            // frmLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(313, 379);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.lblTitle);
            this.Controls.Add(this.lineUsername);
            this.Controls.Add(this.linePassword);
            this.Controls.Add(this.lineDatabase);
            this.Controls.Add(this.btnLogin);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmLogin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "User Login";
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frmLogin_KeyPress);
            this.lineDatabase.ResumeLayout(false);
            this.lineDatabase.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.icoDB)).EndInit();
            this.lineUsername.ResumeLayout(false);
            this.lineUsername.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.icoUser)).EndInit();
            this.linePassword.ResumeLayout(false);
            this.linePassword.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.icoPassword)).EndInit();
            this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TextBox edtUsername;
		private System.Windows.Forms.TextBox edtPassword;
		private FocuslessButton btnLogin;
		private System.Windows.Forms.TextBox edtDatabase;
		private System.Windows.Forms.Panel lineDatabase;
		private System.Windows.Forms.Panel lineUsername;
		private System.Windows.Forms.Panel linePassword;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.PictureBox icoDB;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.PictureBox icoUser;
        private System.Windows.Forms.PictureBox icoPassword;
        private FocuslessButton btnShow;
    }
}