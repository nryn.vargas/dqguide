﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace dqs
{
	public partial class frmLogin : Form
	{
		int IResult;
		UserInfo uinf;

		public const int WM_NCLBUTTONDOWN = 0xA1;
		public const int HT_CAPTION = 0x2;

		[DllImportAttribute("user32.dll")]
		public static extern int SendMessage(IntPtr hWnd,
						 int Msg, int wParam, int lParam);
		[DllImportAttribute("user32.dll")]
		public static extern bool ReleaseCapture();

		public frmLogin()
		{
			InitializeComponent();
			UpdateUI();
			IResult = -1;
		}

		public bool TryLogin(UserInfo uinf, bool showInTaskbar = false)
		{
			try
			{
				this.ShowInTaskbar = showInTaskbar;
				this.uinf = uinf;
				this.ShowDialog();
				return (this.IResult == 200);
			}
			finally
			{
				this.Close();
			}
		}

		private int AuthenticateUser(string dbName)
		{
			int result = 500;
			string uID = this.uinf.username;
			string password = this.uinf.password;
			if (dbName == "")
			{
				dbName = "sample";
				this.edtDatabase.Text = dbName;
			}
			if (Program.DBConnection != null)
			{
				Program.DBConnection.DBDispose();
			}
			Program.DBConnection = new DBConnect("", dbName);
			if (!Program.DBConnection.IsOpen)
			{
				Program.DBConnection.OpenConnection(false);
			}
			if (Program.DBConnection.IsOpen)
			{
				result = 400;
				var res = Program.DBConnection.SelectCmd("SELECT password, is_admin FROM users WHERE username='" + uID + "'");
				if (res.Count <= 0)
                {
					return result;
                }
				if ((uID != "") && (password == res[0]))
				{
					result = 200;
					this.uinf.isAdmin = Convert.ToBoolean(res[1]);
				}

			}
			else
			{
				result = 500; // can't connect to the database
			}
			return result;
		}

		private void frmLogin_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (e.KeyChar == 0x001b)
			{
				e.Handled = true;
				this.Close();
			}
		}

		private void edtUsername_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (e.KeyChar == 0x000d)
			{
				e.Handled = true;
				this.edtPassword.Focus();
			}
		}

		private void edtPassword_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (e.KeyChar == 0x000d)
			{
				e.Handled = true;
				this.btnLogin_Click(null, null);
			}
		}

		private void edtDatabase_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (e.KeyChar == 0x000d)
			{
				e.Handled = true;
				this.edtUsername.Focus();
			}
		}

		private void edt_Enter(object sender, EventArgs e)
		{
			(sender as TextBox).SelectAll();
		}

        private void edt_Leave(object sender, EventArgs e)
		{
			Panel pnl = null;
			if ((sender as TextBox).Name == edtDatabase.Name)
			{
				pnl = lineDatabase;
			}
			else if ((sender as TextBox).Name == edtUsername.Name)
			{
				pnl = lineUsername;
			}
			else if ((sender as TextBox).Name == edtPassword.Name)
			{
				pnl = linePassword;
			}
			if (pnl != null)
			{
			}
		}
		private void btnLogin_Click(object sender, EventArgs e)
		{
			this.uinf.database = this.edtDatabase.Text;
			this.uinf.username = this.edtUsername.Text;
			this.uinf.password = this.edtPassword.Text;
			//
			IResult = this.AuthenticateUser(this.edtDatabase.Text);
			if (IResult == 400)
			{
				MessageBox.Show(
					"Invalid Login credentials",
					"Error",
					MessageBoxButtons.OKCancel,
					MessageBoxIcon.Stop);
			}
			if (IResult == 500)
			{
				MessageBox.Show(
					"Can not connect to the database.",
					"Server Error",
					MessageBoxButtons.OKCancel,
					MessageBoxIcon.Stop);
			}
			if (IResult == 200)
            {
				this.Close();
            }
		}

        private void btnLogin_MouseEnter(object sender, EventArgs e)
		{
			this.btnLogin.FlatAppearance.BorderSize = 0;
			this.btnLogin.BackColor = this.lblTitle.BackColor;
			this.btnLogin.ForeColor = this.lblTitle.ForeColor;
		}

        private void btnLogin_MouseClick(object sender, MouseEventArgs e)
		{
			this.btnLogin.FlatAppearance.BorderSize = 0;
			this.btnLogin.BackColor = this.lblTitle.BackColor;
			this.btnLogin.ForeColor = Color.White;
		}

        private void btnLogin_MouseLeave(object sender, EventArgs e)
		{
			this.btnLogin.FlatAppearance.BorderSize = 2;
			this.btnLogin.BackColor = Color.Transparent;
			this.btnLogin.ForeColor = Color.Black;
		}

        private void lblTitle_MouseDown(object sender, MouseEventArgs e)
        {
			if (e.Button == MouseButtons.Left)
			{
				ReleaseCapture();
				SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
			}
		}

        private void btnClose_Click(object sender, EventArgs e)
        {
			this.Close();
        }

        private void btnClose_MouseEnter(object sender, EventArgs e)
        {
			this.btnClose.BackColor = Color.Red;
        }

        private void btnClose_MouseLeave(object sender, EventArgs e)
        {
			this.btnClose.BackColor = lblTitle.BackColor;
        }

        private void btnShow_MouseDown(object sender, MouseEventArgs e)
        {
			edtPassword.PasswordChar = (char)0;
        }

        private void btnShow_MouseUp(object sender, MouseEventArgs e)
		{
			edtPassword.PasswordChar = '*';
		}

		private void UpdateUI()
		{
			Color colorPrimary = Properties.Settings.Default.colorPrimary;
			this.lblTitle.BackColor = colorPrimary;
			this.lblTitle.ForeColor = Properties.Settings.Default.colorPriText;
			this.btnClose.BackColor = this.lblTitle.BackColor;
			this.btnLogin.FlatAppearance.BorderColor = Properties.Settings.Default.colorPrimary;
			Program.ChangePicBoxColor(icoDB, colorPrimary);
			Program.ChangePicBoxColor(icoUser, colorPrimary);
			Program.ChangePicBoxColor(icoPassword, colorPrimary);
			Program.ChangeButtonColor(btnShow, colorPrimary);
		}
    }
}
