﻿namespace dqs
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.mnuClose = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuPopUp = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.pnlTitle = new System.Windows.Forms.Panel();
            this.lblTitle = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnMin = new System.Windows.Forms.Button();
            this.btnMax = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btnEquipment = new FocuslessButton();
            this.btnAlchemy = new FocuslessButton();
            this.btnBeasts = new FocuslessButton();
            this.btnMaps = new FocuslessButton();
            this.btnBuilds = new FocuslessButton();
            this.btnVocations = new FocuslessButton();
            this.btnInventory = new FocuslessButton();
            this.btnSkills = new FocuslessButton();
            this.btnQuests = new FocuslessButton();
            this.btnSpells = new FocuslessButton();
            this.btnOptions = new FocuslessButton();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.mnuFile = new System.Windows.Forms.ToolStripDropDownButton();
            this.mnuNewUser = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuExit = new System.Windows.Forms.ToolStripMenuItem();
            this.lblUser = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblDB = new System.Windows.Forms.ToolStripStatusLabel();
            this.nb = new TablessControl();
            this.mnuPopUp.SuspendLayout();
            this.pnlTitle.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // mnuClose
            // 
            this.mnuClose.Name = "mnuClose";
            this.mnuClose.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.W)));
            this.mnuClose.Size = new System.Drawing.Size(180, 22);
            this.mnuClose.Text = "Close Tab";
            this.mnuClose.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.mnuClose.Click += new System.EventHandler(this.mnuClose_Click);
            // 
            // mnuPopUp
            // 
            this.mnuPopUp.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuClose});
            this.mnuPopUp.Name = "mnuPopUp";
            this.mnuPopUp.Size = new System.Drawing.Size(181, 48);
            // 
            // pnlTitle
            // 
            this.pnlTitle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(151)))), ((int)(((byte)(167)))));
            this.pnlTitle.Controls.Add(this.lblTitle);
            this.pnlTitle.Controls.Add(this.btnClose);
            this.pnlTitle.Controls.Add(this.btnMin);
            this.pnlTitle.Controls.Add(this.btnMax);
            this.pnlTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTitle.Location = new System.Drawing.Point(0, 0);
            this.pnlTitle.Name = "pnlTitle";
            this.pnlTitle.Size = new System.Drawing.Size(1229, 62);
            this.pnlTitle.TabIndex = 2;
            this.pnlTitle.DoubleClick += new System.EventHandler(this.pnlTitle_DoubleClick);
            this.pnlTitle.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlTitle_MouseDown);
            // 
            // lblTitle
            // 
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblTitle.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.ForeColor = System.Drawing.Color.White;
            this.lblTitle.Location = new System.Drawing.Point(0, 27);
            this.lblTitle.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(1229, 35);
            this.lblTitle.TabIndex = 4;
            this.lblTitle.Text = "Dragon Quest IX Guide";
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblTitle.DoubleClick += new System.EventHandler(this.pnlTitle_DoubleClick);
            this.lblTitle.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlTitle_MouseDown);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Red;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.ForeColor = System.Drawing.Color.White;
            this.btnClose.Location = new System.Drawing.Point(1201, 0);
            this.btnClose.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(28, 27);
            this.btnClose.TabIndex = 3;
            this.btnClose.TabStop = false;
            this.btnClose.Text = "❌";
            this.btnClose.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnMin
            // 
            this.btnMin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMin.FlatAppearance.BorderSize = 0;
            this.btnMin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMin.ForeColor = System.Drawing.Color.White;
            this.btnMin.Location = new System.Drawing.Point(1145, 0);
            this.btnMin.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.btnMin.Name = "btnMin";
            this.btnMin.Size = new System.Drawing.Size(28, 27);
            this.btnMin.TabIndex = 2;
            this.btnMin.TabStop = false;
            this.btnMin.Text = "🗕";
            this.btnMin.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnMin.UseVisualStyleBackColor = true;
            this.btnMin.Click += new System.EventHandler(this.btnMin_Click);
            // 
            // btnMax
            // 
            this.btnMax.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMax.FlatAppearance.BorderSize = 0;
            this.btnMax.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMax.ForeColor = System.Drawing.Color.White;
            this.btnMax.Location = new System.Drawing.Point(1173, 0);
            this.btnMax.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.btnMax.Name = "btnMax";
            this.btnMax.Size = new System.Drawing.Size(28, 27);
            this.btnMax.TabIndex = 1;
            this.btnMax.TabStop = false;
            this.btnMax.Text = "🗖";
            this.btnMax.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnMax.UseVisualStyleBackColor = true;
            this.btnMax.Click += new System.EventHandler(this.btnMax_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 11;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 164F));
            this.tableLayoutPanel1.Controls.Add(this.btnEquipment, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnAlchemy, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnBeasts, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnMaps, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnBuilds, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnVocations, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnInventory, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnSkills, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnQuests, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnSpells, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnOptions, 10, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 62);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 37F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1224, 37);
            this.tableLayoutPanel1.TabIndex = 4;
            // 
            // btnEquipment
            // 
            this.btnEquipment.FlatAppearance.BorderSize = 0;
            this.btnEquipment.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEquipment.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEquipment.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(151)))), ((int)(((byte)(167)))));
            this.btnEquipment.Image = global::dqs.Properties.Resources.equip;
            this.btnEquipment.Location = new System.Drawing.Point(639, 3);
            this.btnEquipment.Name = "btnEquipment";
            this.btnEquipment.Size = new System.Drawing.Size(100, 28);
            this.btnEquipment.TabIndex = 6;
            this.btnEquipment.Tag = "6";
            this.btnEquipment.Text = " Equipment";
            this.btnEquipment.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip.SetToolTip(this.btnEquipment, "Equipment");
            this.btnEquipment.UseVisualStyleBackColor = false;
            this.btnEquipment.Click += new System.EventHandler(this.mnuItem_Click);
            this.btnEquipment.Enter += new System.EventHandler(this.mnuItem_Enter);
            this.btnEquipment.Leave += new System.EventHandler(this.mnuItem_Leave);
            this.btnEquipment.MouseEnter += new System.EventHandler(this.mnuItem_MouseEnter);
            this.btnEquipment.MouseLeave += new System.EventHandler(this.mnuItem_MouseLeave);
            // 
            // btnAlchemy
            // 
            this.btnAlchemy.FlatAppearance.BorderSize = 0;
            this.btnAlchemy.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAlchemy.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAlchemy.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(151)))), ((int)(((byte)(167)))));
            this.btnAlchemy.Image = global::dqs.Properties.Resources.alchemy;
            this.btnAlchemy.Location = new System.Drawing.Point(533, 3);
            this.btnAlchemy.Name = "btnAlchemy";
            this.btnAlchemy.Size = new System.Drawing.Size(100, 28);
            this.btnAlchemy.TabIndex = 5;
            this.btnAlchemy.Tag = "5";
            this.btnAlchemy.Text = " Alchemy";
            this.btnAlchemy.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip.SetToolTip(this.btnAlchemy, "Alchemy");
            this.btnAlchemy.UseVisualStyleBackColor = false;
            this.btnAlchemy.Click += new System.EventHandler(this.mnuItem_Click);
            this.btnAlchemy.Enter += new System.EventHandler(this.mnuItem_Enter);
            this.btnAlchemy.Leave += new System.EventHandler(this.mnuItem_Leave);
            this.btnAlchemy.MouseEnter += new System.EventHandler(this.mnuItem_MouseEnter);
            this.btnAlchemy.MouseLeave += new System.EventHandler(this.mnuItem_MouseLeave);
            // 
            // btnBeasts
            // 
            this.btnBeasts.FlatAppearance.BorderSize = 0;
            this.btnBeasts.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBeasts.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBeasts.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(151)))), ((int)(((byte)(167)))));
            this.btnBeasts.Image = global::dqs.Properties.Resources.beasts;
            this.btnBeasts.Location = new System.Drawing.Point(745, 3);
            this.btnBeasts.Name = "btnBeasts";
            this.btnBeasts.Size = new System.Drawing.Size(100, 28);
            this.btnBeasts.TabIndex = 7;
            this.btnBeasts.Tag = "7";
            this.btnBeasts.Text = " Beasts";
            this.btnBeasts.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip.SetToolTip(this.btnBeasts, "Beasts");
            this.btnBeasts.UseVisualStyleBackColor = false;
            this.btnBeasts.Click += new System.EventHandler(this.mnuItem_Click);
            this.btnBeasts.Enter += new System.EventHandler(this.mnuItem_Enter);
            this.btnBeasts.Leave += new System.EventHandler(this.mnuItem_Leave);
            this.btnBeasts.MouseEnter += new System.EventHandler(this.mnuItem_MouseEnter);
            this.btnBeasts.MouseLeave += new System.EventHandler(this.mnuItem_MouseLeave);
            // 
            // btnMaps
            // 
            this.btnMaps.FlatAppearance.BorderSize = 0;
            this.btnMaps.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMaps.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMaps.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(151)))), ((int)(((byte)(167)))));
            this.btnMaps.Image = global::dqs.Properties.Resources.map;
            this.btnMaps.Location = new System.Drawing.Point(957, 3);
            this.btnMaps.Name = "btnMaps";
            this.btnMaps.Size = new System.Drawing.Size(100, 28);
            this.btnMaps.TabIndex = 9;
            this.btnMaps.Tag = "9";
            this.btnMaps.Text = " Maps";
            this.btnMaps.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip.SetToolTip(this.btnMaps, "Maps");
            this.btnMaps.UseVisualStyleBackColor = false;
            this.btnMaps.Click += new System.EventHandler(this.mnuItem_Click);
            this.btnMaps.Enter += new System.EventHandler(this.mnuItem_Enter);
            this.btnMaps.Leave += new System.EventHandler(this.mnuItem_Leave);
            this.btnMaps.MouseEnter += new System.EventHandler(this.mnuItem_MouseEnter);
            this.btnMaps.MouseLeave += new System.EventHandler(this.mnuItem_MouseLeave);
            // 
            // btnBuilds
            // 
            this.btnBuilds.FlatAppearance.BorderSize = 0;
            this.btnBuilds.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBuilds.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBuilds.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(151)))), ((int)(((byte)(167)))));
            this.btnBuilds.Image = global::dqs.Properties.Resources.build;
            this.btnBuilds.Location = new System.Drawing.Point(851, 3);
            this.btnBuilds.Name = "btnBuilds";
            this.btnBuilds.Size = new System.Drawing.Size(100, 28);
            this.btnBuilds.TabIndex = 8;
            this.btnBuilds.Tag = "8";
            this.btnBuilds.Text = " Builds";
            this.btnBuilds.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip.SetToolTip(this.btnBuilds, "Builds");
            this.btnBuilds.UseVisualStyleBackColor = false;
            this.btnBuilds.Click += new System.EventHandler(this.mnuItem_Click);
            this.btnBuilds.Enter += new System.EventHandler(this.mnuItem_Enter);
            this.btnBuilds.Leave += new System.EventHandler(this.mnuItem_Leave);
            this.btnBuilds.MouseEnter += new System.EventHandler(this.mnuItem_MouseEnter);
            this.btnBuilds.MouseLeave += new System.EventHandler(this.mnuItem_MouseLeave);
            // 
            // btnVocations
            // 
            this.btnVocations.FlatAppearance.BorderSize = 0;
            this.btnVocations.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVocations.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVocations.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(151)))), ((int)(((byte)(167)))));
            this.btnVocations.Image = global::dqs.Properties.Resources.vocations;
            this.btnVocations.Location = new System.Drawing.Point(109, 3);
            this.btnVocations.Name = "btnVocations";
            this.btnVocations.Size = new System.Drawing.Size(100, 28);
            this.btnVocations.TabIndex = 1;
            this.btnVocations.Tag = "1";
            this.btnVocations.Text = " Vocations";
            this.btnVocations.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip.SetToolTip(this.btnVocations, "Vocations");
            this.btnVocations.UseVisualStyleBackColor = false;
            this.btnVocations.Click += new System.EventHandler(this.mnuItem_Click);
            this.btnVocations.Enter += new System.EventHandler(this.mnuItem_Enter);
            this.btnVocations.Leave += new System.EventHandler(this.mnuItem_Leave);
            this.btnVocations.MouseEnter += new System.EventHandler(this.mnuItem_MouseEnter);
            this.btnVocations.MouseLeave += new System.EventHandler(this.mnuItem_MouseLeave);
            // 
            // btnInventory
            // 
            this.btnInventory.FlatAppearance.BorderSize = 0;
            this.btnInventory.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnInventory.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInventory.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(151)))), ((int)(((byte)(167)))));
            this.btnInventory.Image = global::dqs.Properties.Resources.inventory;
            this.btnInventory.Location = new System.Drawing.Point(3, 3);
            this.btnInventory.Name = "btnInventory";
            this.btnInventory.Size = new System.Drawing.Size(100, 28);
            this.btnInventory.TabIndex = 0;
            this.btnInventory.Tag = "0";
            this.btnInventory.Text = " Inventory";
            this.btnInventory.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip.SetToolTip(this.btnInventory, "Inventory");
            this.btnInventory.UseVisualStyleBackColor = false;
            this.btnInventory.Click += new System.EventHandler(this.mnuItem_Click);
            this.btnInventory.Enter += new System.EventHandler(this.mnuItem_Enter);
            this.btnInventory.Leave += new System.EventHandler(this.mnuItem_Leave);
            this.btnInventory.MouseEnter += new System.EventHandler(this.mnuItem_MouseEnter);
            this.btnInventory.MouseLeave += new System.EventHandler(this.mnuItem_MouseLeave);
            // 
            // btnSkills
            // 
            this.btnSkills.FlatAppearance.BorderSize = 0;
            this.btnSkills.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSkills.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSkills.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(151)))), ((int)(((byte)(167)))));
            this.btnSkills.Image = global::dqs.Properties.Resources.skills;
            this.btnSkills.Location = new System.Drawing.Point(215, 3);
            this.btnSkills.Name = "btnSkills";
            this.btnSkills.Size = new System.Drawing.Size(100, 28);
            this.btnSkills.TabIndex = 2;
            this.btnSkills.Tag = "2";
            this.btnSkills.Text = " Skills";
            this.btnSkills.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip.SetToolTip(this.btnSkills, "Skills");
            this.btnSkills.UseVisualStyleBackColor = false;
            this.btnSkills.Click += new System.EventHandler(this.mnuItem_Click);
            this.btnSkills.Enter += new System.EventHandler(this.mnuItem_Enter);
            this.btnSkills.Leave += new System.EventHandler(this.mnuItem_Leave);
            this.btnSkills.MouseEnter += new System.EventHandler(this.mnuItem_MouseEnter);
            this.btnSkills.MouseLeave += new System.EventHandler(this.mnuItem_MouseLeave);
            // 
            // btnQuests
            // 
            this.btnQuests.FlatAppearance.BorderSize = 0;
            this.btnQuests.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnQuests.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnQuests.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(151)))), ((int)(((byte)(167)))));
            this.btnQuests.Image = global::dqs.Properties.Resources.quests;
            this.btnQuests.Location = new System.Drawing.Point(427, 3);
            this.btnQuests.Name = "btnQuests";
            this.btnQuests.Size = new System.Drawing.Size(100, 28);
            this.btnQuests.TabIndex = 4;
            this.btnQuests.Tag = "4";
            this.btnQuests.Text = " Quests";
            this.btnQuests.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip.SetToolTip(this.btnQuests, "Quests");
            this.btnQuests.UseVisualStyleBackColor = false;
            this.btnQuests.Click += new System.EventHandler(this.mnuItem_Click);
            this.btnQuests.Enter += new System.EventHandler(this.mnuItem_Enter);
            this.btnQuests.Leave += new System.EventHandler(this.mnuItem_Leave);
            this.btnQuests.MouseEnter += new System.EventHandler(this.mnuItem_MouseEnter);
            this.btnQuests.MouseLeave += new System.EventHandler(this.mnuItem_MouseLeave);
            // 
            // btnSpells
            // 
            this.btnSpells.FlatAppearance.BorderSize = 0;
            this.btnSpells.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSpells.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSpells.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(151)))), ((int)(((byte)(167)))));
            this.btnSpells.Image = global::dqs.Properties.Resources.spells;
            this.btnSpells.Location = new System.Drawing.Point(321, 3);
            this.btnSpells.Name = "btnSpells";
            this.btnSpells.Size = new System.Drawing.Size(100, 28);
            this.btnSpells.TabIndex = 3;
            this.btnSpells.Tag = "3";
            this.btnSpells.Text = " Spells";
            this.btnSpells.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip.SetToolTip(this.btnSpells, "Spells");
            this.btnSpells.UseVisualStyleBackColor = false;
            this.btnSpells.Click += new System.EventHandler(this.mnuItem_Click);
            this.btnSpells.Enter += new System.EventHandler(this.mnuItem_Enter);
            this.btnSpells.Leave += new System.EventHandler(this.mnuItem_Leave);
            this.btnSpells.MouseEnter += new System.EventHandler(this.mnuItem_MouseEnter);
            this.btnSpells.MouseLeave += new System.EventHandler(this.mnuItem_MouseLeave);
            // 
            // btnOptions
            // 
            this.btnOptions.FlatAppearance.BorderSize = 0;
            this.btnOptions.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOptions.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOptions.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(151)))), ((int)(((byte)(167)))));
            this.btnOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnOptions.Image")));
            this.btnOptions.Location = new System.Drawing.Point(1063, 3);
            this.btnOptions.Name = "btnOptions";
            this.btnOptions.Size = new System.Drawing.Size(97, 28);
            this.btnOptions.TabIndex = 10;
            this.btnOptions.Tag = "9";
            this.btnOptions.Text = "Options";
            this.btnOptions.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip.SetToolTip(this.btnOptions, "Maps");
            this.btnOptions.UseVisualStyleBackColor = false;
            this.btnOptions.Click += new System.EventHandler(this.btnOptions_Click);
            this.btnOptions.Enter += new System.EventHandler(this.mnuItem_Enter);
            this.btnOptions.Leave += new System.EventHandler(this.mnuItem_Leave);
            this.btnOptions.MouseEnter += new System.EventHandler(this.mnuItem_MouseEnter);
            this.btnOptions.MouseLeave += new System.EventHandler(this.mnuItem_MouseLeave);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuFile,
            this.lblUser,
            this.lblDB});
            this.statusStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.statusStrip1.Location = new System.Drawing.Point(0, 594);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.statusStrip1.Size = new System.Drawing.Size(1229, 22);
            this.statusStrip1.SizingGrip = false;
            this.statusStrip1.TabIndex = 0;
            this.statusStrip1.TabStop = true;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // mnuFile
            // 
            this.mnuFile.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.mnuFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuNewUser,
            this.toolStripSeparator1,
            this.mnuExit});
            this.mnuFile.Image = global::dqs.Properties.Resources.burger;
            this.mnuFile.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.mnuFile.Size = new System.Drawing.Size(29, 20);
            this.mnuFile.Text = "mnuFile";
            // 
            // mnuNewUser
            // 
            this.mnuNewUser.Name = "mnuNewUser";
            this.mnuNewUser.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.U)));
            this.mnuNewUser.Size = new System.Drawing.Size(195, 22);
            this.mnuNewUser.Text = "Login New User";
            this.mnuNewUser.Click += new System.EventHandler(this.mnuNewUser_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(192, 6);
            // 
            // mnuExit
            // 
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.X)));
            this.mnuExit.Size = new System.Drawing.Size(195, 22);
            this.mnuExit.Text = "E&xit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // lblUser
            // 
            this.lblUser.AutoSize = false;
            this.lblUser.BackColor = System.Drawing.Color.Transparent;
            this.lblUser.Name = "lblUser";
            this.lblUser.Size = new System.Drawing.Size(150, 17);
            this.lblUser.Spring = true;
            this.lblUser.Text = "[ username ]";
            // 
            // lblDB
            // 
            this.lblDB.AutoSize = false;
            this.lblDB.BackColor = System.Drawing.Color.Transparent;
            this.lblDB.Name = "lblDB";
            this.lblDB.Size = new System.Drawing.Size(100, 17);
            this.lblDB.Spring = true;
            this.lblDB.Text = "[ database ]";
            // 
            // nb
            // 
            this.nb.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.nb.Location = new System.Drawing.Point(2, 99);
            this.nb.Name = "nb";
            this.nb.SelectedIndex = 0;
            this.nb.Size = new System.Drawing.Size(1226, 493);
            this.nb.TabIndex = 6;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1229, 616);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.nb);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.pnlTitle);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MinimumSize = new System.Drawing.Size(600, 500);
            this.Name = "frmMain";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Dragon Quest IX Guide";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmMain_KeyDown);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.frmMain_MouseDown);
            this.Resize += new System.EventHandler(this.frmMain_Resize);
            this.mnuPopUp.ResumeLayout(false);
            this.pnlTitle.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ToolStripMenuItem mnuClose;
        private System.Windows.Forms.ContextMenuStrip mnuPopUp;
        private System.Windows.Forms.Panel pnlTitle;
        private System.Windows.Forms.Button btnMax;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnMin;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.ToolTip toolTip;
        private TablessControl nb;
        private FocuslessButton btnSkills;
        private FocuslessButton btnQuests;
        private FocuslessButton btnSpells;
        private FocuslessButton btnInventory;
        private FocuslessButton btnEquipment;
        private FocuslessButton btnAlchemy;
        private FocuslessButton btnBeasts;
        private FocuslessButton btnMaps;
        private FocuslessButton btnBuilds;
        private FocuslessButton btnVocations;
        private FocuslessButton btnOptions;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel lblUser;
        private System.Windows.Forms.ToolStripStatusLabel lblDB;
        private System.Windows.Forms.ToolStripDropDownButton mnuFile;
        private System.Windows.Forms.ToolStripMenuItem mnuNewUser;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem mnuExit;
    }
}

