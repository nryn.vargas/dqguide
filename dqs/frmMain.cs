﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace dqs
{
	public partial class frmMain : Form
	{
		const int iInv = 100;
		const int iVoc = 200;
		const int iSkl = 300;
		const int iSpl = 400;
		const int iEqp = 500;
		const int iAlc = 600;
		const int iBld = 700;
		const int iBts = 800;
		const int iMap = 900;
		const int iQst = 1000;
		/*
		const string sInv = "Inventory";
		const string sVoc = "Vocations";
		const string sSki = "Skills";
		const string sSpl = "Spells";
		const string sEqp = "Equipment";
		const string sAlc = "Alchemy";
		const string sBld = "Builds";
		const string sBts = "Beasts";
		const string sMap = "Maps";
		const string sQst = "Quests";
		*/
		int iWidth;
		int iHeight;
		int iSplit;

		public const int WM_NCLBUTTONDOWN = 0xA1;
		public const int HT_CAPTION = 0x2;

		private frmInventory tabInvn;
		private frmVocations tabVoc;
		//private frmEquipment tabEqp;
		//private frmAlchemy tabAlc;
		private frmBeasts tabBts;
		private frmQuests tabQst;
		private frmSkills tabSkl;
		private frmSpells tabSpl;
		private frmBuilds tabBld;
		private frmMaps tabMap;

		[DllImportAttribute("user32.dll")]
		public static extern int SendMessage(IntPtr hWnd,
						 int Msg, int wParam, int lParam);
		[DllImportAttribute("user32.dll")]
		public static extern bool ReleaseCapture();

		public frmMain()
		{
			InitializeComponent();
			iSplit = 310;
			iWidth = this.Width;
			iHeight = this.Height;
			this.FormBorderStyle = FormBorderStyle.None;
			this.nb.SelectedIndex = -1;
			UpdateUI();
		}

		private void MainForm_Load(object sender, EventArgs e)
		{
			UserInfo user = new UserInfo();
			frmLogin LoginForm = new frmLogin();
			if (LoginForm.TryLogin(user, true))
			{
				this.btnInventory.Tag = iInv;
				this.btnVocations.Tag = iVoc;
				this.btnSkills.Tag = iSkl;
				this.btnSpells.Tag = iSpl;
				this.btnEquipment.Tag = iEqp;
				this.btnAlchemy.Tag = iAlc;
				this.btnBuilds.Tag = iBld;
				this.btnBeasts.Tag = iBts;
				this.btnMaps.Tag = iMap;
				this.btnQuests.Tag = iQst;
				this.lblDB.Text = user.database;
				this.lblUser.Text = user.username;
				this.btnMin.Focus();
			}
			else
			{
				this.Close();
			}
			nb.SelectedIndex = -1;
		}

		protected override void WndProc(ref Message m)
		{
			const int RESIZE_HANDLE_SIZE = 10;

			switch (m.Msg)
			{
				case 0x0084/*NCHITTEST*/ :
					base.WndProc(ref m);

					if ((int)m.Result == 0x01/*HTCLIENT*/)
					{
						Point screenPoint = new Point(m.LParam.ToInt32());
						Point clientPoint = this.PointToClient(screenPoint);
						if (clientPoint.Y <= RESIZE_HANDLE_SIZE)
						{
							if (clientPoint.X <= RESIZE_HANDLE_SIZE)
								m.Result = (IntPtr)13/*HTTOPLEFT*/ ;
							else if (clientPoint.X < (Size.Width - RESIZE_HANDLE_SIZE))
								m.Result = (IntPtr)12/*HTTOP*/ ;
							else
								m.Result = (IntPtr)14/*HTTOPRIGHT*/ ;
						}
						else if (clientPoint.Y <= (Size.Height - RESIZE_HANDLE_SIZE - 20))
						{
							if (clientPoint.X <= RESIZE_HANDLE_SIZE)
								m.Result = (IntPtr)10/*HTLEFT*/ ;
							else if (clientPoint.X < (Size.Width - RESIZE_HANDLE_SIZE))
								m.Result = (IntPtr)2/*HTCAPTION*/ ;
							else
								m.Result = (IntPtr)11/*HTRIGHT*/ ;
						}
						else
						{
							if (clientPoint.X <= RESIZE_HANDLE_SIZE)
								m.Result = (IntPtr)16/*HTBOTTOMLEFT*/ ;
							else if (clientPoint.X < (Size.Width - RESIZE_HANDLE_SIZE))
								m.Result = (IntPtr)15/*HTBOTTOM*/ ;
							else
								m.Result = (IntPtr)17/*HTBOTTOMRIGHT*/ ;
						}
					}
					return;
			}
			base.WndProc(ref m);
		}

		protected override CreateParams CreateParams
		{
			get
			{
				CreateParams cp = base.CreateParams;
				cp.Style |= 0x20000; // <--- use 0x20000
				return cp;
			}
		}

		private void SetDefaultTabBtn(Button btn, bool forceRecolor = false)
		{
            void Recolor()
            {
				btn.BackColor = Color.White;
				Program.ChangeButtonColor(btn, pnlTitle.BackColor);
			}
			
			if (forceRecolor)
            {
				Recolor();
            }
			else if (btn.BackColor != Color.White)
			{
				Recolor();
			}
		}

		private void SetActiveTabBtn(Button btn)
        {
			btn.BackColor = pnlTitle.BackColor;
			Program.ChangeButtonColor(btn, lblTitle.ForeColor);
		}

		private void SetButtonColors(Button btnHlight = null)
		{
			int iTag = -1;

			void ResetColors(Button btn)
            {
				if (btn.Tag.ToString() == iTag.ToString())
				{
					SetActiveTabBtn(btn);
				}
				else
				{
					SetDefaultTabBtn(btn);
				}
            }

			int idx = nb.SelectedIndex;
            if (idx >= 0)
            {
				iTag = (int)nb.TabPages[idx].Tag;
            }

			ResetColors(btnInventory);
			ResetColors(btnVocations);
			ResetColors(btnSkills);
			ResetColors(btnSpells);
			ResetColors(btnEquipment);
			ResetColors(btnAlchemy);
			ResetColors(btnBuilds);
			ResetColors(btnBeasts);
			ResetColors(btnMaps);
			ResetColors(btnQuests);
			ResetColors(btnOptions);
			if (btnHlight != null)
            {
                if (btnHlight.BackColor != pnlTitle.BackColor)
                {
					btnHlight.BackColor = SystemColors.ControlLight;
				}
			}
		}

		private void mnuItem_Enter(object sender, EventArgs e)
		{
			SetButtonColors(sender as Button);
		}

		private void mnuItem_Leave(object sender, EventArgs e)
		{
			SetButtonColors();
		}

		private void mnuItem_MouseEnter(object sender, EventArgs e)
		{
			SetButtonColors(sender as Button);
		}

		private void mnuItem_MouseLeave(object sender, EventArgs e)
		{
			SetButtonColors();
		}

		private void mnuItem_Click(object sender, EventArgs e)
		{
			int iIndex = -1;
			for (int iNo = 0; iNo < nb.TabPages.Count; iNo++)
			{
				if (nb.TabPages[iNo].Tag.ToString() == (sender as Button).Tag.ToString())
				{
					iIndex = iNo;
					break;
				}
			}

			nb.SuspendLayout();
			GetCurrentSplitterDistance();
			if (iIndex == -1)
			{
				nb.TabPages.Add((sender as Button).Text);
				nb.TabPages[nb.TabCount - 1].Tag = (sender as Button).Tag;
				iIndex = nb.TabCount - 1;
				switch ((sender as Button).Tag)
				{
					case iInv:
						frmInventory frmInvn = new frmInventory();
						frmInvn.Parent = nb.TabPages[iIndex];
						frmInvn.Dock = DockStyle.Fill;
						this.tabInvn = frmInvn;
						break;
					case iBts:
						frmBeasts frmBts = new frmBeasts();
						frmBts.Parent = nb.TabPages[iIndex];
						frmBts.Dock = DockStyle.Fill;
						this.tabBts = frmBts;
						break;
					case iVoc:
						frmVocations frmVoc = new frmVocations();
						frmVoc.Parent = nb.TabPages[iIndex];
						frmVoc.Dock = DockStyle.Fill;
						this.tabVoc = frmVoc;
						break;
					case iQst:
						frmQuests frmQst = new frmQuests();
						frmQst.Parent = nb.TabPages[iIndex];
						frmQst.Dock = DockStyle.Fill;
						this.tabQst = frmQst;
						break;
					case iSkl:
						frmSkills frmSkl = new frmSkills();
						frmSkl.Parent = nb.TabPages[iIndex];
						frmSkl.Dock = DockStyle.Fill;
						this.tabSkl = frmSkl;
						break;
					case iSpl:
						frmSpells frmSpl = new frmSpells();
						frmSpl.Parent = nb.TabPages[iIndex];
						frmSpl.Dock = DockStyle.Fill;
						this.tabSpl = frmSpl;
						break;
					case iBld:
						frmBuilds frmBld = new frmBuilds();
						frmBld.Parent = nb.TabPages[iIndex];
						frmBld.Dock = DockStyle.Fill;
						this.tabBld = frmBld;
						break;
					case iMap:
						frmMaps frmMap = new frmMaps();
						frmMap.Parent = nb.TabPages[iIndex];
						frmMap.Dock = DockStyle.Fill;
						this.tabMap = frmMap;
						break;
				}
			}
			nb.TabPages[iIndex].BackColor = Color.White;
			nb.TabPages[iIndex].ContextMenuStrip = mnuPopUp;
			mnuClose.Enabled = nb.TabCount > 0;
			nb.SelectedIndex = iIndex;
			SetCurrentSplitterDistance();
			nb.ResumeLayout();
			SetButtonColors((sender as Button));
		}

		private void frmMain_MouseDown(object sender, MouseEventArgs e)
		{
			mnuClose.Enabled = nb.TabCount > 0;
			switch (e.Button)
			{
				case MouseButtons.Right:
					mnuPopUp.Show(this, new Point(e.X, e.Y));
					break;
			}
		}

		void FreeClosedTab(object Index)
		{
			if (Index == btnInventory.Tag)
			{
				tabInvn = null;
			}
			if (Index == btnVocations.Tag)
			{
				tabVoc = null;
			}
			if (Index == btnSkills.Tag)
			{
				tabSkl = null;
			}
			if (Index == btnSpells.Tag)
			{
				tabSpl = null;
			}
			if (Index == btnEquipment.Tag)
			{
				//tabEqp = null;
			}
			if (Index == btnAlchemy.Tag)
			{
				//tabAlc = null;
			}
			if (Index == btnBuilds.Tag)
			{
				tabBld = null;
			}
			if (Index == btnBeasts.Tag)
			{
				tabBts = null;
			}
			if (Index == btnMaps.Tag)
			{
				tabMap = null;
			}
			if (Index == btnQuests.Tag)
			{
				tabQst = null;
			}
		}

		private void mnuClose_Click(object sender, EventArgs e)
		{
			nb.SuspendLayout();
			int iIndex = nb.SelectedIndex;
			if (iIndex >= 0)
			{
				FreeClosedTab(nb.TabPages[iIndex].Tag);
				nb.TabPages.RemoveAt(iIndex);
				nb.SelectedIndex = (iIndex - 1);
				if ((nb.SelectedIndex < 0) & (nb.TabPages.Count > 0))
				{
					nb.SelectedIndex = 0;
				}
				mnuClose.Enabled = nb.TabCount > 0;
			}

			nb.ResumeLayout();
			SetButtonColors();
			Application.DoEvents();
			Application.DoEvents();
		}

        private void btnMin_Click(object sender, EventArgs e)
        {
			this.WindowState = FormWindowState.Minimized;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
			if (this.WindowState == FormWindowState.Maximized)
			{
				this.Width = iWidth;
				this.Height = iHeight;
				this.WindowState = FormWindowState.Normal;
				this.btnMax.Text = "🗖";
			}
			else
			{
				iWidth = this.Width;
				iHeight = this.Height;
				Screen screen = Screen.FromControl(this);
				int x = screen.WorkingArea.X - screen.Bounds.X;
				int y = screen.WorkingArea.Y - screen.Bounds.Y;
				this.MaximizedBounds = new Rectangle(x, y,
					screen.WorkingArea.Width, screen.WorkingArea.Height);
				this.MaximumSize = screen.WorkingArea.Size;
				this.WindowState = FormWindowState.Maximized;
				this.btnMax.Text = "🗗";
			}
		}

		private void btnClose_Click(object sender, EventArgs e)
		{
			this.Close();
		}

        private void pnlTitle_MouseDown(object sender, MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Left)
			{
				ReleaseCapture();
				SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
				//
				if (this.WindowState == FormWindowState.Maximized)
				{
					this.btnMax.Text = "🗗";
				}
				else
				{
					this.btnMax.Text = "🗖";
				}
			}
		}

        private void pnlTitle_DoubleClick(object sender, EventArgs e)
        {
			this.btnMax_Click(sender, e);
        }

        private void frmMain_Resize(object sender, EventArgs e)
        {
			if (this.Width <= 1180)
			{
				this.btnInventory.Text = "";
				this.btnVocations.Text = "";
				this.btnSkills.Text = "";
				this.btnSpells.Text = "";
				this.btnEquipment.Text = "";
				this.btnAlchemy.Text = "";
				this.btnBuilds.Text = "";
				this.btnBeasts.Text = "";
				this.btnMaps.Text = "";
				this.btnQuests.Text = "";
				this.btnOptions.Text = "";

				this.btnInventory.Width = 40;
				this.btnVocations.Width = 40;
				this.btnSkills.Width = 40;
				this.btnSpells.Width = 40;
				this.btnEquipment.Width = 40;
				this.btnAlchemy.Width = 40;
				this.btnBuilds.Width = 40;
				this.btnBeasts.Width = 40;
				this.btnMaps.Width = 40;
				this.btnQuests.Width = 40;
				this.btnOptions.Width = 40;
			}
			else
			{
				this.btnInventory.Text = " Inventory";
				this.btnVocations.Text = " Vocations";
				this.btnSkills.Text = " Skills";
				this.btnSpells.Text = " Spells";
				this.btnEquipment.Text = " Equipment";
				this.btnAlchemy.Text = " Alchemy";
				this.btnBuilds.Text = " Builds";
				this.btnBeasts.Text = " Beasts";
				this.btnMaps.Text = " Maps";
				this.btnQuests.Text = " Quests";
				this.btnOptions.Text = " Options";

				this.btnInventory.Width = 100;
				this.btnVocations.Width = 100;
				this.btnSkills.Width = 100;
				this.btnSpells.Width = 100;
				this.btnEquipment.Width = 100;
				this.btnAlchemy.Width = 100;
				this.btnBuilds.Width = 100;
				this.btnBeasts.Width = 100;
				this.btnMaps.Width = 100;
				this.btnQuests.Width = 100;
				this.btnOptions.Width = 100;
			}
        }

        private void btnOptions_Click(object sender, EventArgs e)
        {

			frmOptions OptionsForm = new frmOptions();
			Color color = pnlTitle.BackColor;
			if (OptionsForm.Execute(ref color))
            {
				UpdateUI();
			}
			SetButtonColors();
		}

		private void UpdateUI()
        {
			void UpdateTabButtonColor(FocuslessButton btn)
            {
				bool def = true;
				int idx = nb.SelectedIndex;
                if (idx >= 0)
                {
					if (btn.Tag == nb.TabPages[idx].Tag)
					{
						def = false;
					}
				}
				if (def == false)
                {
					SetActiveTabBtn(btn);
				}
                else
                {
					SetDefaultTabBtn(btn, true);
                }
			}

			Color color = Properties.Settings.Default.colorPrimary;
			lblTitle.ForeColor = Properties.Settings.Default.colorPriText;
			pnlTitle.BackColor = color;
			UpdateTabButtonColor(btnInventory);
			UpdateTabButtonColor(btnVocations);
			UpdateTabButtonColor(btnSkills);
			UpdateTabButtonColor(btnSpells);
			UpdateTabButtonColor(btnQuests);
			UpdateTabButtonColor(btnAlchemy);
			UpdateTabButtonColor(btnEquipment);
			UpdateTabButtonColor(btnBeasts);
			UpdateTabButtonColor(btnBuilds);
			UpdateTabButtonColor(btnMaps);
			UpdateTabButtonColor(btnOptions);
			Program.ChangeButtonColor(mnuFile, color);
			if (tabVoc != null) { tabVoc.UpdateUI(); }
			if (tabBts != null) { tabBts.UpdateUI(); }
			if (tabQst != null) { tabQst.UpdateUI(); }
			if (tabSkl != null) { tabSkl.UpdateUI(); }
			if (tabSpl != null) { tabSpl.UpdateUI(); }
			if (tabBld != null) { tabBld.UpdateUI(); }
			if (tabMap != null) { tabMap.UpdateUI(); }
			if (tabInvn != null) { tabInvn.UpdateUI(); }
		}

        private void mnuExit_Click(object sender, EventArgs e)
        {
			this.Close();
        }

        private void mnuNewUser_Click(object sender, EventArgs e)
		{
            if (nb.TabCount > 0)
			{
				DialogResult result = MessageBox.Show("This will close all open tabs. Continue anyway?", "Warning", MessageBoxButtons.YesNo);
				if (result == DialogResult.Yes)
				{
					while (nb.TabCount > 0)
                    {
						int idx = nb.TabCount - 1;
						FreeClosedTab(nb.TabPages[idx].Tag);
						nb.TabPages.RemoveAt(idx);
					}
					mnuClose.Enabled = nb.TabCount > 0;
				}
				else
				{
					return;
				}
			}
			UserInfo user = new UserInfo();
			frmLogin LoginForm = new frmLogin();
			if (LoginForm.TryLogin(user))
			{
				this.lblDB.Text = user.database;
				this.lblUser.Text = user.username;
			}
		}

        private void frmMain_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == (Keys.Control | Keys.E))
			{
				if (tabInvn != null) { tabInvn.UpdateReadOnly(); }
			}
		}

		private void GetCurrentSplitterDistance()
		{
			int idx = nb.SelectedIndex;
			if (idx < 0)
			{
				iSplit = 310;
			}
			else
			{
				object tag = nb.TabPages[idx].Tag;
				switch (tag)
				{
					case iInv:
						iSplit = tabInvn.GetSplitterDistance();
						break;
					case iBts:
						iSplit = tabBts.GetSplitterDistance();
						break;
					case iVoc:
						iSplit = tabVoc.GetSplitterDistance();
						break;
					case iQst:
						iSplit = tabQst.GetSplitterDistance();
						break;
					case iSkl:
						iSplit = tabSkl.GetSplitterDistance();
						break;
					case iSpl:
						iSplit = tabSpl.GetSplitterDistance();
						break;
					case iBld:
						iSplit = tabBld.GetSplitterDistance();
						break;
						/*
					case iMap:
						iSplit = tabMap.GetSplitterDistance();
						break;
						*/
				}
			}
		}

		private void SetCurrentSplitterDistance()
		{
			int idx = nb.SelectedIndex;
			if (idx >= 0)
			{
				object tag = nb.TabPages[idx].Tag;
				switch (tag)
				{
					case iInv:
						tabInvn.SetSplitterDistance(iSplit);
						break;
					case iBts:
						tabBts.SetSplitterDistance(iSplit);
						break;
					case iVoc:
						tabVoc.SetSplitterDistance(iSplit);
						break;
					case iQst:
						tabQst.SetSplitterDistance(iSplit);
						break;
					case iSkl:
						tabSkl.SetSplitterDistance(iSplit);
						break;
					case iSpl:
						tabSpl.SetSplitterDistance(iSplit);
						break;
					case iBld:
						tabBld.SetSplitterDistance(iSplit);
						break;
						/*
					case iMap:
						tabMap.SetSplitterDistance(iSplit);
						break;
						*/
				}
			}
		}
    }
}
