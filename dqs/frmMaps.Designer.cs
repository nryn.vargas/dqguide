﻿namespace dqs
{
    partial class frmMaps
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.picMap = new ImageViewer();
            this.pnlControls = new System.Windows.Forms.Panel();
            this.btnZoomOut = new CustomColorButton();
            this.edtZoom = new System.Windows.Forms.NumericUpDown();
            this.btnZoomIn = new CustomColorButton();
            this.picMap.SuspendLayout();
            this.pnlControls.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.edtZoom)).BeginInit();
            this.SuspendLayout();
            // 
            // picMap
            // 
            this.picMap.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.picMap.AutoScroll = true;
            this.picMap.Controls.Add(this.pnlControls);
            this.picMap.Image = null;
            this.picMap.Location = new System.Drawing.Point(0, 0);
            this.picMap.Name = "picMap";
            this.picMap.Size = new System.Drawing.Size(952, 617);
            this.picMap.TabIndex = 0;
            this.picMap.Paint += new System.Windows.Forms.PaintEventHandler(this.picMap_Paint);
            this.picMap.Resize += new System.EventHandler(this.picMap_Resize);
            // 
            // pnlControls
            // 
            this.pnlControls.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlControls.Controls.Add(this.btnZoomOut);
            this.pnlControls.Controls.Add(this.edtZoom);
            this.pnlControls.Controls.Add(this.btnZoomIn);
            this.pnlControls.Location = new System.Drawing.Point(838, 3);
            this.pnlControls.Name = "pnlControls";
            this.pnlControls.Size = new System.Drawing.Size(111, 31);
            this.pnlControls.TabIndex = 0;
            // 
            // btnZoomOut
            // 
            this.btnZoomOut.BackColor = System.Drawing.Color.Transparent;
            this.btnZoomOut.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnZoomOut.ForeColor = System.Drawing.Color.Transparent;
            this.btnZoomOut.Image = global::dqs.Properties.Resources.minus;
            this.btnZoomOut.Location = new System.Drawing.Point(80, 3);
            this.btnZoomOut.Name = "btnZoomOut";
            this.btnZoomOut.Size = new System.Drawing.Size(25, 23);
            this.btnZoomOut.TabIndex = 2;
            this.btnZoomOut.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnZoomOut.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.btnZoomOut.UseVisualStyleBackColor = false;
            this.btnZoomOut.Click += new System.EventHandler(this.btnZoomOut_Click);
            // 
            // edtZoom
            // 
            this.edtZoom.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.edtZoom.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.edtZoom.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.edtZoom.Location = new System.Drawing.Point(28, 3);
            this.edtZoom.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.edtZoom.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.edtZoom.Name = "edtZoom";
            this.edtZoom.Size = new System.Drawing.Size(51, 25);
            this.edtZoom.TabIndex = 1;
            this.edtZoom.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.edtZoom.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.edtZoom.ValueChanged += new System.EventHandler(this.edtZoom_ValueChanged);
            this.edtZoom.KeyDown += new System.Windows.Forms.KeyEventHandler(this.edtZoom_KeyDown);
            // 
            // btnZoomIn
            // 
            this.btnZoomIn.BackColor = System.Drawing.Color.Transparent;
            this.btnZoomIn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnZoomIn.ForeColor = System.Drawing.Color.Transparent;
            this.btnZoomIn.Image = global::dqs.Properties.Resources.plus;
            this.btnZoomIn.Location = new System.Drawing.Point(1, 3);
            this.btnZoomIn.Name = "btnZoomIn";
            this.btnZoomIn.Size = new System.Drawing.Size(25, 23);
            this.btnZoomIn.TabIndex = 0;
            this.btnZoomIn.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnZoomIn.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.btnZoomIn.UseVisualStyleBackColor = false;
            this.btnZoomIn.Click += new System.EventHandler(this.btnZoomIn_Click);
            // 
            // frmMaps
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.picMap);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "frmMaps";
            this.Size = new System.Drawing.Size(952, 617);
            this.picMap.ResumeLayout(false);
            this.pnlControls.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.edtZoom)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private ImageViewer picMap;
        private System.Windows.Forms.Panel pnlControls;
        private System.Windows.Forms.NumericUpDown edtZoom;
        private CustomColorButton btnZoomOut;
        private CustomColorButton btnZoomIn;
    }
}
