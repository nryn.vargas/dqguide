﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace dqs
{
    public partial class frmMaps : UserControl
    {
        private Image _OriginalImage;

        public frmMaps()
        {
            InitializeComponent();
            UpdateUI();

            _OriginalImage = (Image)Properties.Resources.ResourceManager.GetObject("worldmap");
        }

        public void UpdateUI()
        {
            Color colorPrimary = Properties.Settings.Default.colorPrimary;
            Color colorPriText = Properties.Settings.Default.colorPriText;

            //Program.ChangeButtonColor(btnZoomIn, colorPrimary);
            //Program.ChangeButtonColor(btnZoomOut, colorPrimary);
        }

        private void btnZoomIn_Click(object sender, EventArgs e)
        {
            picMap.ZoomIn();
        }

        private void btnZoomOut_Click(object sender, EventArgs e)
        {
            picMap.ZoomOut();
        }

        private void picMap_Resize(object sender, EventArgs e)
        {
            picMap.Image = _OriginalImage;
        }

        private void picMap_Paint(object sender, PaintEventArgs e)
        {
            edtZoom.Text = picMap.ScaleFactor;
        }

        private void edtZoom_SetZoom()
        {
            int ZoomValue = (int)edtZoom.Value;
            if (ZoomValue > 500)
            {
                MessageBox.Show("Map can't be zoomed in to more than 500%",
                    "Map Zooming Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
                edtZoom.Value = 500;
            }
            else if (ZoomValue < 10)
            {
                MessageBox.Show("Map can't be zoomed out to less than 10%",
                    "Map Zooming Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
                edtZoom.Value = 10;
            }
            else
            {
                picMap.Zoom((float)edtZoom.Value / 100);
            }
        }

        private void edtZoom_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                edtZoom_SetZoom();
                e.Handled = true;
                e.SuppressKeyPress = true;
            }
        }

        private void edtZoom_ValueChanged(object sender, EventArgs e)
        {
            edtZoom_SetZoom();
        }
    }
}
