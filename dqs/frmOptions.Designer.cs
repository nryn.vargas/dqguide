﻿namespace dqs
{
    partial class frmOptions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlTop = new System.Windows.Forms.Panel();
            this.btnClose = new FocuslessButton();
            this.lblTitle = new System.Windows.Forms.Label();
            this.btnReset = new FocuslessButton();
            this.btnGreen = new CustomColorButton();
            this.btnLightGreen = new CustomColorButton();
            this.btnLime = new CustomColorButton();
            this.btnYellow = new CustomColorButton();
            this.btnAmber = new CustomColorButton();
            this.btnOrange = new CustomColorButton();
            this.btnDeepOrange = new CustomColorButton();
            this.btnRed = new CustomColorButton();
            this.btnPink = new CustomColorButton();
            this.btnPurple = new CustomColorButton();
            this.btnDeepPurple = new CustomColorButton();
            this.btnIndigo = new CustomColorButton();
            this.btnBlue = new CustomColorButton();
            this.btnLightBlue = new CustomColorButton();
            this.btnCyan = new CustomColorButton();
            this.btnTeal = new CustomColorButton();
            this.btnBrown = new CustomColorButton();
            this.btnBlack = new CustomColorButton();
            this.btnGray = new CustomColorButton();
            this.btnBlueGray = new CustomColorButton();
            this.btnOK = new FocuslessButton();
            this.btnCancel = new FocuslessButton();
            this.pnlTop.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlTop
            // 
            this.pnlTop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(151)))), ((int)(((byte)(167)))));
            this.pnlTop.Controls.Add(this.btnClose);
            this.pnlTop.Controls.Add(this.lblTitle);
            this.pnlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTop.Location = new System.Drawing.Point(0, 0);
            this.pnlTop.Name = "pnlTop";
            this.pnlTop.Size = new System.Drawing.Size(539, 38);
            this.pnlTop.TabIndex = 0;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Red;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.ForeColor = System.Drawing.Color.White;
            this.btnClose.Location = new System.Drawing.Point(511, 0);
            this.btnClose.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(28, 27);
            this.btnClose.TabIndex = 6;
            this.btnClose.TabStop = false;
            this.btnClose.Text = "❌";
            this.btnClose.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // lblTitle
            // 
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblTitle.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.ForeColor = System.Drawing.Color.White;
            this.lblTitle.Location = new System.Drawing.Point(0, -3);
            this.lblTitle.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblTitle.Size = new System.Drawing.Size(539, 41);
            this.lblTitle.TabIndex = 0;
            this.lblTitle.Text = "Options";
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblTitle.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lblTitle_MouseDown);
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(22, 304);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(92, 30);
            this.btnReset.TabIndex = 20;
            this.btnReset.Text = "&Reset";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // btnGreen
            // 
            this.btnGreen.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(175)))), ((int)(((byte)(80)))));
            this.btnGreen.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(94)))), ((int)(((byte)(32)))));
            this.btnGreen.FlatAppearance.BorderSize = 0;
            this.btnGreen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGreen.ForeColor = System.Drawing.Color.Black;
            this.btnGreen.Location = new System.Drawing.Point(150, 158);
            this.btnGreen.Name = "btnGreen";
            this.btnGreen.Size = new System.Drawing.Size(105, 30);
            this.btnGreen.TabIndex = 9;
            this.btnGreen.Text = "Green";
            this.btnGreen.UseVisualStyleBackColor = false;
            this.btnGreen.Click += new System.EventHandler(this.btnColor_Click);
            // 
            // btnLightGreen
            // 
            this.btnLightGreen.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(195)))), ((int)(((byte)(74)))));
            this.btnLightGreen.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(105)))), ((int)(((byte)(30)))));
            this.btnLightGreen.FlatAppearance.BorderSize = 0;
            this.btnLightGreen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLightGreen.ForeColor = System.Drawing.Color.Black;
            this.btnLightGreen.Location = new System.Drawing.Point(150, 194);
            this.btnLightGreen.Name = "btnLightGreen";
            this.btnLightGreen.Size = new System.Drawing.Size(105, 30);
            this.btnLightGreen.TabIndex = 13;
            this.btnLightGreen.Text = "Light Green";
            this.btnLightGreen.UseVisualStyleBackColor = false;
            this.btnLightGreen.Click += new System.EventHandler(this.btnColor_Click);
            // 
            // btnLime
            // 
            this.btnLime.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(205)))), ((int)(((byte)(220)))), ((int)(((byte)(57)))));
            this.btnLime.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(130)))), ((int)(((byte)(119)))), ((int)(((byte)(23)))));
            this.btnLime.FlatAppearance.BorderSize = 0;
            this.btnLime.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLime.ForeColor = System.Drawing.Color.Black;
            this.btnLime.Location = new System.Drawing.Point(150, 230);
            this.btnLime.Name = "btnLime";
            this.btnLime.Size = new System.Drawing.Size(105, 30);
            this.btnLime.TabIndex = 17;
            this.btnLime.Text = "Lime";
            this.btnLime.UseVisualStyleBackColor = false;
            this.btnLime.Click += new System.EventHandler(this.btnColor_Click);
            // 
            // btnYellow
            // 
            this.btnYellow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(235)))), ((int)(((byte)(59)))));
            this.btnYellow.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(127)))), ((int)(((byte)(23)))));
            this.btnYellow.FlatAppearance.BorderSize = 0;
            this.btnYellow.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnYellow.ForeColor = System.Drawing.Color.Black;
            this.btnYellow.Location = new System.Drawing.Point(281, 230);
            this.btnYellow.Name = "btnYellow";
            this.btnYellow.Size = new System.Drawing.Size(105, 30);
            this.btnYellow.TabIndex = 18;
            this.btnYellow.Text = "Yellow";
            this.btnYellow.UseVisualStyleBackColor = false;
            this.btnYellow.Click += new System.EventHandler(this.btnColor_Click);
            // 
            // btnAmber
            // 
            this.btnAmber.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(193)))), ((int)(((byte)(7)))));
            this.btnAmber.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(111)))), ((int)(((byte)(0)))));
            this.btnAmber.FlatAppearance.BorderSize = 0;
            this.btnAmber.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAmber.ForeColor = System.Drawing.Color.Black;
            this.btnAmber.Location = new System.Drawing.Point(281, 194);
            this.btnAmber.Name = "btnAmber";
            this.btnAmber.Size = new System.Drawing.Size(105, 30);
            this.btnAmber.TabIndex = 14;
            this.btnAmber.Text = "Amber";
            this.btnAmber.UseVisualStyleBackColor = false;
            this.btnAmber.Click += new System.EventHandler(this.btnColor_Click);
            // 
            // btnOrange
            // 
            this.btnOrange.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(152)))), ((int)(((byte)(0)))));
            this.btnOrange.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(81)))), ((int)(((byte)(0)))));
            this.btnOrange.FlatAppearance.BorderSize = 0;
            this.btnOrange.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOrange.ForeColor = System.Drawing.Color.Black;
            this.btnOrange.Location = new System.Drawing.Point(281, 158);
            this.btnOrange.Name = "btnOrange";
            this.btnOrange.Size = new System.Drawing.Size(105, 30);
            this.btnOrange.TabIndex = 10;
            this.btnOrange.Text = "Orange";
            this.btnOrange.UseVisualStyleBackColor = false;
            this.btnOrange.Click += new System.EventHandler(this.btnColor_Click);
            // 
            // btnDeepOrange
            // 
            this.btnDeepOrange.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(87)))), ((int)(((byte)(34)))));
            this.btnDeepOrange.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(54)))), ((int)(((byte)(12)))));
            this.btnDeepOrange.FlatAppearance.BorderSize = 0;
            this.btnDeepOrange.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDeepOrange.ForeColor = System.Drawing.Color.Black;
            this.btnDeepOrange.Location = new System.Drawing.Point(281, 122);
            this.btnDeepOrange.Name = "btnDeepOrange";
            this.btnDeepOrange.Size = new System.Drawing.Size(105, 30);
            this.btnDeepOrange.TabIndex = 6;
            this.btnDeepOrange.Text = "Deep Orange";
            this.btnDeepOrange.UseVisualStyleBackColor = false;
            this.btnDeepOrange.Click += new System.EventHandler(this.btnColor_Click);
            // 
            // btnRed
            // 
            this.btnRed.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(67)))), ((int)(((byte)(54)))));
            this.btnRed.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(183)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.btnRed.FlatAppearance.BorderSize = 0;
            this.btnRed.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRed.ForeColor = System.Drawing.Color.White;
            this.btnRed.Location = new System.Drawing.Point(411, 230);
            this.btnRed.Name = "btnRed";
            this.btnRed.Size = new System.Drawing.Size(105, 30);
            this.btnRed.TabIndex = 19;
            this.btnRed.Text = "Red";
            this.btnRed.UseVisualStyleBackColor = false;
            this.btnRed.Click += new System.EventHandler(this.btnColor_Click);
            // 
            // btnPink
            // 
            this.btnPink.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(30)))), ((int)(((byte)(99)))));
            this.btnPink.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(14)))), ((int)(((byte)(79)))));
            this.btnPink.FlatAppearance.BorderSize = 0;
            this.btnPink.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPink.ForeColor = System.Drawing.Color.White;
            this.btnPink.Location = new System.Drawing.Point(411, 194);
            this.btnPink.Name = "btnPink";
            this.btnPink.Size = new System.Drawing.Size(105, 30);
            this.btnPink.TabIndex = 15;
            this.btnPink.Text = "Pink";
            this.btnPink.UseVisualStyleBackColor = false;
            this.btnPink.Click += new System.EventHandler(this.btnColor_Click);
            // 
            // btnPurple
            // 
            this.btnPurple.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(156)))), ((int)(((byte)(39)))), ((int)(((byte)(176)))));
            this.btnPurple.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(20)))), ((int)(((byte)(140)))));
            this.btnPurple.FlatAppearance.BorderSize = 0;
            this.btnPurple.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPurple.ForeColor = System.Drawing.Color.White;
            this.btnPurple.Location = new System.Drawing.Point(411, 158);
            this.btnPurple.Name = "btnPurple";
            this.btnPurple.Size = new System.Drawing.Size(105, 30);
            this.btnPurple.TabIndex = 11;
            this.btnPurple.Text = "Purple";
            this.btnPurple.UseVisualStyleBackColor = false;
            this.btnPurple.Click += new System.EventHandler(this.btnColor_Click);
            // 
            // btnDeepPurple
            // 
            this.btnDeepPurple.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(103)))), ((int)(((byte)(58)))), ((int)(((byte)(183)))));
            this.btnDeepPurple.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(27)))), ((int)(((byte)(146)))));
            this.btnDeepPurple.FlatAppearance.BorderSize = 0;
            this.btnDeepPurple.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDeepPurple.ForeColor = System.Drawing.Color.White;
            this.btnDeepPurple.Location = new System.Drawing.Point(411, 122);
            this.btnDeepPurple.Name = "btnDeepPurple";
            this.btnDeepPurple.Size = new System.Drawing.Size(105, 30);
            this.btnDeepPurple.TabIndex = 7;
            this.btnDeepPurple.Text = "Deep Purple";
            this.btnDeepPurple.UseVisualStyleBackColor = false;
            this.btnDeepPurple.Click += new System.EventHandler(this.btnColor_Click);
            // 
            // btnIndigo
            // 
            this.btnIndigo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(81)))), ((int)(((byte)(181)))));
            this.btnIndigo.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(35)))), ((int)(((byte)(126)))));
            this.btnIndigo.FlatAppearance.BorderSize = 0;
            this.btnIndigo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIndigo.ForeColor = System.Drawing.Color.White;
            this.btnIndigo.Location = new System.Drawing.Point(22, 122);
            this.btnIndigo.Name = "btnIndigo";
            this.btnIndigo.Size = new System.Drawing.Size(105, 30);
            this.btnIndigo.TabIndex = 4;
            this.btnIndigo.Text = "Indigo";
            this.btnIndigo.UseVisualStyleBackColor = false;
            this.btnIndigo.Click += new System.EventHandler(this.btnColor_Click);
            // 
            // btnBlue
            // 
            this.btnBlue.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(150)))), ((int)(((byte)(243)))));
            this.btnBlue.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(71)))), ((int)(((byte)(161)))));
            this.btnBlue.FlatAppearance.BorderSize = 0;
            this.btnBlue.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBlue.ForeColor = System.Drawing.Color.Black;
            this.btnBlue.Location = new System.Drawing.Point(22, 158);
            this.btnBlue.Name = "btnBlue";
            this.btnBlue.Size = new System.Drawing.Size(105, 30);
            this.btnBlue.TabIndex = 8;
            this.btnBlue.Text = "Blue";
            this.btnBlue.UseVisualStyleBackColor = false;
            this.btnBlue.Click += new System.EventHandler(this.btnColor_Click);
            // 
            // btnLightBlue
            // 
            this.btnLightBlue.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(169)))), ((int)(((byte)(244)))));
            this.btnLightBlue.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(87)))), ((int)(((byte)(155)))));
            this.btnLightBlue.FlatAppearance.BorderSize = 0;
            this.btnLightBlue.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLightBlue.ForeColor = System.Drawing.Color.Black;
            this.btnLightBlue.Location = new System.Drawing.Point(22, 194);
            this.btnLightBlue.Name = "btnLightBlue";
            this.btnLightBlue.Size = new System.Drawing.Size(105, 30);
            this.btnLightBlue.TabIndex = 12;
            this.btnLightBlue.Text = "Light Blue";
            this.btnLightBlue.UseVisualStyleBackColor = false;
            this.btnLightBlue.Click += new System.EventHandler(this.btnColor_Click);
            // 
            // btnCyan
            // 
            this.btnCyan.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(188)))), ((int)(((byte)(212)))));
            this.btnCyan.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(96)))), ((int)(((byte)(100)))));
            this.btnCyan.FlatAppearance.BorderSize = 0;
            this.btnCyan.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCyan.ForeColor = System.Drawing.Color.White;
            this.btnCyan.Location = new System.Drawing.Point(22, 230);
            this.btnCyan.Name = "btnCyan";
            this.btnCyan.Size = new System.Drawing.Size(105, 30);
            this.btnCyan.TabIndex = 16;
            this.btnCyan.Text = "Cyan";
            this.btnCyan.UseVisualStyleBackColor = false;
            this.btnCyan.Click += new System.EventHandler(this.btnColor_Click);
            // 
            // btnTeal
            // 
            this.btnTeal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(150)))), ((int)(((byte)(136)))));
            this.btnTeal.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(77)))), ((int)(((byte)(64)))));
            this.btnTeal.FlatAppearance.BorderSize = 0;
            this.btnTeal.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTeal.ForeColor = System.Drawing.Color.White;
            this.btnTeal.Location = new System.Drawing.Point(150, 122);
            this.btnTeal.Name = "btnTeal";
            this.btnTeal.Size = new System.Drawing.Size(105, 30);
            this.btnTeal.TabIndex = 5;
            this.btnTeal.Text = "Teal";
            this.btnTeal.UseVisualStyleBackColor = false;
            this.btnTeal.Click += new System.EventHandler(this.btnColor_Click);
            // 
            // btnBrown
            // 
            this.btnBrown.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(121)))), ((int)(((byte)(85)))), ((int)(((byte)(72)))));
            this.btnBrown.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(39)))), ((int)(((byte)(35)))));
            this.btnBrown.FlatAppearance.BorderSize = 0;
            this.btnBrown.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBrown.ForeColor = System.Drawing.Color.White;
            this.btnBrown.Location = new System.Drawing.Point(281, 86);
            this.btnBrown.Name = "btnBrown";
            this.btnBrown.Size = new System.Drawing.Size(105, 30);
            this.btnBrown.TabIndex = 2;
            this.btnBrown.Text = "Brown";
            this.btnBrown.UseVisualStyleBackColor = false;
            this.btnBrown.Click += new System.EventHandler(this.btnColor_Click);
            // 
            // btnBlack
            // 
            this.btnBlack.BackColor = System.Drawing.Color.Black;
            this.btnBlack.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(117)))), ((int)(((byte)(117)))), ((int)(((byte)(117)))));
            this.btnBlack.FlatAppearance.BorderSize = 0;
            this.btnBlack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBlack.ForeColor = System.Drawing.Color.White;
            this.btnBlack.Location = new System.Drawing.Point(411, 86);
            this.btnBlack.Name = "btnBlack";
            this.btnBlack.Size = new System.Drawing.Size(105, 30);
            this.btnBlack.TabIndex = 3;
            this.btnBlack.Text = "Black";
            this.btnBlack.UseVisualStyleBackColor = false;
            this.btnBlack.Click += new System.EventHandler(this.btnColor_Click);
            // 
            // btnGray
            // 
            this.btnGray.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(158)))), ((int)(((byte)(158)))));
            this.btnGray.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(33)))), ((int)(((byte)(33)))));
            this.btnGray.FlatAppearance.BorderSize = 0;
            this.btnGray.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGray.ForeColor = System.Drawing.Color.Black;
            this.btnGray.Location = new System.Drawing.Point(150, 86);
            this.btnGray.Name = "btnGray";
            this.btnGray.Size = new System.Drawing.Size(105, 30);
            this.btnGray.TabIndex = 1;
            this.btnGray.Text = "Gray";
            this.btnGray.UseVisualStyleBackColor = false;
            this.btnGray.Click += new System.EventHandler(this.btnColor_Click);
            // 
            // btnBlueGray
            // 
            this.btnBlueGray.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(96)))), ((int)(((byte)(125)))), ((int)(((byte)(139)))));
            this.btnBlueGray.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(50)))), ((int)(((byte)(56)))));
            this.btnBlueGray.FlatAppearance.BorderSize = 0;
            this.btnBlueGray.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBlueGray.ForeColor = System.Drawing.Color.White;
            this.btnBlueGray.Location = new System.Drawing.Point(22, 86);
            this.btnBlueGray.Name = "btnBlueGray";
            this.btnBlueGray.Size = new System.Drawing.Size(105, 30);
            this.btnBlueGray.TabIndex = 0;
            this.btnBlueGray.Text = "Blue Gray";
            this.btnBlueGray.UseVisualStyleBackColor = false;
            this.btnBlueGray.Click += new System.EventHandler(this.btnColor_Click);
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(326, 304);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(92, 30);
            this.btnOK.TabIndex = 21;
            this.btnOK.Text = "&OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(424, 304);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(92, 30);
            this.btnCancel.TabIndex = 22;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // frmOptions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(539, 354);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.btnGreen);
            this.Controls.Add(this.btnLightGreen);
            this.Controls.Add(this.btnLime);
            this.Controls.Add(this.btnYellow);
            this.Controls.Add(this.btnAmber);
            this.Controls.Add(this.btnOrange);
            this.Controls.Add(this.btnDeepOrange);
            this.Controls.Add(this.btnRed);
            this.Controls.Add(this.btnPink);
            this.Controls.Add(this.btnPurple);
            this.Controls.Add(this.btnDeepPurple);
            this.Controls.Add(this.btnIndigo);
            this.Controls.Add(this.btnBlue);
            this.Controls.Add(this.btnLightBlue);
            this.Controls.Add(this.btnCyan);
            this.Controls.Add(this.btnTeal);
            this.Controls.Add(this.btnBrown);
            this.Controls.Add(this.btnBlack);
            this.Controls.Add(this.btnGray);
            this.Controls.Add(this.btnBlueGray);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.pnlTop);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmOptions";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Options";
            this.Shown += new System.EventHandler(this.frmOptions_Shown);
            this.pnlTop.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel pnlTop;
        private FocuslessButton btnCancel;
        private FocuslessButton btnOK;
        private System.Windows.Forms.Label lblTitle;
        private CustomColorButton btnBlueGray;
        private CustomColorButton btnGray;
        private CustomColorButton btnBlack;
        private CustomColorButton btnBrown;
        private CustomColorButton btnTeal;
        private CustomColorButton btnCyan;
        private CustomColorButton btnLightBlue;
        private CustomColorButton btnBlue;
        private CustomColorButton btnIndigo;
        private CustomColorButton btnDeepPurple;
        private CustomColorButton btnPurple;
        private CustomColorButton btnPink;
        private CustomColorButton btnRed;
        private CustomColorButton btnDeepOrange;
        private CustomColorButton btnOrange;
        private CustomColorButton btnAmber;
        private CustomColorButton btnYellow;
        private CustomColorButton btnLime;
        private CustomColorButton btnLightGreen;
        private CustomColorButton btnGreen;
        private FocuslessButton btnReset;
        private FocuslessButton btnClose;
    }
}