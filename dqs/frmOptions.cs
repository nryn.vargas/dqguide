﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace dqs
{
    public partial class frmOptions : Form
    {

        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd,
                         int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();
        public frmOptions()
        {
            InitializeComponent();
        }

        public bool Execute(ref Color color)
        {
            try
            {
                this.pnlTop.BackColor = color;
                this.ShowDialog();
                if (this.DialogResult == DialogResult.OK)
                {
                    color = pnlTop.BackColor;
                }
                return (this.DialogResult == DialogResult.OK);
            }
            finally
            {
                this.Close();
            }
        }

        private void frmOptions_Shown(object sender, EventArgs e)
        {
            this.btnOK.Focus();
        }

        private void btnColorMain_Click(object sender, EventArgs e)
        {
            ColorDialog colorDlg = new ColorDialog();
            colorDlg.AllowFullOpen = true;
            colorDlg.AnyColor = true;
            colorDlg.SolidColorOnly = false;
            colorDlg.Color = pnlTop.BackColor;

            if (colorDlg.ShowDialog() == DialogResult.OK)
            {
                pnlTop.BackColor = colorDlg.Color;
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.colorPrimary = pnlTop.BackColor;
            Properties.Settings.Default.colorPriText = lblTitle.ForeColor;
            Properties.Settings.Default.Save();
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void btnColor_Click(object sender, EventArgs e)
        {
            pnlTop.BackColor = (sender as Button).BackColor;
            lblTitle.ForeColor = (sender as Button).ForeColor;
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.colorPrimary = Color.FromArgb(0, 151, 167);
            Properties.Settings.Default.colorPriText = Color.White;
            Properties.Settings.Default.Save();
            //
            pnlTop.BackColor = Color.FromArgb(0, 151, 167);
            lblTitle.ForeColor = Color.White;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void lblTitle_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void btnColor_GotFocus(object sender, EventArgs e)
        {
            (sender as Button).FlatAppearance.BorderSize = 1;
            if ((sender as Button).BackColor == Color.Black)
            {
                (sender as Button).FlatAppearance.BorderColor = Color.Gray;
            }
            else
            {
                (sender as Button).FlatAppearance.BorderColor = Color.Black;
            }
        }

        private void btnColor_LostFocus(object sender, EventArgs e)
        {
            (sender as Button).FlatAppearance.BorderSize = 0;
        }
    }
}
