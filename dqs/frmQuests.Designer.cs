﻿namespace dqs
{
    partial class frmQuests
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            this.lin3 = new System.Windows.Forms.Panel();
            this.lblDetail = new System.Windows.Forms.Label();
            this.edtRequire = new System.Windows.Forms.TextBox();
            this.lblReward = new System.Windows.Forms.Label();
            this.lblRequire = new System.Windows.Forms.Label();
            this.lblRes = new System.Windows.Forms.Label();
            this.lblReq = new System.Windows.Forms.Label();
            this.edtReward = new System.Windows.Forms.TextBox();
            this.edtResTo = new System.Windows.Forms.TextBox();
            this.edtReqBy = new System.Windows.Forms.TextBox();
            this.lin2 = new System.Windows.Forms.Panel();
            this.edtLoc = new System.Windows.Forms.TextBox();
            this.lblLoc = new System.Windows.Forms.Label();
            this.lin1 = new System.Windows.Forms.Panel();
            this.edtCode = new System.Windows.Forms.TextBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblFtSearchIn = new System.Windows.Forms.Label();
            this.lblFtKeyword = new System.Windows.Forms.Label();
            this.lblFtTitle = new System.Windows.Forms.Label();
            this.edtFtKey = new System.Windows.Forms.TextBox();
            this.edtTitle = new System.Windows.Forms.TextBox();
            this.edtFtTitle = new System.Windows.Forms.TextBox();
            this.sconFilter = new System.Windows.Forms.SplitContainer();
            this.dgQuests = new System.Windows.Forms.DataGridView();
            this.colCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colTitle = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colLocation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAvailable = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colReqBy = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colResTo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colRequire = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colReward = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDetail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colHint = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnFilter = new System.Windows.Forms.Button();
            this.edtFtOrder2 = new System.Windows.Forms.ComboBox();
            this.lblOrder = new System.Windows.Forms.Label();
            this.edtFtOrder = new System.Windows.Forms.ComboBox();
            this.lblFtCaption = new System.Windows.Forms.Label();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnApply = new System.Windows.Forms.Button();
            this.btnFtAvail = new System.Windows.Forms.Button();
            this.btnFtLoc = new System.Windows.Forms.Button();
            this.btnFtHint = new System.Windows.Forms.Button();
            this.btnFtDetail = new System.Windows.Forms.Button();
            this.btnFtReward = new System.Windows.Forms.Button();
            this.btnFtReq = new System.Windows.Forms.Button();
            this.btnFtResTo = new System.Windows.Forms.Button();
            this.btnFtReqBy = new System.Windows.Forms.Button();
            this.btnFtAll = new System.Windows.Forms.Button();
            this.sconQuests = new System.Windows.Forms.SplitContainer();
            this.edtHint = new System.Windows.Forms.RichTextBox();
            this.edtDetail = new System.Windows.Forms.RichTextBox();
            this.lblHint = new System.Windows.Forms.Label();
            this.edtAvail = new System.Windows.Forms.TextBox();
            this.lblAvailable = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.sconFilter)).BeginInit();
            this.sconFilter.Panel1.SuspendLayout();
            this.sconFilter.Panel2.SuspendLayout();
            this.sconFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgQuests)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sconQuests)).BeginInit();
            this.sconQuests.Panel1.SuspendLayout();
            this.sconQuests.Panel2.SuspendLayout();
            this.sconQuests.SuspendLayout();
            this.SuspendLayout();
            // 
            // lin3
            // 
            this.lin3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lin3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(66)))), ((int)(((byte)(66)))));
            this.lin3.Enabled = false;
            this.lin3.Location = new System.Drawing.Point(3, 339);
            this.lin3.Name = "lin3";
            this.lin3.Size = new System.Drawing.Size(706, 1);
            this.lin3.TabIndex = 28;
            // 
            // lblDetail
            // 
            this.lblDetail.AutoSize = true;
            this.lblDetail.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblDetail.Location = new System.Drawing.Point(8, 354);
            this.lblDetail.Name = "lblDetail";
            this.lblDetail.Size = new System.Drawing.Size(42, 15);
            this.lblDetail.TabIndex = 27;
            this.lblDetail.Text = "Details";
            // 
            // edtRequire
            // 
            this.edtRequire.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtRequire.BackColor = System.Drawing.Color.White;
            this.edtRequire.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.edtRequire.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.edtRequire.Location = new System.Drawing.Point(111, 207);
            this.edtRequire.Multiline = true;
            this.edtRequire.Name = "edtRequire";
            this.edtRequire.ReadOnly = true;
            this.edtRequire.Size = new System.Drawing.Size(603, 55);
            this.edtRequire.TabIndex = 26;
            this.edtRequire.Text = "[details]";
            // 
            // lblReward
            // 
            this.lblReward.AutoSize = true;
            this.lblReward.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblReward.Location = new System.Drawing.Point(8, 278);
            this.lblReward.Name = "lblReward";
            this.lblReward.Size = new System.Drawing.Size(46, 15);
            this.lblReward.TabIndex = 25;
            this.lblReward.Text = "Reward";
            // 
            // lblRequire
            // 
            this.lblRequire.AutoSize = true;
            this.lblRequire.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblRequire.Location = new System.Drawing.Point(8, 207);
            this.lblRequire.Name = "lblRequire";
            this.lblRequire.Size = new System.Drawing.Size(75, 15);
            this.lblRequire.TabIndex = 24;
            this.lblRequire.Text = "Requirement";
            // 
            // lblRes
            // 
            this.lblRes.AutoSize = true;
            this.lblRes.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblRes.Location = new System.Drawing.Point(8, 160);
            this.lblRes.Name = "lblRes";
            this.lblRes.Size = new System.Drawing.Size(72, 15);
            this.lblRes.TabIndex = 8;
            this.lblRes.Text = "Response To";
            // 
            // lblReq
            // 
            this.lblReq.AutoSize = true;
            this.lblReq.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblReq.Location = new System.Drawing.Point(8, 127);
            this.lblReq.Name = "lblReq";
            this.lblReq.Size = new System.Drawing.Size(78, 15);
            this.lblReq.TabIndex = 6;
            this.lblReq.Text = "Requested By";
            // 
            // edtReward
            // 
            this.edtReward.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtReward.BackColor = System.Drawing.Color.White;
            this.edtReward.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.edtReward.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.edtReward.Location = new System.Drawing.Point(111, 273);
            this.edtReward.Multiline = true;
            this.edtReward.Name = "edtReward";
            this.edtReward.ReadOnly = true;
            this.edtReward.Size = new System.Drawing.Size(603, 55);
            this.edtReward.TabIndex = 18;
            this.edtReward.Text = "[reward]";
            // 
            // edtResTo
            // 
            this.edtResTo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtResTo.BackColor = System.Drawing.Color.White;
            this.edtResTo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.edtResTo.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.edtResTo.Location = new System.Drawing.Point(111, 155);
            this.edtResTo.Name = "edtResTo";
            this.edtResTo.ReadOnly = true;
            this.edtResTo.Size = new System.Drawing.Size(603, 22);
            this.edtResTo.TabIndex = 9;
            this.edtResTo.Text = "[response_to]";
            // 
            // edtReqBy
            // 
            this.edtReqBy.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtReqBy.BackColor = System.Drawing.Color.White;
            this.edtReqBy.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.edtReqBy.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.edtReqBy.Location = new System.Drawing.Point(111, 122);
            this.edtReqBy.Name = "edtReqBy";
            this.edtReqBy.ReadOnly = true;
            this.edtReqBy.Size = new System.Drawing.Size(603, 22);
            this.edtReqBy.TabIndex = 7;
            this.edtReqBy.Text = "[requested_by]";
            // 
            // lin2
            // 
            this.lin2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lin2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(66)))), ((int)(((byte)(66)))));
            this.lin2.Enabled = false;
            this.lin2.Location = new System.Drawing.Point(3, 194);
            this.lin2.Name = "lin2";
            this.lin2.Size = new System.Drawing.Size(706, 1);
            this.lin2.TabIndex = 12;
            // 
            // edtLoc
            // 
            this.edtLoc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtLoc.BackColor = System.Drawing.Color.White;
            this.edtLoc.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.edtLoc.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.edtLoc.Location = new System.Drawing.Point(111, 56);
            this.edtLoc.Name = "edtLoc";
            this.edtLoc.ReadOnly = true;
            this.edtLoc.Size = new System.Drawing.Size(603, 22);
            this.edtLoc.TabIndex = 3;
            this.edtLoc.Text = "[location]";
            // 
            // lblLoc
            // 
            this.lblLoc.AutoSize = true;
            this.lblLoc.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblLoc.Location = new System.Drawing.Point(8, 61);
            this.lblLoc.Name = "lblLoc";
            this.lblLoc.Size = new System.Drawing.Size(53, 15);
            this.lblLoc.TabIndex = 2;
            this.lblLoc.Text = "Location";
            // 
            // lin1
            // 
            this.lin1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lin1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(66)))), ((int)(((byte)(66)))));
            this.lin1.Enabled = false;
            this.lin1.Location = new System.Drawing.Point(3, 44);
            this.lin1.Name = "lin1";
            this.lin1.Size = new System.Drawing.Size(706, 1);
            this.lin1.TabIndex = 11;
            // 
            // edtCode
            // 
            this.edtCode.BackColor = System.Drawing.Color.White;
            this.edtCode.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.edtCode.Font = new System.Drawing.Font("Segoe UI", 20.25F);
            this.edtCode.Location = new System.Drawing.Point(5, 3);
            this.edtCode.Name = "edtCode";
            this.edtCode.ReadOnly = true;
            this.edtCode.Size = new System.Drawing.Size(58, 36);
            this.edtCode.TabIndex = 1;
            this.edtCode.Text = "[000]";
            // 
            // btnCancel
            // 
            this.btnCancel.AutoSize = true;
            this.btnCancel.BackColor = System.Drawing.Color.Transparent;
            this.btnCancel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnCancel.Image = global::dqs.Properties.Resources.close_thick;
            this.btnCancel.Location = new System.Drawing.Point(0, 592);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(227, 31);
            this.btnCancel.TabIndex = 19;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lblFtSearchIn
            // 
            this.lblFtSearchIn.AutoSize = true;
            this.lblFtSearchIn.Location = new System.Drawing.Point(18, 132);
            this.lblFtSearchIn.Name = "lblFtSearchIn";
            this.lblFtSearchIn.Size = new System.Drawing.Size(58, 15);
            this.lblFtSearchIn.TabIndex = 4;
            this.lblFtSearchIn.Text = "Search in:";
            // 
            // lblFtKeyword
            // 
            this.lblFtKeyword.AutoSize = true;
            this.lblFtKeyword.Location = new System.Drawing.Point(18, 82);
            this.lblFtKeyword.Name = "lblFtKeyword";
            this.lblFtKeyword.Size = new System.Drawing.Size(53, 15);
            this.lblFtKeyword.TabIndex = 2;
            this.lblFtKeyword.Text = "Keyword";
            // 
            // lblFtTitle
            // 
            this.lblFtTitle.AutoSize = true;
            this.lblFtTitle.Location = new System.Drawing.Point(18, 30);
            this.lblFtTitle.Name = "lblFtTitle";
            this.lblFtTitle.Size = new System.Drawing.Size(29, 15);
            this.lblFtTitle.TabIndex = 0;
            this.lblFtTitle.Text = "Title";
            // 
            // edtFtKey
            // 
            this.edtFtKey.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtFtKey.Location = new System.Drawing.Point(21, 100);
            this.edtFtKey.Name = "edtFtKey";
            this.edtFtKey.Size = new System.Drawing.Size(173, 23);
            this.edtFtKey.TabIndex = 3;
            // 
            // edtTitle
            // 
            this.edtTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtTitle.BackColor = System.Drawing.Color.White;
            this.edtTitle.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.edtTitle.Font = new System.Drawing.Font("Segoe UI", 20.25F);
            this.edtTitle.Location = new System.Drawing.Point(69, 3);
            this.edtTitle.Name = "edtTitle";
            this.edtTitle.ReadOnly = true;
            this.edtTitle.Size = new System.Drawing.Size(645, 36);
            this.edtTitle.TabIndex = 0;
            this.edtTitle.Text = "[name]";
            // 
            // edtFtTitle
            // 
            this.edtFtTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtFtTitle.Location = new System.Drawing.Point(21, 50);
            this.edtFtTitle.Name = "edtFtTitle";
            this.edtFtTitle.Size = new System.Drawing.Size(173, 23);
            this.edtFtTitle.TabIndex = 1;
            // 
            // sconFilter
            // 
            this.sconFilter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.sconFilter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sconFilter.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.sconFilter.IsSplitterFixed = true;
            this.sconFilter.Location = new System.Drawing.Point(0, 0);
            this.sconFilter.Name = "sconFilter";
            // 
            // sconFilter.Panel1
            // 
            this.sconFilter.Panel1.BackColor = System.Drawing.Color.White;
            this.sconFilter.Panel1.Controls.Add(this.dgQuests);
            this.sconFilter.Panel1.Controls.Add(this.btnFilter);
            // 
            // sconFilter.Panel2
            // 
            this.sconFilter.Panel2.Controls.Add(this.edtFtOrder2);
            this.sconFilter.Panel2.Controls.Add(this.lblOrder);
            this.sconFilter.Panel2.Controls.Add(this.edtFtOrder);
            this.sconFilter.Panel2.Controls.Add(this.lblFtCaption);
            this.sconFilter.Panel2.Controls.Add(this.btnClear);
            this.sconFilter.Panel2.Controls.Add(this.btnApply);
            this.sconFilter.Panel2.Controls.Add(this.btnFtAvail);
            this.sconFilter.Panel2.Controls.Add(this.btnFtLoc);
            this.sconFilter.Panel2.Controls.Add(this.btnFtHint);
            this.sconFilter.Panel2.Controls.Add(this.btnFtDetail);
            this.sconFilter.Panel2.Controls.Add(this.btnFtReward);
            this.sconFilter.Panel2.Controls.Add(this.btnFtReq);
            this.sconFilter.Panel2.Controls.Add(this.btnFtResTo);
            this.sconFilter.Panel2.Controls.Add(this.btnFtReqBy);
            this.sconFilter.Panel2.Controls.Add(this.btnFtAll);
            this.sconFilter.Panel2.Controls.Add(this.btnCancel);
            this.sconFilter.Panel2.Controls.Add(this.lblFtSearchIn);
            this.sconFilter.Panel2.Controls.Add(this.lblFtKeyword);
            this.sconFilter.Panel2.Controls.Add(this.edtFtKey);
            this.sconFilter.Panel2.Controls.Add(this.lblFtTitle);
            this.sconFilter.Panel2.Controls.Add(this.edtFtTitle);
            this.sconFilter.Panel2.Resize += new System.EventHandler(this.sconFilter_Panel2_Resize);
            this.sconFilter.Size = new System.Drawing.Size(310, 625);
            this.sconFilter.SplitterDistance = 75;
            this.sconFilter.SplitterWidth = 6;
            this.sconFilter.TabIndex = 1;
            // 
            // dgQuests
            // 
            this.dgQuests.AllowUserToAddRows = false;
            this.dgQuests.AllowUserToDeleteRows = false;
            this.dgQuests.AllowUserToResizeColumns = false;
            this.dgQuests.AllowUserToResizeRows = false;
            this.dgQuests.BackgroundColor = System.Drawing.Color.White;
            this.dgQuests.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgQuests.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle19.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle19.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle19.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle19.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle19.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgQuests.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle19;
            this.dgQuests.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgQuests.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colCode,
            this.colTitle,
            this.colLocation,
            this.colAvailable,
            this.colReqBy,
            this.colResTo,
            this.colRequire,
            this.colReward,
            this.colDetail,
            this.colHint});
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle21.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle21.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle21.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle21.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(151)))), ((int)(((byte)(167)))));
            dataGridViewCellStyle21.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle21.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgQuests.DefaultCellStyle = dataGridViewCellStyle21;
            this.dgQuests.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgQuests.GridColor = System.Drawing.SystemColors.ControlLight;
            this.dgQuests.Location = new System.Drawing.Point(0, 0);
            this.dgQuests.MultiSelect = false;
            this.dgQuests.Name = "dgQuests";
            this.dgQuests.ReadOnly = true;
            this.dgQuests.RowHeadersVisible = false;
            this.dgQuests.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgQuests.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgQuests.ShowEditingIcon = false;
            this.dgQuests.Size = new System.Drawing.Size(73, 592);
            this.dgQuests.StandardTab = true;
            this.dgQuests.TabIndex = 0;
            this.dgQuests.SelectionChanged += new System.EventHandler(this.dgQuests_SelectionChanged);
            // 
            // colCode
            // 
            this.colCode.DataPropertyName = "code";
            this.colCode.HeaderText = "Code";
            this.colCode.Name = "colCode";
            this.colCode.ReadOnly = true;
            this.colCode.Visible = false;
            // 
            // colTitle
            // 
            this.colTitle.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colTitle.DataPropertyName = "title";
            dataGridViewCellStyle20.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colTitle.DefaultCellStyle = dataGridViewCellStyle20;
            this.colTitle.HeaderText = "Title";
            this.colTitle.Name = "colTitle";
            this.colTitle.ReadOnly = true;
            this.colTitle.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // colLocation
            // 
            this.colLocation.DataPropertyName = "location";
            this.colLocation.HeaderText = "Location";
            this.colLocation.Name = "colLocation";
            this.colLocation.ReadOnly = true;
            this.colLocation.Visible = false;
            // 
            // colAvailable
            // 
            this.colAvailable.DataPropertyName = "available";
            this.colAvailable.HeaderText = "Available";
            this.colAvailable.Name = "colAvailable";
            this.colAvailable.ReadOnly = true;
            this.colAvailable.Visible = false;
            // 
            // colReqBy
            // 
            this.colReqBy.DataPropertyName = "requested_by";
            this.colReqBy.HeaderText = "ReqBy";
            this.colReqBy.Name = "colReqBy";
            this.colReqBy.ReadOnly = true;
            this.colReqBy.Visible = false;
            // 
            // colResTo
            // 
            this.colResTo.DataPropertyName = "response_to";
            this.colResTo.HeaderText = "ResTo";
            this.colResTo.Name = "colResTo";
            this.colResTo.ReadOnly = true;
            this.colResTo.Visible = false;
            // 
            // colRequire
            // 
            this.colRequire.DataPropertyName = "requirement";
            this.colRequire.HeaderText = "Require";
            this.colRequire.Name = "colRequire";
            this.colRequire.ReadOnly = true;
            this.colRequire.Visible = false;
            // 
            // colReward
            // 
            this.colReward.DataPropertyName = "reward";
            this.colReward.HeaderText = "Reward";
            this.colReward.Name = "colReward";
            this.colReward.ReadOnly = true;
            this.colReward.Visible = false;
            // 
            // colDetail
            // 
            this.colDetail.DataPropertyName = "detail";
            this.colDetail.HeaderText = "Detail";
            this.colDetail.Name = "colDetail";
            this.colDetail.ReadOnly = true;
            this.colDetail.Visible = false;
            // 
            // colHint
            // 
            this.colHint.DataPropertyName = "hint";
            this.colHint.HeaderText = "Hint";
            this.colHint.Name = "colHint";
            this.colHint.ReadOnly = true;
            this.colHint.Visible = false;
            // 
            // btnFilter
            // 
            this.btnFilter.AutoSize = true;
            this.btnFilter.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnFilter.Image = global::dqs.Properties.Resources.filter;
            this.btnFilter.Location = new System.Drawing.Point(0, 592);
            this.btnFilter.Name = "btnFilter";
            this.btnFilter.Size = new System.Drawing.Size(73, 31);
            this.btnFilter.TabIndex = 1;
            this.btnFilter.Text = "Filter";
            this.btnFilter.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFilter.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnFilter.UseVisualStyleBackColor = true;
            this.btnFilter.Click += new System.EventHandler(this.btnFilter_Click);
            // 
            // edtFtOrder2
            // 
            this.edtFtOrder2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.edtFtOrder2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.edtFtOrder2.FormattingEnabled = true;
            this.edtFtOrder2.Items.AddRange(new object[] {
            "ASC",
            "DESC"});
            this.edtFtOrder2.Location = new System.Drawing.Point(126, 331);
            this.edtFtOrder2.Name = "edtFtOrder2";
            this.edtFtOrder2.Size = new System.Drawing.Size(68, 23);
            this.edtFtOrder2.TabIndex = 16;
            // 
            // lblOrder
            // 
            this.lblOrder.AutoSize = true;
            this.lblOrder.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOrder.Location = new System.Drawing.Point(6, 313);
            this.lblOrder.Name = "lblOrder";
            this.lblOrder.Size = new System.Drawing.Size(57, 15);
            this.lblOrder.TabIndex = 14;
            this.lblOrder.Text = "Order By";
            // 
            // edtFtOrder
            // 
            this.edtFtOrder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtFtOrder.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.edtFtOrder.FormattingEnabled = true;
            this.edtFtOrder.Items.AddRange(new object[] {
            "Quest No.",
            "Name",
            "Location",
            "Requested By",
            "Response To"});
            this.edtFtOrder.Location = new System.Drawing.Point(12, 331);
            this.edtFtOrder.Name = "edtFtOrder";
            this.edtFtOrder.Size = new System.Drawing.Size(108, 23);
            this.edtFtOrder.TabIndex = 15;
            // 
            // lblFtCaption
            // 
            this.lblFtCaption.AutoSize = true;
            this.lblFtCaption.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFtCaption.Location = new System.Drawing.Point(6, 6);
            this.lblFtCaption.Name = "lblFtCaption";
            this.lblFtCaption.Size = new System.Drawing.Size(58, 15);
            this.lblFtCaption.TabIndex = 17;
            this.lblFtCaption.Text = "Filtrer By";
            // 
            // btnClear
            // 
            this.btnClear.AutoSize = true;
            this.btnClear.BackColor = System.Drawing.Color.Transparent;
            this.btnClear.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnClear.Image = global::dqs.Properties.Resources.eraser;
            this.btnClear.Location = new System.Drawing.Point(0, 530);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(227, 31);
            this.btnClear.TabIndex = 17;
            this.btnClear.Text = "C&lear";
            this.btnClear.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnClear.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnApply
            // 
            this.btnApply.AutoSize = true;
            this.btnApply.BackColor = System.Drawing.Color.Transparent;
            this.btnApply.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnApply.Image = global::dqs.Properties.Resources.check_bold;
            this.btnApply.Location = new System.Drawing.Point(0, 561);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(227, 31);
            this.btnApply.TabIndex = 18;
            this.btnApply.Text = "&Apply";
            this.btnApply.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnApply.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnApply.UseVisualStyleBackColor = false;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // btnFtAvail
            // 
            this.btnFtAvail.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnFtAvail.FlatAppearance.BorderSize = 0;
            this.btnFtAvail.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFtAvail.Location = new System.Drawing.Point(146, 186);
            this.btnFtAvail.Name = "btnFtAvail";
            this.btnFtAvail.Size = new System.Drawing.Size(125, 23);
            this.btnFtAvail.TabIndex = 7;
            this.btnFtAvail.Tag = "0";
            this.btnFtAvail.Text = "Available";
            this.btnFtAvail.UseVisualStyleBackColor = false;
            this.btnFtAvail.Click += new System.EventHandler(this.btnFt_Click);
            // 
            // btnFtLoc
            // 
            this.btnFtLoc.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnFtLoc.FlatAppearance.BorderSize = 0;
            this.btnFtLoc.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFtLoc.Location = new System.Drawing.Point(12, 186);
            this.btnFtLoc.Name = "btnFtLoc";
            this.btnFtLoc.Size = new System.Drawing.Size(125, 23);
            this.btnFtLoc.TabIndex = 6;
            this.btnFtLoc.Tag = "0";
            this.btnFtLoc.Text = "Location";
            this.btnFtLoc.UseVisualStyleBackColor = false;
            this.btnFtLoc.Click += new System.EventHandler(this.btnFt_Click);
            // 
            // btnFtHint
            // 
            this.btnFtHint.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnFtHint.FlatAppearance.BorderSize = 0;
            this.btnFtHint.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFtHint.Location = new System.Drawing.Point(146, 272);
            this.btnFtHint.Name = "btnFtHint";
            this.btnFtHint.Size = new System.Drawing.Size(125, 23);
            this.btnFtHint.TabIndex = 13;
            this.btnFtHint.Tag = "0";
            this.btnFtHint.Text = "Hint";
            this.btnFtHint.UseVisualStyleBackColor = false;
            this.btnFtHint.Click += new System.EventHandler(this.btnFt_Click);
            // 
            // btnFtDetail
            // 
            this.btnFtDetail.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnFtDetail.FlatAppearance.BorderSize = 0;
            this.btnFtDetail.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFtDetail.Location = new System.Drawing.Point(12, 272);
            this.btnFtDetail.Name = "btnFtDetail";
            this.btnFtDetail.Size = new System.Drawing.Size(125, 23);
            this.btnFtDetail.TabIndex = 12;
            this.btnFtDetail.Tag = "0";
            this.btnFtDetail.Text = "Detail";
            this.btnFtDetail.UseVisualStyleBackColor = false;
            this.btnFtDetail.Click += new System.EventHandler(this.btnFt_Click);
            // 
            // btnFtReward
            // 
            this.btnFtReward.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnFtReward.FlatAppearance.BorderSize = 0;
            this.btnFtReward.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFtReward.Location = new System.Drawing.Point(146, 243);
            this.btnFtReward.Name = "btnFtReward";
            this.btnFtReward.Size = new System.Drawing.Size(125, 23);
            this.btnFtReward.TabIndex = 11;
            this.btnFtReward.Tag = "0";
            this.btnFtReward.Text = "Reward";
            this.btnFtReward.UseVisualStyleBackColor = false;
            this.btnFtReward.Click += new System.EventHandler(this.btnFt_Click);
            // 
            // btnFtReq
            // 
            this.btnFtReq.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnFtReq.FlatAppearance.BorderSize = 0;
            this.btnFtReq.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFtReq.Location = new System.Drawing.Point(12, 243);
            this.btnFtReq.Name = "btnFtReq";
            this.btnFtReq.Size = new System.Drawing.Size(125, 23);
            this.btnFtReq.TabIndex = 10;
            this.btnFtReq.Tag = "0";
            this.btnFtReq.Text = "Requirement";
            this.btnFtReq.UseVisualStyleBackColor = false;
            this.btnFtReq.Click += new System.EventHandler(this.btnFt_Click);
            // 
            // btnFtResTo
            // 
            this.btnFtResTo.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnFtResTo.FlatAppearance.BorderSize = 0;
            this.btnFtResTo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFtResTo.Location = new System.Drawing.Point(146, 215);
            this.btnFtResTo.Name = "btnFtResTo";
            this.btnFtResTo.Size = new System.Drawing.Size(125, 23);
            this.btnFtResTo.TabIndex = 9;
            this.btnFtResTo.Tag = "0";
            this.btnFtResTo.Text = "Response To";
            this.btnFtResTo.UseVisualStyleBackColor = false;
            this.btnFtResTo.Click += new System.EventHandler(this.btnFt_Click);
            // 
            // btnFtReqBy
            // 
            this.btnFtReqBy.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnFtReqBy.FlatAppearance.BorderSize = 0;
            this.btnFtReqBy.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFtReqBy.Location = new System.Drawing.Point(12, 215);
            this.btnFtReqBy.Name = "btnFtReqBy";
            this.btnFtReqBy.Size = new System.Drawing.Size(125, 23);
            this.btnFtReqBy.TabIndex = 8;
            this.btnFtReqBy.Tag = "0";
            this.btnFtReqBy.Text = "Requested By";
            this.btnFtReqBy.UseVisualStyleBackColor = false;
            this.btnFtReqBy.Click += new System.EventHandler(this.btnFt_Click);
            // 
            // btnFtAll
            // 
            this.btnFtAll.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFtAll.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnFtAll.FlatAppearance.BorderSize = 0;
            this.btnFtAll.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFtAll.Location = new System.Drawing.Point(67, 156);
            this.btnFtAll.Name = "btnFtAll";
            this.btnFtAll.Size = new System.Drawing.Size(127, 23);
            this.btnFtAll.TabIndex = 5;
            this.btnFtAll.Tag = "0";
            this.btnFtAll.Text = "All Fields";
            this.btnFtAll.UseVisualStyleBackColor = false;
            this.btnFtAll.Click += new System.EventHandler(this.btnFtAll_Click);
            // 
            // sconQuests
            // 
            this.sconQuests.BackColor = System.Drawing.Color.White;
            this.sconQuests.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.sconQuests.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sconQuests.Location = new System.Drawing.Point(0, 0);
            this.sconQuests.Name = "sconQuests";
            // 
            // sconQuests.Panel1
            // 
            this.sconQuests.Panel1.Controls.Add(this.sconFilter);
            this.sconQuests.Panel1.Cursor = System.Windows.Forms.Cursors.Default;
            this.sconQuests.Panel1MinSize = 310;
            // 
            // sconQuests.Panel2
            // 
            this.sconQuests.Panel2.BackColor = System.Drawing.Color.White;
            this.sconQuests.Panel2.Controls.Add(this.edtHint);
            this.sconQuests.Panel2.Controls.Add(this.edtDetail);
            this.sconQuests.Panel2.Controls.Add(this.lblHint);
            this.sconQuests.Panel2.Controls.Add(this.edtAvail);
            this.sconQuests.Panel2.Controls.Add(this.lblAvailable);
            this.sconQuests.Panel2.Controls.Add(this.lin3);
            this.sconQuests.Panel2.Controls.Add(this.lblDetail);
            this.sconQuests.Panel2.Controls.Add(this.edtRequire);
            this.sconQuests.Panel2.Controls.Add(this.lblReward);
            this.sconQuests.Panel2.Controls.Add(this.lblRequire);
            this.sconQuests.Panel2.Controls.Add(this.lblRes);
            this.sconQuests.Panel2.Controls.Add(this.lblReq);
            this.sconQuests.Panel2.Controls.Add(this.edtReward);
            this.sconQuests.Panel2.Controls.Add(this.edtResTo);
            this.sconQuests.Panel2.Controls.Add(this.edtReqBy);
            this.sconQuests.Panel2.Controls.Add(this.lin2);
            this.sconQuests.Panel2.Controls.Add(this.edtLoc);
            this.sconQuests.Panel2.Controls.Add(this.lblLoc);
            this.sconQuests.Panel2.Controls.Add(this.lin1);
            this.sconQuests.Panel2.Controls.Add(this.edtCode);
            this.sconQuests.Panel2.Controls.Add(this.edtTitle);
            this.sconQuests.Size = new System.Drawing.Size(1050, 625);
            this.sconQuests.SplitterDistance = 310;
            this.sconQuests.SplitterWidth = 5;
            this.sconQuests.TabIndex = 1;
            this.sconQuests.TabStop = false;
            // 
            // edtHint
            // 
            this.edtHint.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtHint.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.edtHint.Location = new System.Drawing.Point(111, 447);
            this.edtHint.Name = "edtHint";
            this.edtHint.Size = new System.Drawing.Size(603, 163);
            this.edtHint.TabIndex = 81;
            this.edtHint.Text = "[hint]";
            // 
            // edtDetail
            // 
            this.edtDetail.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtDetail.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.edtDetail.Location = new System.Drawing.Point(111, 351);
            this.edtDetail.Name = "edtDetail";
            this.edtDetail.Size = new System.Drawing.Size(603, 85);
            this.edtDetail.TabIndex = 17;
            this.edtDetail.Text = "[details]";
            // 
            // lblHint
            // 
            this.lblHint.AutoSize = true;
            this.lblHint.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblHint.Location = new System.Drawing.Point(8, 447);
            this.lblHint.Name = "lblHint";
            this.lblHint.Size = new System.Drawing.Size(30, 15);
            this.lblHint.TabIndex = 80;
            this.lblHint.Text = "Hint";
            // 
            // edtAvail
            // 
            this.edtAvail.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtAvail.BackColor = System.Drawing.Color.White;
            this.edtAvail.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.edtAvail.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.edtAvail.Location = new System.Drawing.Point(111, 89);
            this.edtAvail.Name = "edtAvail";
            this.edtAvail.ReadOnly = true;
            this.edtAvail.Size = new System.Drawing.Size(603, 22);
            this.edtAvail.TabIndex = 5;
            this.edtAvail.Text = "[available]";
            // 
            // lblAvailable
            // 
            this.lblAvailable.AutoSize = true;
            this.lblAvailable.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblAvailable.Location = new System.Drawing.Point(8, 94);
            this.lblAvailable.Name = "lblAvailable";
            this.lblAvailable.Size = new System.Drawing.Size(55, 15);
            this.lblAvailable.TabIndex = 4;
            this.lblAvailable.Text = "Available";
            // 
            // frmQuests
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.sconQuests);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "frmQuests";
            this.Size = new System.Drawing.Size(1050, 625);
            this.sconFilter.Panel1.ResumeLayout(false);
            this.sconFilter.Panel1.PerformLayout();
            this.sconFilter.Panel2.ResumeLayout(false);
            this.sconFilter.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sconFilter)).EndInit();
            this.sconFilter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgQuests)).EndInit();
            this.sconQuests.Panel1.ResumeLayout(false);
            this.sconQuests.Panel2.ResumeLayout(false);
            this.sconQuests.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sconQuests)).EndInit();
            this.sconQuests.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel lin3;
        private System.Windows.Forms.Label lblDetail;
        private System.Windows.Forms.TextBox edtRequire;
        private System.Windows.Forms.Label lblReward;
        private System.Windows.Forms.Label lblRequire;
        private System.Windows.Forms.Label lblRes;
        private System.Windows.Forms.Label lblReq;
        private System.Windows.Forms.TextBox edtReward;
        private System.Windows.Forms.TextBox edtResTo;
        private System.Windows.Forms.TextBox edtReqBy;
        private System.Windows.Forms.Panel lin2;
        private System.Windows.Forms.TextBox edtLoc;
        private System.Windows.Forms.Label lblLoc;
        private System.Windows.Forms.Panel lin1;
        private System.Windows.Forms.TextBox edtCode;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblFtSearchIn;
        private System.Windows.Forms.Label lblFtKeyword;
        private System.Windows.Forms.Label lblFtTitle;
        private System.Windows.Forms.TextBox edtFtKey;
        private System.Windows.Forms.TextBox edtTitle;
        private System.Windows.Forms.TextBox edtFtTitle;
        private System.Windows.Forms.SplitContainer sconFilter;
        private System.Windows.Forms.DataGridView dgQuests;
        private System.Windows.Forms.Button btnFilter;
        private System.Windows.Forms.SplitContainer sconQuests;
        private System.Windows.Forms.TextBox edtAvail;
        private System.Windows.Forms.Label lblAvailable;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colTitle;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLocation;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAvailable;
        private System.Windows.Forms.DataGridViewTextBoxColumn colReqBy;
        private System.Windows.Forms.DataGridViewTextBoxColumn colResTo;
        private System.Windows.Forms.DataGridViewTextBoxColumn colRequire;
        private System.Windows.Forms.DataGridViewTextBoxColumn colReward;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDetail;
        private System.Windows.Forms.DataGridViewTextBoxColumn colHint;
        private System.Windows.Forms.Label lblHint;
        private System.Windows.Forms.Button btnFtAll;
        private System.Windows.Forms.Button btnFtHint;
        private System.Windows.Forms.Button btnFtDetail;
        private System.Windows.Forms.Button btnFtReward;
        private System.Windows.Forms.Button btnFtReq;
        private System.Windows.Forms.Button btnFtResTo;
        private System.Windows.Forms.Button btnFtReqBy;
        private System.Windows.Forms.Button btnFtAvail;
        private System.Windows.Forms.Button btnFtLoc;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnApply;
        private System.Windows.Forms.Label lblFtCaption;
        private System.Windows.Forms.ComboBox edtFtOrder2;
        private System.Windows.Forms.Label lblOrder;
        private System.Windows.Forms.ComboBox edtFtOrder;
        private System.Windows.Forms.RichTextBox edtDetail;
        private System.Windows.Forms.RichTextBox edtHint;
    }
}
