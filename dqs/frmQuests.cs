﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace dqs
{
    public partial class frmQuests : UserControl
    {
        private bool iUpdating;

        public frmQuests()
        {
            InitializeComponent();
            this.edtFtOrder.SelectedIndex = 0;
            this.edtFtOrder2.SelectedIndex = 0;
            this.sconFilter.Panel1Collapsed = false;
            this.sconFilter.Panel2Collapsed = true;
            UpdateUI();
            LoadData();
        }

        private void LoadData()
        {            
            void AppendStr(ref string ASource, string SubString)
            {
                if (ASource == "") { ASource = SubString; }
                else 
                { 
                    ASource = ASource + ", " + SubString;
                }
            }

            string ParseOrder(string raw)
            {
                raw = raw.ToLower();
                string result = "q.title";

                if (raw == "name") { result = "q.title"; }
                if (raw == "quest no.") { result = "q.code"; }
                if (raw == "location") { result = "q.location"; }
                if (raw == "response to") { result = "q.response_to"; }
                if (raw == "requested by") { result = "q.requested_by"; }
                return result;
            }

            iUpdating = true;
            string StrParam = "";
            string StrWhere = "WHERE (q.title <> '') ";
            string StrSelect = "SELECT q.id, q.code, q.title, " +
                "q.location, q.available, q.requested_by, q.response_to, " +
                "q.requirement, q.reward, q.detail, q.hint " +
                "FROM quests q ";

            if (btnFtLoc.Tag.ToString() == "1") { AppendStr(ref StrParam, "location"); }
            if (btnFtAvail.Tag.ToString() == "1") { AppendStr(ref StrParam, "available"); }
            if (btnFtReqBy.Tag.ToString() == "1") { AppendStr(ref StrParam, "requested_by"); }
            if (btnFtResTo.Tag.ToString() == "1") { AppendStr(ref StrParam, "response_to"); }
            if (btnFtReq.Tag.ToString() == "1") { AppendStr(ref StrParam, "requirement"); }
            if (btnFtReward.Tag.ToString() == "1") { AppendStr(ref StrParam, "reward"); }
            if (btnFtDetail.Tag.ToString() == "1") { AppendStr(ref StrParam, "detail"); }
            if (btnFtHint.Tag.ToString() == "1") { AppendStr(ref StrParam, "hint"); }

            if (edtFtKey.Text != "") 
            {
                StrParam = "CONCAT(" + StrParam + ")";
                StrWhere = StrWhere + "AND (LOWER(" + StrParam + ") like '%" + edtFtKey.Text.ToLower() + "%') ";
            }

            StrSelect = StrSelect + StrWhere + "ORDER BY " +
                ParseOrder(this.edtFtOrder.Text) + " " + this.edtFtOrder2.Text;
            DataTable Ttable = Program.DBConnection.SelectTable(StrSelect);
            BindingSource SBind = new BindingSource();
            SBind.DataSource = Ttable;

            dgQuests.AutoGenerateColumns = false;
            dgQuests.DataSource = Ttable;
            dgQuests.DataSource = SBind;
            dgQuests.Refresh();
            iUpdating = false;
            dgQuests_SelectionChanged(dgQuests, null);
            dgQuests.Focus();
        }

        private void btnFilter_Click(object sender, EventArgs e)
        {
            sconFilter.Panel1Collapsed = true;
            sconFilter.Panel2Collapsed = false;
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            edtFtTitle.Clear();
            edtFtKey.Clear();
            btnFtAll.Tag = 1;
            btnFtAll_Click(btnFtAll, e);
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            LoadData();
            sconFilter.Panel1Collapsed = false;
            sconFilter.Panel2Collapsed = true;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            sconFilter.Panel1Collapsed = false;
            sconFilter.Panel2Collapsed = true;
        }

        private void dgQuests_SelectionChanged(object sender, EventArgs e)
        {
            void LoadText(TextBox textBox, DataGridViewTextBoxColumn ColName, String format = "")
            {
                if (format == "")
                {
                    textBox.Text = dgQuests.SelectedRows[0].Cells[ColName.Index].Value.ToString();
                }
                else
                {
                    int ival = Int32.Parse(dgQuests.SelectedRows[0].Cells[ColName.Index].Value.ToString());
                    textBox.Text = ival.ToString(format);
                }
            }

            void LoadRich(RichTextBox textBox, DataGridViewTextBoxColumn ColName, String format = "")
            {
                if (format == "")
                {
                    textBox.Text = dgQuests.SelectedRows[0].Cells[ColName.Index].Value.ToString();
                }
                else
                {
                    int ival = Int32.Parse(dgQuests.SelectedRows[0].Cells[ColName.Index].Value.ToString());
                    textBox.Text = ival.ToString(format);
                }
            }

            if ((iUpdating == false) && (dgQuests.SelectedRows.Count > 0))
            {
                this.Cursor = Cursors.WaitCursor;
                LoadText(edtTitle, colTitle);
                LoadText(edtCode, colCode, "000");
                LoadText(edtLoc, colLocation);
                LoadText(edtAvail, colAvailable);
                LoadText(edtReqBy, colReqBy);
                LoadText(edtResTo, colResTo);
                LoadText(edtRequire, colRequire);
                LoadText(edtReward, colReward);
                LoadRich(edtDetail, colDetail);
                LoadRich(edtHint, colHint);
                this.Cursor = Cursors.Default;
                this.Cursor = Cursors.Default;
                this.Cursor = Cursors.Default;
            }
        }

        private void ToggleBtn(object sender, int ITag = -1)
        {
            if (ITag != -1)
            {
                (sender as Button).Tag = ITag;
            }
            else
            {
                if ((sender as Button).Tag.ToString() == "0") { (sender as Button).Tag = 1; }
                else { (sender as Button).Tag = 0; }
            }
            if ((sender as Button).Tag.ToString() == "0")
            {
                (sender as Button).BackColor = Color.FromName("ButtonFace");
                (sender as Button).ForeColor = Color.FromName("ControlText");
            }
            else
            {
                (sender as Button).BackColor = Properties.Settings.Default.colorPrimary; //Color.FromArgb(0, 151, 167);
                (sender as Button).ForeColor = Properties.Settings.Default.colorPriText; // Color.White;
            }
        }

        private void btnFtAll_Click(object sender, EventArgs e)
        {
            int iTag;
            if ((sender as Button).Tag.ToString() == "0")
            {
                iTag = 1;
            }
            else
            {
                iTag = 0;
            }
            (sender as Button).Tag = iTag;
            ToggleBtn(sender, iTag);
            ToggleBtn(btnFtLoc, iTag);
            ToggleBtn(btnFtAvail, iTag);
            ToggleBtn(btnFtReqBy, iTag);
            ToggleBtn(btnFtResTo, iTag);
            ToggleBtn(btnFtReq, iTag);
            ToggleBtn(btnFtReward, iTag);
            ToggleBtn(btnFtDetail, iTag);
            ToggleBtn(btnFtHint, iTag);
        }

        private void btnFt_Click(object sender, EventArgs e)
        {
            if ((sender as Button).Tag.ToString() == "0")
            {
                (sender as Button).Tag = 0;
            }
            else
            {
                (sender as Button).Tag = 1;
            }
            ToggleBtn(sender);
        }

        private void sconFilter_Panel2_Resize(object sender, EventArgs e)
        {
            int width = (sconFilter.Panel2.Width - 42) / 2;
            btnFtLoc.Width    = width;
            btnFtAvail.Width  = width;
            btnFtReqBy.Width  = width;
            btnFtResTo.Width  = width;
            btnFtReq.Width    = width;
            btnFtReward.Width = width;
            btnFtDetail.Width = width;
            btnFtHint.Width   = width;
            width = (width + 26);
            btnFtAvail.Location  = new Point(width, btnFtAvail.Location.Y);
            btnFtResTo.Location  = new Point(width, btnFtResTo.Location.Y);
            btnFtReward.Location = new Point(width, btnFtReward.Location.Y);
            btnFtHint.Location   = new Point(width, btnFtHint.Location.Y);
        }

        public void UpdateUI()
        {
            Color colorPrimary = Properties.Settings.Default.colorPrimary;
            Color colorPriText = Properties.Settings.Default.colorPriText;

            void UpdateButtons(Button button)
            {
                if (button.ForeColor != Color.FromName("ControlText"))
                {
                    button.BackColor = Properties.Settings.Default.colorPrimary; //Color.FromArgb(0, 151, 167);
                    button.ForeColor = Properties.Settings.Default.colorPriText; // Color.White;
                }
            }

            dgQuests.DefaultCellStyle.SelectionBackColor = colorPrimary;
            dgQuests.DefaultCellStyle.SelectionForeColor = colorPriText;
            UpdateButtons(btnFtAll);
            UpdateButtons(btnFtLoc);
            UpdateButtons(btnFtAvail);
            UpdateButtons(btnFtReqBy);
            UpdateButtons(btnFtResTo);
            UpdateButtons(btnFtReq);
            UpdateButtons(btnFtReward);
            UpdateButtons(btnFtDetail);
            UpdateButtons(btnFtHint);
            Program.ChangeButtonColor(btnFilter, colorPrimary);
            Program.ChangeButtonColor(btnClear, colorPrimary);
            Program.ChangeButtonColor(btnApply, colorPrimary);
            Program.ChangeButtonColor(btnCancel, colorPrimary);
        }

        public int GetSplitterDistance()
        {
            return sconQuests.SplitterDistance;
        }

        public void SetSplitterDistance(int split)
        {
            sconQuests.SplitterDistance = split;
        }
    }
}
