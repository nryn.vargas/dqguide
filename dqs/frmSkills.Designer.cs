﻿namespace dqs
{
    partial class frmSkills
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.lblMP = new System.Windows.Forms.Label();
            this.lblTarget = new System.Windows.Forms.Label();
            this.edtMP = new System.Windows.Forms.TextBox();
            this.lblFor = new System.Windows.Forms.Label();
            this.edtTarget = new System.Windows.Forms.TextBox();
            this.edtFor = new System.Windows.Forms.TextBox();
            this.edtDesc = new System.Windows.Forms.TextBox();
            this.lin1 = new System.Windows.Forms.Panel();
            this.picImage = new System.Windows.Forms.PictureBox();
            this.edtName = new System.Windows.Forms.TextBox();
            this.dgSkills = new System.Windows.Forms.DataGridView();
            this.colID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colFor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDesc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colSP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colMP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colFilename = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sconFilter = new System.Windows.Forms.SplitContainer();
            this.btnFilter = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnApply = new System.Windows.Forms.Button();
            this.edtFtFor2 = new System.Windows.Forms.ComboBox();
            this.edtFtFor = new System.Windows.Forms.ComboBox();
            this.lblFtFor = new System.Windows.Forms.Label();
            this.lblFtCaption = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.edtFtOrder2 = new System.Windows.Forms.ComboBox();
            this.lblOrder = new System.Windows.Forms.Label();
            this.edtFtOrder = new System.Windows.Forms.ComboBox();
            this.lblFtDesc = new System.Windows.Forms.Label();
            this.edtFtDesc = new System.Windows.Forms.TextBox();
            this.edtFtTarget = new System.Windows.Forms.ComboBox();
            this.lblFtTarget = new System.Windows.Forms.Label();
            this.lblFtName = new System.Windows.Forms.Label();
            this.edtFtName = new System.Windows.Forms.TextBox();
            this.sconSkl = new System.Windows.Forms.SplitContainer();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lin2 = new System.Windows.Forms.Panel();
            this.edtNotes = new System.Windows.Forms.TextBox();
            this.lblNotes = new System.Windows.Forms.Label();
            this.lblDesc = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.picImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgSkills)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sconFilter)).BeginInit();
            this.sconFilter.Panel1.SuspendLayout();
            this.sconFilter.Panel2.SuspendLayout();
            this.sconFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sconSkl)).BeginInit();
            this.sconSkl.Panel1.SuspendLayout();
            this.sconSkl.Panel2.SuspendLayout();
            this.sconSkl.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblMP
            // 
            this.lblMP.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.lblMP.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblMP.Location = new System.Drawing.Point(341, 195);
            this.lblMP.Name = "lblMP";
            this.lblMP.Size = new System.Drawing.Size(58, 17);
            this.lblMP.TabIndex = 11;
            this.lblMP.Text = "MP";
            // 
            // lblTarget
            // 
            this.lblTarget.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.lblTarget.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblTarget.Location = new System.Drawing.Point(3, 404);
            this.lblTarget.Name = "lblTarget";
            this.lblTarget.Size = new System.Drawing.Size(58, 17);
            this.lblTarget.TabIndex = 9;
            this.lblTarget.Text = "Target";
            this.lblTarget.Visible = false;
            // 
            // edtMP
            // 
            this.edtMP.BackColor = System.Drawing.Color.White;
            this.edtMP.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.edtMP.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.edtMP.Location = new System.Drawing.Point(442, 189);
            this.edtMP.Name = "edtMP";
            this.edtMP.ReadOnly = true;
            this.edtMP.Size = new System.Drawing.Size(426, 22);
            this.edtMP.TabIndex = 12;
            this.edtMP.Text = "[mp]";
            // 
            // lblFor
            // 
            this.lblFor.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.lblFor.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblFor.Location = new System.Drawing.Point(341, 241);
            this.lblFor.Name = "lblFor";
            this.lblFor.Size = new System.Drawing.Size(58, 17);
            this.lblFor.TabIndex = 7;
            this.lblFor.Text = "Skill For";
            // 
            // edtTarget
            // 
            this.edtTarget.BackColor = System.Drawing.Color.White;
            this.edtTarget.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.edtTarget.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.edtTarget.Location = new System.Drawing.Point(124, 404);
            this.edtTarget.Name = "edtTarget";
            this.edtTarget.ReadOnly = true;
            this.edtTarget.Size = new System.Drawing.Size(210, 22);
            this.edtTarget.TabIndex = 10;
            this.edtTarget.Text = "[target]";
            this.edtTarget.Visible = false;
            // 
            // edtFor
            // 
            this.edtFor.BackColor = System.Drawing.Color.White;
            this.edtFor.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.edtFor.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.edtFor.Location = new System.Drawing.Point(442, 235);
            this.edtFor.Name = "edtFor";
            this.edtFor.ReadOnly = true;
            this.edtFor.Size = new System.Drawing.Size(426, 22);
            this.edtFor.TabIndex = 8;
            this.edtFor.Text = "[skill_for]";
            // 
            // edtDesc
            // 
            this.edtDesc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtDesc.BackColor = System.Drawing.Color.White;
            this.edtDesc.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.edtDesc.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.edtDesc.Location = new System.Drawing.Point(442, 66);
            this.edtDesc.Multiline = true;
            this.edtDesc.Name = "edtDesc";
            this.edtDesc.ReadOnly = true;
            this.edtDesc.Size = new System.Drawing.Size(272, 108);
            this.edtDesc.TabIndex = 4;
            this.edtDesc.Text = "[description]";
            // 
            // lin1
            // 
            this.lin1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lin1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(66)))), ((int)(((byte)(66)))));
            this.lin1.Enabled = false;
            this.lin1.Location = new System.Drawing.Point(3, 51);
            this.lin1.Name = "lin1";
            this.lin1.Size = new System.Drawing.Size(711, 1);
            this.lin1.TabIndex = 3;
            // 
            // picImage
            // 
            this.picImage.BackColor = System.Drawing.Color.White;
            this.picImage.Enabled = false;
            this.picImage.Location = new System.Drawing.Point(6, 55);
            this.picImage.Name = "picImage";
            this.picImage.Size = new System.Drawing.Size(328, 217);
            this.picImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picImage.TabIndex = 1;
            this.picImage.TabStop = false;
            // 
            // edtName
            // 
            this.edtName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtName.BackColor = System.Drawing.Color.White;
            this.edtName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.edtName.Font = new System.Drawing.Font("Segoe UI", 20.25F);
            this.edtName.Location = new System.Drawing.Point(6, 3);
            this.edtName.Name = "edtName";
            this.edtName.ReadOnly = true;
            this.edtName.Size = new System.Drawing.Size(720, 36);
            this.edtName.TabIndex = 0;
            this.edtName.Text = "[name]";
            // 
            // dgSkills
            // 
            this.dgSkills.AllowUserToAddRows = false;
            this.dgSkills.AllowUserToDeleteRows = false;
            this.dgSkills.AllowUserToResizeColumns = false;
            this.dgSkills.AllowUserToResizeRows = false;
            this.dgSkills.BackgroundColor = System.Drawing.Color.White;
            this.dgSkills.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgSkills.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 9F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgSkills.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgSkills.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgSkills.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colID,
            this.colType,
            this.colFor,
            this.colName,
            this.colDesc,
            this.colSP,
            this.colMP,
            this.colFilename});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 9F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(151)))), ((int)(((byte)(167)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgSkills.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgSkills.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgSkills.GridColor = System.Drawing.SystemColors.ControlLight;
            this.dgSkills.Location = new System.Drawing.Point(0, 0);
            this.dgSkills.MultiSelect = false;
            this.dgSkills.Name = "dgSkills";
            this.dgSkills.ReadOnly = true;
            this.dgSkills.RowHeadersVisible = false;
            this.dgSkills.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgSkills.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgSkills.ShowEditingIcon = false;
            this.dgSkills.Size = new System.Drawing.Size(71, 530);
            this.dgSkills.StandardTab = true;
            this.dgSkills.TabIndex = 0;
            this.dgSkills.SelectionChanged += new System.EventHandler(this.dgSkills_SelectionChanged);
            // 
            // colID
            // 
            this.colID.DataPropertyName = "id";
            this.colID.HeaderText = "ID";
            this.colID.Name = "colID";
            this.colID.ReadOnly = true;
            this.colID.Visible = false;
            // 
            // colType
            // 
            this.colType.DataPropertyName = "skill_type";
            this.colType.HeaderText = "Skill Type";
            this.colType.Name = "colType";
            this.colType.ReadOnly = true;
            this.colType.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.colType.Visible = false;
            // 
            // colFor
            // 
            this.colFor.DataPropertyName = "skill_for";
            this.colFor.HeaderText = "Skill For";
            this.colFor.Name = "colFor";
            this.colFor.ReadOnly = true;
            this.colFor.Visible = false;
            // 
            // colName
            // 
            this.colName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colName.DataPropertyName = "name";
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colName.DefaultCellStyle = dataGridViewCellStyle2;
            this.colName.HeaderText = "Name";
            this.colName.Name = "colName";
            this.colName.ReadOnly = true;
            this.colName.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // colDesc
            // 
            this.colDesc.DataPropertyName = "description";
            this.colDesc.HeaderText = "Description";
            this.colDesc.Name = "colDesc";
            this.colDesc.ReadOnly = true;
            this.colDesc.Visible = false;
            // 
            // colSP
            // 
            this.colSP.DataPropertyName = "skill_point";
            this.colSP.HeaderText = "SP";
            this.colSP.Name = "colSP";
            this.colSP.ReadOnly = true;
            this.colSP.Visible = false;
            // 
            // colMP
            // 
            this.colMP.DataPropertyName = "mp";
            this.colMP.HeaderText = "MP";
            this.colMP.Name = "colMP";
            this.colMP.ReadOnly = true;
            this.colMP.Visible = false;
            // 
            // colFilename
            // 
            this.colFilename.DataPropertyName = "filename";
            this.colFilename.HeaderText = "Filename";
            this.colFilename.Name = "colFilename";
            this.colFilename.ReadOnly = true;
            this.colFilename.Visible = false;
            // 
            // sconFilter
            // 
            this.sconFilter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.sconFilter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sconFilter.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.sconFilter.IsSplitterFixed = true;
            this.sconFilter.Location = new System.Drawing.Point(0, 0);
            this.sconFilter.Name = "sconFilter";
            // 
            // sconFilter.Panel1
            // 
            this.sconFilter.Panel1.BackColor = System.Drawing.Color.White;
            this.sconFilter.Panel1.Controls.Add(this.dgSkills);
            this.sconFilter.Panel1.Controls.Add(this.btnFilter);
            // 
            // sconFilter.Panel2
            // 
            this.sconFilter.Panel2.Controls.Add(this.btnClear);
            this.sconFilter.Panel2.Controls.Add(this.btnApply);
            this.sconFilter.Panel2.Controls.Add(this.edtFtFor2);
            this.sconFilter.Panel2.Controls.Add(this.edtFtFor);
            this.sconFilter.Panel2.Controls.Add(this.lblFtFor);
            this.sconFilter.Panel2.Controls.Add(this.lblFtCaption);
            this.sconFilter.Panel2.Controls.Add(this.btnCancel);
            this.sconFilter.Panel2.Controls.Add(this.edtFtOrder2);
            this.sconFilter.Panel2.Controls.Add(this.lblOrder);
            this.sconFilter.Panel2.Controls.Add(this.edtFtOrder);
            this.sconFilter.Panel2.Controls.Add(this.lblFtDesc);
            this.sconFilter.Panel2.Controls.Add(this.edtFtDesc);
            this.sconFilter.Panel2.Controls.Add(this.edtFtTarget);
            this.sconFilter.Panel2.Controls.Add(this.lblFtTarget);
            this.sconFilter.Panel2.Controls.Add(this.lblFtName);
            this.sconFilter.Panel2.Controls.Add(this.edtFtName);
            this.sconFilter.Size = new System.Drawing.Size(310, 568);
            this.sconFilter.SplitterDistance = 73;
            this.sconFilter.SplitterWidth = 6;
            this.sconFilter.TabIndex = 1;
            // 
            // btnFilter
            // 
            this.btnFilter.AutoSize = true;
            this.btnFilter.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnFilter.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.btnFilter.Image = global::dqs.Properties.Resources.filter;
            this.btnFilter.Location = new System.Drawing.Point(0, 530);
            this.btnFilter.Name = "btnFilter";
            this.btnFilter.Size = new System.Drawing.Size(71, 36);
            this.btnFilter.TabIndex = 1;
            this.btnFilter.Text = "&Filter";
            this.btnFilter.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFilter.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnFilter.UseVisualStyleBackColor = true;
            this.btnFilter.Click += new System.EventHandler(this.btnFilter_Click);
            // 
            // btnClear
            // 
            this.btnClear.AutoSize = true;
            this.btnClear.BackColor = System.Drawing.Color.Transparent;
            this.btnClear.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnClear.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.btnClear.Image = global::dqs.Properties.Resources.eraser;
            this.btnClear.Location = new System.Drawing.Point(0, 458);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(229, 36);
            this.btnClear.TabIndex = 15;
            this.btnClear.Text = "C&lear";
            this.btnClear.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnClear.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnApply
            // 
            this.btnApply.AutoSize = true;
            this.btnApply.BackColor = System.Drawing.Color.Transparent;
            this.btnApply.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnApply.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.btnApply.Image = global::dqs.Properties.Resources.check_bold;
            this.btnApply.Location = new System.Drawing.Point(0, 494);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(229, 36);
            this.btnApply.TabIndex = 14;
            this.btnApply.Text = "&Apply";
            this.btnApply.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnApply.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnApply.UseVisualStyleBackColor = false;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // edtFtFor2
            // 
            this.edtFtFor2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtFtFor2.BackColor = System.Drawing.Color.White;
            this.edtFtFor2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.edtFtFor2.Enabled = false;
            this.edtFtFor2.FormattingEnabled = true;
            this.edtFtFor2.Location = new System.Drawing.Point(21, 179);
            this.edtFtFor2.Name = "edtFtFor2";
            this.edtFtFor2.Size = new System.Drawing.Size(175, 23);
            this.edtFtFor2.TabIndex = 9;
            // 
            // edtFtFor
            // 
            this.edtFtFor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtFtFor.BackColor = System.Drawing.Color.White;
            this.edtFtFor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.edtFtFor.FormattingEnabled = true;
            this.edtFtFor.Location = new System.Drawing.Point(21, 150);
            this.edtFtFor.Name = "edtFtFor";
            this.edtFtFor.Size = new System.Drawing.Size(175, 23);
            this.edtFtFor.TabIndex = 8;
            this.edtFtFor.SelectedIndexChanged += new System.EventHandler(this.edtFtFor_SelectedIndexChanged);
            // 
            // lblFtFor
            // 
            this.lblFtFor.AutoSize = true;
            this.lblFtFor.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.lblFtFor.Location = new System.Drawing.Point(18, 132);
            this.lblFtFor.Name = "lblFtFor";
            this.lblFtFor.Size = new System.Drawing.Size(48, 15);
            this.lblFtFor.TabIndex = 7;
            this.lblFtFor.Text = "Skill For";
            // 
            // lblFtCaption
            // 
            this.lblFtCaption.AutoSize = true;
            this.lblFtCaption.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFtCaption.Location = new System.Drawing.Point(6, 6);
            this.lblFtCaption.Name = "lblFtCaption";
            this.lblFtCaption.Size = new System.Drawing.Size(58, 15);
            this.lblFtCaption.TabIndex = 0;
            this.lblFtCaption.Text = "Filtrer By";
            // 
            // btnCancel
            // 
            this.btnCancel.AutoSize = true;
            this.btnCancel.BackColor = System.Drawing.Color.Transparent;
            this.btnCancel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnCancel.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.btnCancel.Image = global::dqs.Properties.Resources.close_thick;
            this.btnCancel.Location = new System.Drawing.Point(0, 530);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(229, 36);
            this.btnCancel.TabIndex = 13;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // edtFtOrder2
            // 
            this.edtFtOrder2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.edtFtOrder2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.edtFtOrder2.FormattingEnabled = true;
            this.edtFtOrder2.Items.AddRange(new object[] {
            "ASC",
            "DESC"});
            this.edtFtOrder2.Location = new System.Drawing.Point(130, 280);
            this.edtFtOrder2.Name = "edtFtOrder2";
            this.edtFtOrder2.Size = new System.Drawing.Size(66, 23);
            this.edtFtOrder2.TabIndex = 12;
            // 
            // lblOrder
            // 
            this.lblOrder.AutoSize = true;
            this.lblOrder.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.lblOrder.Location = new System.Drawing.Point(6, 262);
            this.lblOrder.Name = "lblOrder";
            this.lblOrder.Size = new System.Drawing.Size(57, 15);
            this.lblOrder.TabIndex = 10;
            this.lblOrder.Text = "Order By";
            // 
            // edtFtOrder
            // 
            this.edtFtOrder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtFtOrder.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.edtFtOrder.FormattingEnabled = true;
            this.edtFtOrder.Items.AddRange(new object[] {
            "Name",
            "Skill For",
            "MP"});
            this.edtFtOrder.Location = new System.Drawing.Point(21, 280);
            this.edtFtOrder.Name = "edtFtOrder";
            this.edtFtOrder.Size = new System.Drawing.Size(103, 23);
            this.edtFtOrder.TabIndex = 11;
            // 
            // lblFtDesc
            // 
            this.lblFtDesc.AutoSize = true;
            this.lblFtDesc.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.lblFtDesc.Location = new System.Drawing.Point(18, 82);
            this.lblFtDesc.Name = "lblFtDesc";
            this.lblFtDesc.Size = new System.Drawing.Size(67, 15);
            this.lblFtDesc.TabIndex = 3;
            this.lblFtDesc.Text = "Description";
            // 
            // edtFtDesc
            // 
            this.edtFtDesc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtFtDesc.Location = new System.Drawing.Point(21, 100);
            this.edtFtDesc.Name = "edtFtDesc";
            this.edtFtDesc.Size = new System.Drawing.Size(175, 23);
            this.edtFtDesc.TabIndex = 4;
            // 
            // edtFtTarget
            // 
            this.edtFtTarget.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtFtTarget.BackColor = System.Drawing.Color.White;
            this.edtFtTarget.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.edtFtTarget.FormattingEnabled = true;
            this.edtFtTarget.Location = new System.Drawing.Point(21, 330);
            this.edtFtTarget.Name = "edtFtTarget";
            this.edtFtTarget.Size = new System.Drawing.Size(175, 23);
            this.edtFtTarget.TabIndex = 6;
            this.edtFtTarget.Visible = false;
            // 
            // lblFtTarget
            // 
            this.lblFtTarget.AutoSize = true;
            this.lblFtTarget.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.lblFtTarget.Location = new System.Drawing.Point(18, 312);
            this.lblFtTarget.Name = "lblFtTarget";
            this.lblFtTarget.Size = new System.Drawing.Size(39, 15);
            this.lblFtTarget.TabIndex = 5;
            this.lblFtTarget.Text = "Target";
            this.lblFtTarget.Visible = false;
            // 
            // lblFtName
            // 
            this.lblFtName.AutoSize = true;
            this.lblFtName.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.lblFtName.Location = new System.Drawing.Point(18, 30);
            this.lblFtName.Name = "lblFtName";
            this.lblFtName.Size = new System.Drawing.Size(39, 15);
            this.lblFtName.TabIndex = 1;
            this.lblFtName.Text = "Name";
            // 
            // edtFtName
            // 
            this.edtFtName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtFtName.Location = new System.Drawing.Point(21, 50);
            this.edtFtName.Name = "edtFtName";
            this.edtFtName.Size = new System.Drawing.Size(175, 23);
            this.edtFtName.TabIndex = 2;
            // 
            // sconSkl
            // 
            this.sconSkl.BackColor = System.Drawing.Color.White;
            this.sconSkl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.sconSkl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sconSkl.Location = new System.Drawing.Point(0, 0);
            this.sconSkl.Name = "sconSkl";
            // 
            // sconSkl.Panel1
            // 
            this.sconSkl.Panel1.Controls.Add(this.sconFilter);
            this.sconSkl.Panel1.Cursor = System.Windows.Forms.Cursors.Default;
            this.sconSkl.Panel1MinSize = 310;
            // 
            // sconSkl.Panel2
            // 
            this.sconSkl.Panel2.BackColor = System.Drawing.Color.White;
            this.sconSkl.Panel2.Controls.Add(this.panel1);
            this.sconSkl.Panel2.Controls.Add(this.lin2);
            this.sconSkl.Panel2.Controls.Add(this.edtNotes);
            this.sconSkl.Panel2.Controls.Add(this.lblNotes);
            this.sconSkl.Panel2.Controls.Add(this.lblDesc);
            this.sconSkl.Panel2.Controls.Add(this.lblMP);
            this.sconSkl.Panel2.Controls.Add(this.lblTarget);
            this.sconSkl.Panel2.Controls.Add(this.edtMP);
            this.sconSkl.Panel2.Controls.Add(this.lblFor);
            this.sconSkl.Panel2.Controls.Add(this.edtTarget);
            this.sconSkl.Panel2.Controls.Add(this.edtFor);
            this.sconSkl.Panel2.Controls.Add(this.edtDesc);
            this.sconSkl.Panel2.Controls.Add(this.lin1);
            this.sconSkl.Panel2.Controls.Add(this.picImage);
            this.sconSkl.Panel2.Controls.Add(this.edtName);
            this.sconSkl.Size = new System.Drawing.Size(1053, 568);
            this.sconSkl.SplitterDistance = 310;
            this.sconSkl.SplitterWidth = 5;
            this.sconSkl.TabIndex = 1;
            this.sconSkl.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(66)))), ((int)(((byte)(66)))));
            this.panel1.Enabled = false;
            this.panel1.Location = new System.Drawing.Point(3, 279);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(711, 1);
            this.panel1.TabIndex = 27;
            // 
            // lin2
            // 
            this.lin2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lin2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(66)))), ((int)(((byte)(66)))));
            this.lin2.Enabled = false;
            this.lin2.Location = new System.Drawing.Point(341, 181);
            this.lin2.Name = "lin2";
            this.lin2.Size = new System.Drawing.Size(361, 1);
            this.lin2.TabIndex = 26;
            // 
            // edtNotes
            // 
            this.edtNotes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtNotes.BackColor = System.Drawing.Color.White;
            this.edtNotes.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.edtNotes.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.edtNotes.Location = new System.Drawing.Point(124, 298);
            this.edtNotes.Multiline = true;
            this.edtNotes.Name = "edtNotes";
            this.edtNotes.ReadOnly = true;
            this.edtNotes.Size = new System.Drawing.Size(591, 59);
            this.edtNotes.TabIndex = 25;
            this.edtNotes.Text = "[notes]";
            // 
            // lblNotes
            // 
            this.lblNotes.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.lblNotes.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblNotes.Location = new System.Drawing.Point(3, 303);
            this.lblNotes.Name = "lblNotes";
            this.lblNotes.Size = new System.Drawing.Size(94, 24);
            this.lblNotes.TabIndex = 24;
            this.lblNotes.Text = "Notes";
            // 
            // lblDesc
            // 
            this.lblDesc.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.lblDesc.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblDesc.Location = new System.Drawing.Point(341, 72);
            this.lblDesc.Name = "lblDesc";
            this.lblDesc.Size = new System.Drawing.Size(94, 24);
            this.lblDesc.TabIndex = 23;
            this.lblDesc.Text = "Description";
            // 
            // frmSkills
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.sconSkl);
            this.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.Name = "frmSkills";
            this.Size = new System.Drawing.Size(1053, 568);
            ((System.ComponentModel.ISupportInitialize)(this.picImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgSkills)).EndInit();
            this.sconFilter.Panel1.ResumeLayout(false);
            this.sconFilter.Panel1.PerformLayout();
            this.sconFilter.Panel2.ResumeLayout(false);
            this.sconFilter.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sconFilter)).EndInit();
            this.sconFilter.ResumeLayout(false);
            this.sconSkl.Panel1.ResumeLayout(false);
            this.sconSkl.Panel2.ResumeLayout(false);
            this.sconSkl.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sconSkl)).EndInit();
            this.sconSkl.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label lblMP;
        private System.Windows.Forms.Label lblTarget;
        private System.Windows.Forms.TextBox edtMP;
        private System.Windows.Forms.Label lblFor;
        private System.Windows.Forms.TextBox edtTarget;
        private System.Windows.Forms.TextBox edtFor;
        private System.Windows.Forms.TextBox edtDesc;
        private System.Windows.Forms.Panel lin1;
        private System.Windows.Forms.PictureBox picImage;
        private System.Windows.Forms.TextBox edtName;
        private System.Windows.Forms.DataGridView dgSkills;
        private System.Windows.Forms.SplitContainer sconFilter;
        private System.Windows.Forms.Button btnFilter;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.ComboBox edtFtOrder2;
        private System.Windows.Forms.Label lblOrder;
        private System.Windows.Forms.ComboBox edtFtOrder;
        private System.Windows.Forms.Label lblFtDesc;
        private System.Windows.Forms.TextBox edtFtDesc;
        private System.Windows.Forms.ComboBox edtFtTarget;
        private System.Windows.Forms.Label lblFtTarget;
        private System.Windows.Forms.Label lblFtName;
        private System.Windows.Forms.TextBox edtFtName;
        private System.Windows.Forms.SplitContainer sconSkl;
        private System.Windows.Forms.TextBox edtNotes;
        private System.Windows.Forms.Label lblNotes;
        private System.Windows.Forms.Label lblDesc;
        private System.Windows.Forms.Panel lin2;
        private System.Windows.Forms.Label lblFtCaption;
        private System.Windows.Forms.ComboBox edtFtFor2;
        private System.Windows.Forms.ComboBox edtFtFor;
        private System.Windows.Forms.Label lblFtFor;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnApply;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridViewTextBoxColumn colID;
        private System.Windows.Forms.DataGridViewTextBoxColumn colType;
        private System.Windows.Forms.DataGridViewTextBoxColumn colFor;
        private System.Windows.Forms.DataGridViewTextBoxColumn colName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDesc;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSP;
        private System.Windows.Forms.DataGridViewTextBoxColumn colMP;
        private System.Windows.Forms.DataGridViewTextBoxColumn colFilename;
    }
}
