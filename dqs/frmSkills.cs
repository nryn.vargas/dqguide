﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Data;
using System.IO;
using System.Windows.Forms;

namespace dqs
{
    public partial class frmSkills : UserControl
    {
        private bool iUpdating;
        public frmSkills()
        {
            InitializeComponent();
            InitializeSkillFor();
            InitializeSkillFor2();
            InitializeTarget();
            sconFilter.Panel1Collapsed = false;
            sconFilter.Panel2Collapsed = true;
            this.edtFtOrder.SelectedIndex = 0;
            this.edtFtOrder2.SelectedIndex = 0;
            UpdateUI();
            LoadData();
        }

        private void InitializeTarget()
        {
            List<string> list = Program.DBConnection.SelectAllVal("SELECT name FROM target_types");
            foreach (string item in list)
            {
                this.edtFtTarget.Items.Add(item);
            }
            this.edtFtTarget.SelectedIndex = 0;
        }

        private void InitializeSkillFor()
        {

            edtFtFor.Items.Clear();
            edtFtFor.Items.Add("Any/All");
            edtFtFor.Items.Add("Vocations");
            edtFtFor.Items.Add("Weapons");
            edtFtFor.SelectedIndex = 0;
        }

        private void InitializeSkillFor2()
        {
            void AddToItems(List<string> items)
            {
                foreach (string item in items)
                {
                    this.edtFtFor2.Items.Add(item);
                }
                this.edtFtFor2.SelectedIndex = 0;
            }

            edtFtFor2.Items.Clear();
            edtFtFor2.Items.Add("Any/All");
            edtFtFor2.SelectedIndex = 0;
            if (edtFtFor.Text == "Any/All")
            {
                edtFtFor2.Enabled = false;
            }
            else
            {
                edtFtFor2.Enabled = true;
                if (edtFtFor.Text == "Vocations")
                {
                    List<string> list = Program.DBConnection.SelectAllVal("SELECT name FROM vocations");
                    AddToItems(list);
                }
                else
                {
                    List<string> list = Program.DBConnection.SelectAllVal("SELECT name FROM weapons");
                    AddToItems(list);
                }
            }
        }

        private void edtFtFor_SelectedIndexChanged(object sender, EventArgs e)
        {
            InitializeSkillFor2();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            edtFtName.Clear();
            edtFtDesc.Clear();
            edtFtTarget.SelectedIndex = 0;
            edtFtFor.SelectedIndex = 0;
            edtFtFor2.SelectedIndex = 0;
            edtFtOrder.SelectedIndex = 0;
            edtFtOrder2.SelectedIndex = 0;
        }

        private void btnFilter_Click(object sender, EventArgs e)
        {
            sconFilter.Panel1Collapsed = true;
            sconFilter.Panel2Collapsed = false;
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            LoadData();
            sconFilter.Panel1Collapsed = false;
            sconFilter.Panel2Collapsed = true;
        }

        private void LoadData()
        {
            iUpdating = true;
            string StrWhere = "WHERE (name <> '') ";
            string StrSelect = "SELECT * FROM active_skills ";
            if (edtFtName.Text != "")
            {
                StrWhere = StrWhere + "AND (name LIKE '%" + edtFtName.Text + "%') ";
            }
            if (edtFtDesc.Text != "")
            {
                StrWhere = StrWhere + "AND (description LIKE '%" + edtFtDesc.Text + "%') ";
            }
            if (edtFtFor.Text == "Vocations")
            {
                StrWhere = StrWhere + "AND (skill_type = 'V') ";
            }
            else if (edtFtFor.Text == "Weapons")
            {
                StrWhere = StrWhere + "AND (skill_type = 'W') ";
            }
            if ((edtFtFor2.Text != "") & (edtFtFor2.Text != "Any/All"))
            {
                StrWhere = StrWhere + "AND (skill_for LIKE '%" + edtFtFor2.Text + "%') ";
            }
            string strOrder = "name";
            if (edtFtOrder.Text == "Skill For") 
            { 
                strOrder = "skill_type, skill_for"; 
            }
            else 
            { 
                strOrder = edtFtOrder.Text; 
            }
            StrSelect = StrSelect + StrWhere + "ORDER BY " +
                strOrder + " " + this.edtFtOrder2.Text;
            DataTable Ttable = Program.DBConnection.SelectTable(StrSelect);
            BindingSource SBind = new BindingSource();
            SBind.DataSource = Ttable;

            dgSkills.AutoGenerateColumns = false;
            dgSkills.DataSource = Ttable;
            dgSkills.DataSource = SBind;
            dgSkills.Refresh();
            iUpdating = false;
            dgSkills_SelectionChanged(dgSkills, null);
            dgSkills.Focus();
        }

        private void dgSkills_SelectionChanged(object sender, EventArgs e)
        {
            void LoadText(TextBox textBox, DataGridViewTextBoxColumn ColName)
            {
                textBox.Text = dgSkills.SelectedRows[0].Cells[ColName.Index].Value.ToString();
            }

            void LoadImage(PictureBox pictureBox, DataGridViewTextBoxColumn column)
            {
                string sfile = dgSkills.SelectedRows[0].Cells[column.Index].Value.ToString();
                Image img;
                Size newsize;
                img = (Image)Properties.Resources.ResourceManager.GetObject("placeholder");
                newsize = new Size((int)(img.Width), (int)(img.Height));
                picImage.Tag = -1;
                if (sfile != "")
                {
                    if (File.Exists("skills\\" + sfile))
                    {
                        sfile = "skills\\" + sfile;
                        try
                        {
                            picImage.Tag = 1;
                            img = Image.FromFile(sfile);
                        }
                        catch (Exception)
                        {
                            //picImage.Tag = -1;
                        }
                    }
                    if (picImage.Tag.Equals(1))
                    {
                        if ((img.Height < picImage.Height) || (img.Width < picImage.Width))
                        {
                            newsize.Width = (img.Width * 5);
                            newsize.Height = (img.Height * 5);
                            while ((newsize.Height > picImage.Height) || (newsize.Width > picImage.Width))
                            {
                                newsize.Height = (int)(0.9 * newsize.Height);
                                newsize.Width = (int)(0.9 * newsize.Width);
                            }
                        }
                        else
                        {
                            newsize = new Size((int)(img.Width * 0.9), (int)(img.Height * 0.9));
                            while ((newsize.Height > picImage.Height) || (newsize.Width > picImage.Width))
                            {
                                newsize.Height = (int)(0.9 * newsize.Height);
                                newsize.Width = (int)(0.9 * newsize.Width);
                            }
                        }
                    }
                }
                Bitmap bmp = new Bitmap(img, newsize);
                picImage.Image = bmp;
                img.Dispose();
                if (picImage.Tag.Equals(-1))
                {
                    Program.ChangePicBoxColor(picImage, Properties.Settings.Default.colorPrimary);
                }
            }

            if ((iUpdating == false) && (dgSkills.SelectedRows.Count > 0))
            {
                this.Cursor = Cursors.WaitCursor;
                edtNotes.Clear();
                LoadText(edtName, colName);
                LoadText(edtDesc, colDesc);
                //LoadText(edtTarget, colTarget);
                LoadText(edtFor, colFor);
                LoadText(edtMP, colMP);
                LoadImage(picImage, colFilename);
                this.Cursor = Cursors.Default;
                this.Cursor = Cursors.Default;
                this.Cursor = Cursors.Default;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            sconFilter.Panel1Collapsed = false;
            sconFilter.Panel2Collapsed = true;
        }

        public void UpdateUI()
        {
            Color colorPrimary = Properties.Settings.Default.colorPrimary;
            Color colorPriText = Properties.Settings.Default.colorPriText;

            dgSkills.DefaultCellStyle.SelectionBackColor = colorPrimary;
            dgSkills.DefaultCellStyle.SelectionForeColor = colorPriText;
            Program.ChangeButtonColor(btnFilter, colorPrimary);
            Program.ChangeButtonColor(btnClear, colorPrimary);
            Program.ChangeButtonColor(btnApply, colorPrimary);
            Program.ChangeButtonColor(btnCancel, colorPrimary);
        }

        public int GetSplitterDistance()
        {
            return sconSkl.SplitterDistance;
        }

        public void SetSplitterDistance(int split)
        {
            sconSkl.SplitterDistance = split;
        }
    }
}
