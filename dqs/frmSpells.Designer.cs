﻿namespace dqs
{
    partial class frmSpells
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            this.edtDesc = new System.Windows.Forms.TextBox();
            this.lin1 = new System.Windows.Forms.Panel();
            this.edtName = new System.Windows.Forms.TextBox();
            this.edtFtOrder2 = new System.Windows.Forms.ComboBox();
            this.lblOrder = new System.Windows.Forms.Label();
            this.edtFtOrder = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lblFtICasters = new System.Windows.Forms.Label();
            this.edtFtDesc = new System.Windows.Forms.TextBox();
            this.edtFtCasters = new System.Windows.Forms.TextBox();
            this.edtFtTarget = new System.Windows.Forms.ComboBox();
            this.lblFtTarget = new System.Windows.Forms.Label();
            this.lblFtName = new System.Windows.Forms.Label();
            this.edtFtName = new System.Windows.Forms.TextBox();
            this.dgSpells = new System.Windows.Forms.DataGridView();
            this.colID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colMP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colTarget = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDesc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCaster = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colFilename = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sconFilter = new System.Windows.Forms.SplitContainer();
            this.btnFilter = new System.Windows.Forms.Button();
            this.lblFtCaption = new System.Windows.Forms.Label();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnApply = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.sconSpl = new System.Windows.Forms.SplitContainer();
            this.dgCaster = new System.Windows.Forms.DataGridView();
            this.colCastLevel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCastVocation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblVoc = new System.Windows.Forms.Label();
            this.lin3 = new System.Windows.Forms.Panel();
            this.lblDesc = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.edtTarget = new System.Windows.Forms.TextBox();
            this.lblTarget = new System.Windows.Forms.Label();
            this.lblMP = new System.Windows.Forms.Label();
            this.edtMP = new System.Windows.Forms.TextBox();
            this.picImage = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgSpells)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sconFilter)).BeginInit();
            this.sconFilter.Panel1.SuspendLayout();
            this.sconFilter.Panel2.SuspendLayout();
            this.sconFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sconSpl)).BeginInit();
            this.sconSpl.Panel1.SuspendLayout();
            this.sconSpl.Panel2.SuspendLayout();
            this.sconSpl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgCaster)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picImage)).BeginInit();
            this.SuspendLayout();
            // 
            // edtDesc
            // 
            this.edtDesc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtDesc.BackColor = System.Drawing.Color.White;
            this.edtDesc.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.edtDesc.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.edtDesc.Location = new System.Drawing.Point(379, 49);
            this.edtDesc.Multiline = true;
            this.edtDesc.Name = "edtDesc";
            this.edtDesc.ReadOnly = true;
            this.edtDesc.Size = new System.Drawing.Size(382, 63);
            this.edtDesc.TabIndex = 5;
            this.edtDesc.Text = "[description]";
            // 
            // lin1
            // 
            this.lin1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lin1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(66)))), ((int)(((byte)(66)))));
            this.lin1.Enabled = false;
            this.lin1.Location = new System.Drawing.Point(3, 44);
            this.lin1.Name = "lin1";
            this.lin1.Size = new System.Drawing.Size(758, 1);
            this.lin1.TabIndex = 11;
            // 
            // edtName
            // 
            this.edtName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtName.BackColor = System.Drawing.Color.White;
            this.edtName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.edtName.Font = new System.Drawing.Font("Segoe UI", 20.25F);
            this.edtName.Location = new System.Drawing.Point(5, 3);
            this.edtName.Name = "edtName";
            this.edtName.ReadOnly = true;
            this.edtName.Size = new System.Drawing.Size(756, 36);
            this.edtName.TabIndex = 4;
            this.edtName.Text = "[name]";
            // 
            // edtFtOrder2
            // 
            this.edtFtOrder2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.edtFtOrder2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.edtFtOrder2.FormattingEnabled = true;
            this.edtFtOrder2.Items.AddRange(new object[] {
            "ASC",
            "DESC"});
            this.edtFtOrder2.Location = new System.Drawing.Point(130, 280);
            this.edtFtOrder2.Name = "edtFtOrder2";
            this.edtFtOrder2.Size = new System.Drawing.Size(68, 23);
            this.edtFtOrder2.TabIndex = 10;
            // 
            // lblOrder
            // 
            this.lblOrder.AutoSize = true;
            this.lblOrder.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOrder.Location = new System.Drawing.Point(6, 262);
            this.lblOrder.Name = "lblOrder";
            this.lblOrder.Size = new System.Drawing.Size(57, 15);
            this.lblOrder.TabIndex = 8;
            this.lblOrder.Text = "Order By";
            // 
            // edtFtOrder
            // 
            this.edtFtOrder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtFtOrder.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.edtFtOrder.FormattingEnabled = true;
            this.edtFtOrder.Items.AddRange(new object[] {
            "Name",
            "MP",
            "Target"});
            this.edtFtOrder.Location = new System.Drawing.Point(21, 280);
            this.edtFtOrder.Name = "edtFtOrder";
            this.edtFtOrder.Size = new System.Drawing.Size(103, 23);
            this.edtFtOrder.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 182);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 15);
            this.label3.TabIndex = 6;
            this.label3.Text = "Description";
            // 
            // lblFtICasters
            // 
            this.lblFtICasters.AutoSize = true;
            this.lblFtICasters.Location = new System.Drawing.Point(18, 132);
            this.lblFtICasters.Name = "lblFtICasters";
            this.lblFtICasters.Size = new System.Drawing.Size(45, 15);
            this.lblFtICasters.TabIndex = 4;
            this.lblFtICasters.Text = "Casters";
            // 
            // edtFtDesc
            // 
            this.edtFtDesc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtFtDesc.Location = new System.Drawing.Point(21, 200);
            this.edtFtDesc.Name = "edtFtDesc";
            this.edtFtDesc.Size = new System.Drawing.Size(175, 23);
            this.edtFtDesc.TabIndex = 7;
            // 
            // edtFtCasters
            // 
            this.edtFtCasters.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtFtCasters.Location = new System.Drawing.Point(21, 150);
            this.edtFtCasters.Name = "edtFtCasters";
            this.edtFtCasters.Size = new System.Drawing.Size(175, 23);
            this.edtFtCasters.TabIndex = 5;
            // 
            // edtFtTarget
            // 
            this.edtFtTarget.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtFtTarget.BackColor = System.Drawing.Color.White;
            this.edtFtTarget.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.edtFtTarget.FormattingEnabled = true;
            this.edtFtTarget.Location = new System.Drawing.Point(21, 100);
            this.edtFtTarget.Name = "edtFtTarget";
            this.edtFtTarget.Size = new System.Drawing.Size(175, 23);
            this.edtFtTarget.TabIndex = 3;
            // 
            // lblFtTarget
            // 
            this.lblFtTarget.AutoSize = true;
            this.lblFtTarget.Location = new System.Drawing.Point(18, 82);
            this.lblFtTarget.Name = "lblFtTarget";
            this.lblFtTarget.Size = new System.Drawing.Size(39, 15);
            this.lblFtTarget.TabIndex = 2;
            this.lblFtTarget.Text = "Target";
            // 
            // lblFtName
            // 
            this.lblFtName.AutoSize = true;
            this.lblFtName.Location = new System.Drawing.Point(18, 30);
            this.lblFtName.Name = "lblFtName";
            this.lblFtName.Size = new System.Drawing.Size(39, 15);
            this.lblFtName.TabIndex = 0;
            this.lblFtName.Text = "Name";
            // 
            // edtFtName
            // 
            this.edtFtName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtFtName.Location = new System.Drawing.Point(21, 50);
            this.edtFtName.Name = "edtFtName";
            this.edtFtName.Size = new System.Drawing.Size(175, 23);
            this.edtFtName.TabIndex = 1;
            // 
            // dgSpells
            // 
            this.dgSpells.AllowUserToAddRows = false;
            this.dgSpells.AllowUserToDeleteRows = false;
            this.dgSpells.AllowUserToResizeColumns = false;
            this.dgSpells.AllowUserToResizeRows = false;
            this.dgSpells.BackgroundColor = System.Drawing.Color.White;
            this.dgSpells.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgSpells.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgSpells.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle13;
            this.dgSpells.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgSpells.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colID,
            this.colName,
            this.colMP,
            this.colTarget,
            this.colDesc,
            this.colCaster,
            this.colFilename});
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(151)))), ((int)(((byte)(167)))));
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgSpells.DefaultCellStyle = dataGridViewCellStyle15;
            this.dgSpells.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgSpells.GridColor = System.Drawing.SystemColors.ControlLight;
            this.dgSpells.Location = new System.Drawing.Point(0, 0);
            this.dgSpells.MultiSelect = false;
            this.dgSpells.Name = "dgSpells";
            this.dgSpells.ReadOnly = true;
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle16.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle16.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgSpells.RowHeadersDefaultCellStyle = dataGridViewCellStyle16;
            this.dgSpells.RowHeadersVisible = false;
            this.dgSpells.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgSpells.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgSpells.ShowEditingIcon = false;
            this.dgSpells.Size = new System.Drawing.Size(73, 592);
            this.dgSpells.StandardTab = true;
            this.dgSpells.TabIndex = 0;
            this.dgSpells.SelectionChanged += new System.EventHandler(this.dgSpells_SelectionChanged);
            // 
            // colID
            // 
            this.colID.DataPropertyName = "id";
            this.colID.HeaderText = "ID";
            this.colID.Name = "colID";
            this.colID.ReadOnly = true;
            this.colID.Visible = false;
            // 
            // colName
            // 
            this.colName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colName.DataPropertyName = "name";
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colName.DefaultCellStyle = dataGridViewCellStyle14;
            this.colName.HeaderText = "Name";
            this.colName.Name = "colName";
            this.colName.ReadOnly = true;
            this.colName.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // colMP
            // 
            this.colMP.DataPropertyName = "mp";
            this.colMP.HeaderText = "MP";
            this.colMP.Name = "colMP";
            this.colMP.ReadOnly = true;
            this.colMP.Visible = false;
            // 
            // colTarget
            // 
            this.colTarget.DataPropertyName = "target";
            this.colTarget.HeaderText = "Target";
            this.colTarget.Name = "colTarget";
            this.colTarget.ReadOnly = true;
            this.colTarget.Visible = false;
            // 
            // colDesc
            // 
            this.colDesc.DataPropertyName = "description";
            this.colDesc.HeaderText = "Description";
            this.colDesc.Name = "colDesc";
            this.colDesc.ReadOnly = true;
            this.colDesc.Visible = false;
            // 
            // colCaster
            // 
            this.colCaster.DataPropertyName = "caster";
            this.colCaster.HeaderText = "Caster";
            this.colCaster.Name = "colCaster";
            this.colCaster.ReadOnly = true;
            this.colCaster.Visible = false;
            // 
            // colFilename
            // 
            this.colFilename.DataPropertyName = "filename";
            this.colFilename.HeaderText = "Filename";
            this.colFilename.Name = "colFilename";
            this.colFilename.ReadOnly = true;
            this.colFilename.Visible = false;
            // 
            // sconFilter
            // 
            this.sconFilter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.sconFilter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sconFilter.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.sconFilter.IsSplitterFixed = true;
            this.sconFilter.Location = new System.Drawing.Point(0, 0);
            this.sconFilter.Name = "sconFilter";
            // 
            // sconFilter.Panel1
            // 
            this.sconFilter.Panel1.BackColor = System.Drawing.Color.White;
            this.sconFilter.Panel1.Controls.Add(this.dgSpells);
            this.sconFilter.Panel1.Controls.Add(this.btnFilter);
            // 
            // sconFilter.Panel2
            // 
            this.sconFilter.Panel2.Controls.Add(this.lblFtCaption);
            this.sconFilter.Panel2.Controls.Add(this.btnClear);
            this.sconFilter.Panel2.Controls.Add(this.btnApply);
            this.sconFilter.Panel2.Controls.Add(this.edtFtOrder2);
            this.sconFilter.Panel2.Controls.Add(this.btnCancel);
            this.sconFilter.Panel2.Controls.Add(this.lblOrder);
            this.sconFilter.Panel2.Controls.Add(this.edtFtOrder);
            this.sconFilter.Panel2.Controls.Add(this.label3);
            this.sconFilter.Panel2.Controls.Add(this.lblFtICasters);
            this.sconFilter.Panel2.Controls.Add(this.edtFtDesc);
            this.sconFilter.Panel2.Controls.Add(this.edtFtCasters);
            this.sconFilter.Panel2.Controls.Add(this.edtFtTarget);
            this.sconFilter.Panel2.Controls.Add(this.lblFtTarget);
            this.sconFilter.Panel2.Controls.Add(this.lblFtName);
            this.sconFilter.Panel2.Controls.Add(this.edtFtName);
            this.sconFilter.Panel2MinSize = 210;
            this.sconFilter.Size = new System.Drawing.Size(310, 625);
            this.sconFilter.SplitterDistance = 75;
            this.sconFilter.SplitterWidth = 6;
            this.sconFilter.TabIndex = 1;
            // 
            // btnFilter
            // 
            this.btnFilter.AutoSize = true;
            this.btnFilter.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnFilter.Image = global::dqs.Properties.Resources.filter;
            this.btnFilter.Location = new System.Drawing.Point(0, 592);
            this.btnFilter.Name = "btnFilter";
            this.btnFilter.Size = new System.Drawing.Size(73, 31);
            this.btnFilter.TabIndex = 1;
            this.btnFilter.Text = "Filter";
            this.btnFilter.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFilter.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnFilter.UseVisualStyleBackColor = true;
            this.btnFilter.Click += new System.EventHandler(this.btnFilter_Click);
            // 
            // lblFtCaption
            // 
            this.lblFtCaption.AutoSize = true;
            this.lblFtCaption.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFtCaption.Location = new System.Drawing.Point(6, 6);
            this.lblFtCaption.Name = "lblFtCaption";
            this.lblFtCaption.Size = new System.Drawing.Size(58, 15);
            this.lblFtCaption.TabIndex = 14;
            this.lblFtCaption.Text = "Filtrer By";
            // 
            // btnClear
            // 
            this.btnClear.AutoSize = true;
            this.btnClear.BackColor = System.Drawing.Color.Transparent;
            this.btnClear.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnClear.Image = global::dqs.Properties.Resources.eraser;
            this.btnClear.Location = new System.Drawing.Point(0, 530);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(227, 31);
            this.btnClear.TabIndex = 11;
            this.btnClear.Text = "C&lear";
            this.btnClear.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnClear.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnApply
            // 
            this.btnApply.AutoSize = true;
            this.btnApply.BackColor = System.Drawing.Color.Transparent;
            this.btnApply.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnApply.Image = global::dqs.Properties.Resources.check_bold;
            this.btnApply.Location = new System.Drawing.Point(0, 561);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(227, 31);
            this.btnApply.TabIndex = 12;
            this.btnApply.Text = "&Apply";
            this.btnApply.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnApply.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnApply.UseVisualStyleBackColor = false;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.AutoSize = true;
            this.btnCancel.BackColor = System.Drawing.Color.Transparent;
            this.btnCancel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnCancel.Image = global::dqs.Properties.Resources.close_thick;
            this.btnCancel.Location = new System.Drawing.Point(0, 592);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(227, 31);
            this.btnCancel.TabIndex = 13;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // sconSpl
            // 
            this.sconSpl.BackColor = System.Drawing.Color.White;
            this.sconSpl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.sconSpl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sconSpl.Location = new System.Drawing.Point(0, 0);
            this.sconSpl.Name = "sconSpl";
            // 
            // sconSpl.Panel1
            // 
            this.sconSpl.Panel1.Controls.Add(this.sconFilter);
            this.sconSpl.Panel1.Cursor = System.Windows.Forms.Cursors.Default;
            this.sconSpl.Panel1MinSize = 310;
            // 
            // sconSpl.Panel2
            // 
            this.sconSpl.Panel2.BackColor = System.Drawing.Color.White;
            this.sconSpl.Panel2.Controls.Add(this.dgCaster);
            this.sconSpl.Panel2.Controls.Add(this.lblVoc);
            this.sconSpl.Panel2.Controls.Add(this.lin3);
            this.sconSpl.Panel2.Controls.Add(this.lblDesc);
            this.sconSpl.Panel2.Controls.Add(this.panel1);
            this.sconSpl.Panel2.Controls.Add(this.edtTarget);
            this.sconSpl.Panel2.Controls.Add(this.lblTarget);
            this.sconSpl.Panel2.Controls.Add(this.lblMP);
            this.sconSpl.Panel2.Controls.Add(this.edtMP);
            this.sconSpl.Panel2.Controls.Add(this.edtDesc);
            this.sconSpl.Panel2.Controls.Add(this.lin1);
            this.sconSpl.Panel2.Controls.Add(this.picImage);
            this.sconSpl.Panel2.Controls.Add(this.edtName);
            this.sconSpl.Size = new System.Drawing.Size(1100, 625);
            this.sconSpl.SplitterDistance = 310;
            this.sconSpl.SplitterWidth = 5;
            this.sconSpl.TabIndex = 1;
            this.sconSpl.TabStop = false;
            // 
            // dgCaster
            // 
            this.dgCaster.AllowUserToAddRows = false;
            this.dgCaster.AllowUserToDeleteRows = false;
            this.dgCaster.AllowUserToResizeRows = false;
            this.dgCaster.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgCaster.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgCaster.BackgroundColor = System.Drawing.Color.White;
            this.dgCaster.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgCaster.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dgCaster.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgCaster.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colCastLevel,
            this.colCastVocation});
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle18.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle18.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle18.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle18.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(151)))), ((int)(((byte)(167)))));
            dataGridViewCellStyle18.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgCaster.DefaultCellStyle = dataGridViewCellStyle18;
            this.dgCaster.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgCaster.Location = new System.Drawing.Point(5, 276);
            this.dgCaster.MultiSelect = false;
            this.dgCaster.Name = "dgCaster";
            this.dgCaster.ReadOnly = true;
            this.dgCaster.RowHeadersVisible = false;
            this.dgCaster.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgCaster.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgCaster.ShowEditingIcon = false;
            this.dgCaster.Size = new System.Drawing.Size(756, 344);
            this.dgCaster.StandardTab = true;
            this.dgCaster.TabIndex = 25;
            // 
            // colCastLevel
            // 
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle17.NullValue = null;
            this.colCastLevel.DefaultCellStyle = dataGridViewCellStyle17;
            this.colCastLevel.HeaderText = "Level";
            this.colCastLevel.Name = "colCastLevel";
            this.colCastLevel.ReadOnly = true;
            this.colCastLevel.Width = 85;
            // 
            // colCastVocation
            // 
            this.colCastVocation.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colCastVocation.HeaderText = "Vocation";
            this.colCastVocation.Name = "colCastVocation";
            this.colCastVocation.ReadOnly = true;
            this.colCastVocation.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // lblVoc
            // 
            this.lblVoc.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.lblVoc.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblVoc.Location = new System.Drawing.Point(7, 252);
            this.lblVoc.Name = "lblVoc";
            this.lblVoc.Size = new System.Drawing.Size(279, 21);
            this.lblVoc.TabIndex = 24;
            this.lblVoc.Text = "Available for";
            // 
            // lin3
            // 
            this.lin3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lin3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(66)))), ((int)(((byte)(66)))));
            this.lin3.Enabled = false;
            this.lin3.Location = new System.Drawing.Point(3, 243);
            this.lin3.Name = "lin3";
            this.lin3.Size = new System.Drawing.Size(758, 1);
            this.lin3.TabIndex = 23;
            // 
            // lblDesc
            // 
            this.lblDesc.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.lblDesc.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblDesc.Location = new System.Drawing.Point(292, 54);
            this.lblDesc.Name = "lblDesc";
            this.lblDesc.Size = new System.Drawing.Size(81, 21);
            this.lblDesc.TabIndex = 22;
            this.lblDesc.Text = "Description";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(66)))), ((int)(((byte)(66)))));
            this.panel1.Enabled = false;
            this.panel1.Location = new System.Drawing.Point(295, 129);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(458, 1);
            this.panel1.TabIndex = 12;
            // 
            // edtTarget
            // 
            this.edtTarget.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtTarget.BackColor = System.Drawing.Color.White;
            this.edtTarget.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.edtTarget.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.edtTarget.Location = new System.Drawing.Point(379, 183);
            this.edtTarget.Name = "edtTarget";
            this.edtTarget.ReadOnly = true;
            this.edtTarget.Size = new System.Drawing.Size(382, 22);
            this.edtTarget.TabIndex = 3;
            this.edtTarget.Text = "[target]";
            // 
            // lblTarget
            // 
            this.lblTarget.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.lblTarget.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblTarget.Location = new System.Drawing.Point(292, 188);
            this.lblTarget.Name = "lblTarget";
            this.lblTarget.Size = new System.Drawing.Size(81, 21);
            this.lblTarget.TabIndex = 2;
            this.lblTarget.Text = "Target";
            // 
            // lblMP
            // 
            this.lblMP.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.lblMP.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblMP.Location = new System.Drawing.Point(292, 145);
            this.lblMP.Name = "lblMP";
            this.lblMP.Size = new System.Drawing.Size(81, 21);
            this.lblMP.TabIndex = 0;
            this.lblMP.Text = "MP";
            // 
            // edtMP
            // 
            this.edtMP.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtMP.BackColor = System.Drawing.Color.White;
            this.edtMP.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.edtMP.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.edtMP.Location = new System.Drawing.Point(379, 140);
            this.edtMP.Name = "edtMP";
            this.edtMP.ReadOnly = true;
            this.edtMP.Size = new System.Drawing.Size(382, 22);
            this.edtMP.TabIndex = 1;
            this.edtMP.Text = "[mp]";
            // 
            // picImage
            // 
            this.picImage.BackColor = System.Drawing.Color.White;
            this.picImage.Enabled = false;
            this.picImage.Location = new System.Drawing.Point(5, 49);
            this.picImage.Name = "picImage";
            this.picImage.Size = new System.Drawing.Size(281, 188);
            this.picImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picImage.TabIndex = 1;
            this.picImage.TabStop = false;
            // 
            // frmSpells
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.sconSpl);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "frmSpells";
            this.Size = new System.Drawing.Size(1100, 625);
            ((System.ComponentModel.ISupportInitialize)(this.dgSpells)).EndInit();
            this.sconFilter.Panel1.ResumeLayout(false);
            this.sconFilter.Panel1.PerformLayout();
            this.sconFilter.Panel2.ResumeLayout(false);
            this.sconFilter.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sconFilter)).EndInit();
            this.sconFilter.ResumeLayout(false);
            this.sconSpl.Panel1.ResumeLayout(false);
            this.sconSpl.Panel2.ResumeLayout(false);
            this.sconSpl.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sconSpl)).EndInit();
            this.sconSpl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgCaster)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picImage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TextBox edtDesc;
        private System.Windows.Forms.Panel lin1;
        private System.Windows.Forms.PictureBox picImage;
        private System.Windows.Forms.TextBox edtName;
        private System.Windows.Forms.ComboBox edtFtOrder2;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblOrder;
        private System.Windows.Forms.ComboBox edtFtOrder;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblFtICasters;
        private System.Windows.Forms.TextBox edtFtDesc;
        private System.Windows.Forms.TextBox edtFtCasters;
        private System.Windows.Forms.ComboBox edtFtTarget;
        private System.Windows.Forms.Label lblFtTarget;
        private System.Windows.Forms.Label lblFtName;
        private System.Windows.Forms.TextBox edtFtName;
        private System.Windows.Forms.DataGridView dgSpells;
        private System.Windows.Forms.SplitContainer sconFilter;
        private System.Windows.Forms.Button btnFilter;
        private System.Windows.Forms.SplitContainer sconSpl;
        private System.Windows.Forms.TextBox edtTarget;
        private System.Windows.Forms.Label lblTarget;
        private System.Windows.Forms.Label lblMP;
        private System.Windows.Forms.TextBox edtMP;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridViewTextBoxColumn colID;
        private System.Windows.Forms.DataGridViewTextBoxColumn colName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colMP;
        private System.Windows.Forms.DataGridViewTextBoxColumn colTarget;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDesc;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCaster;
        private System.Windows.Forms.DataGridViewTextBoxColumn colFilename;
        private System.Windows.Forms.Label lblDesc;
        private System.Windows.Forms.Label lblVoc;
        private System.Windows.Forms.Panel lin3;
        private System.Windows.Forms.DataGridView dgCaster;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCastLevel;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCastVocation;
        private System.Windows.Forms.Button btnApply;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Label lblFtCaption;
    }
}
