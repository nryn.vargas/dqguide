﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Data;
using System.IO;
using System.Windows.Forms;

namespace dqs
{
    public partial class frmSpells : UserControl
	{
		private bool iUpdating;
		public frmSpells()
        {
            InitializeComponent();
			this.sconFilter.Panel2Collapsed = true;
			this.sconFilter.Panel1Collapsed = false;
			this.sconFilter.Panel2Collapsed = true;
			this.edtFtTarget.Items.Add("Any/All");
			List<string> list = Program.DBConnection.SelectAllVal(
				"SELECT name FROM target_types");
			foreach (string item in list)
			{
				this.edtFtTarget.Items.Add(item);
			}
			this.edtFtTarget.SelectedIndex = 0;
			this.edtFtOrder.SelectedIndex = 0;
			this.edtFtOrder2.SelectedIndex = 0;
			UpdateUI();
            LoadData();
		}

		private void LoadData()
		{
			iUpdating = true;
			string StrWhere = "WHERE (s.name <> '') ";
			string StrSelect = "SELECT s.id, s.name, s.mp, t.name target, s.caster, s.description, filename " +
				"FROM spells s " +
				"INNER JOIN target_types t ON s.target_type=t.id ";
			if (edtFtName.Text != "")
			{
				StrWhere = StrWhere + "AND (s.name LIKE '%" + edtFtName.Text + "%') ";
			}
			if (edtFtTarget.SelectedIndex > 0)
			{
				StrWhere = StrWhere + "AND (t.name LIKE '%" + edtFtTarget.Text + "%') ";
			}
			if (edtFtDesc.Text != "")
			{
				StrWhere = StrWhere + "AND (description LIKE '%" + edtFtDesc.Text + "%') ";
			}
			if (edtFtCasters.Text != "")
			{
				char[] delim = {','};
				string[] casters = edtFtCasters.Text.Split(delim);
				string StrWhereCaster = "";
				foreach (string caster in casters)
                {
					StrWhereCaster = StrWhereCaster + " OR (caster LIKE '%" + caster.Trim() + "%') ";
				}
                while (StrWhereCaster.StartsWith(" OR "))
                {
					StrWhereCaster = StrWhereCaster.Remove(0, 4);
				}
				StrWhere = StrWhere + "AND (" + StrWhereCaster + ") ";
			}
			StrSelect = StrSelect + StrWhere + "ORDER BY " +
				this.edtFtOrder.Text + " " + this.edtFtOrder2.Text;
			DataTable Ttable = Program.DBConnection.SelectTable(StrSelect);
			BindingSource SBind = new BindingSource();
			SBind.DataSource = Ttable;

			dgSpells.AutoGenerateColumns = false;
			dgSpells.DataSource = Ttable;
			dgSpells.DataSource = SBind;
			dgSpells.Refresh();
			iUpdating = false;
			dgSpells_SelectionChanged(dgSpells, null);
			dgSpells.Focus();
		}

		private void btnFilter_Click(object sender, EventArgs e)
		{
			sconFilter.Panel1Collapsed = true;
			sconFilter.Panel2Collapsed = false;
		}

		private void dgSpells_SelectionChanged(object sender, EventArgs e)
		{
			void LoadText(TextBox textBox, DataGridViewTextBoxColumn ColName)
			{
				textBox.Text = dgSpells.SelectedRows[0].Cells[ColName.Index].Value.ToString();
			}

			void LoadImage(PictureBox pictureBox, DataGridViewTextBoxColumn column)
            {
				string sfile = dgSpells.SelectedRows[0].Cells[column.Index].Value.ToString();
				Image img;
				Size newsize;
				img = (Image)Properties.Resources.ResourceManager.GetObject("placeholder");
				newsize = new Size((int)(img.Width), (int)(img.Height));
				picImage.Tag = -1;
				if (sfile != "")
				{
					if (File.Exists("spells\\" + sfile))
					{
						sfile = "spells\\" + sfile;
						try
						{
							picImage.Tag = 1;
							img = Image.FromFile(sfile);
						}
						catch (Exception)
						{
							//picImage.Tag = -1;
						}
					}
					if (picImage.Tag.Equals(1))
					{
						if ((img.Height < picImage.Height) || (img.Width < picImage.Width))
						{
							newsize.Width = (img.Width * 5);
							newsize.Height = (img.Height * 5);
							while ((newsize.Height > picImage.Height) || (newsize.Width > picImage.Width))
							{
								newsize.Height = (int)(0.9 * newsize.Height);
								newsize.Width = (int)(0.9 * newsize.Width);
							}
						}
						else
						{
							newsize = new Size((int)(img.Width * 0.9), (int)(img.Height * 0.9));
							while ((newsize.Height > picImage.Height) || (newsize.Width > picImage.Width))
							{
								newsize.Height = (int)(0.9 * newsize.Height);
								newsize.Width = (int)(0.9 * newsize.Width);
							}
						}
					}
				}
				Bitmap bmp = new Bitmap(img, newsize);
				picImage.Image = bmp;
				img.Dispose();
				if (picImage.Tag.Equals(-1))
				{
					Program.ChangePicBoxColor(picImage, Properties.Settings.Default.colorPrimary);
				}
			}

			void ParseCasters(string rawString)
			{
				char[] delim = { ',' };
				List<string> names = new List<string>();
				List<string> levels = new List<string>();

				string[] casters = rawString.Split(delim);
				delim[0] = ':';
				foreach (string caster in casters)
                {
					string[] val = caster.Split(delim);
					names.Add((val[0]).Trim());
					levels.Add((val[1]).Trim());
				}
                while (dgCaster.Rows.Count > 0)
                {
					dgCaster.Rows.RemoveAt(0);

				}
				foreach (string name in names)
				{
					int irw = dgCaster.Rows.Add();
					DataGridViewRow row = (DataGridViewRow)dgCaster.Rows[irw];
					int idx = names.IndexOf(name);
					row.Cells[1].Value = names[idx];
					row.Cells[0].Value = levels[idx];
				}
			}

			if ((iUpdating == false) && (dgSpells.SelectedRows.Count > 0))
			{
				this.Cursor = Cursors.WaitCursor;
				LoadText(edtName, colName);
				LoadText(edtDesc, colDesc);
				LoadText(edtMP, colMP);
				LoadText(edtTarget, colTarget);
				LoadImage(picImage, colFilename);
				ParseCasters(
					dgSpells.SelectedRows[0].Cells[colCaster.Index].Value.ToString());
				this.Cursor = Cursors.Default;
				this.Cursor = Cursors.Default;
				this.Cursor = Cursors.Default;
			}
		}

		private void btnClear_Click(object sender, EventArgs e)
		{
			edtFtName.Clear();
			edtFtDesc.Clear();
			edtFtCasters.Clear();
			edtFtTarget.SelectedIndex = 0;
			edtFtOrder.SelectedIndex = 0;
			edtFtOrder2.SelectedIndex = 0;
		}

		private void btnApply_Click(object sender, EventArgs e)
		{
			LoadData();
			sconFilter.Panel1Collapsed = false;
			sconFilter.Panel2Collapsed = true;
		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			sconFilter.Panel1Collapsed = false;
			sconFilter.Panel2Collapsed = true;
		}

		public void UpdateUI()
        {
            Color colorPrimary = Properties.Settings.Default.colorPrimary;
            Color colorPriText = Properties.Settings.Default.colorPriText;
			//
            dgSpells.DefaultCellStyle.SelectionBackColor = colorPrimary;
            dgSpells.DefaultCellStyle.SelectionForeColor = colorPriText;
			dgCaster.DefaultCellStyle.SelectionBackColor = colorPrimary;
			dgCaster.DefaultCellStyle.SelectionForeColor = colorPriText;
			Program.ChangeButtonColor(btnFilter, colorPrimary);
			Program.ChangeButtonColor(btnClear, colorPrimary);
			Program.ChangeButtonColor(btnApply, colorPrimary);
			Program.ChangeButtonColor(btnCancel, colorPrimary);
		}

		public int GetSplitterDistance()
		{
			return sconSpl.SplitterDistance;
		}

		public void SetSplitterDistance(int split)
		{
			sconSpl.SplitterDistance = split;
		}
    }
}
