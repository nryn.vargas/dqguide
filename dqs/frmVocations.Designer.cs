﻿namespace dqs
{
    partial class frmVocations
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            this.sconVoc = new System.Windows.Forms.SplitContainer();
            this.sconFilter = new System.Windows.Forms.SplitContainer();
            this.dgVoc = new System.Windows.Forms.DataGridView();
            this.colID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAbility = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDesc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCoup = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCoupDesc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colBuild = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colFilename = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colIAxes = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.colIBoomerangs = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.colIBows = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.colIClaws = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.colIFans = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.colIFisticuffs = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.colIHammers = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.colIKnives = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.colIShield = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.colISpears = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.colIStaves = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.colISwords = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.colIWands = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.colIWhips = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.btnFilter = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnApply = new System.Windows.Forms.Button();
            this.lblFtCaption = new System.Windows.Forms.Label();
            this.lblFWeapons = new System.Windows.Forms.Label();
            this.clFWeapons = new System.Windows.Forms.CheckedListBox();
            this.edtFtOrder2 = new System.Windows.Forms.ComboBox();
            this.lblOrder = new System.Windows.Forms.Label();
            this.edtFtOrder = new System.Windows.Forms.ComboBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.edtFtBuild = new System.Windows.Forms.ComboBox();
            this.lblFtBuild = new System.Windows.Forms.Label();
            this.lin4 = new System.Windows.Forms.Panel();
            this.lblWeapons = new System.Windows.Forms.Label();
            this.pnlWeapons = new System.Windows.Forms.Panel();
            this.lblWhips = new System.Windows.Forms.Label();
            this.lblAxes = new System.Windows.Forms.Label();
            this.lblBoomerangs = new System.Windows.Forms.Label();
            this.lblBows = new System.Windows.Forms.Label();
            this.lblWands = new System.Windows.Forms.Label();
            this.lblClaws = new System.Windows.Forms.Label();
            this.lblFans = new System.Windows.Forms.Label();
            this.lblSwords = new System.Windows.Forms.Label();
            this.lblFisticuffs = new System.Windows.Forms.Label();
            this.lblHammers = new System.Windows.Forms.Label();
            this.lblStaves = new System.Windows.Forms.Label();
            this.lblKnives = new System.Windows.Forms.Label();
            this.lblShields = new System.Windows.Forms.Label();
            this.lblSpears = new System.Windows.Forms.Label();
            this.lin3 = new System.Windows.Forms.Panel();
            this.pnl1 = new System.Windows.Forms.Panel();
            this.edtAbility = new System.Windows.Forms.TextBox();
            this.lblDesc = new System.Windows.Forms.Label();
            this.lblAbility = new System.Windows.Forms.Label();
            this.edtCoup = new System.Windows.Forms.TextBox();
            this.edtCoupDesc = new System.Windows.Forms.TextBox();
            this.edtDesc = new System.Windows.Forms.TextBox();
            this.lin2 = new System.Windows.Forms.Panel();
            this.lblCoup = new System.Windows.Forms.Label();
            this.picImage = new System.Windows.Forms.PictureBox();
            this.edtBuild = new System.Windows.Forms.TextBox();
            this.lblBuild = new System.Windows.Forms.Label();
            this.dgVocSkills = new System.Windows.Forms.DataGridView();
            this.colSP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colSkill = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colMP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colSkillDesc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lin1 = new System.Windows.Forms.Panel();
            this.edtName = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.sconVoc)).BeginInit();
            this.sconVoc.Panel1.SuspendLayout();
            this.sconVoc.Panel2.SuspendLayout();
            this.sconVoc.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sconFilter)).BeginInit();
            this.sconFilter.Panel1.SuspendLayout();
            this.sconFilter.Panel2.SuspendLayout();
            this.sconFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgVoc)).BeginInit();
            this.pnlWeapons.SuspendLayout();
            this.pnl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgVocSkills)).BeginInit();
            this.SuspendLayout();
            // 
            // sconVoc
            // 
            this.sconVoc.BackColor = System.Drawing.Color.White;
            this.sconVoc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.sconVoc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sconVoc.Location = new System.Drawing.Point(0, 0);
            this.sconVoc.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.sconVoc.Name = "sconVoc";
            // 
            // sconVoc.Panel1
            // 
            this.sconVoc.Panel1.Controls.Add(this.sconFilter);
            this.sconVoc.Panel1MinSize = 310;
            // 
            // sconVoc.Panel2
            // 
            this.sconVoc.Panel2.Controls.Add(this.lin4);
            this.sconVoc.Panel2.Controls.Add(this.lblWeapons);
            this.sconVoc.Panel2.Controls.Add(this.pnlWeapons);
            this.sconVoc.Panel2.Controls.Add(this.lin3);
            this.sconVoc.Panel2.Controls.Add(this.pnl1);
            this.sconVoc.Panel2.Controls.Add(this.edtBuild);
            this.sconVoc.Panel2.Controls.Add(this.lblBuild);
            this.sconVoc.Panel2.Controls.Add(this.dgVocSkills);
            this.sconVoc.Panel2.Controls.Add(this.lin1);
            this.sconVoc.Panel2.Controls.Add(this.edtName);
            this.sconVoc.Size = new System.Drawing.Size(1100, 625);
            this.sconVoc.SplitterDistance = 310;
            this.sconVoc.SplitterWidth = 5;
            this.sconVoc.TabIndex = 1;
            this.sconVoc.TabStop = false;
            // 
            // sconFilter
            // 
            this.sconFilter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.sconFilter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sconFilter.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.sconFilter.IsSplitterFixed = true;
            this.sconFilter.Location = new System.Drawing.Point(0, 0);
            this.sconFilter.Name = "sconFilter";
            // 
            // sconFilter.Panel1
            // 
            this.sconFilter.Panel1.BackColor = System.Drawing.Color.White;
            this.sconFilter.Panel1.Controls.Add(this.dgVoc);
            this.sconFilter.Panel1.Controls.Add(this.btnFilter);
            // 
            // sconFilter.Panel2
            // 
            this.sconFilter.Panel2.Controls.Add(this.btnClear);
            this.sconFilter.Panel2.Controls.Add(this.btnApply);
            this.sconFilter.Panel2.Controls.Add(this.lblFtCaption);
            this.sconFilter.Panel2.Controls.Add(this.lblFWeapons);
            this.sconFilter.Panel2.Controls.Add(this.clFWeapons);
            this.sconFilter.Panel2.Controls.Add(this.edtFtOrder2);
            this.sconFilter.Panel2.Controls.Add(this.lblOrder);
            this.sconFilter.Panel2.Controls.Add(this.edtFtOrder);
            this.sconFilter.Panel2.Controls.Add(this.btnCancel);
            this.sconFilter.Panel2.Controls.Add(this.edtFtBuild);
            this.sconFilter.Panel2.Controls.Add(this.lblFtBuild);
            this.sconFilter.Panel2MinSize = 210;
            this.sconFilter.Size = new System.Drawing.Size(310, 625);
            this.sconFilter.SplitterDistance = 75;
            this.sconFilter.SplitterWidth = 6;
            this.sconFilter.TabIndex = 2;
            // 
            // dgVoc
            // 
            this.dgVoc.AllowUserToAddRows = false;
            this.dgVoc.AllowUserToDeleteRows = false;
            this.dgVoc.AllowUserToResizeColumns = false;
            this.dgVoc.AllowUserToResizeRows = false;
            this.dgVoc.BackgroundColor = System.Drawing.Color.White;
            this.dgVoc.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgVoc.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgVoc.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgVoc.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgVoc.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colID,
            this.colName,
            this.colAbility,
            this.colDesc,
            this.colCoup,
            this.colCoupDesc,
            this.colBuild,
            this.colFilename,
            this.colIAxes,
            this.colIBoomerangs,
            this.colIBows,
            this.colIClaws,
            this.colIFans,
            this.colIFisticuffs,
            this.colIHammers,
            this.colIKnives,
            this.colIShield,
            this.colISpears,
            this.colIStaves,
            this.colISwords,
            this.colIWands,
            this.colIWhips});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(151)))), ((int)(((byte)(167)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgVoc.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgVoc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgVoc.GridColor = System.Drawing.SystemColors.ControlLight;
            this.dgVoc.Location = new System.Drawing.Point(0, 0);
            this.dgVoc.MultiSelect = false;
            this.dgVoc.Name = "dgVoc";
            this.dgVoc.ReadOnly = true;
            this.dgVoc.RowHeadersVisible = false;
            this.dgVoc.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgVoc.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgVoc.ShowEditingIcon = false;
            this.dgVoc.Size = new System.Drawing.Size(73, 592);
            this.dgVoc.StandardTab = true;
            this.dgVoc.TabIndex = 0;
            this.dgVoc.SelectionChanged += new System.EventHandler(this.dgVoc_SelectionChanged);
            // 
            // colID
            // 
            this.colID.DataPropertyName = "id";
            this.colID.HeaderText = "ID";
            this.colID.Name = "colID";
            this.colID.ReadOnly = true;
            this.colID.Visible = false;
            // 
            // colName
            // 
            this.colName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colName.DataPropertyName = "name";
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colName.DefaultCellStyle = dataGridViewCellStyle2;
            this.colName.HeaderText = "Name";
            this.colName.Name = "colName";
            this.colName.ReadOnly = true;
            this.colName.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // colAbility
            // 
            this.colAbility.DataPropertyName = "ability";
            this.colAbility.HeaderText = "Ability";
            this.colAbility.Name = "colAbility";
            this.colAbility.ReadOnly = true;
            this.colAbility.Visible = false;
            // 
            // colDesc
            // 
            this.colDesc.DataPropertyName = "description";
            this.colDesc.HeaderText = "Description";
            this.colDesc.Name = "colDesc";
            this.colDesc.ReadOnly = true;
            this.colDesc.Visible = false;
            // 
            // colCoup
            // 
            this.colCoup.DataPropertyName = "coup";
            this.colCoup.HeaderText = "Coup";
            this.colCoup.Name = "colCoup";
            this.colCoup.ReadOnly = true;
            this.colCoup.Visible = false;
            // 
            // colCoupDesc
            // 
            this.colCoupDesc.DataPropertyName = "coup_desc";
            this.colCoupDesc.HeaderText = "Coup Desc";
            this.colCoupDesc.Name = "colCoupDesc";
            this.colCoupDesc.ReadOnly = true;
            this.colCoupDesc.Visible = false;
            // 
            // colBuild
            // 
            this.colBuild.DataPropertyName = "build";
            this.colBuild.HeaderText = "Build";
            this.colBuild.Name = "colBuild";
            this.colBuild.ReadOnly = true;
            this.colBuild.Visible = false;
            // 
            // colFilename
            // 
            this.colFilename.DataPropertyName = "filename";
            this.colFilename.HeaderText = "Filename";
            this.colFilename.Name = "colFilename";
            this.colFilename.ReadOnly = true;
            this.colFilename.Visible = false;
            // 
            // colIAxes
            // 
            this.colIAxes.DataPropertyName = "is_axes";
            this.colIAxes.HeaderText = "Axes";
            this.colIAxes.IndeterminateValue = "";
            this.colIAxes.Name = "colIAxes";
            this.colIAxes.ReadOnly = true;
            this.colIAxes.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colIAxes.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.colIAxes.Visible = false;
            // 
            // colIBoomerangs
            // 
            this.colIBoomerangs.DataPropertyName = "is_boomerangs";
            this.colIBoomerangs.HeaderText = "Boomerangs";
            this.colIBoomerangs.IndeterminateValue = "";
            this.colIBoomerangs.Name = "colIBoomerangs";
            this.colIBoomerangs.ReadOnly = true;
            this.colIBoomerangs.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colIBoomerangs.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.colIBoomerangs.Visible = false;
            // 
            // colIBows
            // 
            this.colIBows.DataPropertyName = "is_bows";
            this.colIBows.HeaderText = "Bows";
            this.colIBows.Name = "colIBows";
            this.colIBows.ReadOnly = true;
            this.colIBows.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colIBows.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.colIBows.Visible = false;
            // 
            // colIClaws
            // 
            this.colIClaws.DataPropertyName = "is_claws";
            this.colIClaws.HeaderText = "Claws";
            this.colIClaws.Name = "colIClaws";
            this.colIClaws.ReadOnly = true;
            this.colIClaws.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colIClaws.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.colIClaws.Visible = false;
            // 
            // colIFans
            // 
            this.colIFans.DataPropertyName = "is_fans";
            this.colIFans.HeaderText = "Fans";
            this.colIFans.Name = "colIFans";
            this.colIFans.ReadOnly = true;
            this.colIFans.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colIFans.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.colIFans.Visible = false;
            // 
            // colIFisticuffs
            // 
            this.colIFisticuffs.DataPropertyName = "is_fisticuffs";
            this.colIFisticuffs.HeaderText = "Fisticuffs";
            this.colIFisticuffs.Name = "colIFisticuffs";
            this.colIFisticuffs.ReadOnly = true;
            this.colIFisticuffs.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colIFisticuffs.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.colIFisticuffs.Visible = false;
            // 
            // colIHammers
            // 
            this.colIHammers.DataPropertyName = "is_hammers";
            this.colIHammers.HeaderText = "Hammers";
            this.colIHammers.Name = "colIHammers";
            this.colIHammers.ReadOnly = true;
            this.colIHammers.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colIHammers.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.colIHammers.Visible = false;
            // 
            // colIKnives
            // 
            this.colIKnives.DataPropertyName = "is_knives";
            this.colIKnives.HeaderText = "Knives";
            this.colIKnives.Name = "colIKnives";
            this.colIKnives.ReadOnly = true;
            this.colIKnives.Visible = false;
            // 
            // colIShield
            // 
            this.colIShield.DataPropertyName = "is_shields";
            this.colIShield.HeaderText = "Shields";
            this.colIShield.Name = "colIShield";
            this.colIShield.ReadOnly = true;
            this.colIShield.Visible = false;
            // 
            // colISpears
            // 
            this.colISpears.DataPropertyName = "is_spears";
            this.colISpears.HeaderText = "Spears";
            this.colISpears.Name = "colISpears";
            this.colISpears.ReadOnly = true;
            this.colISpears.Visible = false;
            // 
            // colIStaves
            // 
            this.colIStaves.DataPropertyName = "is_staves";
            this.colIStaves.HeaderText = "Staves";
            this.colIStaves.Name = "colIStaves";
            this.colIStaves.ReadOnly = true;
            this.colIStaves.Visible = false;
            // 
            // colISwords
            // 
            this.colISwords.DataPropertyName = "is_swords";
            this.colISwords.HeaderText = "Swords";
            this.colISwords.Name = "colISwords";
            this.colISwords.ReadOnly = true;
            this.colISwords.Visible = false;
            // 
            // colIWands
            // 
            this.colIWands.DataPropertyName = "is_wands";
            this.colIWands.HeaderText = "Wands";
            this.colIWands.Name = "colIWands";
            this.colIWands.ReadOnly = true;
            this.colIWands.Visible = false;
            // 
            // colIWhips
            // 
            this.colIWhips.DataPropertyName = "is_whips";
            this.colIWhips.HeaderText = "Whips";
            this.colIWhips.Name = "colIWhips";
            this.colIWhips.ReadOnly = true;
            this.colIWhips.Visible = false;
            // 
            // btnFilter
            // 
            this.btnFilter.AutoSize = true;
            this.btnFilter.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnFilter.Image = global::dqs.Properties.Resources.filter;
            this.btnFilter.Location = new System.Drawing.Point(0, 592);
            this.btnFilter.Name = "btnFilter";
            this.btnFilter.Size = new System.Drawing.Size(73, 31);
            this.btnFilter.TabIndex = 1;
            this.btnFilter.Text = "Filter";
            this.btnFilter.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFilter.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnFilter.UseVisualStyleBackColor = true;
            this.btnFilter.Click += new System.EventHandler(this.btnFilter_Click);
            // 
            // btnClear
            // 
            this.btnClear.AutoSize = true;
            this.btnClear.BackColor = System.Drawing.Color.Transparent;
            this.btnClear.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnClear.Image = global::dqs.Properties.Resources.eraser;
            this.btnClear.Location = new System.Drawing.Point(0, 530);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(227, 31);
            this.btnClear.TabIndex = 7;
            this.btnClear.Text = "C&lear";
            this.btnClear.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnClear.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnApply
            // 
            this.btnApply.AutoSize = true;
            this.btnApply.BackColor = System.Drawing.Color.Transparent;
            this.btnApply.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnApply.Image = global::dqs.Properties.Resources.check_bold;
            this.btnApply.Location = new System.Drawing.Point(0, 561);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(227, 31);
            this.btnApply.TabIndex = 8;
            this.btnApply.Text = "&Apply";
            this.btnApply.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnApply.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnApply.UseVisualStyleBackColor = false;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // lblFtCaption
            // 
            this.lblFtCaption.AutoSize = true;
            this.lblFtCaption.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFtCaption.Location = new System.Drawing.Point(6, 6);
            this.lblFtCaption.Name = "lblFtCaption";
            this.lblFtCaption.Size = new System.Drawing.Size(58, 15);
            this.lblFtCaption.TabIndex = 8;
            this.lblFtCaption.Text = "Filtrer By";
            // 
            // lblFWeapons
            // 
            this.lblFWeapons.AutoSize = true;
            this.lblFWeapons.Location = new System.Drawing.Point(18, 82);
            this.lblFWeapons.Name = "lblFWeapons";
            this.lblFWeapons.Size = new System.Drawing.Size(56, 15);
            this.lblFWeapons.TabIndex = 2;
            this.lblFWeapons.Text = "Weapons";
            // 
            // clFWeapons
            // 
            this.clFWeapons.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.clFWeapons.CheckOnClick = true;
            this.clFWeapons.FormattingEnabled = true;
            this.clFWeapons.Items.AddRange(new object[] {
            "Axes",
            "Bows",
            "Fans",
            "Hammers",
            "Shields",
            "Staves",
            "Wands",
            "Boomerangs",
            "Claws",
            "Fisticuffs",
            "Knives",
            "Spears",
            "Swords",
            "Whips"});
            this.clFWeapons.Location = new System.Drawing.Point(21, 100);
            this.clFWeapons.MultiColumn = true;
            this.clFWeapons.Name = "clFWeapons";
            this.clFWeapons.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.clFWeapons.Size = new System.Drawing.Size(275, 126);
            this.clFWeapons.TabIndex = 3;
            this.clFWeapons.Click += new System.EventHandler(this.clFWeapons_Click);
            // 
            // edtFtOrder2
            // 
            this.edtFtOrder2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.edtFtOrder2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.edtFtOrder2.FormattingEnabled = true;
            this.edtFtOrder2.Items.AddRange(new object[] {
            "ASC",
            "DESC"});
            this.edtFtOrder2.Location = new System.Drawing.Point(130, 280);
            this.edtFtOrder2.Name = "edtFtOrder2";
            this.edtFtOrder2.Size = new System.Drawing.Size(68, 23);
            this.edtFtOrder2.TabIndex = 6;
            // 
            // lblOrder
            // 
            this.lblOrder.AutoSize = true;
            this.lblOrder.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOrder.Location = new System.Drawing.Point(6, 262);
            this.lblOrder.Name = "lblOrder";
            this.lblOrder.Size = new System.Drawing.Size(57, 15);
            this.lblOrder.TabIndex = 4;
            this.lblOrder.Text = "Order By";
            // 
            // edtFtOrder
            // 
            this.edtFtOrder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtFtOrder.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.edtFtOrder.FormattingEnabled = true;
            this.edtFtOrder.Items.AddRange(new object[] {
            "ID",
            "Name",
            "Build",
            "Ability",
            "Coup"});
            this.edtFtOrder.Location = new System.Drawing.Point(21, 280);
            this.edtFtOrder.Name = "edtFtOrder";
            this.edtFtOrder.Size = new System.Drawing.Size(103, 23);
            this.edtFtOrder.TabIndex = 5;
            // 
            // btnCancel
            // 
            this.btnCancel.AutoSize = true;
            this.btnCancel.BackColor = System.Drawing.Color.Transparent;
            this.btnCancel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnCancel.Image = global::dqs.Properties.Resources.close_thick;
            this.btnCancel.Location = new System.Drawing.Point(0, 592);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(227, 31);
            this.btnCancel.TabIndex = 9;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // edtFtBuild
            // 
            this.edtFtBuild.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtFtBuild.BackColor = System.Drawing.Color.White;
            this.edtFtBuild.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.edtFtBuild.FormattingEnabled = true;
            this.edtFtBuild.Items.AddRange(new object[] {
            "All",
            "Attacker",
            "Healer",
            "Spellcaster",
            "Tank"});
            this.edtFtBuild.Location = new System.Drawing.Point(21, 50);
            this.edtFtBuild.Name = "edtFtBuild";
            this.edtFtBuild.Size = new System.Drawing.Size(177, 23);
            this.edtFtBuild.TabIndex = 1;
            // 
            // lblFtBuild
            // 
            this.lblFtBuild.AutoSize = true;
            this.lblFtBuild.Location = new System.Drawing.Point(18, 30);
            this.lblFtBuild.Name = "lblFtBuild";
            this.lblFtBuild.Size = new System.Drawing.Size(34, 15);
            this.lblFtBuild.TabIndex = 0;
            this.lblFtBuild.Text = "Build";
            // 
            // lin4
            // 
            this.lin4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lin4.BackColor = System.Drawing.Color.Gray;
            this.lin4.Enabled = false;
            this.lin4.ForeColor = System.Drawing.Color.DarkGray;
            this.lin4.Location = new System.Drawing.Point(6, 368);
            this.lin4.Name = "lin4";
            this.lin4.Size = new System.Drawing.Size(754, 1);
            this.lin4.TabIndex = 16;
            // 
            // lblWeapons
            // 
            this.lblWeapons.AutoSize = true;
            this.lblWeapons.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWeapons.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblWeapons.Location = new System.Drawing.Point(3, 296);
            this.lblWeapons.Name = "lblWeapons";
            this.lblWeapons.Size = new System.Drawing.Size(56, 15);
            this.lblWeapons.TabIndex = 28;
            this.lblWeapons.Text = "Weapons";
            // 
            // pnlWeapons
            // 
            this.pnlWeapons.Controls.Add(this.lblWhips);
            this.pnlWeapons.Controls.Add(this.lblAxes);
            this.pnlWeapons.Controls.Add(this.lblBoomerangs);
            this.pnlWeapons.Controls.Add(this.lblBows);
            this.pnlWeapons.Controls.Add(this.lblWands);
            this.pnlWeapons.Controls.Add(this.lblClaws);
            this.pnlWeapons.Controls.Add(this.lblFans);
            this.pnlWeapons.Controls.Add(this.lblSwords);
            this.pnlWeapons.Controls.Add(this.lblFisticuffs);
            this.pnlWeapons.Controls.Add(this.lblHammers);
            this.pnlWeapons.Controls.Add(this.lblStaves);
            this.pnlWeapons.Controls.Add(this.lblKnives);
            this.pnlWeapons.Controls.Add(this.lblShields);
            this.pnlWeapons.Controls.Add(this.lblSpears);
            this.pnlWeapons.Location = new System.Drawing.Point(175, 288);
            this.pnlWeapons.Name = "pnlWeapons";
            this.pnlWeapons.Size = new System.Drawing.Size(602, 62);
            this.pnlWeapons.TabIndex = 27;
            // 
            // lblWhips
            // 
            this.lblWhips.BackColor = System.Drawing.Color.LightGray;
            this.lblWhips.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblWhips.Location = new System.Drawing.Point(519, 33);
            this.lblWhips.Name = "lblWhips";
            this.lblWhips.Size = new System.Drawing.Size(80, 24);
            this.lblWhips.TabIndex = 35;
            this.lblWhips.Text = "Whips";
            this.lblWhips.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblAxes
            // 
            this.lblAxes.BackColor = System.Drawing.Color.LightGray;
            this.lblAxes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblAxes.Location = new System.Drawing.Point(3, 3);
            this.lblAxes.Name = "lblAxes";
            this.lblAxes.Size = new System.Drawing.Size(80, 24);
            this.lblAxes.TabIndex = 22;
            this.lblAxes.Text = "Axes";
            this.lblAxes.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblBoomerangs
            // 
            this.lblBoomerangs.BackColor = System.Drawing.Color.LightGray;
            this.lblBoomerangs.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblBoomerangs.Location = new System.Drawing.Point(89, 3);
            this.lblBoomerangs.Name = "lblBoomerangs";
            this.lblBoomerangs.Size = new System.Drawing.Size(80, 24);
            this.lblBoomerangs.TabIndex = 23;
            this.lblBoomerangs.Text = "Boomerangs";
            this.lblBoomerangs.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblBows
            // 
            this.lblBows.BackColor = System.Drawing.Color.LightGray;
            this.lblBows.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblBows.Location = new System.Drawing.Point(175, 3);
            this.lblBows.Name = "lblBows";
            this.lblBows.Size = new System.Drawing.Size(80, 24);
            this.lblBows.TabIndex = 24;
            this.lblBows.Text = "Bows";
            this.lblBows.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblWands
            // 
            this.lblWands.BackColor = System.Drawing.Color.LightGray;
            this.lblWands.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblWands.Location = new System.Drawing.Point(433, 33);
            this.lblWands.Name = "lblWands";
            this.lblWands.Size = new System.Drawing.Size(80, 24);
            this.lblWands.TabIndex = 34;
            this.lblWands.Text = "Wands";
            this.lblWands.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblClaws
            // 
            this.lblClaws.BackColor = System.Drawing.Color.LightGray;
            this.lblClaws.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblClaws.Location = new System.Drawing.Point(261, 3);
            this.lblClaws.Name = "lblClaws";
            this.lblClaws.Size = new System.Drawing.Size(80, 24);
            this.lblClaws.TabIndex = 25;
            this.lblClaws.Text = "Claws";
            this.lblClaws.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblFans
            // 
            this.lblFans.BackColor = System.Drawing.Color.LightGray;
            this.lblFans.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblFans.Location = new System.Drawing.Point(347, 3);
            this.lblFans.Name = "lblFans";
            this.lblFans.Size = new System.Drawing.Size(80, 24);
            this.lblFans.TabIndex = 26;
            this.lblFans.Text = "Fans";
            this.lblFans.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblSwords
            // 
            this.lblSwords.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(151)))), ((int)(((byte)(167)))));
            this.lblSwords.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblSwords.ForeColor = System.Drawing.Color.White;
            this.lblSwords.Location = new System.Drawing.Point(347, 33);
            this.lblSwords.Name = "lblSwords";
            this.lblSwords.Size = new System.Drawing.Size(80, 24);
            this.lblSwords.TabIndex = 33;
            this.lblSwords.Text = "Swords";
            this.lblSwords.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblFisticuffs
            // 
            this.lblFisticuffs.BackColor = System.Drawing.Color.LightGray;
            this.lblFisticuffs.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblFisticuffs.Location = new System.Drawing.Point(433, 3);
            this.lblFisticuffs.Name = "lblFisticuffs";
            this.lblFisticuffs.Size = new System.Drawing.Size(80, 24);
            this.lblFisticuffs.TabIndex = 27;
            this.lblFisticuffs.Text = "Fisticuffs";
            this.lblFisticuffs.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblHammers
            // 
            this.lblHammers.BackColor = System.Drawing.Color.LightGray;
            this.lblHammers.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblHammers.Location = new System.Drawing.Point(519, 3);
            this.lblHammers.Name = "lblHammers";
            this.lblHammers.Size = new System.Drawing.Size(80, 24);
            this.lblHammers.TabIndex = 28;
            this.lblHammers.Text = "Hammers";
            this.lblHammers.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblStaves
            // 
            this.lblStaves.BackColor = System.Drawing.Color.LightGray;
            this.lblStaves.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblStaves.Location = new System.Drawing.Point(261, 33);
            this.lblStaves.Name = "lblStaves";
            this.lblStaves.Size = new System.Drawing.Size(80, 24);
            this.lblStaves.TabIndex = 32;
            this.lblStaves.Text = "Staves";
            this.lblStaves.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblKnives
            // 
            this.lblKnives.BackColor = System.Drawing.Color.LightGray;
            this.lblKnives.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblKnives.Location = new System.Drawing.Point(3, 33);
            this.lblKnives.Name = "lblKnives";
            this.lblKnives.Size = new System.Drawing.Size(80, 24);
            this.lblKnives.TabIndex = 29;
            this.lblKnives.Text = "Knives";
            this.lblKnives.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblShields
            // 
            this.lblShields.BackColor = System.Drawing.Color.LightGray;
            this.lblShields.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblShields.Location = new System.Drawing.Point(89, 33);
            this.lblShields.Name = "lblShields";
            this.lblShields.Size = new System.Drawing.Size(80, 24);
            this.lblShields.TabIndex = 30;
            this.lblShields.Text = "Shields";
            this.lblShields.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblSpears
            // 
            this.lblSpears.BackColor = System.Drawing.Color.LightGray;
            this.lblSpears.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblSpears.Location = new System.Drawing.Point(175, 33);
            this.lblSpears.Name = "lblSpears";
            this.lblSpears.Size = new System.Drawing.Size(80, 24);
            this.lblSpears.TabIndex = 31;
            this.lblSpears.Text = "Spears";
            this.lblSpears.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lin3
            // 
            this.lin3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lin3.BackColor = System.Drawing.Color.Gray;
            this.lin3.Enabled = false;
            this.lin3.ForeColor = System.Drawing.Color.DarkGray;
            this.lin3.Location = new System.Drawing.Point(3, 276);
            this.lin3.Name = "lin3";
            this.lin3.Size = new System.Drawing.Size(754, 1);
            this.lin3.TabIndex = 15;
            // 
            // pnl1
            // 
            this.pnl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnl1.Controls.Add(this.edtAbility);
            this.pnl1.Controls.Add(this.lblDesc);
            this.pnl1.Controls.Add(this.lblAbility);
            this.pnl1.Controls.Add(this.edtCoup);
            this.pnl1.Controls.Add(this.edtCoupDesc);
            this.pnl1.Controls.Add(this.edtDesc);
            this.pnl1.Controls.Add(this.lin2);
            this.pnl1.Controls.Add(this.lblCoup);
            this.pnl1.Controls.Add(this.picImage);
            this.pnl1.Location = new System.Drawing.Point(3, 45);
            this.pnl1.Name = "pnl1";
            this.pnl1.Size = new System.Drawing.Size(757, 225);
            this.pnl1.TabIndex = 25;
            // 
            // edtAbility
            // 
            this.edtAbility.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtAbility.BackColor = System.Drawing.Color.White;
            this.edtAbility.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.edtAbility.Font = new System.Drawing.Font("Segoe UI", 14.25F);
            this.edtAbility.Location = new System.Drawing.Point(315, 21);
            this.edtAbility.Name = "edtAbility";
            this.edtAbility.ReadOnly = true;
            this.edtAbility.Size = new System.Drawing.Size(439, 26);
            this.edtAbility.TabIndex = 26;
            this.edtAbility.Text = "[ability]";
            // 
            // lblDesc
            // 
            this.lblDesc.BackColor = System.Drawing.Color.White;
            this.lblDesc.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDesc.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblDesc.Location = new System.Drawing.Point(191, 57);
            this.lblDesc.Name = "lblDesc";
            this.lblDesc.Size = new System.Drawing.Size(118, 24);
            this.lblDesc.TabIndex = 28;
            this.lblDesc.Text = "Description";
            this.lblDesc.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblAbility
            // 
            this.lblAbility.BackColor = System.Drawing.Color.White;
            this.lblAbility.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAbility.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblAbility.Location = new System.Drawing.Point(191, 23);
            this.lblAbility.Name = "lblAbility";
            this.lblAbility.Size = new System.Drawing.Size(118, 26);
            this.lblAbility.TabIndex = 27;
            this.lblAbility.Text = "Ability";
            this.lblAbility.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // edtCoup
            // 
            this.edtCoup.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtCoup.BackColor = System.Drawing.Color.White;
            this.edtCoup.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.edtCoup.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.edtCoup.Location = new System.Drawing.Point(315, 143);
            this.edtCoup.Name = "edtCoup";
            this.edtCoup.ReadOnly = true;
            this.edtCoup.Size = new System.Drawing.Size(439, 26);
            this.edtCoup.TabIndex = 4;
            this.edtCoup.Text = "[coup]";
            // 
            // edtCoupDesc
            // 
            this.edtCoupDesc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtCoupDesc.BackColor = System.Drawing.Color.White;
            this.edtCoupDesc.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.edtCoupDesc.Location = new System.Drawing.Point(315, 175);
            this.edtCoupDesc.Multiline = true;
            this.edtCoupDesc.Name = "edtCoupDesc";
            this.edtCoupDesc.ReadOnly = true;
            this.edtCoupDesc.Size = new System.Drawing.Size(439, 44);
            this.edtCoupDesc.TabIndex = 5;
            this.edtCoupDesc.Text = "[coup_desc]";
            // 
            // edtDesc
            // 
            this.edtDesc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtDesc.BackColor = System.Drawing.Color.White;
            this.edtDesc.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.edtDesc.Location = new System.Drawing.Point(315, 62);
            this.edtDesc.Multiline = true;
            this.edtDesc.Name = "edtDesc";
            this.edtDesc.ReadOnly = true;
            this.edtDesc.Size = new System.Drawing.Size(439, 48);
            this.edtDesc.TabIndex = 2;
            this.edtDesc.Text = "[description]";
            // 
            // lin2
            // 
            this.lin2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lin2.BackColor = System.Drawing.Color.Gray;
            this.lin2.Enabled = false;
            this.lin2.ForeColor = System.Drawing.Color.DarkGray;
            this.lin2.Location = new System.Drawing.Point(191, 126);
            this.lin2.Name = "lin2";
            this.lin2.Size = new System.Drawing.Size(554, 1);
            this.lin2.TabIndex = 14;
            // 
            // lblCoup
            // 
            this.lblCoup.BackColor = System.Drawing.Color.White;
            this.lblCoup.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCoup.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblCoup.Location = new System.Drawing.Point(191, 142);
            this.lblCoup.Name = "lblCoup";
            this.lblCoup.Size = new System.Drawing.Size(118, 32);
            this.lblCoup.TabIndex = 3;
            this.lblCoup.Text = "Coup de Grâce";
            this.lblCoup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // picImage
            // 
            this.picImage.BackColor = System.Drawing.Color.White;
            this.picImage.Enabled = false;
            this.picImage.Location = new System.Drawing.Point(3, 3);
            this.picImage.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.picImage.Name = "picImage";
            this.picImage.Size = new System.Drawing.Size(182, 216);
            this.picImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picImage.TabIndex = 4;
            this.picImage.TabStop = false;
            // 
            // edtBuild
            // 
            this.edtBuild.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtBuild.BackColor = System.Drawing.Color.White;
            this.edtBuild.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.edtBuild.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.edtBuild.Location = new System.Drawing.Point(175, 383);
            this.edtBuild.Name = "edtBuild";
            this.edtBuild.ReadOnly = true;
            this.edtBuild.Size = new System.Drawing.Size(582, 26);
            this.edtBuild.TabIndex = 7;
            this.edtBuild.Text = "[build]";
            // 
            // lblBuild
            // 
            this.lblBuild.AutoSize = true;
            this.lblBuild.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBuild.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblBuild.Location = new System.Drawing.Point(6, 391);
            this.lblBuild.Name = "lblBuild";
            this.lblBuild.Size = new System.Drawing.Size(131, 15);
            this.lblBuild.TabIndex = 6;
            this.lblBuild.Text = "▼ Recommended Build";
            this.lblBuild.Click += new System.EventHandler(this.lblBuild_Click);
            // 
            // dgVocSkills
            // 
            this.dgVocSkills.AllowUserToAddRows = false;
            this.dgVocSkills.AllowUserToDeleteRows = false;
            this.dgVocSkills.AllowUserToResizeRows = false;
            this.dgVocSkills.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgVocSkills.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgVocSkills.BackgroundColor = System.Drawing.Color.White;
            this.dgVocSkills.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgVocSkills.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dgVocSkills.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgVocSkills.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colSP,
            this.colSkill,
            this.colMP,
            this.colSkillDesc});
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(151)))), ((int)(((byte)(167)))));
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgVocSkills.DefaultCellStyle = dataGridViewCellStyle8;
            this.dgVocSkills.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgVocSkills.GridColor = System.Drawing.SystemColors.ControlLight;
            this.dgVocSkills.Location = new System.Drawing.Point(175, 415);
            this.dgVocSkills.MultiSelect = false;
            this.dgVocSkills.Name = "dgVocSkills";
            this.dgVocSkills.ReadOnly = true;
            this.dgVocSkills.RowHeadersVisible = false;
            this.dgVocSkills.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgVocSkills.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgVocSkills.ShowEditingIcon = false;
            this.dgVocSkills.Size = new System.Drawing.Size(599, 193);
            this.dgVocSkills.StandardTab = true;
            this.dgVocSkills.TabIndex = 8;
            this.dgVocSkills.Visible = false;
            this.dgVocSkills.Resize += new System.EventHandler(this.dgVocSkills_Resize);
            // 
            // colSP
            // 
            this.colSP.DataPropertyName = "skill_point";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.colSP.DefaultCellStyle = dataGridViewCellStyle4;
            this.colSP.HeaderText = "SP";
            this.colSP.Name = "colSP";
            this.colSP.ReadOnly = true;
            this.colSP.Width = 50;
            // 
            // colSkill
            // 
            this.colSkill.DataPropertyName = "skill";
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.colSkill.DefaultCellStyle = dataGridViewCellStyle5;
            this.colSkill.HeaderText = "Skill";
            this.colSkill.Name = "colSkill";
            this.colSkill.ReadOnly = true;
            this.colSkill.Width = 150;
            // 
            // colMP
            // 
            this.colMP.DataPropertyName = "mp";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.colMP.DefaultCellStyle = dataGridViewCellStyle6;
            this.colMP.HeaderText = "MP";
            this.colMP.Name = "colMP";
            this.colMP.ReadOnly = true;
            this.colMP.Width = 50;
            // 
            // colSkillDesc
            // 
            this.colSkillDesc.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colSkillDesc.DataPropertyName = "description";
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.colSkillDesc.DefaultCellStyle = dataGridViewCellStyle7;
            this.colSkillDesc.HeaderText = "Description";
            this.colSkillDesc.Name = "colSkillDesc";
            this.colSkillDesc.ReadOnly = true;
            // 
            // lin1
            // 
            this.lin1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lin1.BackColor = System.Drawing.Color.Gray;
            this.lin1.Enabled = false;
            this.lin1.ForeColor = System.Drawing.Color.DarkGray;
            this.lin1.Location = new System.Drawing.Point(3, 44);
            this.lin1.Name = "lin1";
            this.lin1.Padding = new System.Windows.Forms.Padding(0, 10, 0, 0);
            this.lin1.Size = new System.Drawing.Size(754, 1);
            this.lin1.TabIndex = 13;
            // 
            // edtName
            // 
            this.edtName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtName.BackColor = System.Drawing.Color.White;
            this.edtName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.edtName.Font = new System.Drawing.Font("Segoe UI", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.edtName.ForeColor = System.Drawing.SystemColors.ControlText;
            this.edtName.Location = new System.Drawing.Point(5, 3);
            this.edtName.Name = "edtName";
            this.edtName.ReadOnly = true;
            this.edtName.Size = new System.Drawing.Size(752, 36);
            this.edtName.TabIndex = 0;
            this.edtName.Text = "[vocation_name]";
            // 
            // frmVocations
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.sconVoc);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frmVocations";
            this.Size = new System.Drawing.Size(1100, 625);
            this.sconVoc.Panel1.ResumeLayout(false);
            this.sconVoc.Panel2.ResumeLayout(false);
            this.sconVoc.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sconVoc)).EndInit();
            this.sconVoc.ResumeLayout(false);
            this.sconFilter.Panel1.ResumeLayout(false);
            this.sconFilter.Panel1.PerformLayout();
            this.sconFilter.Panel2.ResumeLayout(false);
            this.sconFilter.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sconFilter)).EndInit();
            this.sconFilter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgVoc)).EndInit();
            this.pnlWeapons.ResumeLayout(false);
            this.pnl1.ResumeLayout(false);
            this.pnl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgVocSkills)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer sconVoc;
        private System.Windows.Forms.SplitContainer sconFilter;
        private System.Windows.Forms.ComboBox edtFtBuild;
        private System.Windows.Forms.Label lblFtBuild;
        private System.Windows.Forms.Button btnFilter;
        private System.Windows.Forms.DataGridView dgVoc;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.ComboBox edtFtOrder2;
        private System.Windows.Forms.Label lblOrder;
        private System.Windows.Forms.ComboBox edtFtOrder;
        private System.Windows.Forms.Panel lin1;
        private System.Windows.Forms.TextBox edtName;
        private System.Windows.Forms.Label lblCoup;
        private System.Windows.Forms.Panel lin2;
        private System.Windows.Forms.TextBox edtDesc;
        private System.Windows.Forms.TextBox edtCoupDesc;
        private System.Windows.Forms.TextBox edtCoup;
        private System.Windows.Forms.DataGridViewTextBoxColumn colID;
        private System.Windows.Forms.DataGridViewTextBoxColumn colName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAbility;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDesc;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCoup;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCoupDesc;
        private System.Windows.Forms.DataGridViewTextBoxColumn colBuild;
        private System.Windows.Forms.DataGridViewTextBoxColumn colFilename;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colIAxes;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colIBoomerangs;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colIBows;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colIClaws;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colIFans;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colIFisticuffs;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colIHammers;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colIKnives;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colIShield;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colISpears;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colIStaves;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colISwords;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colIWands;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colIWhips;
        private System.Windows.Forms.DataGridView dgVocSkills;
        private System.Windows.Forms.CheckedListBox clFWeapons;
        private System.Windows.Forms.Label lblFWeapons;
        private System.Windows.Forms.Label lblBuild;
        private System.Windows.Forms.TextBox edtBuild;
        private System.Windows.Forms.Panel pnl1;
        private System.Windows.Forms.Label lblWhips;
        private System.Windows.Forms.Label lblWands;
        private System.Windows.Forms.Label lblSwords;
        private System.Windows.Forms.Label lblStaves;
        private System.Windows.Forms.Label lblSpears;
        private System.Windows.Forms.Label lblShields;
        private System.Windows.Forms.Label lblKnives;
        private System.Windows.Forms.Label lblHammers;
        private System.Windows.Forms.Label lblFisticuffs;
        private System.Windows.Forms.Label lblFans;
        private System.Windows.Forms.Label lblClaws;
        private System.Windows.Forms.Label lblBows;
        private System.Windows.Forms.Label lblBoomerangs;
        private System.Windows.Forms.Label lblAxes;
        private System.Windows.Forms.PictureBox picImage;
        private System.Windows.Forms.Panel lin3;
        private System.Windows.Forms.Label lblAbility;
        private System.Windows.Forms.TextBox edtAbility;
        private System.Windows.Forms.Label lblDesc;
        private System.Windows.Forms.Button btnApply;
        private System.Windows.Forms.Label lblFtCaption;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSP;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSkill;
        private System.Windows.Forms.DataGridViewTextBoxColumn colMP;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSkillDesc;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Label lblWeapons;
        private System.Windows.Forms.Panel pnlWeapons;
        private System.Windows.Forms.Panel lin4;
    }
}
