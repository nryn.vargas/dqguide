﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace dqs
{
    public partial class frmVocations : UserControl
    {
		private bool iUpdating; 

		public frmVocations()
        {
            InitializeComponent();
			UpdateUI();
			this.edtFtBuild.SelectedIndex = 0;
			this.edtFtOrder.SelectedIndex = 0;
			this.edtFtOrder2.SelectedIndex = 0;
			this.sconFilter.Panel1Collapsed = false;
			this.sconFilter.Panel2Collapsed = true;
			LoadData();
		}

		private void LoadData()
		{
			iUpdating = true;
			string StrWhere = "WHERE (v.name <> '') ";
			string StrSelect = "SELECT v.id, v.name, v.ability, " +
				"v.is_axes, v.is_boomerangs, v.is_bows, v.is_claws, v.is_fans, " +
				"v.is_fisticuffs, v.is_hammers, v.is_knives, v.is_shields, " +
				"v.is_spears, v.is_staves, v.is_swords, v.is_wands, v.is_whips, " +
				"v.description, v.coup, v.coup_desc, v.build, v.filename " +
				"FROM vocations v ";

			for (int idx = 0; idx < clFWeapons.CheckedItems.Count; idx++)
			{
				if (clFWeapons.CheckedItems[idx].ToString().ToLower() == "axes") { StrWhere = StrWhere + "AND (is_axes = true) "; }
				if (clFWeapons.CheckedItems[idx].ToString().ToLower() == "boomerangs") { StrWhere = StrWhere + "AND (is_boomerangs = true) "; }
				if (clFWeapons.CheckedItems[idx].ToString().ToLower() == "bows") { StrWhere = StrWhere + "AND (is_bows = true) "; }
				if (clFWeapons.CheckedItems[idx].ToString().ToLower() == "claws") { StrWhere = StrWhere + "AND (is_claws = true) "; }
				if (clFWeapons.CheckedItems[idx].ToString().ToLower() == "fans") { StrWhere = StrWhere + "AND (is_fans = true) "; }
				if (clFWeapons.CheckedItems[idx].ToString().ToLower() == "fisticuffs") { StrWhere = StrWhere + "AND (is_fisticuffs = true) "; }
				if (clFWeapons.CheckedItems[idx].ToString().ToLower() == "hammers") { StrWhere = StrWhere + "AND (is_hammers = true) "; }
				if (clFWeapons.CheckedItems[idx].ToString().ToLower() == "knives") { StrWhere = StrWhere + "AND (is_knives = true) "; }
				if (clFWeapons.CheckedItems[idx].ToString().ToLower() == "shields") { StrWhere = StrWhere + "AND (is_shields = true) "; }
				if (clFWeapons.CheckedItems[idx].ToString().ToLower() == "spears") { StrWhere = StrWhere + "AND (is_spears = true) "; }
				if (clFWeapons.CheckedItems[idx].ToString().ToLower() == "staves") { StrWhere = StrWhere + "AND (is_staves = true) "; }
				if (clFWeapons.CheckedItems[idx].ToString().ToLower() == "swords") { StrWhere = StrWhere + "AND (is_swords = true) "; }
				if (clFWeapons.CheckedItems[idx].ToString().ToLower() == "wands") { StrWhere = StrWhere + "AND (is_wands = true) "; }
				if (clFWeapons.CheckedItems[idx].ToString().ToLower() == "whips") { StrWhere = StrWhere + "AND (is_whips = true) "; }
			}

			if (edtFtBuild.SelectedIndex > 0) { StrWhere = StrWhere + "AND (build like '%" + edtFtBuild.Text.ToLower() + "%') "; }

			StrSelect = StrSelect + StrWhere + "ORDER BY " +
				this.edtFtOrder.Text + " " + this.edtFtOrder2.Text;
			DataTable Ttable = Program.DBConnection.SelectTable(StrSelect);
			BindingSource SBind = new BindingSource();
			SBind.DataSource = Ttable;

			dgVoc.AutoGenerateColumns = false;
			dgVoc.DataSource = Ttable;
			dgVoc.DataSource = SBind;
			dgVoc.Refresh();
			iUpdating = false;
			dgVoc_SelectionChanged(dgVoc, null);
			dgVoc.Focus();
		}

		private void btnFilter_Click(object sender, EventArgs e)
		{
			sconFilter.Panel1Collapsed = true;
			sconFilter.Panel2Collapsed = false;
		}

		private void btnClear_Click(object sender, EventArgs e)
		{
			for (int idx = 0; idx < clFWeapons.Items.Count; idx++)
			{
				clFWeapons.SetItemCheckState(idx, CheckState.Unchecked);
			}
			edtFtBuild.SelectedIndex = 0;
			edtFtOrder.SelectedIndex = 0;
			edtFtOrder2.SelectedIndex = 0;
		}

		private void btnApply_Click(object sender, EventArgs e)
		{
			LoadData();
			sconFilter.Panel1Collapsed = false;
			sconFilter.Panel2Collapsed = true;
		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			sconFilter.Panel1Collapsed = false;
			sconFilter.Panel2Collapsed = true;
		}

		private void dgVoc_SelectionChanged(object sender, EventArgs e)
		{
			void LoadText(TextBox textBox, DataGridViewTextBoxColumn ColName)
			{
				textBox.Text = dgVoc.SelectedRows[0].Cells[ColName.Index].Value.ToString();
			}

			void LoadLabels(Label label, DataGridViewCheckBoxColumn ColName)
            {
				if (dgVoc.SelectedRows[0].Cells[ColName.Index].Value.ToString().ToLower() == "true")
				{
					label.BackColor = Properties.Settings.Default.colorPrimary; // Color.FromArgb(0, 151, 167);
					label.ForeColor = Properties.Settings.Default.colorPriText; // Color.White;
				}
				else
				{
					label.BackColor = Color.LightGray;
					label.ForeColor = Color.Gray;
				}
            }

			if ((iUpdating == false) && (dgVoc.SelectedRows.Count > 0))
			{
				this.Cursor = Cursors.WaitCursor;
				LoadText(edtName, colName);
				LoadText(edtAbility, colAbility);
				LoadText(edtDesc, colDesc);
				LoadText(edtCoup, colCoup);
				LoadText(edtCoupDesc, colCoupDesc);
				LoadText(edtBuild, colBuild);
				LoadLabels(lblAxes, colIAxes);
				LoadLabels(lblBoomerangs, colIBoomerangs);
				LoadLabels(lblBows, colIBows);
				LoadLabels(lblClaws, colIClaws);
				LoadLabels(lblFans, colIFans);
				LoadLabels(lblFisticuffs, colIFisticuffs);
				LoadLabels(lblHammers, colIHammers);
				LoadLabels(lblKnives, colIKnives);
				LoadLabels(lblShields, colIShield);
				LoadLabels(lblSpears, colISpears);
				LoadLabels(lblStaves, colIStaves);
				LoadLabels(lblSwords, colISwords);
				LoadLabels(lblWands, colIWands);
				LoadLabels(lblWhips, colIWhips);
				//
				string SID = dgVoc.SelectedRows[0].Cells[colID.Index].Value.ToString();
				string StrSelect = "SELECT skill_point, skill, mp, description " +
					"FROM vocation_skills WHERE vocation_id = " + SID + " " +
					"ORDER BY skill_point ASC";
				DataTable Ttable = Program.DBConnection.SelectTable(StrSelect);
				BindingSource SBind = new BindingSource();
				SBind.DataSource = Ttable;

				dgVocSkills.AutoGenerateColumns = false;
				dgVocSkills.DataSource = Ttable;
				dgVocSkills.DataSource = SBind;
				dgVocSkills.Refresh();
				//
				string sfile = dgVoc.SelectedRows[0].Cells[colFilename.Index].Value.ToString();
				if (sfile != "")
				{
					if (File.Exists("vocations\\" + sfile))
					{
						sfile = "vocations\\" + sfile;
					}
					else
					{
						sfile = "vocations\\default.png";
					}
					Size newsize;
					Image img;
					try
					{
						img = Image.FromFile(sfile);
					}
					catch (Exception)
					{
						img = Image.FromFile("vocations\\default.png");
					}
					if ((img.Height < picImage.Height) || (img.Width < picImage.Width))
					{
						newsize = new Size((int)(img.Width * 5), (int)(img.Height * 5));
						while ((newsize.Height > picImage.Height) || (newsize.Width > picImage.Width))
						{
							newsize.Height = (int)(0.8 * newsize.Height);
							newsize.Width = (int)(0.8 * newsize.Width);
						}
					}
					else
					{
						newsize = new Size((int)(img.Width * 0.8), (int)(img.Height * 0.8));
						while ((newsize.Height > picImage.Height) || (newsize.Width > picImage.Width))
						{
							newsize.Height = (int)(0.8 * newsize.Height);
							newsize.Width = (int)(0.8 * newsize.Width);
						}
					}
					Bitmap bmp = new Bitmap(img, newsize);
					picImage.Image = bmp;
					img.Dispose();
				}
				this.Cursor = Cursors.Default;
				this.Cursor = Cursors.Default;
				this.Cursor = Cursors.Default;
			}
		}

        private void dgVocSkills_Resize(object sender, EventArgs e)
        {
			colSkillDesc.Width = dgVocSkills.Width - (20 + colSP.Width + colSkill.Width + colMP.Width);
        }

        private void clFWeapons_Click(object sender, EventArgs e)
        {
			for (int idx = 0; idx < clFWeapons.Items.Count; idx++)
            {
				if (clFWeapons.GetItemRectangle(idx).Contains(clFWeapons.PointToClient(MousePosition)))
				{ 
					switch (clFWeapons.GetItemCheckState(idx))
                    {
						case CheckState.Checked:
							clFWeapons.SetItemCheckState(idx, CheckState.Unchecked);
							break;
						case CheckState.Unchecked:
							clFWeapons.SetItemCheckState(idx, CheckState.Checked);
							break;
						case CheckState.Indeterminate:
							clFWeapons.SetItemCheckState(idx, CheckState.Checked);
							break;
					}
				}
            }
		}

		private void lblBuild_Click(object sender, EventArgs e)
		{
			dgVocSkills.Visible = !dgVocSkills.Visible;
			if (dgVocSkills.Visible) { lblBuild.Text = "▲ Recommended Build"; }
			else { lblBuild.Text = "▼ Recommended Build"; }
		}

		public void UpdateUI()
		{
			Color colorPrimary = Properties.Settings.Default.colorPrimary;
			Color colorPriText = Properties.Settings.Default.colorPriText;

			void UpdateLabels(Label label)
            {
				if (label.ForeColor != Color.Gray)
				{
					label.BackColor = colorPrimary;
					label.ForeColor = colorPriText;
				}
			}

			dgVoc.DefaultCellStyle.SelectionBackColor = colorPrimary;
			dgVoc.DefaultCellStyle.SelectionForeColor = colorPriText;
			dgVocSkills.DefaultCellStyle.SelectionBackColor = colorPrimary;
			dgVocSkills.DefaultCellStyle.SelectionForeColor = colorPriText;
			UpdateLabels(lblAxes);
			UpdateLabels(lblBoomerangs);
			UpdateLabels(lblBows);
			UpdateLabels(lblClaws);
			UpdateLabels(lblFans);
			UpdateLabels(lblFisticuffs);
			UpdateLabels(lblHammers);
			UpdateLabels(lblKnives);
			UpdateLabels(lblShields);
			UpdateLabels(lblSpears);
			UpdateLabels(lblStaves);
			UpdateLabels(lblSwords);
			UpdateLabels(lblWands);
			UpdateLabels(lblWhips);
			Program.ChangeButtonColor(btnFilter, colorPrimary);
			Program.ChangeButtonColor(btnClear, colorPrimary);
			Program.ChangeButtonColor(btnApply, colorPrimary);
			Program.ChangeButtonColor(btnCancel, colorPrimary);
		}

		public int GetSplitterDistance()
        {
			return sconVoc.SplitterDistance;
		}

		public void SetSplitterDistance(int split)
		{
			sconVoc.SplitterDistance = split;
		}
    }
}
